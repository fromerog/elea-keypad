/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

//-----------------------------------------------
// Definiciones pulsador
//-----------------------------------------------
#define TuningSensor 0  //IMPORTANTE: Si este define está a 1, inicia el I2C para ajustar los CapSense. Para funcionamiento normal poner a 0.
#define ON  1
#define OFF 0

//BUZZER ACTIVO BAJO PARA CRCUITOS ORIGINALES ; COMENTAR PARA CI004xx
//#define BUZZER_ACT_LOW   

#define CORDON ExternalINPUT_Read()

//Definicion alarmas
#define AlarmaTouchID   1
#define AlarmaExtID     2
#define AlarmaResetID   3
#define AlarmaBatID     4
#define KeepAliveID     5
#define ServiceID       6

/* WDT counter configuration */
#define TiempoEscaneoReposo 250 //Tiempo entre escaneos en reposo (en milisegundos)
#define TiempoEscaneoAccion 10  //Tiempo entre escaneos en accion (en milisegundos)
#define WDT_COUNT0_MATCH_Reposo (TiempoEscaneoReposo*1000/31.25)
#define WDT_COUNT0_MATCH_Accion (TiempoEscaneoAccion*1000/31.25)

#define WDT_COUNT0_MATCH (TiempoEscaneo*1000/31.25) //Calculado a partir de la freq. del ILO (32KHz)

//Tiempo entre escaneos
#define CheckBatteryTime    86400  // 4 vez al día: nº interrupciones del WDT por segundo * 3600 (1 hora) * 24 (1 dia)
//#define CheckBatteryTime  864000  // 1 vez al día: 10 interrupciones del WDT por segundo * 3600 (1 hora) * 24 (1 dia)
//#define CheckBatteryTime  100     //una vez al día: 10 interrupciones del WDT por segundo * 3600 (1hora) * 24 (1 dia)

# define Mantenimiento      1460    // 4 veces al dia * 24 (1 dia) * 365 (1 año). Mantenimiento anual. Envia la baliza
                                    // con el identificador de mantenimiento y en el pulsador se activan los LEDs de
                                    // Alarma y Bateria para notificar que debe de realizarse el mantenimiento del sistema.

//Tiempo de espera de reset por cordon
#define CheckCordonResetTime 50 // En base a 100ms de periodo de WDT

//Declaracion funciones
void LED_Alarma(int accion);
void LED_Bateria(int accion);
void Zumbador(int accion);
void Rele_TESA(int accion);
/* [] END OF FILE */
