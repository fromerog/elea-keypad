Last Modified Date & Time: 10/23/2017 17:17:33

EleaTouch: TopDesign.cysch
	UART [v3.0] to [v3.20]
	SW [v2.10] to [v2.20]
	LED_A [v2.10] to [v2.20]
	BUZZER [v2.10] to [v2.20]
	Radio [v2.10] to [v2.20]
	Radio_reset [v2.10] to [v2.20]
	LED_B [v2.10] to [v2.20]
	CapSense [v2.30] to [v2.60]

EleaTouch: EleaTouch.cydwr
	cy_boot [v5.20] to [v5.50]
	cy_lfclk [v1.0] to [v1.10]
	LIN_Dynamic [v3.20] to [v3.40]


