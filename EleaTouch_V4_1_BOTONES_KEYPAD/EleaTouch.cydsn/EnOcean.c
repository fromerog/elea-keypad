/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "EnOcean.h"
#include <project.h>

//-----------------------------------------------
// Definicion variables EnOcean
//-----------------------------------------------
//Mensajes EnOcean


#ifdef ENOCEAN_CODIGOS_ANTIGUOS
MensajeENO AlarmaTouch[12] = { 0x55, 0x00, 0x07, 0x00, 0x01, 0xF6, 0x43, 'E', 'L', 'E', 'A', 0x30};
MensajeENO AlarmaCordon[12] = { 0x55, 0x00, 0x07, 0x00, 0x01, 0xF6, 0x43, 'E', 'L', 'E', 'A', 0x31};
MensajeENO Reseteo[12] = { 0x55, 0x00, 0x07, 0x00, 0x01, 0xF6, 0x43, 'E', 'L', 'E', 'A', 0x32};
MensajeENO BatBaja[12] = { 0x55, 0x00, 0x07, 0x00, 0x01, 0xF6, 0x40, 'E', 'L', 'E', 'A', 0x33};
#endif

#ifdef ENOCEAN_CODIGOS_NUEVOS
MensajeENO AlarmaTouch[12] =    { 0x55, 0x00, 0x07, 0x00, 0x01, 0xF6, 0xAF, 'E', 'L', 'E', 'A', 0x20};
MensajeENO AlarmaCordon[12] =   { 0x55, 0x00, 0x07, 0x00, 0x01, 0xF6, 0xAF, 'E', 'L', 'E', 'A', 0x20};
MensajeENO Reseteo[12] =        { 0x55, 0x00, 0x07, 0x00, 0x01, 0xD5, 0xF2, 'E', 'L', 'E', 'A', 0x20};
MensajeENO BatBaja[19] =        { 0x55, 0x00, 0x07, 0x00, 0x01, 0xF6, 0xF1, 0x00, 0x00, 0x00, 0x00, 0x33, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00};
MensajeENO KeepAlive[19] =      { 0x55, 0x00, 0x07, 0x00, 0x01, 0xF6, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x33, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00};
MensajeENO Service[19] =        { 0x55, 0x00, 0x07, 0x00, 0x01, 0xF6, 0xFC, 0x00, 0x00, 0x00, 0x00, 0x33, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00};  // Mantenimiento ID 0xFC
#endif


MensajeENO Dormir[10] =         { 0x55, 0x00, 0x05, 0x00, 0x05, 0x01, 0x00, 0xFF, 0xFF, 0xFF};
int const CRCTable[256] =            { 0x00, 0x07, 0x0e, 0x09, 0x1c, 0x1b, 0x12, 0x15,
                                       0x38, 0x3f, 0x36, 0x31, 0x24, 0x23, 0x2a, 0x2d,
                                       0x70, 0x77, 0x7e, 0x79, 0x6c, 0x6b, 0x62, 0x65,
                                       0x48, 0x4f, 0x46, 0x41, 0x54, 0x53, 0x5a, 0x5d,
                                       0xe0, 0xe7, 0xee, 0xe9, 0xfc, 0xfb, 0xf2, 0xf5,
                                       0xd8, 0xdf, 0xd6, 0xd1, 0xc4, 0xc3, 0xca, 0xcd,
                                       0x90, 0x97, 0x9e, 0x99, 0x8c, 0x8b, 0x82, 0x85,
                                       0xa8, 0xaf, 0xa6, 0xa1, 0xb4, 0xb3, 0xba, 0xbd,
                                       0xc7, 0xc0, 0xc9, 0xce, 0xdb, 0xdc, 0xd5, 0xd2,
                                       0xff, 0xf8, 0xf1, 0xf6, 0xe3, 0xe4, 0xed, 0xea,
                                       0xb7, 0xb0, 0xb9, 0xbe, 0xab, 0xac, 0xa5, 0xa2,
                                       0x8f, 0x88, 0x81, 0x86, 0x93, 0x94, 0x9d, 0x9a,
                                       0x27, 0x20, 0x29, 0x2e, 0x3b, 0x3c, 0x35, 0x32,
                                       0x1f, 0x18, 0x11, 0x16, 0x03, 0x04, 0x0d, 0x0a,
                                       0x57, 0x50, 0x59, 0x5e, 0x4b, 0x4c, 0x45, 0x42,
                                       0x6f, 0x68, 0x61, 0x66, 0x73, 0x74, 0x7d, 0x7a,
                                       0x89, 0x8e, 0x87, 0x80, 0x95, 0x92, 0x9b, 0x9c,
                                       0xb1, 0xb6, 0xbf, 0xb8, 0xad, 0xaa, 0xa3, 0xa4,
                                       0xf9, 0xfe, 0xf7, 0xf0, 0xe5, 0xe2, 0xeb, 0xec,
                                       0xc1, 0xc6, 0xcf, 0xc8, 0xdd, 0xda, 0xd3, 0xd4,
                                       0x69, 0x6e, 0x67, 0x60, 0x75, 0x72, 0x7b, 0x7c,
                                       0x51, 0x56, 0x5f, 0x58, 0x4d, 0x4a, 0x43, 0x44,
                                       0x19, 0x1e, 0x17, 0x10, 0x05, 0x02, 0x0b, 0x0c,
                                       0x21, 0x26, 0x2f, 0x28, 0x3d, 0x3a, 0x33, 0x34,
                                       0x4e, 0x49, 0x40, 0x47, 0x52, 0x55, 0x5c, 0x5b,
                                       0x76, 0x71, 0x78, 0x7f, 0x6A, 0x6d, 0x64, 0x63,
                                       0x3e, 0x39, 0x30, 0x37, 0x22, 0x25, 0x2c, 0x2b,
                                       0x06, 0x01, 0x08, 0x0f, 0x1a, 0x1d, 0x14, 0x13,
                                       0xae, 0xa9, 0xa0, 0xa7, 0xb2, 0xb5, 0xbc, 0xbb,
                                       0x96, 0x91, 0x98, 0x9f, 0x8a, 0x8D, 0x84, 0x83,
                                       0xde, 0xd9, 0xd0, 0xd7, 0xc2, 0xc5, 0xcc, 0xcb,
                                       0xe6, 0xe1, 0xe8, 0xef, 0xfa, 0xfd, 0xf4, 0xf3
                                     };


//-----------------------------------------------
// Definicion funciones EnOcean
//-----------------------------------------------
void TipoMensajeENO(int tipo)
{
    switch(tipo)
    {
        case 1:
            EnviarMensajeENO(AlarmaTouch);
            break;
        case 2:
            EnviarMensajeENO(AlarmaCordon);
            break;
        case 3:
            EnviarMensajeENO(Reseteo);
            break;
        case 4:
            EnviarMensajeENO(BatBaja);
            break;
#ifdef ENOCEAN_CODIGOS_NUEVOS
        case 5:
            EnviarMensajeENO(KeepAlive);
            break;
        case 6:
            EnviarMensajeENO(Service);
            break;
#endif
        default: break;    
    }
}

void EnoceanON()
{
    enoceanState = ENO_ON;
    
    EnoRESET_SetDriveMode(EnoRESET_DM_STRONG);
    EnoPWR_SetDriveMode(EnoPWR_DM_STRONG);
    EnoUART_rx_SetDriveMode(EnoUART_rx_DM_STRONG);
    EnoUART_tx_SetDriveMode(EnoUART_tx_DM_STRONG);
    EnoRESET_Write(1u);
    EnoPWR_Write(0u); //reset enocean y enciende
    CyDelay(2);  // 2ms delay to let V rise to be stabilized.
    EnoRESET_Write(0u);
    EnoUART_Start();
    CyDelay(20);
    EnoUART_SpiUartClearRxBuffer();        //MUY IMPORTANTE, limpia el buffer de recepcion de la UART
    

}

void EnoceanOFF()
{
    enoceanState = ENO_OFF;
    
    EnoUART_Stop();
    EnoUART_rx_SetDriveMode(EnoUART_rx_DM_DIG_HIZ);
    EnoUART_tx_Write(0u);
    EnoUART_tx_SetDriveMode(EnoUART_tx_DM_RES_DWN);
    EnoPWR_Write(1u);
    EnoRESET_Write(0u);
    EnoRESET_SetDriveMode(EnoRESET_DM_RES_DWN);
    EnoPWR_SetDriveMode(EnoPWR_DM_DIG_HIZ);

}

void EnviarMensajeENO(MensajeENO mensaje[])
{
    EnoceanON();
    
    int i,valor = 0;
    uint8 CRC=0;
    
    if(mensaje[5] == 0xF6) valor = 19;
    //if((mensaje[5] == 0xF6) || (mensaje[5] == 0xD5)) valor = 12;
    //if(mensaje[5] == 0xA5) valor = 15;

    for (i=0;i<5;i++)
    {
        EnoUART_UartPutChar(mensaje[i]);
    }
    for (i=1;i<5;i++)
    {
        CRC=CRCTable[CRC^mensaje[i]];
    }
    EnoUART_UartPutChar(CRC);
    
    for (i=5;i<valor;i++)
    {
        EnoUART_UartPutChar(mensaje[i]);
    }
    CRC=0;
    for (i=5;i<valor;i++)
    {
        CRC=CRCTable[CRC^mensaje[i]];
    }
    EnoUART_UartPutChar(CRC);
    CyDelay(100);//Pequeña espera para que de tiempo a mandar el mensaje
    
    EnoceanOFF();
}




struct EnoTelegram receiveEnocean(void)
{
    uint8 received[23] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
    uint8 bufferENO;
    uint8 count = 0; 
    struct EnoTelegram parsedTelegram;
            
    //EnoUART_SpiUartClearRxBuffer();
    
    

    while (EnoUART_SpiUartGetRxBufferSize() == 0)
    {
    }
    
    for (;;) 
    {
        if (EnoUART_SpiUartGetRxBufferSize() != 0)
        {   
            bufferENO = EnoUART_UartGetByte();
            received[count] = bufferENO;
            count++;
        }

        if (received[0] != 0x55)
            break;      
        
        if ((received[4] ==0x02)&& count == 8)
            break;
        
        if ((received[2] == 0x01) && count == 8) 
            break;
        
        if (received[6] == 0xF6 && count == 20)
            break;
        
        if (received[6] == 0xD5 && count == 20)
            break;
        
        if (received[6] == 0xA5 && count == 23)
            break;
        
        if (received[6] == 0xD2 && count == 23)
            break;
        
        if (count > 23)
            break;

    }
   
    if (received[6] == RPS )
        {   
            parsedTelegram.RORG = RPS;
            parsedTelegram.DATA[0] = received[7];  
            parsedTelegram.ID[0] = received[8];
            parsedTelegram.ID[1] = received[9];
            parsedTelegram.ID[2] = received[10];
            parsedTelegram.ID[3] = received[11];
            parsedTelegram.STATUS = received[12];
        }
                 
    else if (received[6] == BS1 )
        {    
            parsedTelegram.RORG = BS1;  
            parsedTelegram.DATA[0] = received[7];  
            parsedTelegram.ID[0] = received[8];
            parsedTelegram.ID[1] = received[9];
            parsedTelegram.ID[2] = received[10];
            parsedTelegram.ID[3] = received[11];
            parsedTelegram.STATUS = received[12];  
        }
        
    else if (received[6] == BS4 )
        { 
            parsedTelegram.RORG = BS4;
            parsedTelegram.DATA[0] = received[7];  
            parsedTelegram.DATA[1] = received[8];  
            parsedTelegram.DATA[2] = received[9];            
            parsedTelegram.DATA[3] = received[10];          
            parsedTelegram.ID[0] = received[11];
            parsedTelegram.ID[1] = received[12];
            parsedTelegram.ID[2] = received[13];
            parsedTelegram.ID[3] = received[14];
            parsedTelegram.STATUS = received[15];     
        }
                
    else
        {
            parsedTelegram.RORG = 0x00;
            parsedTelegram.DATA[0] = 0x00;  
            parsedTelegram.ID[0] = 0x00;
            parsedTelegram.ID[1] = 0x00;
            parsedTelegram.ID[2] = 0x00;
            parsedTelegram.ID[3] = 0x00;
            parsedTelegram.STATUS = 0x00;
        }

    EnoUART_SpiUartClearRxBuffer();
    
    return parsedTelegram;
}

/* [] END OF FILE */
