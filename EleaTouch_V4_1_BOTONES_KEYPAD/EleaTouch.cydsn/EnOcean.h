/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */


#include <project.h>

//#define ENOCEAN_CODIGOS_ANTIGUOS
#define ENOCEAN_CODIGOS_NUEVOS

typedef int MensajeENO; //Definicion tipo mensaje

//-----------------------------------------------
// Declaracion funciones EnOcean
//-----------------------------------------------
void EnviarMensajeENO(MensajeENO mensaje[]);
void TipoMensajeENO(int tipo);
void SleepENO();
struct EnoTelegram receiveEnocean();
void EnoceanON();
void EnoceanOFF();
void checkEnoID(void);



#define RPS 0xF6
#define BS1 0xD5
#define BS4 0xA5
#define VLD 0xD2
#define RESP 0x02

#define ENO_ON 1
#define ENO_OFF 0

uint8 extern enoceanState;

struct EnoTelegram
    {
        uint8 RORG;
        uint8 ID[4];
        uint8 DATA[4];
        uint8 STATUS;
    };