/***************************************************************************//**
* \file EnoUART_SPI_UART_PVT.h
* \version 3.20
*
* \brief
*  This private file provides constants and parameter values for the
*  SCB Component in SPI and UART modes.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_SPI_UART_PVT_EnoUART_H)
#define CY_SCB_SPI_UART_PVT_EnoUART_H

#include "EnoUART_SPI_UART.h"


/***************************************
*     Internal Global Vars
***************************************/

#if (EnoUART_INTERNAL_RX_SW_BUFFER_CONST)
    extern volatile uint32  EnoUART_rxBufferHead;
    extern volatile uint32  EnoUART_rxBufferTail;
    
    /**
    * \addtogroup group_globals
    * @{
    */
    
    /** Sets when internal software receive buffer overflow
     *  was occurred.
    */  
    extern volatile uint8   EnoUART_rxBufferOverflow;
    /** @} globals */
#endif /* (EnoUART_INTERNAL_RX_SW_BUFFER_CONST) */

#if (EnoUART_INTERNAL_TX_SW_BUFFER_CONST)
    extern volatile uint32  EnoUART_txBufferHead;
    extern volatile uint32  EnoUART_txBufferTail;
#endif /* (EnoUART_INTERNAL_TX_SW_BUFFER_CONST) */

#if (EnoUART_INTERNAL_RX_SW_BUFFER)
    extern volatile uint8 EnoUART_rxBufferInternal[EnoUART_INTERNAL_RX_BUFFER_SIZE];
#endif /* (EnoUART_INTERNAL_RX_SW_BUFFER) */

#if (EnoUART_INTERNAL_TX_SW_BUFFER)
    extern volatile uint8 EnoUART_txBufferInternal[EnoUART_TX_BUFFER_SIZE];
#endif /* (EnoUART_INTERNAL_TX_SW_BUFFER) */


/***************************************
*     Private Function Prototypes
***************************************/

void EnoUART_SpiPostEnable(void);
void EnoUART_SpiStop(void);

#if (EnoUART_SCB_MODE_SPI_CONST_CFG)
    void EnoUART_SpiInit(void);
#endif /* (EnoUART_SCB_MODE_SPI_CONST_CFG) */

#if (EnoUART_SPI_WAKE_ENABLE_CONST)
    void EnoUART_SpiSaveConfig(void);
    void EnoUART_SpiRestoreConfig(void);
#endif /* (EnoUART_SPI_WAKE_ENABLE_CONST) */

void EnoUART_UartPostEnable(void);
void EnoUART_UartStop(void);

#if (EnoUART_SCB_MODE_UART_CONST_CFG)
    void EnoUART_UartInit(void);
#endif /* (EnoUART_SCB_MODE_UART_CONST_CFG) */

#if (EnoUART_UART_WAKE_ENABLE_CONST)
    void EnoUART_UartSaveConfig(void);
    void EnoUART_UartRestoreConfig(void);
#endif /* (EnoUART_UART_WAKE_ENABLE_CONST) */


/***************************************
*         UART API Constants
***************************************/

/* UART RX and TX position to be used in EnoUART_SetPins() */
#define EnoUART_UART_RX_PIN_ENABLE    (EnoUART_UART_RX)
#define EnoUART_UART_TX_PIN_ENABLE    (EnoUART_UART_TX)

/* UART RTS and CTS position to be used in  EnoUART_SetPins() */
#define EnoUART_UART_RTS_PIN_ENABLE    (0x10u)
#define EnoUART_UART_CTS_PIN_ENABLE    (0x20u)


/***************************************
* The following code is DEPRECATED and
* must not be used.
***************************************/

/* Interrupt processing */
#define EnoUART_SpiUartEnableIntRx(intSourceMask)  EnoUART_SetRxInterruptMode(intSourceMask)
#define EnoUART_SpiUartEnableIntTx(intSourceMask)  EnoUART_SetTxInterruptMode(intSourceMask)
uint32  EnoUART_SpiUartDisableIntRx(void);
uint32  EnoUART_SpiUartDisableIntTx(void);


#endif /* (CY_SCB_SPI_UART_PVT_EnoUART_H) */


/* [] END OF FILE */
