/*******************************************************************************
* File Name: SinUsar_17.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "SinUsar_17.h"

static SinUsar_17_BACKUP_STRUCT  SinUsar_17_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: SinUsar_17_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function applies only to SIO and USBIO pins.
*  It should not be called for GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet SinUsar_17_SUT.c usage_SinUsar_17_Sleep_Wakeup
*******************************************************************************/
void SinUsar_17_Sleep(void)
{
    #if defined(SinUsar_17__PC)
        SinUsar_17_backup.pcState = SinUsar_17_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            SinUsar_17_backup.usbState = SinUsar_17_CR1_REG;
            SinUsar_17_USB_POWER_REG |= SinUsar_17_USBIO_ENTER_SLEEP;
            SinUsar_17_CR1_REG &= SinUsar_17_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(SinUsar_17__SIO)
        SinUsar_17_backup.sioState = SinUsar_17_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        SinUsar_17_SIO_REG &= (uint32)(~SinUsar_17_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: SinUsar_17_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep(). This 
* function applies only to SIO and USBIO pins. It should not be called for
* GPIO or GPIO_OVT pins.
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to SinUsar_17_Sleep() for an example usage.
*******************************************************************************/
void SinUsar_17_Wakeup(void)
{
    #if defined(SinUsar_17__PC)
        SinUsar_17_PC = SinUsar_17_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            SinUsar_17_USB_POWER_REG &= SinUsar_17_USBIO_EXIT_SLEEP_PH1;
            SinUsar_17_CR1_REG = SinUsar_17_backup.usbState;
            SinUsar_17_USB_POWER_REG &= SinUsar_17_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(SinUsar_17__SIO)
        SinUsar_17_SIO_REG = SinUsar_17_backup.sioState;
    #endif
}


/* [] END OF FILE */
