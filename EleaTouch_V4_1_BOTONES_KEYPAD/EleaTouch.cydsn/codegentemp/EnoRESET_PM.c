/*******************************************************************************
* File Name: EnoRESET.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "EnoRESET.h"

static EnoRESET_BACKUP_STRUCT  EnoRESET_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: EnoRESET_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function applies only to SIO and USBIO pins.
*  It should not be called for GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet EnoRESET_SUT.c usage_EnoRESET_Sleep_Wakeup
*******************************************************************************/
void EnoRESET_Sleep(void)
{
    #if defined(EnoRESET__PC)
        EnoRESET_backup.pcState = EnoRESET_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            EnoRESET_backup.usbState = EnoRESET_CR1_REG;
            EnoRESET_USB_POWER_REG |= EnoRESET_USBIO_ENTER_SLEEP;
            EnoRESET_CR1_REG &= EnoRESET_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(EnoRESET__SIO)
        EnoRESET_backup.sioState = EnoRESET_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        EnoRESET_SIO_REG &= (uint32)(~EnoRESET_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: EnoRESET_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep(). This 
* function applies only to SIO and USBIO pins. It should not be called for
* GPIO or GPIO_OVT pins.
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to EnoRESET_Sleep() for an example usage.
*******************************************************************************/
void EnoRESET_Wakeup(void)
{
    #if defined(EnoRESET__PC)
        EnoRESET_PC = EnoRESET_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            EnoRESET_USB_POWER_REG &= EnoRESET_USBIO_EXIT_SLEEP_PH1;
            EnoRESET_CR1_REG = EnoRESET_backup.usbState;
            EnoRESET_USB_POWER_REG &= EnoRESET_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(EnoRESET__SIO)
        EnoRESET_SIO_REG = EnoRESET_backup.sioState;
    #endif
}


/* [] END OF FILE */
