/*******************************************************************************
* File Name: EnoRESET.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_EnoRESET_ALIASES_H) /* Pins EnoRESET_ALIASES_H */
#define CY_PINS_EnoRESET_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define EnoRESET_0			(EnoRESET__0__PC)
#define EnoRESET_0_PS		(EnoRESET__0__PS)
#define EnoRESET_0_PC		(EnoRESET__0__PC)
#define EnoRESET_0_DR		(EnoRESET__0__DR)
#define EnoRESET_0_SHIFT	(EnoRESET__0__SHIFT)
#define EnoRESET_0_INTR	((uint16)((uint16)0x0003u << (EnoRESET__0__SHIFT*2u)))

#define EnoRESET_INTR_ALL	 ((uint16)(EnoRESET_0_INTR))


#endif /* End Pins EnoRESET_ALIASES_H */


/* [] END OF FILE */
