/*******************************************************************************
* File Name: ExternalINPUT.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ExternalINPUT_ALIASES_H) /* Pins ExternalINPUT_ALIASES_H */
#define CY_PINS_ExternalINPUT_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define ExternalINPUT_0			(ExternalINPUT__0__PC)
#define ExternalINPUT_0_PS		(ExternalINPUT__0__PS)
#define ExternalINPUT_0_PC		(ExternalINPUT__0__PC)
#define ExternalINPUT_0_DR		(ExternalINPUT__0__DR)
#define ExternalINPUT_0_SHIFT	(ExternalINPUT__0__SHIFT)
#define ExternalINPUT_0_INTR	((uint16)((uint16)0x0003u << (ExternalINPUT__0__SHIFT*2u)))

#define ExternalINPUT_INTR_ALL	 ((uint16)(ExternalINPUT_0_INTR))


#endif /* End Pins ExternalINPUT_ALIASES_H */


/* [] END OF FILE */
