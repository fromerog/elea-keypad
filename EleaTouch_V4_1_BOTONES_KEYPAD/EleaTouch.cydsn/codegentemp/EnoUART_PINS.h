/***************************************************************************//**
* \file EnoUART_PINS.h
* \version 3.20
*
* \brief
*  This file provides constants and parameter values for the pin components
*  buried into SCB Component.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PINS_EnoUART_H)
#define CY_SCB_PINS_EnoUART_H

#include "cydevice_trm.h"
#include "cyfitter.h"
#include "cytypes.h"


/***************************************
*   Conditional Compilation Parameters
****************************************/

/* Unconfigured pins */
#define EnoUART_REMOVE_RX_WAKE_SCL_MOSI_PIN  (1u)
#define EnoUART_REMOVE_RX_SCL_MOSI_PIN      (1u)
#define EnoUART_REMOVE_TX_SDA_MISO_PIN      (1u)
#define EnoUART_REMOVE_SCLK_PIN      (1u)
#define EnoUART_REMOVE_SS0_PIN      (1u)
#define EnoUART_REMOVE_SS1_PIN                 (1u)
#define EnoUART_REMOVE_SS2_PIN                 (1u)
#define EnoUART_REMOVE_SS3_PIN                 (1u)

/* Mode defined pins */
#define EnoUART_REMOVE_I2C_PINS                (1u)
#define EnoUART_REMOVE_SPI_MASTER_PINS         (1u)
#define EnoUART_REMOVE_SPI_MASTER_SCLK_PIN     (1u)
#define EnoUART_REMOVE_SPI_MASTER_MOSI_PIN     (1u)
#define EnoUART_REMOVE_SPI_MASTER_MISO_PIN     (1u)
#define EnoUART_REMOVE_SPI_MASTER_SS0_PIN      (1u)
#define EnoUART_REMOVE_SPI_MASTER_SS1_PIN      (1u)
#define EnoUART_REMOVE_SPI_MASTER_SS2_PIN      (1u)
#define EnoUART_REMOVE_SPI_MASTER_SS3_PIN      (1u)
#define EnoUART_REMOVE_SPI_SLAVE_PINS          (1u)
#define EnoUART_REMOVE_SPI_SLAVE_MOSI_PIN      (1u)
#define EnoUART_REMOVE_SPI_SLAVE_MISO_PIN      (1u)
#define EnoUART_REMOVE_UART_TX_PIN             (0u)
#define EnoUART_REMOVE_UART_RX_TX_PIN          (1u)
#define EnoUART_REMOVE_UART_RX_PIN             (0u)
#define EnoUART_REMOVE_UART_RX_WAKE_PIN        (1u)
#define EnoUART_REMOVE_UART_RTS_PIN            (1u)
#define EnoUART_REMOVE_UART_CTS_PIN            (1u)

/* Unconfigured pins */
#define EnoUART_RX_WAKE_SCL_MOSI_PIN (0u == EnoUART_REMOVE_RX_WAKE_SCL_MOSI_PIN)
#define EnoUART_RX_SCL_MOSI_PIN     (0u == EnoUART_REMOVE_RX_SCL_MOSI_PIN)
#define EnoUART_TX_SDA_MISO_PIN     (0u == EnoUART_REMOVE_TX_SDA_MISO_PIN)
#define EnoUART_SCLK_PIN     (0u == EnoUART_REMOVE_SCLK_PIN)
#define EnoUART_SS0_PIN     (0u == EnoUART_REMOVE_SS0_PIN)
#define EnoUART_SS1_PIN                (0u == EnoUART_REMOVE_SS1_PIN)
#define EnoUART_SS2_PIN                (0u == EnoUART_REMOVE_SS2_PIN)
#define EnoUART_SS3_PIN                (0u == EnoUART_REMOVE_SS3_PIN)

/* Mode defined pins */
#define EnoUART_I2C_PINS               (0u == EnoUART_REMOVE_I2C_PINS)
#define EnoUART_SPI_MASTER_PINS        (0u == EnoUART_REMOVE_SPI_MASTER_PINS)
#define EnoUART_SPI_MASTER_SCLK_PIN    (0u == EnoUART_REMOVE_SPI_MASTER_SCLK_PIN)
#define EnoUART_SPI_MASTER_MOSI_PIN    (0u == EnoUART_REMOVE_SPI_MASTER_MOSI_PIN)
#define EnoUART_SPI_MASTER_MISO_PIN    (0u == EnoUART_REMOVE_SPI_MASTER_MISO_PIN)
#define EnoUART_SPI_MASTER_SS0_PIN     (0u == EnoUART_REMOVE_SPI_MASTER_SS0_PIN)
#define EnoUART_SPI_MASTER_SS1_PIN     (0u == EnoUART_REMOVE_SPI_MASTER_SS1_PIN)
#define EnoUART_SPI_MASTER_SS2_PIN     (0u == EnoUART_REMOVE_SPI_MASTER_SS2_PIN)
#define EnoUART_SPI_MASTER_SS3_PIN     (0u == EnoUART_REMOVE_SPI_MASTER_SS3_PIN)
#define EnoUART_SPI_SLAVE_PINS         (0u == EnoUART_REMOVE_SPI_SLAVE_PINS)
#define EnoUART_SPI_SLAVE_MOSI_PIN     (0u == EnoUART_REMOVE_SPI_SLAVE_MOSI_PIN)
#define EnoUART_SPI_SLAVE_MISO_PIN     (0u == EnoUART_REMOVE_SPI_SLAVE_MISO_PIN)
#define EnoUART_UART_TX_PIN            (0u == EnoUART_REMOVE_UART_TX_PIN)
#define EnoUART_UART_RX_TX_PIN         (0u == EnoUART_REMOVE_UART_RX_TX_PIN)
#define EnoUART_UART_RX_PIN            (0u == EnoUART_REMOVE_UART_RX_PIN)
#define EnoUART_UART_RX_WAKE_PIN       (0u == EnoUART_REMOVE_UART_RX_WAKE_PIN)
#define EnoUART_UART_RTS_PIN           (0u == EnoUART_REMOVE_UART_RTS_PIN)
#define EnoUART_UART_CTS_PIN           (0u == EnoUART_REMOVE_UART_CTS_PIN)


/***************************************
*             Includes
****************************************/

#if (EnoUART_RX_WAKE_SCL_MOSI_PIN)
    #include "EnoUART_uart_rx_wake_i2c_scl_spi_mosi.h"
#endif /* (EnoUART_RX_SCL_MOSI) */

#if (EnoUART_RX_SCL_MOSI_PIN)
    #include "EnoUART_uart_rx_i2c_scl_spi_mosi.h"
#endif /* (EnoUART_RX_SCL_MOSI) */

#if (EnoUART_TX_SDA_MISO_PIN)
    #include "EnoUART_uart_tx_i2c_sda_spi_miso.h"
#endif /* (EnoUART_TX_SDA_MISO) */

#if (EnoUART_SCLK_PIN)
    #include "EnoUART_spi_sclk.h"
#endif /* (EnoUART_SCLK) */

#if (EnoUART_SS0_PIN)
    #include "EnoUART_spi_ss0.h"
#endif /* (EnoUART_SS0_PIN) */

#if (EnoUART_SS1_PIN)
    #include "EnoUART_spi_ss1.h"
#endif /* (EnoUART_SS1_PIN) */

#if (EnoUART_SS2_PIN)
    #include "EnoUART_spi_ss2.h"
#endif /* (EnoUART_SS2_PIN) */

#if (EnoUART_SS3_PIN)
    #include "EnoUART_spi_ss3.h"
#endif /* (EnoUART_SS3_PIN) */

#if (EnoUART_I2C_PINS)
    #include "EnoUART_scl.h"
    #include "EnoUART_sda.h"
#endif /* (EnoUART_I2C_PINS) */

#if (EnoUART_SPI_MASTER_PINS)
#if (EnoUART_SPI_MASTER_SCLK_PIN)
    #include "EnoUART_sclk_m.h"
#endif /* (EnoUART_SPI_MASTER_SCLK_PIN) */

#if (EnoUART_SPI_MASTER_MOSI_PIN)
    #include "EnoUART_mosi_m.h"
#endif /* (EnoUART_SPI_MASTER_MOSI_PIN) */

#if (EnoUART_SPI_MASTER_MISO_PIN)
    #include "EnoUART_miso_m.h"
#endif /*(EnoUART_SPI_MASTER_MISO_PIN) */
#endif /* (EnoUART_SPI_MASTER_PINS) */

#if (EnoUART_SPI_SLAVE_PINS)
    #include "EnoUART_sclk_s.h"
    #include "EnoUART_ss_s.h"

#if (EnoUART_SPI_SLAVE_MOSI_PIN)
    #include "EnoUART_mosi_s.h"
#endif /* (EnoUART_SPI_SLAVE_MOSI_PIN) */

#if (EnoUART_SPI_SLAVE_MISO_PIN)
    #include "EnoUART_miso_s.h"
#endif /*(EnoUART_SPI_SLAVE_MISO_PIN) */
#endif /* (EnoUART_SPI_SLAVE_PINS) */

#if (EnoUART_SPI_MASTER_SS0_PIN)
    #include "EnoUART_ss0_m.h"
#endif /* (EnoUART_SPI_MASTER_SS0_PIN) */

#if (EnoUART_SPI_MASTER_SS1_PIN)
    #include "EnoUART_ss1_m.h"
#endif /* (EnoUART_SPI_MASTER_SS1_PIN) */

#if (EnoUART_SPI_MASTER_SS2_PIN)
    #include "EnoUART_ss2_m.h"
#endif /* (EnoUART_SPI_MASTER_SS2_PIN) */

#if (EnoUART_SPI_MASTER_SS3_PIN)
    #include "EnoUART_ss3_m.h"
#endif /* (EnoUART_SPI_MASTER_SS3_PIN) */

#if (EnoUART_UART_TX_PIN)
    #include "EnoUART_tx.h"
#endif /* (EnoUART_UART_TX_PIN) */

#if (EnoUART_UART_RX_TX_PIN)
    #include "EnoUART_rx_tx.h"
#endif /* (EnoUART_UART_RX_TX_PIN) */

#if (EnoUART_UART_RX_PIN)
    #include "EnoUART_rx.h"
#endif /* (EnoUART_UART_RX_PIN) */

#if (EnoUART_UART_RX_WAKE_PIN)
    #include "EnoUART_rx_wake.h"
#endif /* (EnoUART_UART_RX_WAKE_PIN) */

#if (EnoUART_UART_RTS_PIN)
    #include "EnoUART_rts.h"
#endif /* (EnoUART_UART_RTS_PIN) */

#if (EnoUART_UART_CTS_PIN)
    #include "EnoUART_cts.h"
#endif /* (EnoUART_UART_CTS_PIN) */


/***************************************
*              Registers
***************************************/

#if (EnoUART_RX_SCL_MOSI_PIN)
    #define EnoUART_RX_SCL_MOSI_HSIOM_REG   (*(reg32 *) EnoUART_uart_rx_i2c_scl_spi_mosi__0__HSIOM)
    #define EnoUART_RX_SCL_MOSI_HSIOM_PTR   ( (reg32 *) EnoUART_uart_rx_i2c_scl_spi_mosi__0__HSIOM)
    
    #define EnoUART_RX_SCL_MOSI_HSIOM_MASK      (EnoUART_uart_rx_i2c_scl_spi_mosi__0__HSIOM_MASK)
    #define EnoUART_RX_SCL_MOSI_HSIOM_POS       (EnoUART_uart_rx_i2c_scl_spi_mosi__0__HSIOM_SHIFT)
    #define EnoUART_RX_SCL_MOSI_HSIOM_SEL_GPIO  (EnoUART_uart_rx_i2c_scl_spi_mosi__0__HSIOM_GPIO)
    #define EnoUART_RX_SCL_MOSI_HSIOM_SEL_I2C   (EnoUART_uart_rx_i2c_scl_spi_mosi__0__HSIOM_I2C)
    #define EnoUART_RX_SCL_MOSI_HSIOM_SEL_SPI   (EnoUART_uart_rx_i2c_scl_spi_mosi__0__HSIOM_SPI)
    #define EnoUART_RX_SCL_MOSI_HSIOM_SEL_UART  (EnoUART_uart_rx_i2c_scl_spi_mosi__0__HSIOM_UART)
    
#elif (EnoUART_RX_WAKE_SCL_MOSI_PIN)
    #define EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG   (*(reg32 *) EnoUART_uart_rx_wake_i2c_scl_spi_mosi__0__HSIOM)
    #define EnoUART_RX_WAKE_SCL_MOSI_HSIOM_PTR   ( (reg32 *) EnoUART_uart_rx_wake_i2c_scl_spi_mosi__0__HSIOM)
    
    #define EnoUART_RX_WAKE_SCL_MOSI_HSIOM_MASK      (EnoUART_uart_rx_wake_i2c_scl_spi_mosi__0__HSIOM_MASK)
    #define EnoUART_RX_WAKE_SCL_MOSI_HSIOM_POS       (EnoUART_uart_rx_wake_i2c_scl_spi_mosi__0__HSIOM_SHIFT)
    #define EnoUART_RX_WAKE_SCL_MOSI_HSIOM_SEL_GPIO  (EnoUART_uart_rx_wake_i2c_scl_spi_mosi__0__HSIOM_GPIO)
    #define EnoUART_RX_WAKE_SCL_MOSI_HSIOM_SEL_I2C   (EnoUART_uart_rx_wake_i2c_scl_spi_mosi__0__HSIOM_I2C)
    #define EnoUART_RX_WAKE_SCL_MOSI_HSIOM_SEL_SPI   (EnoUART_uart_rx_wake_i2c_scl_spi_mosi__0__HSIOM_SPI)
    #define EnoUART_RX_WAKE_SCL_MOSI_HSIOM_SEL_UART  (EnoUART_uart_rx_wake_i2c_scl_spi_mosi__0__HSIOM_UART)    
   
    #define EnoUART_RX_WAKE_SCL_MOSI_INTCFG_REG (*(reg32 *) EnoUART_uart_rx_wake_i2c_scl_spi_mosi__0__INTCFG)
    #define EnoUART_RX_WAKE_SCL_MOSI_INTCFG_PTR ( (reg32 *) EnoUART_uart_rx_wake_i2c_scl_spi_mosi__0__INTCFG)
    #define EnoUART_RX_WAKE_SCL_MOSI_INTCFG_TYPE_POS  (EnoUART_uart_rx_wake_i2c_scl_spi_mosi__SHIFT)
    #define EnoUART_RX_WAKE_SCL_MOSI_INTCFG_TYPE_MASK ((uint32) EnoUART_INTCFG_TYPE_MASK << \
                                                                           EnoUART_RX_WAKE_SCL_MOSI_INTCFG_TYPE_POS)
#else
    /* None of pins EnoUART_RX_SCL_MOSI_PIN or EnoUART_RX_WAKE_SCL_MOSI_PIN present.*/
#endif /* (EnoUART_RX_SCL_MOSI_PIN) */

#if (EnoUART_TX_SDA_MISO_PIN)
    #define EnoUART_TX_SDA_MISO_HSIOM_REG   (*(reg32 *) EnoUART_uart_tx_i2c_sda_spi_miso__0__HSIOM)
    #define EnoUART_TX_SDA_MISO_HSIOM_PTR   ( (reg32 *) EnoUART_uart_tx_i2c_sda_spi_miso__0__HSIOM)
    
    #define EnoUART_TX_SDA_MISO_HSIOM_MASK      (EnoUART_uart_tx_i2c_sda_spi_miso__0__HSIOM_MASK)
    #define EnoUART_TX_SDA_MISO_HSIOM_POS       (EnoUART_uart_tx_i2c_sda_spi_miso__0__HSIOM_SHIFT)
    #define EnoUART_TX_SDA_MISO_HSIOM_SEL_GPIO  (EnoUART_uart_tx_i2c_sda_spi_miso__0__HSIOM_GPIO)
    #define EnoUART_TX_SDA_MISO_HSIOM_SEL_I2C   (EnoUART_uart_tx_i2c_sda_spi_miso__0__HSIOM_I2C)
    #define EnoUART_TX_SDA_MISO_HSIOM_SEL_SPI   (EnoUART_uart_tx_i2c_sda_spi_miso__0__HSIOM_SPI)
    #define EnoUART_TX_SDA_MISO_HSIOM_SEL_UART  (EnoUART_uart_tx_i2c_sda_spi_miso__0__HSIOM_UART)
#endif /* (EnoUART_TX_SDA_MISO_PIN) */

#if (EnoUART_SCLK_PIN)
    #define EnoUART_SCLK_HSIOM_REG   (*(reg32 *) EnoUART_spi_sclk__0__HSIOM)
    #define EnoUART_SCLK_HSIOM_PTR   ( (reg32 *) EnoUART_spi_sclk__0__HSIOM)
    
    #define EnoUART_SCLK_HSIOM_MASK      (EnoUART_spi_sclk__0__HSIOM_MASK)
    #define EnoUART_SCLK_HSIOM_POS       (EnoUART_spi_sclk__0__HSIOM_SHIFT)
    #define EnoUART_SCLK_HSIOM_SEL_GPIO  (EnoUART_spi_sclk__0__HSIOM_GPIO)
    #define EnoUART_SCLK_HSIOM_SEL_I2C   (EnoUART_spi_sclk__0__HSIOM_I2C)
    #define EnoUART_SCLK_HSIOM_SEL_SPI   (EnoUART_spi_sclk__0__HSIOM_SPI)
    #define EnoUART_SCLK_HSIOM_SEL_UART  (EnoUART_spi_sclk__0__HSIOM_UART)
#endif /* (EnoUART_SCLK_PIN) */

#if (EnoUART_SS0_PIN)
    #define EnoUART_SS0_HSIOM_REG   (*(reg32 *) EnoUART_spi_ss0__0__HSIOM)
    #define EnoUART_SS0_HSIOM_PTR   ( (reg32 *) EnoUART_spi_ss0__0__HSIOM)
    
    #define EnoUART_SS0_HSIOM_MASK      (EnoUART_spi_ss0__0__HSIOM_MASK)
    #define EnoUART_SS0_HSIOM_POS       (EnoUART_spi_ss0__0__HSIOM_SHIFT)
    #define EnoUART_SS0_HSIOM_SEL_GPIO  (EnoUART_spi_ss0__0__HSIOM_GPIO)
    #define EnoUART_SS0_HSIOM_SEL_I2C   (EnoUART_spi_ss0__0__HSIOM_I2C)
    #define EnoUART_SS0_HSIOM_SEL_SPI   (EnoUART_spi_ss0__0__HSIOM_SPI)
#if !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1)
    #define EnoUART_SS0_HSIOM_SEL_UART  (EnoUART_spi_ss0__0__HSIOM_UART)
#endif /* !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1) */
#endif /* (EnoUART_SS0_PIN) */

#if (EnoUART_SS1_PIN)
    #define EnoUART_SS1_HSIOM_REG  (*(reg32 *) EnoUART_spi_ss1__0__HSIOM)
    #define EnoUART_SS1_HSIOM_PTR  ( (reg32 *) EnoUART_spi_ss1__0__HSIOM)
    
    #define EnoUART_SS1_HSIOM_MASK     (EnoUART_spi_ss1__0__HSIOM_MASK)
    #define EnoUART_SS1_HSIOM_POS      (EnoUART_spi_ss1__0__HSIOM_SHIFT)
    #define EnoUART_SS1_HSIOM_SEL_GPIO (EnoUART_spi_ss1__0__HSIOM_GPIO)
    #define EnoUART_SS1_HSIOM_SEL_I2C  (EnoUART_spi_ss1__0__HSIOM_I2C)
    #define EnoUART_SS1_HSIOM_SEL_SPI  (EnoUART_spi_ss1__0__HSIOM_SPI)
#endif /* (EnoUART_SS1_PIN) */

#if (EnoUART_SS2_PIN)
    #define EnoUART_SS2_HSIOM_REG     (*(reg32 *) EnoUART_spi_ss2__0__HSIOM)
    #define EnoUART_SS2_HSIOM_PTR     ( (reg32 *) EnoUART_spi_ss2__0__HSIOM)
    
    #define EnoUART_SS2_HSIOM_MASK     (EnoUART_spi_ss2__0__HSIOM_MASK)
    #define EnoUART_SS2_HSIOM_POS      (EnoUART_spi_ss2__0__HSIOM_SHIFT)
    #define EnoUART_SS2_HSIOM_SEL_GPIO (EnoUART_spi_ss2__0__HSIOM_GPIO)
    #define EnoUART_SS2_HSIOM_SEL_I2C  (EnoUART_spi_ss2__0__HSIOM_I2C)
    #define EnoUART_SS2_HSIOM_SEL_SPI  (EnoUART_spi_ss2__0__HSIOM_SPI)
#endif /* (EnoUART_SS2_PIN) */

#if (EnoUART_SS3_PIN)
    #define EnoUART_SS3_HSIOM_REG     (*(reg32 *) EnoUART_spi_ss3__0__HSIOM)
    #define EnoUART_SS3_HSIOM_PTR     ( (reg32 *) EnoUART_spi_ss3__0__HSIOM)
    
    #define EnoUART_SS3_HSIOM_MASK     (EnoUART_spi_ss3__0__HSIOM_MASK)
    #define EnoUART_SS3_HSIOM_POS      (EnoUART_spi_ss3__0__HSIOM_SHIFT)
    #define EnoUART_SS3_HSIOM_SEL_GPIO (EnoUART_spi_ss3__0__HSIOM_GPIO)
    #define EnoUART_SS3_HSIOM_SEL_I2C  (EnoUART_spi_ss3__0__HSIOM_I2C)
    #define EnoUART_SS3_HSIOM_SEL_SPI  (EnoUART_spi_ss3__0__HSIOM_SPI)
#endif /* (EnoUART_SS3_PIN) */

#if (EnoUART_I2C_PINS)
    #define EnoUART_SCL_HSIOM_REG  (*(reg32 *) EnoUART_scl__0__HSIOM)
    #define EnoUART_SCL_HSIOM_PTR  ( (reg32 *) EnoUART_scl__0__HSIOM)
    
    #define EnoUART_SCL_HSIOM_MASK     (EnoUART_scl__0__HSIOM_MASK)
    #define EnoUART_SCL_HSIOM_POS      (EnoUART_scl__0__HSIOM_SHIFT)
    #define EnoUART_SCL_HSIOM_SEL_GPIO (EnoUART_sda__0__HSIOM_GPIO)
    #define EnoUART_SCL_HSIOM_SEL_I2C  (EnoUART_sda__0__HSIOM_I2C)
    
    #define EnoUART_SDA_HSIOM_REG  (*(reg32 *) EnoUART_sda__0__HSIOM)
    #define EnoUART_SDA_HSIOM_PTR  ( (reg32 *) EnoUART_sda__0__HSIOM)
    
    #define EnoUART_SDA_HSIOM_MASK     (EnoUART_sda__0__HSIOM_MASK)
    #define EnoUART_SDA_HSIOM_POS      (EnoUART_sda__0__HSIOM_SHIFT)
    #define EnoUART_SDA_HSIOM_SEL_GPIO (EnoUART_sda__0__HSIOM_GPIO)
    #define EnoUART_SDA_HSIOM_SEL_I2C  (EnoUART_sda__0__HSIOM_I2C)
#endif /* (EnoUART_I2C_PINS) */

#if (EnoUART_SPI_SLAVE_PINS)
    #define EnoUART_SCLK_S_HSIOM_REG   (*(reg32 *) EnoUART_sclk_s__0__HSIOM)
    #define EnoUART_SCLK_S_HSIOM_PTR   ( (reg32 *) EnoUART_sclk_s__0__HSIOM)
    
    #define EnoUART_SCLK_S_HSIOM_MASK      (EnoUART_sclk_s__0__HSIOM_MASK)
    #define EnoUART_SCLK_S_HSIOM_POS       (EnoUART_sclk_s__0__HSIOM_SHIFT)
    #define EnoUART_SCLK_S_HSIOM_SEL_GPIO  (EnoUART_sclk_s__0__HSIOM_GPIO)
    #define EnoUART_SCLK_S_HSIOM_SEL_SPI   (EnoUART_sclk_s__0__HSIOM_SPI)
    
    #define EnoUART_SS0_S_HSIOM_REG    (*(reg32 *) EnoUART_ss0_s__0__HSIOM)
    #define EnoUART_SS0_S_HSIOM_PTR    ( (reg32 *) EnoUART_ss0_s__0__HSIOM)
    
    #define EnoUART_SS0_S_HSIOM_MASK       (EnoUART_ss0_s__0__HSIOM_MASK)
    #define EnoUART_SS0_S_HSIOM_POS        (EnoUART_ss0_s__0__HSIOM_SHIFT)
    #define EnoUART_SS0_S_HSIOM_SEL_GPIO   (EnoUART_ss0_s__0__HSIOM_GPIO)  
    #define EnoUART_SS0_S_HSIOM_SEL_SPI    (EnoUART_ss0_s__0__HSIOM_SPI)
#endif /* (EnoUART_SPI_SLAVE_PINS) */

#if (EnoUART_SPI_SLAVE_MOSI_PIN)
    #define EnoUART_MOSI_S_HSIOM_REG   (*(reg32 *) EnoUART_mosi_s__0__HSIOM)
    #define EnoUART_MOSI_S_HSIOM_PTR   ( (reg32 *) EnoUART_mosi_s__0__HSIOM)
    
    #define EnoUART_MOSI_S_HSIOM_MASK      (EnoUART_mosi_s__0__HSIOM_MASK)
    #define EnoUART_MOSI_S_HSIOM_POS       (EnoUART_mosi_s__0__HSIOM_SHIFT)
    #define EnoUART_MOSI_S_HSIOM_SEL_GPIO  (EnoUART_mosi_s__0__HSIOM_GPIO)
    #define EnoUART_MOSI_S_HSIOM_SEL_SPI   (EnoUART_mosi_s__0__HSIOM_SPI)
#endif /* (EnoUART_SPI_SLAVE_MOSI_PIN) */

#if (EnoUART_SPI_SLAVE_MISO_PIN)
    #define EnoUART_MISO_S_HSIOM_REG   (*(reg32 *) EnoUART_miso_s__0__HSIOM)
    #define EnoUART_MISO_S_HSIOM_PTR   ( (reg32 *) EnoUART_miso_s__0__HSIOM)
    
    #define EnoUART_MISO_S_HSIOM_MASK      (EnoUART_miso_s__0__HSIOM_MASK)
    #define EnoUART_MISO_S_HSIOM_POS       (EnoUART_miso_s__0__HSIOM_SHIFT)
    #define EnoUART_MISO_S_HSIOM_SEL_GPIO  (EnoUART_miso_s__0__HSIOM_GPIO)
    #define EnoUART_MISO_S_HSIOM_SEL_SPI   (EnoUART_miso_s__0__HSIOM_SPI)
#endif /* (EnoUART_SPI_SLAVE_MISO_PIN) */

#if (EnoUART_SPI_MASTER_MISO_PIN)
    #define EnoUART_MISO_M_HSIOM_REG   (*(reg32 *) EnoUART_miso_m__0__HSIOM)
    #define EnoUART_MISO_M_HSIOM_PTR   ( (reg32 *) EnoUART_miso_m__0__HSIOM)
    
    #define EnoUART_MISO_M_HSIOM_MASK      (EnoUART_miso_m__0__HSIOM_MASK)
    #define EnoUART_MISO_M_HSIOM_POS       (EnoUART_miso_m__0__HSIOM_SHIFT)
    #define EnoUART_MISO_M_HSIOM_SEL_GPIO  (EnoUART_miso_m__0__HSIOM_GPIO)
    #define EnoUART_MISO_M_HSIOM_SEL_SPI   (EnoUART_miso_m__0__HSIOM_SPI)
#endif /* (EnoUART_SPI_MASTER_MISO_PIN) */

#if (EnoUART_SPI_MASTER_MOSI_PIN)
    #define EnoUART_MOSI_M_HSIOM_REG   (*(reg32 *) EnoUART_mosi_m__0__HSIOM)
    #define EnoUART_MOSI_M_HSIOM_PTR   ( (reg32 *) EnoUART_mosi_m__0__HSIOM)
    
    #define EnoUART_MOSI_M_HSIOM_MASK      (EnoUART_mosi_m__0__HSIOM_MASK)
    #define EnoUART_MOSI_M_HSIOM_POS       (EnoUART_mosi_m__0__HSIOM_SHIFT)
    #define EnoUART_MOSI_M_HSIOM_SEL_GPIO  (EnoUART_mosi_m__0__HSIOM_GPIO)
    #define EnoUART_MOSI_M_HSIOM_SEL_SPI   (EnoUART_mosi_m__0__HSIOM_SPI)
#endif /* (EnoUART_SPI_MASTER_MOSI_PIN) */

#if (EnoUART_SPI_MASTER_SCLK_PIN)
    #define EnoUART_SCLK_M_HSIOM_REG   (*(reg32 *) EnoUART_sclk_m__0__HSIOM)
    #define EnoUART_SCLK_M_HSIOM_PTR   ( (reg32 *) EnoUART_sclk_m__0__HSIOM)
    
    #define EnoUART_SCLK_M_HSIOM_MASK      (EnoUART_sclk_m__0__HSIOM_MASK)
    #define EnoUART_SCLK_M_HSIOM_POS       (EnoUART_sclk_m__0__HSIOM_SHIFT)
    #define EnoUART_SCLK_M_HSIOM_SEL_GPIO  (EnoUART_sclk_m__0__HSIOM_GPIO)
    #define EnoUART_SCLK_M_HSIOM_SEL_SPI   (EnoUART_sclk_m__0__HSIOM_SPI)
#endif /* (EnoUART_SPI_MASTER_SCLK_PIN) */

#if (EnoUART_SPI_MASTER_SS0_PIN)
    #define EnoUART_SS0_M_HSIOM_REG    (*(reg32 *) EnoUART_ss0_m__0__HSIOM)
    #define EnoUART_SS0_M_HSIOM_PTR    ( (reg32 *) EnoUART_ss0_m__0__HSIOM)
    
    #define EnoUART_SS0_M_HSIOM_MASK       (EnoUART_ss0_m__0__HSIOM_MASK)
    #define EnoUART_SS0_M_HSIOM_POS        (EnoUART_ss0_m__0__HSIOM_SHIFT)
    #define EnoUART_SS0_M_HSIOM_SEL_GPIO   (EnoUART_ss0_m__0__HSIOM_GPIO)
    #define EnoUART_SS0_M_HSIOM_SEL_SPI    (EnoUART_ss0_m__0__HSIOM_SPI)
#endif /* (EnoUART_SPI_MASTER_SS0_PIN) */

#if (EnoUART_SPI_MASTER_SS1_PIN)
    #define EnoUART_SS1_M_HSIOM_REG    (*(reg32 *) EnoUART_ss1_m__0__HSIOM)
    #define EnoUART_SS1_M_HSIOM_PTR    ( (reg32 *) EnoUART_ss1_m__0__HSIOM)
    
    #define EnoUART_SS1_M_HSIOM_MASK       (EnoUART_ss1_m__0__HSIOM_MASK)
    #define EnoUART_SS1_M_HSIOM_POS        (EnoUART_ss1_m__0__HSIOM_SHIFT)
    #define EnoUART_SS1_M_HSIOM_SEL_GPIO   (EnoUART_ss1_m__0__HSIOM_GPIO)
    #define EnoUART_SS1_M_HSIOM_SEL_SPI    (EnoUART_ss1_m__0__HSIOM_SPI)
#endif /* (EnoUART_SPI_MASTER_SS1_PIN) */

#if (EnoUART_SPI_MASTER_SS2_PIN)
    #define EnoUART_SS2_M_HSIOM_REG    (*(reg32 *) EnoUART_ss2_m__0__HSIOM)
    #define EnoUART_SS2_M_HSIOM_PTR    ( (reg32 *) EnoUART_ss2_m__0__HSIOM)
    
    #define EnoUART_SS2_M_HSIOM_MASK       (EnoUART_ss2_m__0__HSIOM_MASK)
    #define EnoUART_SS2_M_HSIOM_POS        (EnoUART_ss2_m__0__HSIOM_SHIFT)
    #define EnoUART_SS2_M_HSIOM_SEL_GPIO   (EnoUART_ss2_m__0__HSIOM_GPIO)
    #define EnoUART_SS2_M_HSIOM_SEL_SPI    (EnoUART_ss2_m__0__HSIOM_SPI)
#endif /* (EnoUART_SPI_MASTER_SS2_PIN) */

#if (EnoUART_SPI_MASTER_SS3_PIN)
    #define EnoUART_SS3_M_HSIOM_REG    (*(reg32 *) EnoUART_ss3_m__0__HSIOM)
    #define EnoUART_SS3_M_HSIOM_PTR    ( (reg32 *) EnoUART_ss3_m__0__HSIOM)
    
    #define EnoUART_SS3_M_HSIOM_MASK      (EnoUART_ss3_m__0__HSIOM_MASK)
    #define EnoUART_SS3_M_HSIOM_POS       (EnoUART_ss3_m__0__HSIOM_SHIFT)
    #define EnoUART_SS3_M_HSIOM_SEL_GPIO  (EnoUART_ss3_m__0__HSIOM_GPIO)
    #define EnoUART_SS3_M_HSIOM_SEL_SPI   (EnoUART_ss3_m__0__HSIOM_SPI)
#endif /* (EnoUART_SPI_MASTER_SS3_PIN) */

#if (EnoUART_UART_RX_PIN)
    #define EnoUART_RX_HSIOM_REG   (*(reg32 *) EnoUART_rx__0__HSIOM)
    #define EnoUART_RX_HSIOM_PTR   ( (reg32 *) EnoUART_rx__0__HSIOM)
    
    #define EnoUART_RX_HSIOM_MASK      (EnoUART_rx__0__HSIOM_MASK)
    #define EnoUART_RX_HSIOM_POS       (EnoUART_rx__0__HSIOM_SHIFT)
    #define EnoUART_RX_HSIOM_SEL_GPIO  (EnoUART_rx__0__HSIOM_GPIO)
    #define EnoUART_RX_HSIOM_SEL_UART  (EnoUART_rx__0__HSIOM_UART)
#endif /* (EnoUART_UART_RX_PIN) */

#if (EnoUART_UART_RX_WAKE_PIN)
    #define EnoUART_RX_WAKE_HSIOM_REG   (*(reg32 *) EnoUART_rx_wake__0__HSIOM)
    #define EnoUART_RX_WAKE_HSIOM_PTR   ( (reg32 *) EnoUART_rx_wake__0__HSIOM)
    
    #define EnoUART_RX_WAKE_HSIOM_MASK      (EnoUART_rx_wake__0__HSIOM_MASK)
    #define EnoUART_RX_WAKE_HSIOM_POS       (EnoUART_rx_wake__0__HSIOM_SHIFT)
    #define EnoUART_RX_WAKE_HSIOM_SEL_GPIO  (EnoUART_rx_wake__0__HSIOM_GPIO)
    #define EnoUART_RX_WAKE_HSIOM_SEL_UART  (EnoUART_rx_wake__0__HSIOM_UART)
#endif /* (EnoUART_UART_WAKE_RX_PIN) */

#if (EnoUART_UART_CTS_PIN)
    #define EnoUART_CTS_HSIOM_REG   (*(reg32 *) EnoUART_cts__0__HSIOM)
    #define EnoUART_CTS_HSIOM_PTR   ( (reg32 *) EnoUART_cts__0__HSIOM)
    
    #define EnoUART_CTS_HSIOM_MASK      (EnoUART_cts__0__HSIOM_MASK)
    #define EnoUART_CTS_HSIOM_POS       (EnoUART_cts__0__HSIOM_SHIFT)
    #define EnoUART_CTS_HSIOM_SEL_GPIO  (EnoUART_cts__0__HSIOM_GPIO)
    #define EnoUART_CTS_HSIOM_SEL_UART  (EnoUART_cts__0__HSIOM_UART)
#endif /* (EnoUART_UART_CTS_PIN) */

#if (EnoUART_UART_TX_PIN)
    #define EnoUART_TX_HSIOM_REG   (*(reg32 *) EnoUART_tx__0__HSIOM)
    #define EnoUART_TX_HSIOM_PTR   ( (reg32 *) EnoUART_tx__0__HSIOM)
    
    #define EnoUART_TX_HSIOM_MASK      (EnoUART_tx__0__HSIOM_MASK)
    #define EnoUART_TX_HSIOM_POS       (EnoUART_tx__0__HSIOM_SHIFT)
    #define EnoUART_TX_HSIOM_SEL_GPIO  (EnoUART_tx__0__HSIOM_GPIO)
    #define EnoUART_TX_HSIOM_SEL_UART  (EnoUART_tx__0__HSIOM_UART)
#endif /* (EnoUART_UART_TX_PIN) */

#if (EnoUART_UART_RX_TX_PIN)
    #define EnoUART_RX_TX_HSIOM_REG   (*(reg32 *) EnoUART_rx_tx__0__HSIOM)
    #define EnoUART_RX_TX_HSIOM_PTR   ( (reg32 *) EnoUART_rx_tx__0__HSIOM)
    
    #define EnoUART_RX_TX_HSIOM_MASK      (EnoUART_rx_tx__0__HSIOM_MASK)
    #define EnoUART_RX_TX_HSIOM_POS       (EnoUART_rx_tx__0__HSIOM_SHIFT)
    #define EnoUART_RX_TX_HSIOM_SEL_GPIO  (EnoUART_rx_tx__0__HSIOM_GPIO)
    #define EnoUART_RX_TX_HSIOM_SEL_UART  (EnoUART_rx_tx__0__HSIOM_UART)
#endif /* (EnoUART_UART_RX_TX_PIN) */

#if (EnoUART_UART_RTS_PIN)
    #define EnoUART_RTS_HSIOM_REG      (*(reg32 *) EnoUART_rts__0__HSIOM)
    #define EnoUART_RTS_HSIOM_PTR      ( (reg32 *) EnoUART_rts__0__HSIOM)
    
    #define EnoUART_RTS_HSIOM_MASK     (EnoUART_rts__0__HSIOM_MASK)
    #define EnoUART_RTS_HSIOM_POS      (EnoUART_rts__0__HSIOM_SHIFT)    
    #define EnoUART_RTS_HSIOM_SEL_GPIO (EnoUART_rts__0__HSIOM_GPIO)
    #define EnoUART_RTS_HSIOM_SEL_UART (EnoUART_rts__0__HSIOM_UART)    
#endif /* (EnoUART_UART_RTS_PIN) */


/***************************************
*        Registers Constants
***************************************/

/* HSIOM switch values. */ 
#define EnoUART_HSIOM_DEF_SEL      (0x00u)
#define EnoUART_HSIOM_GPIO_SEL     (0x00u)
/* The HSIOM values provided below are valid only for EnoUART_CY_SCBIP_V0 
* and EnoUART_CY_SCBIP_V1. It is not recommended to use them for 
* EnoUART_CY_SCBIP_V2. Use pin name specific HSIOM constants provided 
* above instead for any SCB IP block version.
*/
#define EnoUART_HSIOM_UART_SEL     (0x09u)
#define EnoUART_HSIOM_I2C_SEL      (0x0Eu)
#define EnoUART_HSIOM_SPI_SEL      (0x0Fu)

/* Pins settings index. */
#define EnoUART_RX_WAKE_SCL_MOSI_PIN_INDEX   (0u)
#define EnoUART_RX_SCL_MOSI_PIN_INDEX       (0u)
#define EnoUART_TX_SDA_MISO_PIN_INDEX       (1u)
#define EnoUART_SCLK_PIN_INDEX       (2u)
#define EnoUART_SS0_PIN_INDEX       (3u)
#define EnoUART_SS1_PIN_INDEX                  (4u)
#define EnoUART_SS2_PIN_INDEX                  (5u)
#define EnoUART_SS3_PIN_INDEX                  (6u)

/* Pins settings mask. */
#define EnoUART_RX_WAKE_SCL_MOSI_PIN_MASK ((uint32) 0x01u << EnoUART_RX_WAKE_SCL_MOSI_PIN_INDEX)
#define EnoUART_RX_SCL_MOSI_PIN_MASK     ((uint32) 0x01u << EnoUART_RX_SCL_MOSI_PIN_INDEX)
#define EnoUART_TX_SDA_MISO_PIN_MASK     ((uint32) 0x01u << EnoUART_TX_SDA_MISO_PIN_INDEX)
#define EnoUART_SCLK_PIN_MASK     ((uint32) 0x01u << EnoUART_SCLK_PIN_INDEX)
#define EnoUART_SS0_PIN_MASK     ((uint32) 0x01u << EnoUART_SS0_PIN_INDEX)
#define EnoUART_SS1_PIN_MASK                ((uint32) 0x01u << EnoUART_SS1_PIN_INDEX)
#define EnoUART_SS2_PIN_MASK                ((uint32) 0x01u << EnoUART_SS2_PIN_INDEX)
#define EnoUART_SS3_PIN_MASK                ((uint32) 0x01u << EnoUART_SS3_PIN_INDEX)

/* Pin interrupt constants. */
#define EnoUART_INTCFG_TYPE_MASK           (0x03u)
#define EnoUART_INTCFG_TYPE_FALLING_EDGE   (0x02u)

/* Pin Drive Mode constants. */
#define EnoUART_PIN_DM_ALG_HIZ  (0u)
#define EnoUART_PIN_DM_DIG_HIZ  (1u)
#define EnoUART_PIN_DM_OD_LO    (4u)
#define EnoUART_PIN_DM_STRONG   (6u)


/***************************************
*          Macro Definitions
***************************************/

/* Return drive mode of the pin */
#define EnoUART_DM_MASK    (0x7u)
#define EnoUART_DM_SIZE    (3u)
#define EnoUART_GET_P4_PIN_DM(reg, pos) \
    ( ((reg) & (uint32) ((uint32) EnoUART_DM_MASK << (EnoUART_DM_SIZE * (pos)))) >> \
                                                              (EnoUART_DM_SIZE * (pos)) )

#if (EnoUART_TX_SDA_MISO_PIN)
    #define EnoUART_CHECK_TX_SDA_MISO_PIN_USED \
                (EnoUART_PIN_DM_ALG_HIZ != \
                    EnoUART_GET_P4_PIN_DM(EnoUART_uart_tx_i2c_sda_spi_miso_PC, \
                                                   EnoUART_uart_tx_i2c_sda_spi_miso_SHIFT))
#endif /* (EnoUART_TX_SDA_MISO_PIN) */

#if (EnoUART_SS0_PIN)
    #define EnoUART_CHECK_SS0_PIN_USED \
                (EnoUART_PIN_DM_ALG_HIZ != \
                    EnoUART_GET_P4_PIN_DM(EnoUART_spi_ss0_PC, \
                                                   EnoUART_spi_ss0_SHIFT))
#endif /* (EnoUART_SS0_PIN) */

/* Set bits-mask in register */
#define EnoUART_SET_REGISTER_BITS(reg, mask, pos, mode) \
                    do                                           \
                    {                                            \
                        (reg) = (((reg) & ((uint32) ~(uint32) (mask))) | ((uint32) ((uint32) (mode) << (pos)))); \
                    }while(0)

/* Set bit in the register */
#define EnoUART_SET_REGISTER_BIT(reg, mask, val) \
                    ((val) ? ((reg) |= (mask)) : ((reg) &= ((uint32) ~((uint32) (mask)))))

#define EnoUART_SET_HSIOM_SEL(reg, mask, pos, sel) EnoUART_SET_REGISTER_BITS(reg, mask, pos, sel)
#define EnoUART_SET_INCFG_TYPE(reg, mask, pos, intType) \
                                                        EnoUART_SET_REGISTER_BITS(reg, mask, pos, intType)
#define EnoUART_SET_INP_DIS(reg, mask, val) EnoUART_SET_REGISTER_BIT(reg, mask, val)

/* EnoUART_SET_I2C_SCL_DR(val) - Sets I2C SCL DR register.
*  EnoUART_SET_I2C_SCL_HSIOM_SEL(sel) - Sets I2C SCL HSIOM settings.
*/
/* SCB I2C: scl signal */
#if (EnoUART_CY_SCBIP_V0)
#if (EnoUART_I2C_PINS)
    #define EnoUART_SET_I2C_SCL_DR(val) EnoUART_scl_Write(val)

    #define EnoUART_SET_I2C_SCL_HSIOM_SEL(sel) \
                          EnoUART_SET_HSIOM_SEL(EnoUART_SCL_HSIOM_REG,  \
                                                         EnoUART_SCL_HSIOM_MASK, \
                                                         EnoUART_SCL_HSIOM_POS,  \
                                                         (sel))
    #define EnoUART_WAIT_SCL_SET_HIGH  (0u == EnoUART_scl_Read())

/* Unconfigured SCB: scl signal */
#elif (EnoUART_RX_WAKE_SCL_MOSI_PIN)
    #define EnoUART_SET_I2C_SCL_DR(val) \
                            EnoUART_uart_rx_wake_i2c_scl_spi_mosi_Write(val)

    #define EnoUART_SET_I2C_SCL_HSIOM_SEL(sel) \
                    EnoUART_SET_HSIOM_SEL(EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG,  \
                                                   EnoUART_RX_WAKE_SCL_MOSI_HSIOM_MASK, \
                                                   EnoUART_RX_WAKE_SCL_MOSI_HSIOM_POS,  \
                                                   (sel))

    #define EnoUART_WAIT_SCL_SET_HIGH  (0u == EnoUART_uart_rx_wake_i2c_scl_spi_mosi_Read())

#elif (EnoUART_RX_SCL_MOSI_PIN)
    #define EnoUART_SET_I2C_SCL_DR(val) \
                            EnoUART_uart_rx_i2c_scl_spi_mosi_Write(val)


    #define EnoUART_SET_I2C_SCL_HSIOM_SEL(sel) \
                            EnoUART_SET_HSIOM_SEL(EnoUART_RX_SCL_MOSI_HSIOM_REG,  \
                                                           EnoUART_RX_SCL_MOSI_HSIOM_MASK, \
                                                           EnoUART_RX_SCL_MOSI_HSIOM_POS,  \
                                                           (sel))

    #define EnoUART_WAIT_SCL_SET_HIGH  (0u == EnoUART_uart_rx_i2c_scl_spi_mosi_Read())

#else
    #define EnoUART_SET_I2C_SCL_DR(val)        do{ /* Does nothing */ }while(0)
    #define EnoUART_SET_I2C_SCL_HSIOM_SEL(sel) do{ /* Does nothing */ }while(0)

    #define EnoUART_WAIT_SCL_SET_HIGH  (0u)
#endif /* (EnoUART_I2C_PINS) */

/* SCB I2C: sda signal */
#if (EnoUART_I2C_PINS)
    #define EnoUART_WAIT_SDA_SET_HIGH  (0u == EnoUART_sda_Read())
/* Unconfigured SCB: sda signal */
#elif (EnoUART_TX_SDA_MISO_PIN)
    #define EnoUART_WAIT_SDA_SET_HIGH  (0u == EnoUART_uart_tx_i2c_sda_spi_miso_Read())
#else
    #define EnoUART_WAIT_SDA_SET_HIGH  (0u)
#endif /* (EnoUART_MOSI_SCL_RX_PIN) */
#endif /* (EnoUART_CY_SCBIP_V0) */

/* Clear UART wakeup source */
#if (EnoUART_RX_SCL_MOSI_PIN)
    #define EnoUART_CLEAR_UART_RX_WAKE_INTR        do{ /* Does nothing */ }while(0)
    
#elif (EnoUART_RX_WAKE_SCL_MOSI_PIN)
    #define EnoUART_CLEAR_UART_RX_WAKE_INTR \
            do{                                      \
                (void) EnoUART_uart_rx_wake_i2c_scl_spi_mosi_ClearInterrupt(); \
            }while(0)

#elif(EnoUART_UART_RX_WAKE_PIN)
    #define EnoUART_CLEAR_UART_RX_WAKE_INTR \
            do{                                      \
                (void) EnoUART_rx_wake_ClearInterrupt(); \
            }while(0)
#else
#endif /* (EnoUART_RX_SCL_MOSI_PIN) */


/***************************************
* The following code is DEPRECATED and
* must not be used.
***************************************/

/* Unconfigured pins */
#define EnoUART_REMOVE_MOSI_SCL_RX_WAKE_PIN    EnoUART_REMOVE_RX_WAKE_SCL_MOSI_PIN
#define EnoUART_REMOVE_MOSI_SCL_RX_PIN         EnoUART_REMOVE_RX_SCL_MOSI_PIN
#define EnoUART_REMOVE_MISO_SDA_TX_PIN         EnoUART_REMOVE_TX_SDA_MISO_PIN
#ifndef EnoUART_REMOVE_SCLK_PIN
#define EnoUART_REMOVE_SCLK_PIN                EnoUART_REMOVE_SCLK_PIN
#endif /* EnoUART_REMOVE_SCLK_PIN */
#ifndef EnoUART_REMOVE_SS0_PIN
#define EnoUART_REMOVE_SS0_PIN                 EnoUART_REMOVE_SS0_PIN
#endif /* EnoUART_REMOVE_SS0_PIN */

/* Unconfigured pins */
#define EnoUART_MOSI_SCL_RX_WAKE_PIN   EnoUART_RX_WAKE_SCL_MOSI_PIN
#define EnoUART_MOSI_SCL_RX_PIN        EnoUART_RX_SCL_MOSI_PIN
#define EnoUART_MISO_SDA_TX_PIN        EnoUART_TX_SDA_MISO_PIN
#ifndef EnoUART_SCLK_PIN
#define EnoUART_SCLK_PIN               EnoUART_SCLK_PIN
#endif /* EnoUART_SCLK_PIN */
#ifndef EnoUART_SS0_PIN
#define EnoUART_SS0_PIN                EnoUART_SS0_PIN
#endif /* EnoUART_SS0_PIN */

#if (EnoUART_MOSI_SCL_RX_WAKE_PIN)
    #define EnoUART_MOSI_SCL_RX_WAKE_HSIOM_REG     EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define EnoUART_MOSI_SCL_RX_WAKE_HSIOM_PTR     EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define EnoUART_MOSI_SCL_RX_WAKE_HSIOM_MASK    EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define EnoUART_MOSI_SCL_RX_WAKE_HSIOM_POS     EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG

    #define EnoUART_MOSI_SCL_RX_WAKE_INTCFG_REG    EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define EnoUART_MOSI_SCL_RX_WAKE_INTCFG_PTR    EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG

    #define EnoUART_MOSI_SCL_RX_WAKE_INTCFG_TYPE_POS   EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define EnoUART_MOSI_SCL_RX_WAKE_INTCFG_TYPE_MASK  EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG
#endif /* (EnoUART_RX_WAKE_SCL_MOSI_PIN) */

#if (EnoUART_MOSI_SCL_RX_PIN)
    #define EnoUART_MOSI_SCL_RX_HSIOM_REG      EnoUART_RX_SCL_MOSI_HSIOM_REG
    #define EnoUART_MOSI_SCL_RX_HSIOM_PTR      EnoUART_RX_SCL_MOSI_HSIOM_PTR
    #define EnoUART_MOSI_SCL_RX_HSIOM_MASK     EnoUART_RX_SCL_MOSI_HSIOM_MASK
    #define EnoUART_MOSI_SCL_RX_HSIOM_POS      EnoUART_RX_SCL_MOSI_HSIOM_POS
#endif /* (EnoUART_MOSI_SCL_RX_PIN) */

#if (EnoUART_MISO_SDA_TX_PIN)
    #define EnoUART_MISO_SDA_TX_HSIOM_REG      EnoUART_TX_SDA_MISO_HSIOM_REG
    #define EnoUART_MISO_SDA_TX_HSIOM_PTR      EnoUART_TX_SDA_MISO_HSIOM_REG
    #define EnoUART_MISO_SDA_TX_HSIOM_MASK     EnoUART_TX_SDA_MISO_HSIOM_REG
    #define EnoUART_MISO_SDA_TX_HSIOM_POS      EnoUART_TX_SDA_MISO_HSIOM_REG
#endif /* (EnoUART_MISO_SDA_TX_PIN_PIN) */

#if (EnoUART_SCLK_PIN)
    #ifndef EnoUART_SCLK_HSIOM_REG
    #define EnoUART_SCLK_HSIOM_REG     EnoUART_SCLK_HSIOM_REG
    #define EnoUART_SCLK_HSIOM_PTR     EnoUART_SCLK_HSIOM_PTR
    #define EnoUART_SCLK_HSIOM_MASK    EnoUART_SCLK_HSIOM_MASK
    #define EnoUART_SCLK_HSIOM_POS     EnoUART_SCLK_HSIOM_POS
    #endif /* EnoUART_SCLK_HSIOM_REG */
#endif /* (EnoUART_SCLK_PIN) */

#if (EnoUART_SS0_PIN)
    #ifndef EnoUART_SS0_HSIOM_REG
    #define EnoUART_SS0_HSIOM_REG      EnoUART_SS0_HSIOM_REG
    #define EnoUART_SS0_HSIOM_PTR      EnoUART_SS0_HSIOM_PTR
    #define EnoUART_SS0_HSIOM_MASK     EnoUART_SS0_HSIOM_MASK
    #define EnoUART_SS0_HSIOM_POS      EnoUART_SS0_HSIOM_POS
    #endif /* EnoUART_SS0_HSIOM_REG */
#endif /* (EnoUART_SS0_PIN) */

#define EnoUART_MOSI_SCL_RX_WAKE_PIN_INDEX EnoUART_RX_WAKE_SCL_MOSI_PIN_INDEX
#define EnoUART_MOSI_SCL_RX_PIN_INDEX      EnoUART_RX_SCL_MOSI_PIN_INDEX
#define EnoUART_MISO_SDA_TX_PIN_INDEX      EnoUART_TX_SDA_MISO_PIN_INDEX
#ifndef EnoUART_SCLK_PIN_INDEX
#define EnoUART_SCLK_PIN_INDEX             EnoUART_SCLK_PIN_INDEX
#endif /* EnoUART_SCLK_PIN_INDEX */
#ifndef EnoUART_SS0_PIN_INDEX
#define EnoUART_SS0_PIN_INDEX              EnoUART_SS0_PIN_INDEX
#endif /* EnoUART_SS0_PIN_INDEX */

#define EnoUART_MOSI_SCL_RX_WAKE_PIN_MASK EnoUART_RX_WAKE_SCL_MOSI_PIN_MASK
#define EnoUART_MOSI_SCL_RX_PIN_MASK      EnoUART_RX_SCL_MOSI_PIN_MASK
#define EnoUART_MISO_SDA_TX_PIN_MASK      EnoUART_TX_SDA_MISO_PIN_MASK
#ifndef EnoUART_SCLK_PIN_MASK
#define EnoUART_SCLK_PIN_MASK             EnoUART_SCLK_PIN_MASK
#endif /* EnoUART_SCLK_PIN_MASK */
#ifndef EnoUART_SS0_PIN_MASK
#define EnoUART_SS0_PIN_MASK              EnoUART_SS0_PIN_MASK
#endif /* EnoUART_SS0_PIN_MASK */

#endif /* (CY_SCB_PINS_EnoUART_H) */


/* [] END OF FILE */
