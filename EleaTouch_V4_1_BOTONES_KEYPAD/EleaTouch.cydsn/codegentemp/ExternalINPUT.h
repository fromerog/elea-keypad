/*******************************************************************************
* File Name: ExternalINPUT.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ExternalINPUT_H) /* Pins ExternalINPUT_H */
#define CY_PINS_ExternalINPUT_H

#include "cytypes.h"
#include "cyfitter.h"
#include "ExternalINPUT_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} ExternalINPUT_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   ExternalINPUT_Read(void);
void    ExternalINPUT_Write(uint8 value);
uint8   ExternalINPUT_ReadDataReg(void);
#if defined(ExternalINPUT__PC) || (CY_PSOC4_4200L) 
    void    ExternalINPUT_SetDriveMode(uint8 mode);
#endif
void    ExternalINPUT_SetInterruptMode(uint16 position, uint16 mode);
uint8   ExternalINPUT_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void ExternalINPUT_Sleep(void); 
void ExternalINPUT_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(ExternalINPUT__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define ExternalINPUT_DRIVE_MODE_BITS        (3)
    #define ExternalINPUT_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - ExternalINPUT_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the ExternalINPUT_SetDriveMode() function.
         *  @{
         */
        #define ExternalINPUT_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define ExternalINPUT_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define ExternalINPUT_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define ExternalINPUT_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define ExternalINPUT_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define ExternalINPUT_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define ExternalINPUT_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define ExternalINPUT_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define ExternalINPUT_MASK               ExternalINPUT__MASK
#define ExternalINPUT_SHIFT              ExternalINPUT__SHIFT
#define ExternalINPUT_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in ExternalINPUT_SetInterruptMode() function.
     *  @{
     */
        #define ExternalINPUT_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define ExternalINPUT_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define ExternalINPUT_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define ExternalINPUT_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(ExternalINPUT__SIO)
    #define ExternalINPUT_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(ExternalINPUT__PC) && (CY_PSOC4_4200L)
    #define ExternalINPUT_USBIO_ENABLE               ((uint32)0x80000000u)
    #define ExternalINPUT_USBIO_DISABLE              ((uint32)(~ExternalINPUT_USBIO_ENABLE))
    #define ExternalINPUT_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define ExternalINPUT_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define ExternalINPUT_USBIO_ENTER_SLEEP          ((uint32)((1u << ExternalINPUT_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << ExternalINPUT_USBIO_SUSPEND_DEL_SHIFT)))
    #define ExternalINPUT_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << ExternalINPUT_USBIO_SUSPEND_SHIFT)))
    #define ExternalINPUT_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << ExternalINPUT_USBIO_SUSPEND_DEL_SHIFT)))
    #define ExternalINPUT_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(ExternalINPUT__PC)
    /* Port Configuration */
    #define ExternalINPUT_PC                 (* (reg32 *) ExternalINPUT__PC)
#endif
/* Pin State */
#define ExternalINPUT_PS                     (* (reg32 *) ExternalINPUT__PS)
/* Data Register */
#define ExternalINPUT_DR                     (* (reg32 *) ExternalINPUT__DR)
/* Input Buffer Disable Override */
#define ExternalINPUT_INP_DIS                (* (reg32 *) ExternalINPUT__PC2)

/* Interrupt configuration Registers */
#define ExternalINPUT_INTCFG                 (* (reg32 *) ExternalINPUT__INTCFG)
#define ExternalINPUT_INTSTAT                (* (reg32 *) ExternalINPUT__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define ExternalINPUT_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(ExternalINPUT__SIO)
    #define ExternalINPUT_SIO_REG            (* (reg32 *) ExternalINPUT__SIO)
#endif /* (ExternalINPUT__SIO_CFG) */

/* USBIO registers */
#if !defined(ExternalINPUT__PC) && (CY_PSOC4_4200L)
    #define ExternalINPUT_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define ExternalINPUT_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define ExternalINPUT_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define ExternalINPUT_DRIVE_MODE_SHIFT       (0x00u)
#define ExternalINPUT_DRIVE_MODE_MASK        (0x07u << ExternalINPUT_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins ExternalINPUT_H */


/* [] END OF FILE */
