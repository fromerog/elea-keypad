/*******************************************************************************
* File Name: IsrExternalINPUT.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_IsrExternalINPUT_H)
#define CY_ISR_IsrExternalINPUT_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void IsrExternalINPUT_Start(void);
void IsrExternalINPUT_StartEx(cyisraddress address);
void IsrExternalINPUT_Stop(void);

CY_ISR_PROTO(IsrExternalINPUT_Interrupt);

void IsrExternalINPUT_SetVector(cyisraddress address);
cyisraddress IsrExternalINPUT_GetVector(void);

void IsrExternalINPUT_SetPriority(uint8 priority);
uint8 IsrExternalINPUT_GetPriority(void);

void IsrExternalINPUT_Enable(void);
uint8 IsrExternalINPUT_GetState(void);
void IsrExternalINPUT_Disable(void);

void IsrExternalINPUT_SetPending(void);
void IsrExternalINPUT_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the IsrExternalINPUT ISR. */
#define IsrExternalINPUT_INTC_VECTOR            ((reg32 *) IsrExternalINPUT__INTC_VECT)

/* Address of the IsrExternalINPUT ISR priority. */
#define IsrExternalINPUT_INTC_PRIOR             ((reg32 *) IsrExternalINPUT__INTC_PRIOR_REG)

/* Priority of the IsrExternalINPUT interrupt. */
#define IsrExternalINPUT_INTC_PRIOR_NUMBER      IsrExternalINPUT__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable IsrExternalINPUT interrupt. */
#define IsrExternalINPUT_INTC_SET_EN            ((reg32 *) IsrExternalINPUT__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the IsrExternalINPUT interrupt. */
#define IsrExternalINPUT_INTC_CLR_EN            ((reg32 *) IsrExternalINPUT__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the IsrExternalINPUT interrupt state to pending. */
#define IsrExternalINPUT_INTC_SET_PD            ((reg32 *) IsrExternalINPUT__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the IsrExternalINPUT interrupt. */
#define IsrExternalINPUT_INTC_CLR_PD            ((reg32 *) IsrExternalINPUT__INTC_CLR_PD_REG)



#endif /* CY_ISR_IsrExternalINPUT_H */


/* [] END OF FILE */
