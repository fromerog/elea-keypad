/*******************************************************************************
* File Name: SinUsar_12.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_SinUsar_12_ALIASES_H) /* Pins SinUsar_12_ALIASES_H */
#define CY_PINS_SinUsar_12_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define SinUsar_12_0			(SinUsar_12__0__PC)
#define SinUsar_12_0_PS		(SinUsar_12__0__PS)
#define SinUsar_12_0_PC		(SinUsar_12__0__PC)
#define SinUsar_12_0_DR		(SinUsar_12__0__DR)
#define SinUsar_12_0_SHIFT	(SinUsar_12__0__SHIFT)
#define SinUsar_12_0_INTR	((uint16)((uint16)0x0003u << (SinUsar_12__0__SHIFT*2u)))

#define SinUsar_12_INTR_ALL	 ((uint16)(SinUsar_12_0_INTR))


#endif /* End Pins SinUsar_12_ALIASES_H */


/* [] END OF FILE */
