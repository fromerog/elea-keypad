/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include <cyPm.h> //Libreria para el control energético
#include "funciones.h"
#include "EnOcean.h"
#include <math.h>

//Definicion de variables
//uint8 flagExt = 0;
//uint8 flagTouch = 0;
//uint8 flagReset = 0;

uint8 lowBatt  = 0;
uint8 revision = 0;
uint16 chequeo = 1;

uint8 blink = 0;
uint8 X = 0;
uint8 count = 0;

uint32 globalLoops = 0;
uint8 mode;
uint8 enoceanState = 0;
uint8 i = 0;

uint8  VALOR   = 0;
uint8  TECLA   = 0;
uint32 PASS    = 0;
uint16 TIMEOUT = 0;
MensajeENO Password[15] = { 0x55, 0x00, 0x0A, 0x00, 0x01, 0xA5, 0x00 , 0x00 , 0x00 , 0x00 , 'E', 'L', 'E', 'A', 0x10};
//MensajeENO BOTON[12] =    { 0x55, 0x00, 0x07, 0x00, 0x01, 0xF6, 0x40, 'E', 'L', 'E', 'A', 0x33};
MensajeENO BOTON[19] =    { 0x55, 0x00, 0x07, 0x07, 0x01, 0xF6, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x33, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00};

//Declaracion de interrupciones
CY_ISR_PROTO(IsrIN);
CY_ISR_PROTO(IsrWDT);
CY_ISR_PROTO(IsrEnoUART);

//Declaracion de modos de funcionamiento
void Init(void);
void CheckBattery(void);
uint8 checkCapsense(void);
void processAlarm(void);
void initCapsense(void);
void Enable_Botones(void);
void Disable_Botones(void);

#define INIT_STATE                  0x00
#define WAIT_ALARM                  0x01
#define SEND_ALARM                  0x02
#define WAIT_CONFIRMATION_RESET     0x03
#define WAIT_RESET                  0x04
#define SEND_RESET                  0x05

#define TRUE  1
#define FALSE 0

#define EXPIRADO            1000   // 3 segundos de intervalo entre pulsacion para introducir el password
#define LONG_PASS           8     // Longitud de la peticion: #+nnnnnnn
#define DESTELLO            10


//*******************************PROGRAMA PRINCIPAL*****************************

int main()
{  
    uint8 firstInit = TRUE;
    CyGlobalIntEnable;                              //Habilitamos interrupciones globales
    mode = INIT_STATE;
    Init();
    CyDelay(1000);
    CheckBattery();
    
    for(;;)
    {  
        switch (mode)
        {
            /* INITIAL STATE */
            case INIT_STATE:
                LED_Alarma(ON);
                EnoceanOFF();
                CyDelay(500);                       //Da tiempo para poner la pila...
                if (!firstInit)                     //si se acaba de encender no activamos el led (se activa el de la comprobacion de bateria)                    LED_Alarma(ON);
                CyDelay(500);                       //y avisa con el LED que el dispositivo está activo
                initCapsense();
                firstInit = FALSE;                
                mode = WAIT_ALARM;
                CySysWdtSetMatch(CY_SYS_WDT_COUNTER0, WDT_COUNT0_MATCH_Reposo);
                break;
            
            /* WAITS FOR AN ALARM, EITHER EXTERNAL INPUT OR TOUCH */
            case WAIT_ALARM:     
                LED_Alarma(OFF);

                TECLA     = FALSE;         
                PASS      = 0;  
                TIMEOUT   = FALSE;   
                
                while(checkCapsense() != 9);
                TECLA = 0x99;
                processAlarm();
                TECLA = 0;
                    
                for (i = 0 ; i  <2 ; i++)
                    {
                        Zumbador(ON);
                        LED_Alarma(ON);
                        CyDelay(50);
                        Zumbador(OFF);
                        LED_Alarma(OFF);
                        CyDelay(50);      
                    }
                CySysWdtSetMatch(CY_SYS_WDT_COUNTER0, WDT_COUNT0_MATCH_Accion);
                CyDelay(100);
                mode = SEND_ALARM;
                break;
                
            /* SENDS AN ALARM DEPENDING ON WHAT TRIGGERED IT */
            case SEND_ALARM:          
                i = 1;
                Enable_Botones();
                // EnoceanON();
                // Salgo cuando pulse *, cuando pase más del tiempo TIMEOUT o se introduzca más numeros de la peticion
                while(TECLA != 9 && TIMEOUT < EXPIRADO && i < LONG_PASS) 
                {
                    TECLA = checkCapsense();        // Leo el teclado
                    
                    if (blink == DESTELLO)
                    {   
                        X ^= 1;
                        LED_A_Write(X);
                        blink = 0;
                    }
                    blink++;
                    
                    if(TECLA >= 1 && TECLA <= 8)    // Si TECLA pertenece al rango [1,8]
                    {                     // Transmito la peticion en telegrama 4BS                                        
                        processAlarm();                        
                        
                        PASS += TECLA;              // Sumo TECLA a PASS
                        PASS *= 10;                 // Multiplico por 10 para qiue la próxima iteracion se agregue la siguiente TECLA
                        TIMEOUT = FALSE;            // Reseteo el TIME OUT
                        i++;
                        TECLA = 0;
                    }                    
                    TIMEOUT++;
                }                
                TECLA = 0x99;
                processAlarm();
                TECLA = 0;
                
                PASS /= 10;
                if (PASS == 12345)
                {
                    Relay_Write(1u);
                    CyDelay(100);
                    Relay_Write(0u);
                }
                
                LED_Alarma(OFF);
                Disable_Botones();
                EnoceanOFF();
                mode = SEND_RESET;
                break;
                    
            /* SENDS RESET TELEGRAM */
            case SEND_RESET:
                LED_Alarma(ON);                     //Enciende el LED de alarma y pita, para avisar de que la alarma ha sido enviada
                CyDelay(100);
                LED_Alarma(OFF);  

                for (i = 0;i<2;i++)
                    {
                        Zumbador(ON);
                        LED_Alarma(ON);
                        CyDelay(100);
                        Zumbador(OFF);
                        LED_Alarma(OFF);
                        CyDelay(150);      
                    }
                CySysWdtSetMatch(CY_SYS_WDT_COUNTER0, WDT_COUNT0_MATCH_Reposo);
                CySoftwareReset();
                mode = INIT_STATE;
                break;
        }
    }
}

void processAlarm(void)
{
    EnoceanON();
    CyDelay(50);
    BOTON[6] = TECLA;
    EnviarMensajeENO(BOTON);
    EnoceanOFF();
}

void Init(void)
{
    EnoceanOFF();

    IsrExternalINPUT_StartEx(IsrIN);                //Habilitamos interrupciones individuales de cordon, timer y bateria baja
    IsrWDT_StartEx(IsrWDT);
    IsrEnoUART_StartEx(IsrEnoUART);

    /* Set WDT counter 0 to generate interrupt on match */
    
	CySysWdtSetMode(CY_SYS_WDT_COUNTER0, CY_SYS_WDT_MODE_INT);
	CySysWdtSetMatch(CY_SYS_WDT_COUNTER0, WDT_COUNT0_MATCH_Reposo);
	CySysWdtSetClearOnMatch(CY_SYS_WDT_COUNTER0, 1u);
    
    /* Enable WDT counters 0 */
    
    CySysWdtEnable(CY_SYS_WDT_COUNTER0_MASK);       //habilita el WDT
}

void initCapsense(void)
{
    CapSense_Start();                               //Inicializamos CapSense   
    CapSense_InitializeAllBaselines();
    CapSense_EnableWidget(CapSense_BUTTON8__BTN);   // Inicializamos el boton 9 = *
}

void Enable_Botones(void)
{    
    CapSense_EnableWidget(CapSense_BUTTON0__BTN);   // Inicializamos el boton 1    
    CapSense_EnableWidget(CapSense_BUTTON1__BTN);   // Inicializamos el boton 2
    CapSense_EnableWidget(CapSense_BUTTON2__BTN);   // Inicializamos el boton 3
    CapSense_EnableWidget(CapSense_BUTTON3__BTN);   // Inicializamos el boton 4
    CapSense_EnableWidget(CapSense_BUTTON4__BTN);   // Inicializamos el boton 5
    CapSense_EnableWidget(CapSense_BUTTON5__BTN);   // Inicializamos el boton 6
    CapSense_EnableWidget(CapSense_BUTTON6__BTN);   // Inicializamos el boton 7
    CapSense_EnableWidget(CapSense_BUTTON7__BTN);   // Inicializamos el boton 8
    CapSense_UpdateEnabledBaselines();
}

void Disable_Botones(void)
{
    CapSense_DisableWidget(CapSense_BUTTON0__BTN);   // Inicializamos el boton 1  
    CapSense_DisableWidget(CapSense_BUTTON1__BTN);   // Inicializamos el boton 2
    CapSense_DisableWidget(CapSense_BUTTON2__BTN);   // Inicializamos el boton 3
    CapSense_DisableWidget(CapSense_BUTTON3__BTN);   // Inicializamos el boton 4
    CapSense_DisableWidget(CapSense_BUTTON4__BTN);   // Inicializamos el boton 5
    CapSense_DisableWidget(CapSense_BUTTON5__BTN);   // Inicializamos el boton 6
    CapSense_DisableWidget(CapSense_BUTTON6__BTN);   // Inicializamos el boton 7
    CapSense_DisableWidget(CapSense_BUTTON7__BTN);   // Inicializamos el boton 8
    CapSense_UpdateEnabledBaselines();
}
    

uint8 checkCapsense(void)
{    
    VALOR = 0;
    
    while (CapSense_CheckIsAnyWidgetActive())       // Si algun sensor sigue activo de la anterior iteracion, espero hasta...
        CapSense_UpdateEnabledBaselines();          // ... que se actualicen las bases y se reseteen
   
    CySysWdtEnable(CY_SYS_WDT_COUNTER0_MASK);       // habilita el WDT
    CapSense_Sleep();
    
    if (mode == SEND_ALARM && enoceanState == ENO_OFF)
        CySysPmSleep();                             // El microcontrolador se queda dormido
    else 
        CySysPmDeepSleep();                         // o muy dormido...
        
    CySysWdtDisable(CY_SYS_WDT_COUNTER0_MASK);      // deshabilita el WDT para que no salten interrupciones mientras está activo
    CapSense_Wakeup();                              // despierto el CapSense

    CapSense_ScanEnabledWidgets();                  // Escaneo todos los widgets habilitados
    while(CapSense_IsBusy())                        // Mientras estes escaneando, me quedo aqui hasta que termine
    {
        CySysPmSleep();                             // Si acaso tarda, me echo una siesta
    }    
    CapSense_UpdateEnabledBaselines();              // Actualizo las bases
    
    if(CapSense_CheckIsAnyWidgetActive())
    {
        if(mode == WAIT_ALARM && CapSense_CheckIsWidgetActive(CapSense_BUTTON8__BTN)) 
        {
            VALOR = 9;
            CapSense_SensorRaw[CapSense_BUTTON8__BTN] = 0;
        }
        
        if(mode == SEND_ALARM && VALOR == 0)           // Miro si hay algun sensor activo
        {
            if (CapSense_CheckIsWidgetActive(CapSense_BUTTON0__BTN)) 
            {
                while((CapSense_CheckIsWidgetActive(CapSense_BUTTON0__BTN)) && (count < 200))
                    count++;
                
                if (count >= 200)
                    VALOR = 1;  // Boton 1
                else 
                    count = 0;
            }
            else if (CapSense_CheckIsWidgetActive(CapSense_BUTTON1__BTN)) 
            {
                VALOR = 4;  // Boton 4
                //while(CapSense_CheckIsWidgetActive(CapSense_BUTTON1__BTN))
                //    CapSense_UpdateEnabledBaselines();
            }
            else if (CapSense_CheckIsWidgetActive(CapSense_BUTTON2__BTN)) 
            {
                VALOR = 7;  // Boton 7
                //while(CapSense_CheckIsWidgetActive(CapSense_BUTTON2__BTN))
                //    CapSense_UpdateEnabledBaselines();
            }
            else if (CapSense_CheckIsWidgetActive(CapSense_BUTTON3__BTN)) 
            {
                VALOR = 2;  // Boton 2
                //while(CapSense_CheckIsWidgetActive(CapSense_BUTTON3__BTN))
                //    CapSense_UpdateEnabledBaselines();
            }
            else if (CapSense_CheckIsWidgetActive(CapSense_BUTTON4__BTN)) 
            {
                VALOR = 5;  // Boton 5
                //while(CapSense_CheckIsWidgetActive(CapSense_BUTTON4__BTN))
                //    CapSense_UpdateEnabledBaselines();
            }
            else if (CapSense_CheckIsWidgetActive(CapSense_BUTTON5__BTN)) 
            {
                VALOR = 8;  // Boton 8
                //while(CapSense_CheckIsWidgetActive(CapSense_BUTTON5__BTN))
                //    CapSense_UpdateEnabledBaselines();
            }
            else if (CapSense_CheckIsWidgetActive(CapSense_BUTTON6__BTN)) 
            {
                VALOR = 3;  // Boton 3    
                //while(CapSense_CheckIsWidgetActive(CapSense_BUTTON6__BTN))
                //    CapSense_UpdateEnabledBaselines();
            }    
            else if (CapSense_CheckIsWidgetActive(CapSense_BUTTON7__BTN)) 
            {
                VALOR = 6;  // Boton 6  
                //while(CapSense_CheckIsWidgetActive(CapSense_BUTTON7__BTN))
                //   CapSense_UpdateEnabledBaselines();
            }      
        }
    }
    else VALOR = 0;
    
    if(VALOR >=1 && VALOR <=8)
    {
        LED_Bateria(ON);
        Zumbador(ON);
        CyDelay(100);
        LED_Bateria(OFF);
        Zumbador(OFF);
        while (CapSense_CheckIsAnyWidgetActive())       // Si algun sensor sigue activo espero hasta...
            CapSense_UpdateEnabledBaselines();          // ... que se actualicen las bases y se reseteen
    }
          
    return VALOR;
}

void CheckBattery(void)
{
    uint8 prevState;
    uint8 typeAlarm = 0;
    
    prevState = enoceanState;
    
    LED_Alarma(ON);
    
    EnoceanON();
    CyDelay(50);   
    
    CySysLvdEnable(CY_LVD_THRESHOLD_3_00_V);
    CyDelay(50);  
    
    if(CySysLvdGetInterruptSource()==CY_SYS_LVD_INT)
    {
        CySysLvdDisable();
        CySysLvdClearInterrupt();
        typeAlarm = AlarmaBatID;
    }
    else
        typeAlarm = KeepAliveID;
    
    LED_Alarma(OFF);
    
    CySysLvdDisable();
    
    if (typeAlarm == AlarmaBatID)
    {        
        LED_Bateria(ON);
        CyDelay(500);
        LED_Bateria(OFF);
        TipoMensajeENO(AlarmaBatID);
        lowBatt = TRUE;
    }
            
    else
        TipoMensajeENO(KeepAliveID);

        /* Rutina de mantenimiento */    
    if (chequeo == Mantenimiento && typeAlarm != AlarmaBatID)// Si hemos alcanzado el año de uso (1460 revisiones de bateria)
    {
        chequeo = Mantenimiento;                    // Me quedo en esta condicion de mantenimiento
        revision = TRUE;                            // Activar la revision anual
        TipoMensajeENO(ServiceID);                  // Alarma de mantenimiento para avisar de la revisión anual
    }
    else
            chequeo++;                              // Incrementando 
    
    if (prevState == ENO_OFF)
        EnoceanOFF();
}

CY_ISR(IsrIN)                                       // Interrupción del boton externo = Activar, Cancelar, Enviar segun estado
{   
    if (mode == WAIT_ALARM)
    {
        //flagExt = TRUE;                             //Activamos el flag de que el cordon ha sido activado
        //flagAlarm = TRUE;                           //Activamos el flag de que el cordon ha sido activado
    }
    CyDelay(200);                                   //Tiempo para evitar rebotes del interruptor
    
    ExternalINPUT_ClearInterrupt();                 //Limpiamos los flags de la interrupcion
    IsrExternalINPUT_ClearPending();
}

CY_ISR(IsrWDT)
{
    if(mode != SEND_ALARM)
    {
        globalLoops++;
    
        if (globalLoops > CheckBatteryTime)
        {
            globalLoops = 0;
            CheckBattery();
        }
        
        if (globalLoops % 100 == 0)                     
        {
            if (lowBatt)                                // Se ha activado el aviso de bateria baja
            {
                LED_Bateria(ON);
                CyDelay(100);
                LED_Bateria(OFF);
            }
        
            if (revision)                               // Se ha activado el aviso de revision anual y se activan ambos LEDs a la vez
            {
                LED_Bateria(ON);
                LED_Alarma(ON);
                CyDelay(100);
                LED_Bateria(OFF);
                LED_Alarma(OFF);
            }
        }
    }
    CySysWdtClearInterrupt(CY_SYS_WDT_COUNTER0_INT);
    IsrWDT_ClearPending(); //Limpia la interrupcion
}

CY_ISR(IsrEnoUART)                                  // Para esta version del teclado, solo se envia sin recepcion
{
    uint32 source = 0;
    struct EnoTelegram telegrama;
    
    telegrama = receiveEnocean();
    
    if (telegrama.RORG == RPS && telegrama.DATA[0] == 0xF3)
    {
        //confReceived = TRUE;
    }
    
    source = EnoUART_GetRxInterruptSourceMasked();
    EnoUART_ClearRxInterruptSource(source);
    IsrEnoUART_ClearPending();                      //Limpia la interrupcion
}


/* [] END OF FILE */
