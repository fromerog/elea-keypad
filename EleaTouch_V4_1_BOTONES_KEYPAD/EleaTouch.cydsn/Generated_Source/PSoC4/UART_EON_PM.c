/*******************************************************************************
* File Name: EnoUART_EON_PM.c
* Version 3.0
*
* Description:
*  This file provides the source code to the Power Management support for
*  the SCB Component.
*
* Note:
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "EnoUART_EON.h"
#include "EnoUART_EON_PVT.h"

#if(EnoUART_EON_SCB_MODE_I2C_INC)
    #include "EnoUART_EON_I2C_PVT.h"
#endif /* (EnoUART_EON_SCB_MODE_I2C_INC) */

#if(EnoUART_EON_SCB_MODE_EZI2C_INC)
    #include "EnoUART_EON_EZI2C_PVT.h"
#endif /* (EnoUART_EON_SCB_MODE_EZI2C_INC) */

#if(EnoUART_EON_SCB_MODE_SPI_INC || EnoUART_EON_SCB_MODE_EnoUART_INC)
    #include "EnoUART_EON_SPI_EnoUART_PVT.h"
#endif /* (EnoUART_EON_SCB_MODE_SPI_INC || EnoUART_EON_SCB_MODE_EnoUART_INC) */


/***************************************
*   Backup Structure declaration
***************************************/

#if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG || \
   (EnoUART_EON_SCB_MODE_I2C_CONST_CFG   && (!EnoUART_EON_I2C_WAKE_ENABLE_CONST))   || \
   (EnoUART_EON_SCB_MODE_EZI2C_CONST_CFG && (!EnoUART_EON_EZI2C_WAKE_ENABLE_CONST)) || \
   (EnoUART_EON_SCB_MODE_SPI_CONST_CFG   && (!EnoUART_EON_SPI_WAKE_ENABLE_CONST))   || \
   (EnoUART_EON_SCB_MODE_EnoUART_CONST_CFG  && (!EnoUART_EON_EnoUART_WAKE_ENABLE_CONST)))

    EnoUART_EON_BACKUP_STRUCT EnoUART_EON_backup =
    {
        0u, /* enableState */
    };
#endif


/*******************************************************************************
* Function Name: EnoUART_EON_Sleep
********************************************************************************
*
* Summary:
*  Prepares the component to enter Deep Sleep.
*  The "Enable wakeup from Sleep Mode" selection has an influence on
*  this function implementation.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void EnoUART_EON_Sleep(void)
{
#if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)

    if(EnoUART_EON_SCB_WAKE_ENABLE_CHECK)
    {
        if(EnoUART_EON_SCB_MODE_I2C_RUNTM_CFG)
        {
            EnoUART_EON_I2CSaveConfig();
        }
        else if(EnoUART_EON_SCB_MODE_EZI2C_RUNTM_CFG)
        {
            EnoUART_EON_EzI2CSaveConfig();
        }
    #if(!EnoUART_EON_CY_SCBIP_V1)
        else if(EnoUART_EON_SCB_MODE_SPI_RUNTM_CFG)
        {
            EnoUART_EON_SpiSaveConfig();
        }
        else if(EnoUART_EON_SCB_MODE_EnoUART_RUNTM_CFG)
        {
            EnoUART_EON_EnoUARTSaveConfig();
        }
    #endif /* (!EnoUART_EON_CY_SCBIP_V1) */
        else
        {
            /* Unknown mode */
        }
    }
    else
    {
        EnoUART_EON_backup.enableState = (uint8) EnoUART_EON_GET_CTRL_ENABLED;

        if(0u != EnoUART_EON_backup.enableState)
        {
            EnoUART_EON_Stop();
        }
    }

#else

    #if (EnoUART_EON_SCB_MODE_I2C_CONST_CFG && EnoUART_EON_I2C_WAKE_ENABLE_CONST)
        EnoUART_EON_I2CSaveConfig();

    #elif (EnoUART_EON_SCB_MODE_EZI2C_CONST_CFG && EnoUART_EON_EZI2C_WAKE_ENABLE_CONST)
        EnoUART_EON_EzI2CSaveConfig();

    #elif (EnoUART_EON_SCB_MODE_SPI_CONST_CFG && EnoUART_EON_SPI_WAKE_ENABLE_CONST)
        EnoUART_EON_SpiSaveConfig();

    #elif (EnoUART_EON_SCB_MODE_EnoUART_CONST_CFG && EnoUART_EON_EnoUART_WAKE_ENABLE_CONST)
        EnoUART_EON_EnoUARTSaveConfig();

    #else

        EnoUART_EON_backup.enableState = (uint8) EnoUART_EON_GET_CTRL_ENABLED;

        if(0u != EnoUART_EON_backup.enableState)
        {
            EnoUART_EON_Stop();
        }

    #endif /* defined (EnoUART_EON_SCB_MODE_I2C_CONST_CFG) && (EnoUART_EON_I2C_WAKE_ENABLE_CONST) */

#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: EnoUART_EON_Wakeup
********************************************************************************
*
* Summary:
*  Prepares the component for the Active mode operation after exiting
*  Deep Sleep. The "Enable wakeup from Sleep Mode" option has an influence
*  on this function implementation.
*  This function should not be called after exiting Sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void EnoUART_EON_Wakeup(void)
{
#if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)

    if(EnoUART_EON_SCB_WAKE_ENABLE_CHECK)
    {
        if(EnoUART_EON_SCB_MODE_I2C_RUNTM_CFG)
        {
            EnoUART_EON_I2CRestoreConfig();
        }
        else if(EnoUART_EON_SCB_MODE_EZI2C_RUNTM_CFG)
        {
            EnoUART_EON_EzI2CRestoreConfig();
        }
    #if(!EnoUART_EON_CY_SCBIP_V1)
        else if(EnoUART_EON_SCB_MODE_SPI_RUNTM_CFG)
        {
            EnoUART_EON_SpiRestoreConfig();
        }
        else if(EnoUART_EON_SCB_MODE_EnoUART_RUNTM_CFG)
        {
            EnoUART_EON_EnoUARTRestoreConfig();
        }
    #endif /* (!EnoUART_EON_CY_SCBIP_V1) */
        else
        {
            /* Unknown mode */
        }
    }
    else
    {
        if(0u != EnoUART_EON_backup.enableState)
        {
            EnoUART_EON_Enable();
        }
    }

#else

    #if (EnoUART_EON_SCB_MODE_I2C_CONST_CFG  && EnoUART_EON_I2C_WAKE_ENABLE_CONST)
        EnoUART_EON_I2CRestoreConfig();

    #elif (EnoUART_EON_SCB_MODE_EZI2C_CONST_CFG && EnoUART_EON_EZI2C_WAKE_ENABLE_CONST)
        EnoUART_EON_EzI2CRestoreConfig();

    #elif (EnoUART_EON_SCB_MODE_SPI_CONST_CFG && EnoUART_EON_SPI_WAKE_ENABLE_CONST)
        EnoUART_EON_SpiRestoreConfig();

    #elif (EnoUART_EON_SCB_MODE_EnoUART_CONST_CFG && EnoUART_EON_EnoUART_WAKE_ENABLE_CONST)
        EnoUART_EON_EnoUARTRestoreConfig();

    #else

        if(0u != EnoUART_EON_backup.enableState)
        {
            EnoUART_EON_Enable();
        }

    #endif /* (EnoUART_EON_I2C_WAKE_ENABLE_CONST) */

#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/* [] END OF FILE */
