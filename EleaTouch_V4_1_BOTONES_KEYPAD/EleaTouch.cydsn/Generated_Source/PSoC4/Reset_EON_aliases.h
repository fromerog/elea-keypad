/*******************************************************************************
* File Name: Reset_EON.h  
* Version 2.10
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Reset_EON_ALIASES_H) /* Pins Reset_EON_ALIASES_H */
#define CY_PINS_Reset_EON_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define Reset_EON_0		(Reset_EON__0__PC)
#define Reset_EON_0_PS		(Reset_EON__0__PS)
#define Reset_EON_0_PC		(Reset_EON__0__PC)
#define Reset_EON_0_DR		(Reset_EON__0__DR)
#define Reset_EON_0_SHIFT	(Reset_EON__0__SHIFT)


#endif /* End Pins Reset_EON_ALIASES_H */


/* [] END OF FILE */
