/*******************************************************************************
* File Name: SinUsar_17.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_SinUsar_17_H) /* Pins SinUsar_17_H */
#define CY_PINS_SinUsar_17_H

#include "cytypes.h"
#include "cyfitter.h"
#include "SinUsar_17_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} SinUsar_17_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   SinUsar_17_Read(void);
void    SinUsar_17_Write(uint8 value);
uint8   SinUsar_17_ReadDataReg(void);
#if defined(SinUsar_17__PC) || (CY_PSOC4_4200L) 
    void    SinUsar_17_SetDriveMode(uint8 mode);
#endif
void    SinUsar_17_SetInterruptMode(uint16 position, uint16 mode);
uint8   SinUsar_17_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void SinUsar_17_Sleep(void); 
void SinUsar_17_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(SinUsar_17__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define SinUsar_17_DRIVE_MODE_BITS        (3)
    #define SinUsar_17_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - SinUsar_17_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the SinUsar_17_SetDriveMode() function.
         *  @{
         */
        #define SinUsar_17_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define SinUsar_17_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define SinUsar_17_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define SinUsar_17_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define SinUsar_17_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define SinUsar_17_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define SinUsar_17_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define SinUsar_17_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define SinUsar_17_MASK               SinUsar_17__MASK
#define SinUsar_17_SHIFT              SinUsar_17__SHIFT
#define SinUsar_17_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in SinUsar_17_SetInterruptMode() function.
     *  @{
     */
        #define SinUsar_17_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define SinUsar_17_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define SinUsar_17_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define SinUsar_17_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(SinUsar_17__SIO)
    #define SinUsar_17_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(SinUsar_17__PC) && (CY_PSOC4_4200L)
    #define SinUsar_17_USBIO_ENABLE               ((uint32)0x80000000u)
    #define SinUsar_17_USBIO_DISABLE              ((uint32)(~SinUsar_17_USBIO_ENABLE))
    #define SinUsar_17_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define SinUsar_17_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define SinUsar_17_USBIO_ENTER_SLEEP          ((uint32)((1u << SinUsar_17_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << SinUsar_17_USBIO_SUSPEND_DEL_SHIFT)))
    #define SinUsar_17_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << SinUsar_17_USBIO_SUSPEND_SHIFT)))
    #define SinUsar_17_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << SinUsar_17_USBIO_SUSPEND_DEL_SHIFT)))
    #define SinUsar_17_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(SinUsar_17__PC)
    /* Port Configuration */
    #define SinUsar_17_PC                 (* (reg32 *) SinUsar_17__PC)
#endif
/* Pin State */
#define SinUsar_17_PS                     (* (reg32 *) SinUsar_17__PS)
/* Data Register */
#define SinUsar_17_DR                     (* (reg32 *) SinUsar_17__DR)
/* Input Buffer Disable Override */
#define SinUsar_17_INP_DIS                (* (reg32 *) SinUsar_17__PC2)

/* Interrupt configuration Registers */
#define SinUsar_17_INTCFG                 (* (reg32 *) SinUsar_17__INTCFG)
#define SinUsar_17_INTSTAT                (* (reg32 *) SinUsar_17__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define SinUsar_17_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(SinUsar_17__SIO)
    #define SinUsar_17_SIO_REG            (* (reg32 *) SinUsar_17__SIO)
#endif /* (SinUsar_17__SIO_CFG) */

/* USBIO registers */
#if !defined(SinUsar_17__PC) && (CY_PSOC4_4200L)
    #define SinUsar_17_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define SinUsar_17_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define SinUsar_17_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define SinUsar_17_DRIVE_MODE_SHIFT       (0x00u)
#define SinUsar_17_DRIVE_MODE_MASK        (0x07u << SinUsar_17_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins SinUsar_17_H */


/* [] END OF FILE */
