/*******************************************************************************
* File Name: SinUsar_29.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "SinUsar_29.h"

static SinUsar_29_BACKUP_STRUCT  SinUsar_29_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: SinUsar_29_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function must be called for SIO and USBIO
*  pins. It is not essential if using GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet SinUsar_29_SUT.c usage_SinUsar_29_Sleep_Wakeup
*******************************************************************************/
void SinUsar_29_Sleep(void)
{
    #if defined(SinUsar_29__PC)
        SinUsar_29_backup.pcState = SinUsar_29_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            SinUsar_29_backup.usbState = SinUsar_29_CR1_REG;
            SinUsar_29_USB_POWER_REG |= SinUsar_29_USBIO_ENTER_SLEEP;
            SinUsar_29_CR1_REG &= SinUsar_29_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(SinUsar_29__SIO)
        SinUsar_29_backup.sioState = SinUsar_29_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        SinUsar_29_SIO_REG &= (uint32)(~SinUsar_29_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: SinUsar_29_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep().
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to SinUsar_29_Sleep() for an example usage.
*******************************************************************************/
void SinUsar_29_Wakeup(void)
{
    #if defined(SinUsar_29__PC)
        SinUsar_29_PC = SinUsar_29_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            SinUsar_29_USB_POWER_REG &= SinUsar_29_USBIO_EXIT_SLEEP_PH1;
            SinUsar_29_CR1_REG = SinUsar_29_backup.usbState;
            SinUsar_29_USB_POWER_REG &= SinUsar_29_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(SinUsar_29__SIO)
        SinUsar_29_SIO_REG = SinUsar_29_backup.sioState;
    #endif
}


/* [] END OF FILE */
