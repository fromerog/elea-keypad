/***************************************************************************//**
* \file ENO_EnoUART_EnoUART.c
* \version 3.20
*
* \brief
*  This file provides the source code to the API for the SCB Component in
*  EnoUART mode.
*
* Note:
*
*******************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ENO_EnoUART_PVT.h"
#include "ENO_EnoUART_SPI_EnoUART_PVT.h"
#include "cyapicallbacks.h"

#if (ENO_EnoUART_EnoUART_WAKE_ENABLE_CONST && ENO_EnoUART_EnoUART_RX_WAKEUP_IRQ)
    /**
    * \addtogroup group_globals
    * \{
    */
    /** This global variable determines whether to enable Skip Start
    * functionality when ENO_EnoUART_Sleep() function is called:
    * 0 � disable, other values � enable. Default value is 1.
    * It is only available when Enable wakeup from Deep Sleep Mode is enabled.
    */
    uint8 ENO_EnoUART_skipStart = 1u;
    /** \} globals */
#endif /* (ENO_EnoUART_EnoUART_WAKE_ENABLE_CONST && ENO_EnoUART_EnoUART_RX_WAKEUP_IRQ) */

#if(ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)

    /***************************************
    *  Configuration Structure Initialization
    ***************************************/

    const ENO_EnoUART_EnoUART_INIT_STRUCT ENO_EnoUART_configEnoUART =
    {
        ENO_EnoUART_EnoUART_SUB_MODE,
        ENO_EnoUART_EnoUART_DIRECTION,
        ENO_EnoUART_EnoUART_DATA_BITS_NUM,
        ENO_EnoUART_EnoUART_PARITY_TYPE,
        ENO_EnoUART_EnoUART_STOP_BITS_NUM,
        ENO_EnoUART_EnoUART_OVS_FACTOR,
        ENO_EnoUART_EnoUART_IRDA_LOW_POWER,
        ENO_EnoUART_EnoUART_MEDIAN_FILTER_ENABLE,
        ENO_EnoUART_EnoUART_RETRY_ON_NACK,
        ENO_EnoUART_EnoUART_IRDA_POLARITY,
        ENO_EnoUART_EnoUART_DROP_ON_PARITY_ERR,
        ENO_EnoUART_EnoUART_DROP_ON_FRAME_ERR,
        ENO_EnoUART_EnoUART_WAKE_ENABLE,
        0u,
        NULL,
        0u,
        NULL,
        ENO_EnoUART_EnoUART_MP_MODE_ENABLE,
        ENO_EnoUART_EnoUART_MP_ACCEPT_ADDRESS,
        ENO_EnoUART_EnoUART_MP_RX_ADDRESS,
        ENO_EnoUART_EnoUART_MP_RX_ADDRESS_MASK,
        (uint32) ENO_EnoUART_SCB_IRQ_INTERNAL,
        ENO_EnoUART_EnoUART_INTR_RX_MASK,
        ENO_EnoUART_EnoUART_RX_TRIGGER_LEVEL,
        ENO_EnoUART_EnoUART_INTR_TX_MASK,
        ENO_EnoUART_EnoUART_TX_TRIGGER_LEVEL,
        (uint8) ENO_EnoUART_EnoUART_BYTE_MODE_ENABLE,
        (uint8) ENO_EnoUART_EnoUART_CTS_ENABLE,
        (uint8) ENO_EnoUART_EnoUART_CTS_POLARITY,
        (uint8) ENO_EnoUART_EnoUART_RTS_POLARITY,
        (uint8) ENO_EnoUART_EnoUART_RTS_FIFO_LEVEL
    };


    /*******************************************************************************
    * Function Name: ENO_EnoUART_EnoUARTInit
    ****************************************************************************//**
    *
    *  Configures the ENO_EnoUART for EnoUART operation.
    *
    *  This function is intended specifically to be used when the ENO_EnoUART
    *  configuration is set to �Unconfigured ENO_EnoUART� in the customizer.
    *  After initializing the ENO_EnoUART in EnoUART mode using this function,
    *  the component can be enabled using the ENO_EnoUART_Start() or
    * ENO_EnoUART_Enable() function.
    *  This function uses a pointer to a structure that provides the configuration
    *  settings. This structure contains the same information that would otherwise
    *  be provided by the customizer settings.
    *
    *  \param config: pointer to a structure that contains the following list of
    *   fields. These fields match the selections available in the customizer.
    *   Refer to the customizer for further description of the settings.
    *
    *******************************************************************************/
    void ENO_EnoUART_EnoUARTInit(const ENO_EnoUART_EnoUART_INIT_STRUCT *config)
    {
        uint32 pinsConfig;

        if (NULL == config)
        {
            CYASSERT(0u != 0u); /* Halt execution due to bad function parameter */
        }
        else
        {
            /* Get direction to configure EnoUART pins: TX, RX or TX+RX */
            pinsConfig  = config->direction;

        #if !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1)
            /* Add RTS and CTS pins to configure */
            pinsConfig |= (0u != config->rtsRxFifoLevel) ? (ENO_EnoUART_EnoUART_RTS_PIN_ENABLE) : (0u);
            pinsConfig |= (0u != config->enableCts)      ? (ENO_EnoUART_EnoUART_CTS_PIN_ENABLE) : (0u);
        #endif /* !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1) */

            /* Configure pins */
            ENO_EnoUART_SetPins(ENO_EnoUART_SCB_MODE_EnoUART, config->mode, pinsConfig);

            /* Store internal configuration */
            ENO_EnoUART_scbMode       = (uint8) ENO_EnoUART_SCB_MODE_EnoUART;
            ENO_EnoUART_scbEnableWake = (uint8) config->enableWake;
            ENO_EnoUART_scbEnableIntr = (uint8) config->enableInterrupt;

            /* Set RX direction internal variables */
            ENO_EnoUART_rxBuffer      =         config->rxBuffer;
            ENO_EnoUART_rxDataBits    = (uint8) config->dataBits;
            ENO_EnoUART_rxBufferSize  = (uint8) config->rxBufferSize;

            /* Set TX direction internal variables */
            ENO_EnoUART_txBuffer      =         config->txBuffer;
            ENO_EnoUART_txDataBits    = (uint8) config->dataBits;
            ENO_EnoUART_txBufferSize  = (uint8) config->txBufferSize;

            /* Configure EnoUART interface */
            if(ENO_EnoUART_EnoUART_MODE_IRDA == config->mode)
            {
                /* OVS settings: IrDA */
                ENO_EnoUART_CTRL_REG  = ((0u != config->enableIrdaLowPower) ?
                                                (ENO_EnoUART_EnoUART_GET_CTRL_OVS_IRDA_LP(config->oversample)) :
                                                (ENO_EnoUART_CTRL_OVS_IRDA_OVS16));
            }
            else
            {
                /* OVS settings: EnoUART and SmartCard */
                ENO_EnoUART_CTRL_REG  = ENO_EnoUART_GET_CTRL_OVS(config->oversample);
            }

            ENO_EnoUART_CTRL_REG     |= ENO_EnoUART_GET_CTRL_BYTE_MODE  (config->enableByteMode)      |
                                             ENO_EnoUART_GET_CTRL_ADDR_ACCEPT(config->multiprocAcceptAddr) |
                                             ENO_EnoUART_CTRL_EnoUART;

            /* Configure sub-mode: EnoUART, SmartCard or IrDA */
            ENO_EnoUART_EnoUART_CTRL_REG = ENO_EnoUART_GET_EnoUART_CTRL_MODE(config->mode);

            /* Configure RX direction */
            ENO_EnoUART_EnoUART_RX_CTRL_REG = ENO_EnoUART_GET_EnoUART_RX_CTRL_MODE(config->stopBits)              |
                                        ENO_EnoUART_GET_EnoUART_RX_CTRL_POLARITY(config->enableInvertedRx)          |
                                        ENO_EnoUART_GET_EnoUART_RX_CTRL_MP_MODE(config->enableMultiproc)            |
                                        ENO_EnoUART_GET_EnoUART_RX_CTRL_DROP_ON_PARITY_ERR(config->dropOnParityErr) |
                                        ENO_EnoUART_GET_EnoUART_RX_CTRL_DROP_ON_FRAME_ERR(config->dropOnFrameErr);

            if(ENO_EnoUART_EnoUART_PARITY_NONE != config->parity)
            {
               ENO_EnoUART_EnoUART_RX_CTRL_REG |= ENO_EnoUART_GET_EnoUART_RX_CTRL_PARITY(config->parity) |
                                                    ENO_EnoUART_EnoUART_RX_CTRL_PARITY_ENABLED;
            }

            ENO_EnoUART_RX_CTRL_REG      = ENO_EnoUART_GET_RX_CTRL_DATA_WIDTH(config->dataBits)       |
                                                ENO_EnoUART_GET_RX_CTRL_MEDIAN(config->enableMedianFilter) |
                                                ENO_EnoUART_GET_EnoUART_RX_CTRL_ENABLED(config->direction);

            ENO_EnoUART_RX_FIFO_CTRL_REG = ENO_EnoUART_GET_RX_FIFO_CTRL_TRIGGER_LEVEL(config->rxTriggerLevel);

            /* Configure MP address */
            ENO_EnoUART_RX_MATCH_REG     = ENO_EnoUART_GET_RX_MATCH_ADDR(config->multiprocAddr) |
                                                ENO_EnoUART_GET_RX_MATCH_MASK(config->multiprocAddrMask);

            /* Configure RX direction */
            ENO_EnoUART_EnoUART_TX_CTRL_REG = ENO_EnoUART_GET_EnoUART_TX_CTRL_MODE(config->stopBits) |
                                                ENO_EnoUART_GET_EnoUART_TX_CTRL_RETRY_NACK(config->enableRetryNack);

            if(ENO_EnoUART_EnoUART_PARITY_NONE != config->parity)
            {
               ENO_EnoUART_EnoUART_TX_CTRL_REG |= ENO_EnoUART_GET_EnoUART_TX_CTRL_PARITY(config->parity) |
                                                    ENO_EnoUART_EnoUART_TX_CTRL_PARITY_ENABLED;
            }

            ENO_EnoUART_TX_CTRL_REG      = ENO_EnoUART_GET_TX_CTRL_DATA_WIDTH(config->dataBits)    |
                                                ENO_EnoUART_GET_EnoUART_TX_CTRL_ENABLED(config->direction);

            ENO_EnoUART_TX_FIFO_CTRL_REG = ENO_EnoUART_GET_TX_FIFO_CTRL_TRIGGER_LEVEL(config->txTriggerLevel);

        #if !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1)
            ENO_EnoUART_EnoUART_FLOW_CTRL_REG = ENO_EnoUART_GET_EnoUART_FLOW_CTRL_CTS_ENABLE(config->enableCts) | \
                                            ENO_EnoUART_GET_EnoUART_FLOW_CTRL_CTS_POLARITY (config->ctsPolarity)  | \
                                            ENO_EnoUART_GET_EnoUART_FLOW_CTRL_RTS_POLARITY (config->rtsPolarity)  | \
                                            ENO_EnoUART_GET_EnoUART_FLOW_CTRL_TRIGGER_LEVEL(config->rtsRxFifoLevel);
        #endif /* !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1) */

            /* Configure interrupt with EnoUART handler but do not enable it */
            CyIntDisable    (ENO_EnoUART_ISR_NUMBER);
            CyIntSetPriority(ENO_EnoUART_ISR_NUMBER, ENO_EnoUART_ISR_PRIORITY);
            (void) CyIntSetVector(ENO_EnoUART_ISR_NUMBER, &ENO_EnoUART_SPI_EnoUART_ISR);

            /* Configure WAKE interrupt */
        #if(ENO_EnoUART_EnoUART_RX_WAKEUP_IRQ)
            CyIntDisable    (ENO_EnoUART_RX_WAKE_ISR_NUMBER);
            CyIntSetPriority(ENO_EnoUART_RX_WAKE_ISR_NUMBER, ENO_EnoUART_RX_WAKE_ISR_PRIORITY);
            (void) CyIntSetVector(ENO_EnoUART_RX_WAKE_ISR_NUMBER, &ENO_EnoUART_EnoUART_WAKEUP_ISR);
        #endif /* (ENO_EnoUART_EnoUART_RX_WAKEUP_IRQ) */

            /* Configure interrupt sources */
            ENO_EnoUART_INTR_I2C_EC_MASK_REG = ENO_EnoUART_NO_INTR_SOURCES;
            ENO_EnoUART_INTR_SPI_EC_MASK_REG = ENO_EnoUART_NO_INTR_SOURCES;
            ENO_EnoUART_INTR_SLAVE_MASK_REG  = ENO_EnoUART_NO_INTR_SOURCES;
            ENO_EnoUART_INTR_MASTER_MASK_REG = ENO_EnoUART_NO_INTR_SOURCES;
            ENO_EnoUART_INTR_RX_MASK_REG     = config->rxInterruptMask;
            ENO_EnoUART_INTR_TX_MASK_REG     = config->txInterruptMask;
        
            /* Configure TX interrupt sources to restore. */
            ENO_EnoUART_IntrTxMask = LO16(ENO_EnoUART_INTR_TX_MASK_REG);

            /* Clear RX buffer indexes */
            ENO_EnoUART_rxBufferHead     = 0u;
            ENO_EnoUART_rxBufferTail     = 0u;
            ENO_EnoUART_rxBufferOverflow = 0u;

            /* Clear TX buffer indexes */
            ENO_EnoUART_txBufferHead = 0u;
            ENO_EnoUART_txBufferTail = 0u;
        }
    }

#else

    /*******************************************************************************
    * Function Name: ENO_EnoUART_EnoUARTInit
    ****************************************************************************//**
    *
    *  Configures the SCB for the EnoUART operation.
    *
    *******************************************************************************/
    void ENO_EnoUART_EnoUARTInit(void)
    {
        /* Configure EnoUART interface */
        ENO_EnoUART_CTRL_REG = ENO_EnoUART_EnoUART_DEFAULT_CTRL;

        /* Configure sub-mode: EnoUART, SmartCard or IrDA */
        ENO_EnoUART_EnoUART_CTRL_REG = ENO_EnoUART_EnoUART_DEFAULT_EnoUART_CTRL;

        /* Configure RX direction */
        ENO_EnoUART_EnoUART_RX_CTRL_REG = ENO_EnoUART_EnoUART_DEFAULT_EnoUART_RX_CTRL;
        ENO_EnoUART_RX_CTRL_REG      = ENO_EnoUART_EnoUART_DEFAULT_RX_CTRL;
        ENO_EnoUART_RX_FIFO_CTRL_REG = ENO_EnoUART_EnoUART_DEFAULT_RX_FIFO_CTRL;
        ENO_EnoUART_RX_MATCH_REG     = ENO_EnoUART_EnoUART_DEFAULT_RX_MATCH_REG;

        /* Configure TX direction */
        ENO_EnoUART_EnoUART_TX_CTRL_REG = ENO_EnoUART_EnoUART_DEFAULT_EnoUART_TX_CTRL;
        ENO_EnoUART_TX_CTRL_REG      = ENO_EnoUART_EnoUART_DEFAULT_TX_CTRL;
        ENO_EnoUART_TX_FIFO_CTRL_REG = ENO_EnoUART_EnoUART_DEFAULT_TX_FIFO_CTRL;

    #if !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1)
        ENO_EnoUART_EnoUART_FLOW_CTRL_REG = ENO_EnoUART_EnoUART_DEFAULT_FLOW_CTRL;
    #endif /* !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1) */

        /* Configure interrupt with EnoUART handler but do not enable it */
    #if(ENO_EnoUART_SCB_IRQ_INTERNAL)
        CyIntDisable    (ENO_EnoUART_ISR_NUMBER);
        CyIntSetPriority(ENO_EnoUART_ISR_NUMBER, ENO_EnoUART_ISR_PRIORITY);
        (void) CyIntSetVector(ENO_EnoUART_ISR_NUMBER, &ENO_EnoUART_SPI_EnoUART_ISR);
    #endif /* (ENO_EnoUART_SCB_IRQ_INTERNAL) */

        /* Configure WAKE interrupt */
    #if(ENO_EnoUART_EnoUART_RX_WAKEUP_IRQ)
        CyIntDisable    (ENO_EnoUART_RX_WAKE_ISR_NUMBER);
        CyIntSetPriority(ENO_EnoUART_RX_WAKE_ISR_NUMBER, ENO_EnoUART_RX_WAKE_ISR_PRIORITY);
        (void) CyIntSetVector(ENO_EnoUART_RX_WAKE_ISR_NUMBER, &ENO_EnoUART_EnoUART_WAKEUP_ISR);
    #endif /* (ENO_EnoUART_EnoUART_RX_WAKEUP_IRQ) */

        /* Configure interrupt sources */
        ENO_EnoUART_INTR_I2C_EC_MASK_REG = ENO_EnoUART_EnoUART_DEFAULT_INTR_I2C_EC_MASK;
        ENO_EnoUART_INTR_SPI_EC_MASK_REG = ENO_EnoUART_EnoUART_DEFAULT_INTR_SPI_EC_MASK;
        ENO_EnoUART_INTR_SLAVE_MASK_REG  = ENO_EnoUART_EnoUART_DEFAULT_INTR_SLAVE_MASK;
        ENO_EnoUART_INTR_MASTER_MASK_REG = ENO_EnoUART_EnoUART_DEFAULT_INTR_MASTER_MASK;
        ENO_EnoUART_INTR_RX_MASK_REG     = ENO_EnoUART_EnoUART_DEFAULT_INTR_RX_MASK;
        ENO_EnoUART_INTR_TX_MASK_REG     = ENO_EnoUART_EnoUART_DEFAULT_INTR_TX_MASK;
    
        /* Configure TX interrupt sources to restore. */
        ENO_EnoUART_IntrTxMask = LO16(ENO_EnoUART_INTR_TX_MASK_REG);

    #if(ENO_EnoUART_INTERNAL_RX_SW_BUFFER_CONST)
        ENO_EnoUART_rxBufferHead     = 0u;
        ENO_EnoUART_rxBufferTail     = 0u;
        ENO_EnoUART_rxBufferOverflow = 0u;
    #endif /* (ENO_EnoUART_INTERNAL_RX_SW_BUFFER_CONST) */

    #if(ENO_EnoUART_INTERNAL_TX_SW_BUFFER_CONST)
        ENO_EnoUART_txBufferHead = 0u;
        ENO_EnoUART_txBufferTail = 0u;
    #endif /* (ENO_EnoUART_INTERNAL_TX_SW_BUFFER_CONST) */
    }
#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */


/*******************************************************************************
* Function Name: ENO_EnoUART_EnoUARTPostEnable
****************************************************************************//**
*
*  Restores HSIOM settings for the EnoUART output pins (TX and/or RTS) to be
*  controlled by the SCB EnoUART.
*
*******************************************************************************/
void ENO_EnoUART_EnoUARTPostEnable(void)
{
#if (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    #if (ENO_EnoUART_TX_SDA_MISO_PIN)
        if (ENO_EnoUART_CHECK_TX_SDA_MISO_PIN_USED)
        {
            /* Set SCB EnoUART to drive the output pin */
            ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_TX_SDA_MISO_HSIOM_REG, ENO_EnoUART_TX_SDA_MISO_HSIOM_MASK,
                                           ENO_EnoUART_TX_SDA_MISO_HSIOM_POS, ENO_EnoUART_TX_SDA_MISO_HSIOM_SEL_EnoUART);
        }
    #endif /* (ENO_EnoUART_TX_SDA_MISO_PIN_PIN) */

    #if !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1)
        #if (ENO_EnoUART_SS0_PIN)
            if (ENO_EnoUART_CHECK_SS0_PIN_USED)
            {
                /* Set SCB EnoUART to drive the output pin */
                ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_SS0_HSIOM_REG, ENO_EnoUART_SS0_HSIOM_MASK,
                                               ENO_EnoUART_SS0_HSIOM_POS, ENO_EnoUART_SS0_HSIOM_SEL_EnoUART);
            }
        #endif /* (ENO_EnoUART_SS0_PIN) */
    #endif /* !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1) */

#else
    #if (ENO_EnoUART_EnoUART_TX_PIN)
         /* Set SCB EnoUART to drive the output pin */
        ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_TX_HSIOM_REG, ENO_EnoUART_TX_HSIOM_MASK,
                                       ENO_EnoUART_TX_HSIOM_POS, ENO_EnoUART_TX_HSIOM_SEL_EnoUART);
    #endif /* (ENO_EnoUART_EnoUART_TX_PIN) */

    #if (ENO_EnoUART_EnoUART_RTS_PIN)
        /* Set SCB EnoUART to drive the output pin */
        ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_RTS_HSIOM_REG, ENO_EnoUART_RTS_HSIOM_MASK,
                                       ENO_EnoUART_RTS_HSIOM_POS, ENO_EnoUART_RTS_HSIOM_SEL_EnoUART);
    #endif /* (ENO_EnoUART_EnoUART_RTS_PIN) */
#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */

    /* Restore TX interrupt sources. */
    ENO_EnoUART_SetTxInterruptMode(ENO_EnoUART_IntrTxMask);
}


/*******************************************************************************
* Function Name: ENO_EnoUART_EnoUARTStop
****************************************************************************//**
*
*  Changes the HSIOM settings for the EnoUART output pins (TX and/or RTS) to keep
*  them inactive after the block is disabled. The output pins are controlled by
*  the GPIO data register. Also, the function disables the skip start feature
*  to not cause it to trigger after the component is enabled.
*
*******************************************************************************/
void ENO_EnoUART_EnoUARTStop(void)
{
#if(ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    #if (ENO_EnoUART_TX_SDA_MISO_PIN)
        if (ENO_EnoUART_CHECK_TX_SDA_MISO_PIN_USED)
        {
            /* Set GPIO to drive output pin */
            ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_TX_SDA_MISO_HSIOM_REG, ENO_EnoUART_TX_SDA_MISO_HSIOM_MASK,
                                           ENO_EnoUART_TX_SDA_MISO_HSIOM_POS, ENO_EnoUART_TX_SDA_MISO_HSIOM_SEL_GPIO);
        }
    #endif /* (ENO_EnoUART_TX_SDA_MISO_PIN_PIN) */

    #if !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1)
        #if (ENO_EnoUART_SS0_PIN)
            if (ENO_EnoUART_CHECK_SS0_PIN_USED)
            {
                /* Set output pin state after block is disabled */
                ENO_EnoUART_spi_ss0_Write(ENO_EnoUART_GET_EnoUART_RTS_INACTIVE);

                /* Set GPIO to drive output pin */
                ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_SS0_HSIOM_REG, ENO_EnoUART_SS0_HSIOM_MASK,
                                               ENO_EnoUART_SS0_HSIOM_POS, ENO_EnoUART_SS0_HSIOM_SEL_GPIO);
            }
        #endif /* (ENO_EnoUART_SS0_PIN) */
    #endif /* !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1) */

#else
    #if (ENO_EnoUART_EnoUART_TX_PIN)
        /* Set GPIO to drive output pin */
        ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_TX_HSIOM_REG, ENO_EnoUART_TX_HSIOM_MASK,
                                       ENO_EnoUART_TX_HSIOM_POS, ENO_EnoUART_TX_HSIOM_SEL_GPIO);
    #endif /* (ENO_EnoUART_EnoUART_TX_PIN) */

    #if (ENO_EnoUART_EnoUART_RTS_PIN)
        /* Set output pin state after block is disabled */
        ENO_EnoUART_rts_Write(ENO_EnoUART_GET_EnoUART_RTS_INACTIVE);

        /* Set GPIO to drive output pin */
        ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_RTS_HSIOM_REG, ENO_EnoUART_RTS_HSIOM_MASK,
                                       ENO_EnoUART_RTS_HSIOM_POS, ENO_EnoUART_RTS_HSIOM_SEL_GPIO);
    #endif /* (ENO_EnoUART_EnoUART_RTS_PIN) */

#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */

#if (ENO_EnoUART_EnoUART_WAKE_ENABLE_CONST)
    /* Disable skip start feature used for wakeup */
    ENO_EnoUART_EnoUART_RX_CTRL_REG &= (uint32) ~ENO_EnoUART_EnoUART_RX_CTRL_SKIP_START;
#endif /* (ENO_EnoUART_EnoUART_WAKE_ENABLE_CONST) */

    /* Store TX interrupt sources (exclude level triggered). */
    ENO_EnoUART_IntrTxMask = LO16(ENO_EnoUART_GetTxInterruptMode() & ENO_EnoUART_INTR_EnoUART_TX_RESTORE);
}


/*******************************************************************************
* Function Name: ENO_EnoUART_EnoUARTSetRxAddress
****************************************************************************//**
*
*  Sets the hardware detectable receiver address for the EnoUART in the
*  Multiprocessor mode.
*
*  \param address: Address for hardware address detection.
*
*******************************************************************************/
void ENO_EnoUART_EnoUARTSetRxAddress(uint32 address)
{
     uint32 matchReg;

    matchReg = ENO_EnoUART_RX_MATCH_REG;

    matchReg &= ((uint32) ~ENO_EnoUART_RX_MATCH_ADDR_MASK); /* Clear address bits */
    matchReg |= ((uint32)  (address & ENO_EnoUART_RX_MATCH_ADDR_MASK)); /* Set address  */

    ENO_EnoUART_RX_MATCH_REG = matchReg;
}


/*******************************************************************************
* Function Name: ENO_EnoUART_EnoUARTSetRxAddressMask
****************************************************************************//**
*
*  Sets the hardware address mask for the EnoUART in the Multiprocessor mode.
*
*  \param addressMask: Address mask.
*   - Bit value 0 � excludes bit from address comparison.
*   - Bit value 1 � the bit needs to match with the corresponding bit
*     of the address.
*
*******************************************************************************/
void ENO_EnoUART_EnoUARTSetRxAddressMask(uint32 addressMask)
{
    uint32 matchReg;

    matchReg = ENO_EnoUART_RX_MATCH_REG;

    matchReg &= ((uint32) ~ENO_EnoUART_RX_MATCH_MASK_MASK); /* Clear address mask bits */
    matchReg |= ((uint32) (addressMask << ENO_EnoUART_RX_MATCH_MASK_POS));

    ENO_EnoUART_RX_MATCH_REG = matchReg;
}


#if(ENO_EnoUART_EnoUART_RX_DIRECTION)
    /*******************************************************************************
    * Function Name: ENO_EnoUART_EnoUARTGetChar
    ****************************************************************************//**
    *
    *  Retrieves next data element from receive buffer.
    *  This function is designed for ASCII characters and returns a char where
    *  1 to 255 are valid characters and 0 indicates an error occurred or no data
    *  is present.
    *  - RX software buffer is disabled: Returns data element retrieved from RX
    *    FIFO.
    *  - RX software buffer is enabled: Returns data element from the software
    *    receive buffer.
    *
    *  \return
    *   Next data element from the receive buffer. ASCII character values from
    *   1 to 255 are valid. A returned zero signifies an error condition or no
    *   data available.
    *
    *  \sideeffect
    *   The errors bits may not correspond with reading characters due to
    *   RX FIFO and software buffer usage.
    *   RX software buffer is enabled: The internal software buffer overflow
    *   is not treated as an error condition.
    *   Check ENO_EnoUART_rxBufferOverflow to capture that error condition.
    *
    *******************************************************************************/
    uint32 ENO_EnoUART_EnoUARTGetChar(void)
    {
        uint32 rxData = 0u;

        /* Reads data only if there is data to read */
        if (0u != ENO_EnoUART_SpiEnoUARTGetRxBufferSize())
        {
            rxData = ENO_EnoUART_SpiEnoUARTReadRxData();
        }

        if (ENO_EnoUART_CHECK_INTR_RX(ENO_EnoUART_INTR_RX_ERR))
        {
            rxData = 0u; /* Error occurred: returns zero */
            ENO_EnoUART_ClearRxInterruptSource(ENO_EnoUART_INTR_RX_ERR);
        }

        return (rxData);
    }


    /*******************************************************************************
    * Function Name: ENO_EnoUART_EnoUARTGetByte
    ****************************************************************************//**
    *
    *  Retrieves the next data element from the receive buffer, returns the
    *  received byte and error condition.
    *   - The RX software buffer is disabled: returns the data element retrieved
    *     from the RX FIFO. Undefined data will be returned if the RX FIFO is
    *     empty.
    *   - The RX software buffer is enabled: returns data element from the
    *     software receive buffer.
    *
    *  \return
    *   Bits 7-0 contain the next data element from the receive buffer and
    *   other bits contain the error condition.
    *   - ENO_EnoUART_EnoUART_RX_OVERFLOW - Attempt to write to a full
    *     receiver FIFO.
    *   - ENO_EnoUART_EnoUART_RX_UNDERFLOW	Attempt to read from an empty
    *     receiver FIFO.
    *   - ENO_EnoUART_EnoUART_RX_FRAME_ERROR - EnoUART framing error detected.
    *   - ENO_EnoUART_EnoUART_RX_PARITY_ERROR - EnoUART parity error detected.
    *
    *  \sideeffect
    *   The errors bits may not correspond with reading characters due to
    *   RX FIFO and software buffer usage.
    *   RX software buffer is enabled: The internal software buffer overflow
    *   is not treated as an error condition.
    *   Check ENO_EnoUART_rxBufferOverflow to capture that error condition.
    *
    *******************************************************************************/
    uint32 ENO_EnoUART_EnoUARTGetByte(void)
    {
        uint32 rxData;
        uint32 tmpStatus;

        #if (ENO_EnoUART_CHECK_RX_SW_BUFFER)
        {
            ENO_EnoUART_DisableInt();
        }
        #endif

        if (0u != ENO_EnoUART_SpiEnoUARTGetRxBufferSize())
        {
            /* Enables interrupt to receive more bytes: at least one byte is in
            * buffer.
            */
            #if (ENO_EnoUART_CHECK_RX_SW_BUFFER)
            {
                ENO_EnoUART_EnableInt();
            }
            #endif

            /* Get received byte */
            rxData = ENO_EnoUART_SpiEnoUARTReadRxData();
        }
        else
        {
            /* Reads a byte directly from RX FIFO: underflow is raised in the
            * case of empty. Otherwise the first received byte will be read.
            */
            rxData = ENO_EnoUART_RX_FIFO_RD_REG;


            /* Enables interrupt to receive more bytes. */
            #if (ENO_EnoUART_CHECK_RX_SW_BUFFER)
            {

                /* The byte has been read from RX FIFO. Clear RX interrupt to
                * not involve interrupt handler when RX FIFO is empty.
                */
                ENO_EnoUART_ClearRxInterruptSource(ENO_EnoUART_INTR_RX_NOT_EMPTY);

                ENO_EnoUART_EnableInt();
            }
            #endif
        }

        /* Get and clear RX error mask */
        tmpStatus = (ENO_EnoUART_GetRxInterruptSource() & ENO_EnoUART_INTR_RX_ERR);
        ENO_EnoUART_ClearRxInterruptSource(ENO_EnoUART_INTR_RX_ERR);

        /* Puts together data and error status:
        * MP mode and accept address: 9th bit is set to notify mark.
        */
        rxData |= ((uint32) (tmpStatus << 8u));

        return (rxData);
    }


    #if !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1)
        /*******************************************************************************
        * Function Name: ENO_EnoUART_EnoUARTSetRtsPolarity
        ****************************************************************************//**
        *
        *  Sets active polarity of RTS output signal.
        *  Only available for PSoC 4100 BLE / PSoC 4200 BLE / PSoC 4100M / PSoC 4200M /
        *  PSoC 4200L / PSoC 4000S / PSoC 4100S / PSoC Analog Coprocessor devices.
        *
        *  \param polarity: Active polarity of RTS output signal.
        *   - ENO_EnoUART_EnoUART_RTS_ACTIVE_LOW  - RTS signal is active low.
        *   - ENO_EnoUART_EnoUART_RTS_ACTIVE_HIGH - RTS signal is active high.
        *
        *******************************************************************************/
        void ENO_EnoUART_EnoUARTSetRtsPolarity(uint32 polarity)
        {
            if(0u != polarity)
            {
                ENO_EnoUART_EnoUART_FLOW_CTRL_REG |= (uint32)  ENO_EnoUART_EnoUART_FLOW_CTRL_RTS_POLARITY;
            }
            else
            {
                ENO_EnoUART_EnoUART_FLOW_CTRL_REG &= (uint32) ~ENO_EnoUART_EnoUART_FLOW_CTRL_RTS_POLARITY;
            }
        }


        /*******************************************************************************
        * Function Name: ENO_EnoUART_EnoUARTSetRtsFifoLevel
        ****************************************************************************//**
        *
        *  Sets level in the RX FIFO for RTS signal activation.
        *  While the RX FIFO has fewer entries than the RX FIFO level the RTS signal
        *  remains active, otherwise the RTS signal becomes inactive.
        *  Only available for PSoC 4100 BLE / PSoC 4200 BLE / PSoC 4100M / PSoC 4200M /
        *  PSoC 4200L / PSoC 4000S / PSoC 4100S / PSoC Analog Coprocessor devices.
        *
        *  \param level: Level in the RX FIFO for RTS signal activation.
        *   The range of valid level values is between 0 and RX FIFO depth - 1.
        *   Setting level value to 0 disables RTS signal activation.
        *
        *******************************************************************************/
        void ENO_EnoUART_EnoUARTSetRtsFifoLevel(uint32 level)
        {
            uint32 EnoUARTFlowCtrl;

            EnoUARTFlowCtrl = ENO_EnoUART_EnoUART_FLOW_CTRL_REG;

            EnoUARTFlowCtrl &= ((uint32) ~ENO_EnoUART_EnoUART_FLOW_CTRL_TRIGGER_LEVEL_MASK); /* Clear level mask bits */
            EnoUARTFlowCtrl |= ((uint32) (ENO_EnoUART_EnoUART_FLOW_CTRL_TRIGGER_LEVEL_MASK & level));

            ENO_EnoUART_EnoUART_FLOW_CTRL_REG = EnoUARTFlowCtrl;
        }
    #endif /* !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1) */

#endif /* (ENO_EnoUART_EnoUART_RX_DIRECTION) */


#if(ENO_EnoUART_EnoUART_TX_DIRECTION)
    /*******************************************************************************
    * Function Name: ENO_EnoUART_EnoUARTPutString
    ****************************************************************************//**
    *
    *  Places a NULL terminated string in the transmit buffer to be sent at the
    *  next available bus time.
    *  This function is blocking and waits until there is a space available to put
    *  requested data in transmit buffer.
    *
    *  \param string: pointer to the null terminated string array to be placed in the
    *   transmit buffer.
    *
    *******************************************************************************/
    void ENO_EnoUART_EnoUARTPutString(const char8 string[])
    {
        uint32 bufIndex;

        bufIndex = 0u;

        /* Blocks the control flow until all data has been sent */
        while(string[bufIndex] != ((char8) 0))
        {
            ENO_EnoUART_EnoUARTPutChar((uint32) string[bufIndex]);
            bufIndex++;
        }
    }


    /*******************************************************************************
    * Function Name: ENO_EnoUART_EnoUARTPutCRLF
    ****************************************************************************//**
    *
    *  Places byte of data followed by a carriage return (0x0D) and line feed
    *  (0x0A) in the transmit buffer.
    *  This function is blocking and waits until there is a space available to put
    *  all requested data in transmit buffer.
    *
    *  \param txDataByte: the data to be transmitted.
    *
    *******************************************************************************/
    void ENO_EnoUART_EnoUARTPutCRLF(uint32 txDataByte)
    {
        ENO_EnoUART_EnoUARTPutChar(txDataByte);  /* Blocks control flow until all data has been sent */
        ENO_EnoUART_EnoUARTPutChar(0x0Du);       /* Blocks control flow until all data has been sent */
        ENO_EnoUART_EnoUARTPutChar(0x0Au);       /* Blocks control flow until all data has been sent */
    }


    #if !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1)
        /*******************************************************************************
        * Function Name: ENO_EnoUARTSCB_EnoUARTEnableCts
        ****************************************************************************//**
        *
        *  Enables usage of CTS input signal by the EnoUART transmitter.
        *  Only available for PSoC 4100 BLE / PSoC 4200 BLE / PSoC 4100M / PSoC 4200M /
        *  PSoC 4200L / PSoC 4000S / PSoC 4100S / PSoC Analog Coprocessor devices.
        *
        *******************************************************************************/
        void ENO_EnoUART_EnoUARTEnableCts(void)
        {
            ENO_EnoUART_EnoUART_FLOW_CTRL_REG |= (uint32)  ENO_EnoUART_EnoUART_FLOW_CTRL_CTS_ENABLE;
        }


        /*******************************************************************************
        * Function Name: ENO_EnoUART_EnoUARTDisableCts
        ****************************************************************************//**
        *
        *  Disables usage of CTS input signal by the EnoUART transmitter.
        *  Only available for PSoC 4100 BLE / PSoC 4200 BLE / PSoC 4100M / PSoC 4200M /
        *  PSoC 4200L / PSoC 4000S / PSoC 4100S / PSoC Analog Coprocessor devices.
        *
        *******************************************************************************/
        void ENO_EnoUART_EnoUARTDisableCts(void)
        {
            ENO_EnoUART_EnoUART_FLOW_CTRL_REG &= (uint32) ~ENO_EnoUART_EnoUART_FLOW_CTRL_CTS_ENABLE;
        }


        /*******************************************************************************
        * Function Name: ENO_EnoUART_EnoUARTSetCtsPolarity
        ****************************************************************************//**
        *
        *  Sets active polarity of CTS input signal.
        *  Only available for PSoC 4100 BLE / PSoC 4200 BLE / PSoC 4100M / PSoC 4200M /
        *  PSoC 4200L / PSoC 4000S / PSoC 4100S / PSoC Analog Coprocessor devices.
        *
        *  \param polarity: Active polarity of CTS output signal.
        *   - ENO_EnoUART_EnoUART_CTS_ACTIVE_LOW  - CTS signal is active low.
        *   - ENO_EnoUART_EnoUART_CTS_ACTIVE_HIGH - CTS signal is active high.
        *
        *******************************************************************************/
        void ENO_EnoUART_EnoUARTSetCtsPolarity(uint32 polarity)
        {
            if (0u != polarity)
            {
                ENO_EnoUART_EnoUART_FLOW_CTRL_REG |= (uint32)  ENO_EnoUART_EnoUART_FLOW_CTRL_CTS_POLARITY;
            }
            else
            {
                ENO_EnoUART_EnoUART_FLOW_CTRL_REG &= (uint32) ~ENO_EnoUART_EnoUART_FLOW_CTRL_CTS_POLARITY;
            }
        }
    #endif /* !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1) */

#endif /* (ENO_EnoUART_EnoUART_TX_DIRECTION) */


#if (ENO_EnoUART_EnoUART_WAKE_ENABLE_CONST)
    /*******************************************************************************
    * Function Name: ENO_EnoUART_EnoUARTSaveConfig
    ****************************************************************************//**
    *
    *  Clears and enables an interrupt on a falling edge of the Rx input. The GPIO
    *  interrupt does not track in the active mode, therefore requires to be 
    *  cleared by this API.
    *
    *******************************************************************************/
    void ENO_EnoUART_EnoUARTSaveConfig(void)
    {
    #if (ENO_EnoUART_EnoUART_RX_WAKEUP_IRQ)
        /* Set SKIP_START if requested (set by default). */
        if (0u != ENO_EnoUART_skipStart)
        {
            ENO_EnoUART_EnoUART_RX_CTRL_REG |= (uint32)  ENO_EnoUART_EnoUART_RX_CTRL_SKIP_START;
        }
        else
        {
            ENO_EnoUART_EnoUART_RX_CTRL_REG &= (uint32) ~ENO_EnoUART_EnoUART_RX_CTRL_SKIP_START;
        }
        
        /* Clear RX GPIO interrupt status and pending interrupt in NVIC because
        * falling edge on RX line occurs while EnoUART communication in active mode.
        * Enable interrupt: next interrupt trigger should wakeup device.
        */
        ENO_EnoUART_CLEAR_EnoUART_RX_WAKE_INTR;
        ENO_EnoUART_RxWakeClearPendingInt();
        ENO_EnoUART_RxWakeEnableInt();
    #endif /* (ENO_EnoUART_EnoUART_RX_WAKEUP_IRQ) */
    }


    /*******************************************************************************
    * Function Name: ENO_EnoUART_EnoUARTRestoreConfig
    ****************************************************************************//**
    *
    *  Disables the RX GPIO interrupt. Until this function is called the interrupt
    *  remains active and triggers on every falling edge of the EnoUART RX line.
    *
    *******************************************************************************/
    void ENO_EnoUART_EnoUARTRestoreConfig(void)
    {
    #if (ENO_EnoUART_EnoUART_RX_WAKEUP_IRQ)
        /* Disable interrupt: no more triggers in active mode */
        ENO_EnoUART_RxWakeDisableInt();
    #endif /* (ENO_EnoUART_EnoUART_RX_WAKEUP_IRQ) */
    }


    #if (ENO_EnoUART_EnoUART_RX_WAKEUP_IRQ)
        /*******************************************************************************
        * Function Name: ENO_EnoUART_EnoUART_WAKEUP_ISR
        ****************************************************************************//**
        *
        *  Handles the Interrupt Service Routine for the SCB EnoUART mode GPIO wakeup
        *  event. This event is configured to trigger on a falling edge of the RX line.
        *
        *******************************************************************************/
        CY_ISR(ENO_EnoUART_EnoUART_WAKEUP_ISR)
        {
        #ifdef ENO_EnoUART_EnoUART_WAKEUP_ISR_ENTRY_CALLBACK
            ENO_EnoUART_EnoUART_WAKEUP_ISR_EntryCallback();
        #endif /* ENO_EnoUART_EnoUART_WAKEUP_ISR_ENTRY_CALLBACK */

            ENO_EnoUART_CLEAR_EnoUART_RX_WAKE_INTR;

        #ifdef ENO_EnoUART_EnoUART_WAKEUP_ISR_EXIT_CALLBACK
            ENO_EnoUART_EnoUART_WAKEUP_ISR_ExitCallback();
        #endif /* ENO_EnoUART_EnoUART_WAKEUP_ISR_EXIT_CALLBACK */
        }
    #endif /* (ENO_EnoUART_EnoUART_RX_WAKEUP_IRQ) */
#endif /* (ENO_EnoUART_EnoUART_RX_WAKEUP_IRQ) */


/* [] END OF FILE */
