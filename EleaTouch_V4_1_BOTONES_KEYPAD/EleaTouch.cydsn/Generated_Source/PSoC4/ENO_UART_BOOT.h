/***************************************************************************//**
* \file ENO_EnoUART_BOOT.h
* \version 3.20
*
* \brief
*  This file provides constants and parameter values of the bootloader
*  communication APIs for the SCB Component.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2014-2016, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_BOOT_ENO_EnoUART_H)
#define CY_SCB_BOOT_ENO_EnoUART_H

#include "ENO_EnoUART_PVT.h"

#if (ENO_EnoUART_SCB_MODE_I2C_INC)
    #include "ENO_EnoUART_I2C.h"
#endif /* (ENO_EnoUART_SCB_MODE_I2C_INC) */

#if (ENO_EnoUART_SCB_MODE_EZI2C_INC)
    #include "ENO_EnoUART_EZI2C.h"
#endif /* (ENO_EnoUART_SCB_MODE_EZI2C_INC) */

#if (ENO_EnoUART_SCB_MODE_SPI_INC || ENO_EnoUART_SCB_MODE_EnoUART_INC)
    #include "ENO_EnoUART_SPI_EnoUART.h"
#endif /* (ENO_EnoUART_SCB_MODE_SPI_INC || ENO_EnoUART_SCB_MODE_EnoUART_INC) */


/***************************************
*  Conditional Compilation Parameters
****************************************/

/* Bootloader communication interface enable */
#define ENO_EnoUART_BTLDR_COMM_ENABLED ((CYDEV_BOOTLOADER_IO_COMP == CyBtldr_ENO_EnoUART) || \
                                             (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_Custom_Interface))

/* Enable I2C bootloader communication */
#if (ENO_EnoUART_SCB_MODE_I2C_INC)
    #define ENO_EnoUART_I2C_BTLDR_COMM_ENABLED     (ENO_EnoUART_BTLDR_COMM_ENABLED && \
                                                            (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             ENO_EnoUART_I2C_SLAVE_CONST))
#else
     #define ENO_EnoUART_I2C_BTLDR_COMM_ENABLED    (0u)
#endif /* (ENO_EnoUART_SCB_MODE_I2C_INC) */

/* EZI2C does not support bootloader communication. Provide empty APIs */
#if (ENO_EnoUART_SCB_MODE_EZI2C_INC)
    #define ENO_EnoUART_EZI2C_BTLDR_COMM_ENABLED   (ENO_EnoUART_BTLDR_COMM_ENABLED && \
                                                         ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
#else
    #define ENO_EnoUART_EZI2C_BTLDR_COMM_ENABLED   (0u)
#endif /* (ENO_EnoUART_EZI2C_BTLDR_COMM_ENABLED) */

/* Enable SPI bootloader communication */
#if (ENO_EnoUART_SCB_MODE_SPI_INC)
    #define ENO_EnoUART_SPI_BTLDR_COMM_ENABLED     (ENO_EnoUART_BTLDR_COMM_ENABLED && \
                                                            (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             ENO_EnoUART_SPI_SLAVE_CONST))
#else
        #define ENO_EnoUART_SPI_BTLDR_COMM_ENABLED (0u)
#endif /* (ENO_EnoUART_SPI_BTLDR_COMM_ENABLED) */

/* Enable EnoUART bootloader communication */
#if (ENO_EnoUART_SCB_MODE_EnoUART_INC)
       #define ENO_EnoUART_EnoUART_BTLDR_COMM_ENABLED    (ENO_EnoUART_BTLDR_COMM_ENABLED && \
                                                            (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             (ENO_EnoUART_EnoUART_RX_DIRECTION && \
                                                              ENO_EnoUART_EnoUART_TX_DIRECTION)))
#else
     #define ENO_EnoUART_EnoUART_BTLDR_COMM_ENABLED   (0u)
#endif /* (ENO_EnoUART_EnoUART_BTLDR_COMM_ENABLED) */

/* Enable bootloader communication */
#define ENO_EnoUART_BTLDR_COMM_MODE_ENABLED    (ENO_EnoUART_I2C_BTLDR_COMM_ENABLED   || \
                                                     ENO_EnoUART_SPI_BTLDR_COMM_ENABLED   || \
                                                     ENO_EnoUART_EZI2C_BTLDR_COMM_ENABLED || \
                                                     ENO_EnoUART_EnoUART_BTLDR_COMM_ENABLED)


/***************************************
*        Function Prototypes
***************************************/

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (ENO_EnoUART_I2C_BTLDR_COMM_ENABLED)
    /* I2C Bootloader physical layer functions */
    void ENO_EnoUART_I2CCyBtldrCommStart(void);
    void ENO_EnoUART_I2CCyBtldrCommStop (void);
    void ENO_EnoUART_I2CCyBtldrCommReset(void);
    cystatus ENO_EnoUART_I2CCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus ENO_EnoUART_I2CCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map I2C specific bootloader communication APIs to SCB specific APIs */
    #if (ENO_EnoUART_SCB_MODE_I2C_CONST_CFG)
        #define ENO_EnoUART_CyBtldrCommStart   ENO_EnoUART_I2CCyBtldrCommStart
        #define ENO_EnoUART_CyBtldrCommStop    ENO_EnoUART_I2CCyBtldrCommStop
        #define ENO_EnoUART_CyBtldrCommReset   ENO_EnoUART_I2CCyBtldrCommReset
        #define ENO_EnoUART_CyBtldrCommRead    ENO_EnoUART_I2CCyBtldrCommRead
        #define ENO_EnoUART_CyBtldrCommWrite   ENO_EnoUART_I2CCyBtldrCommWrite
    #endif /* (ENO_EnoUART_SCB_MODE_I2C_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (ENO_EnoUART_I2C_BTLDR_COMM_ENABLED) */


#if defined(CYDEV_BOOTLOADER_IO_COMP) && (ENO_EnoUART_EZI2C_BTLDR_COMM_ENABLED)
    /* Bootloader physical layer functions */
    void ENO_EnoUART_EzI2CCyBtldrCommStart(void);
    void ENO_EnoUART_EzI2CCyBtldrCommStop (void);
    void ENO_EnoUART_EzI2CCyBtldrCommReset(void);
    cystatus ENO_EnoUART_EzI2CCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus ENO_EnoUART_EzI2CCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map EZI2C specific bootloader communication APIs to SCB specific APIs */
    #if (ENO_EnoUART_SCB_MODE_EZI2C_CONST_CFG)
        #define ENO_EnoUART_CyBtldrCommStart   ENO_EnoUART_EzI2CCyBtldrCommStart
        #define ENO_EnoUART_CyBtldrCommStop    ENO_EnoUART_EzI2CCyBtldrCommStop
        #define ENO_EnoUART_CyBtldrCommReset   ENO_EnoUART_EzI2CCyBtldrCommReset
        #define ENO_EnoUART_CyBtldrCommRead    ENO_EnoUART_EzI2CCyBtldrCommRead
        #define ENO_EnoUART_CyBtldrCommWrite   ENO_EnoUART_EzI2CCyBtldrCommWrite
    #endif /* (ENO_EnoUART_SCB_MODE_EZI2C_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (ENO_EnoUART_EZI2C_BTLDR_COMM_ENABLED) */

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (ENO_EnoUART_SPI_BTLDR_COMM_ENABLED)
    /* SPI Bootloader physical layer functions */
    void ENO_EnoUART_SpiCyBtldrCommStart(void);
    void ENO_EnoUART_SpiCyBtldrCommStop (void);
    void ENO_EnoUART_SpiCyBtldrCommReset(void);
    cystatus ENO_EnoUART_SpiCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus ENO_EnoUART_SpiCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map SPI specific bootloader communication APIs to SCB specific APIs */
    #if (ENO_EnoUART_SCB_MODE_SPI_CONST_CFG)
        #define ENO_EnoUART_CyBtldrCommStart   ENO_EnoUART_SpiCyBtldrCommStart
        #define ENO_EnoUART_CyBtldrCommStop    ENO_EnoUART_SpiCyBtldrCommStop
        #define ENO_EnoUART_CyBtldrCommReset   ENO_EnoUART_SpiCyBtldrCommReset
        #define ENO_EnoUART_CyBtldrCommRead    ENO_EnoUART_SpiCyBtldrCommRead
        #define ENO_EnoUART_CyBtldrCommWrite   ENO_EnoUART_SpiCyBtldrCommWrite
    #endif /* (ENO_EnoUART_SCB_MODE_SPI_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (ENO_EnoUART_SPI_BTLDR_COMM_ENABLED) */

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (ENO_EnoUART_EnoUART_BTLDR_COMM_ENABLED)
    /* EnoUART Bootloader physical layer functions */
    void ENO_EnoUART_EnoUARTCyBtldrCommStart(void);
    void ENO_EnoUART_EnoUARTCyBtldrCommStop (void);
    void ENO_EnoUART_EnoUARTCyBtldrCommReset(void);
    cystatus ENO_EnoUART_EnoUARTCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus ENO_EnoUART_EnoUARTCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map EnoUART specific bootloader communication APIs to SCB specific APIs */
    #if (ENO_EnoUART_SCB_MODE_EnoUART_CONST_CFG)
        #define ENO_EnoUART_CyBtldrCommStart   ENO_EnoUART_EnoUARTCyBtldrCommStart
        #define ENO_EnoUART_CyBtldrCommStop    ENO_EnoUART_EnoUARTCyBtldrCommStop
        #define ENO_EnoUART_CyBtldrCommReset   ENO_EnoUART_EnoUARTCyBtldrCommReset
        #define ENO_EnoUART_CyBtldrCommRead    ENO_EnoUART_EnoUARTCyBtldrCommRead
        #define ENO_EnoUART_CyBtldrCommWrite   ENO_EnoUART_EnoUARTCyBtldrCommWrite
    #endif /* (ENO_EnoUART_SCB_MODE_EnoUART_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (ENO_EnoUART_EnoUART_BTLDR_COMM_ENABLED) */

/**
* \addtogroup group_bootloader
* @{
*/

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (ENO_EnoUART_BTLDR_COMM_ENABLED)
    #if (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
        /* Bootloader physical layer functions */
        void ENO_EnoUART_CyBtldrCommStart(void);
        void ENO_EnoUART_CyBtldrCommStop (void);
        void ENO_EnoUART_CyBtldrCommReset(void);
        cystatus ENO_EnoUART_CyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
        cystatus ENO_EnoUART_CyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    #endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */

    /* Map SCB specific bootloader communication APIs to common APIs */
    #if (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_ENO_EnoUART)
        #define CyBtldrCommStart    ENO_EnoUART_CyBtldrCommStart
        #define CyBtldrCommStop     ENO_EnoUART_CyBtldrCommStop
        #define CyBtldrCommReset    ENO_EnoUART_CyBtldrCommReset
        #define CyBtldrCommWrite    ENO_EnoUART_CyBtldrCommWrite
        #define CyBtldrCommRead     ENO_EnoUART_CyBtldrCommRead
    #endif /* (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_ENO_EnoUART) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (ENO_EnoUART_BTLDR_COMM_ENABLED) */

/** @} group_bootloader */

/***************************************
*           API Constants
***************************************/

/* Timeout unit in milliseconds */
#define ENO_EnoUART_WAIT_1_MS  (1u)

/* Return number of bytes to copy into bootloader buffer */
#define ENO_EnoUART_BYTES_TO_COPY(actBufSize, bufSize) \
                            ( ((uint32)(actBufSize) < (uint32)(bufSize)) ? \
                                ((uint32) (actBufSize)) : ((uint32) (bufSize)) )

/* Size of Read/Write buffers for I2C bootloader  */
#define ENO_EnoUART_I2C_BTLDR_SIZEOF_READ_BUFFER   (64u)
#define ENO_EnoUART_I2C_BTLDR_SIZEOF_WRITE_BUFFER  (64u)

/* Byte to byte time interval: calculated basing on current component
* data rate configuration, can be defined in project if required.
*/
#ifndef ENO_EnoUART_SPI_BYTE_TO_BYTE
    #define ENO_EnoUART_SPI_BYTE_TO_BYTE   (160u)
#endif

/* Byte to byte time interval: calculated basing on current component
* baud rate configuration, can be defined in the project if required.
*/
#ifndef ENO_EnoUART_EnoUART_BYTE_TO_BYTE
    #define ENO_EnoUART_EnoUART_BYTE_TO_BYTE  (350u)
#endif /* ENO_EnoUART_EnoUART_BYTE_TO_BYTE */

#endif /* (CY_SCB_BOOT_ENO_EnoUART_H) */


/* [] END OF FILE */
