/***************************************************************************//**
* \file ENO_EnoUART_SPI_EnoUART.c
* \version 3.20
*
* \brief
*  This file provides the source code to the API for the SCB Component in
*  SPI and EnoUART modes.
*
* Note:
*
*******************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ENO_EnoUART_PVT.h"
#include "ENO_EnoUART_SPI_EnoUART_PVT.h"

/***************************************
*        SPI/EnoUART Private Vars
***************************************/

#if(ENO_EnoUART_INTERNAL_RX_SW_BUFFER_CONST)
    /* Start index to put data into the software receive buffer.*/
    volatile uint32 ENO_EnoUART_rxBufferHead;
    /* Start index to get data from the software receive buffer.*/
    volatile uint32 ENO_EnoUART_rxBufferTail;
    /**
    * \addtogroup group_globals
    * \{
    */
    /** Sets when internal software receive buffer overflow
    *  was occurred.
    */
    volatile uint8  ENO_EnoUART_rxBufferOverflow;
    /** \} globals */
#endif /* (ENO_EnoUART_INTERNAL_RX_SW_BUFFER_CONST) */

#if(ENO_EnoUART_INTERNAL_TX_SW_BUFFER_CONST)
    /* Start index to put data into the software transmit buffer.*/
    volatile uint32 ENO_EnoUART_txBufferHead;
    /* Start index to get data from the software transmit buffer.*/
    volatile uint32 ENO_EnoUART_txBufferTail;
#endif /* (ENO_EnoUART_INTERNAL_TX_SW_BUFFER_CONST) */

#if(ENO_EnoUART_INTERNAL_RX_SW_BUFFER)
    /* Add one element to the buffer to receive full packet. One byte in receive buffer is always empty */
    volatile uint8 ENO_EnoUART_rxBufferInternal[ENO_EnoUART_INTERNAL_RX_BUFFER_SIZE];
#endif /* (ENO_EnoUART_INTERNAL_RX_SW_BUFFER) */

#if(ENO_EnoUART_INTERNAL_TX_SW_BUFFER)
    volatile uint8 ENO_EnoUART_txBufferInternal[ENO_EnoUART_TX_BUFFER_SIZE];
#endif /* (ENO_EnoUART_INTERNAL_TX_SW_BUFFER) */


#if(ENO_EnoUART_RX_DIRECTION)
    /*******************************************************************************
    * Function Name: ENO_EnoUART_SpiEnoUARTReadRxData
    ****************************************************************************//**
    *
    *  Retrieves the next data element from the receive buffer.
    *   - RX software buffer is disabled: Returns data element retrieved from
    *     RX FIFO. Undefined data will be returned if the RX FIFO is empty.
    *   - RX software buffer is enabled: Returns data element from the software
    *     receive buffer. Zero value is returned if the software receive buffer
    *     is empty.
    *
    * \return
    *  Next data element from the receive buffer. 
    *  The amount of data bits to be received depends on RX data bits selection 
    *  (the data bit counting starts from LSB of return value).
    *
    * \globalvars
    *  ENO_EnoUART_rxBufferHead - the start index to put data into the 
    *  software receive buffer.
    *  ENO_EnoUART_rxBufferTail - the start index to get data from the 
    *  software receive buffer.
    *
    *******************************************************************************/
    uint32 ENO_EnoUART_SpiEnoUARTReadRxData(void)
    {
        uint32 rxData = 0u;

    #if (ENO_EnoUART_INTERNAL_RX_SW_BUFFER_CONST)
        uint32 locTail;
    #endif /* (ENO_EnoUART_INTERNAL_RX_SW_BUFFER_CONST) */

        #if (ENO_EnoUART_CHECK_RX_SW_BUFFER)
        {
            if (ENO_EnoUART_rxBufferHead != ENO_EnoUART_rxBufferTail)
            {
                /* There is data in RX software buffer */

                /* Calculate index to read from */
                locTail = (ENO_EnoUART_rxBufferTail + 1u);

                if (ENO_EnoUART_INTERNAL_RX_BUFFER_SIZE == locTail)
                {
                    locTail = 0u;
                }

                /* Get data from RX software buffer */
                rxData = ENO_EnoUART_GetWordFromRxBuffer(locTail);

                /* Change index in the buffer */
                ENO_EnoUART_rxBufferTail = locTail;

                #if (ENO_EnoUART_CHECK_EnoUART_RTS_CONTROL_FLOW)
                {
                    /* Check if RX Not Empty is disabled in the interrupt */
                    if (0u == (ENO_EnoUART_INTR_RX_MASK_REG & ENO_EnoUART_INTR_RX_NOT_EMPTY))
                    {
                        /* Enable RX Not Empty interrupt source to continue
                        * receiving data into software buffer.
                        */
                        ENO_EnoUART_INTR_RX_MASK_REG |= ENO_EnoUART_INTR_RX_NOT_EMPTY;
                    }
                }
                #endif

            }
        }
        #else
        {
            /* Read data from RX FIFO */
            rxData = ENO_EnoUART_RX_FIFO_RD_REG;
        }
        #endif

        return (rxData);
    }


    /*******************************************************************************
    * Function Name: ENO_EnoUART_SpiEnoUARTGetRxBufferSize
    ****************************************************************************//**
    *
    *  Returns the number of received data elements in the receive buffer.
    *   - RX software buffer disabled: returns the number of used entries in
    *     RX FIFO.
    *   - RX software buffer enabled: returns the number of elements which were
    *     placed in the receive buffer. This does not include the hardware RX FIFO.
    *
    * \return
    *  Number of received data elements.
    *
    * \globalvars
    *  ENO_EnoUART_rxBufferHead - the start index to put data into the 
    *  software receive buffer.
    *  ENO_EnoUART_rxBufferTail - the start index to get data from the 
    *  software receive buffer.
    *
    *******************************************************************************/
    uint32 ENO_EnoUART_SpiEnoUARTGetRxBufferSize(void)
    {
        uint32 size;
    #if (ENO_EnoUART_INTERNAL_RX_SW_BUFFER_CONST)
        uint32 locHead;
    #endif /* (ENO_EnoUART_INTERNAL_RX_SW_BUFFER_CONST) */

        #if (ENO_EnoUART_CHECK_RX_SW_BUFFER)
        {
            locHead = ENO_EnoUART_rxBufferHead;

            if(locHead >= ENO_EnoUART_rxBufferTail)
            {
                size = (locHead - ENO_EnoUART_rxBufferTail);
            }
            else
            {
                size = (locHead + (ENO_EnoUART_INTERNAL_RX_BUFFER_SIZE - ENO_EnoUART_rxBufferTail));
            }
        }
        #else
        {
            size = ENO_EnoUART_GET_RX_FIFO_ENTRIES;
        }
        #endif

        return (size);
    }


    /*******************************************************************************
    * Function Name: ENO_EnoUART_SpiEnoUARTClearRxBuffer
    ****************************************************************************//**
    *
    *  Clears the receive buffer and RX FIFO.
    *
    * \globalvars
    *  ENO_EnoUART_rxBufferHead - the start index to put data into the 
    *  software receive buffer.
    *  ENO_EnoUART_rxBufferTail - the start index to get data from the 
    *  software receive buffer.
    *
    *******************************************************************************/
    void ENO_EnoUART_SpiEnoUARTClearRxBuffer(void)
    {
        #if (ENO_EnoUART_CHECK_RX_SW_BUFFER)
        {
            /* Lock from component interruption */
            ENO_EnoUART_DisableInt();

            /* Flush RX software buffer */
            ENO_EnoUART_rxBufferHead = ENO_EnoUART_rxBufferTail;
            ENO_EnoUART_rxBufferOverflow = 0u;

            ENO_EnoUART_CLEAR_RX_FIFO;
            ENO_EnoUART_ClearRxInterruptSource(ENO_EnoUART_INTR_RX_ALL);

            #if (ENO_EnoUART_CHECK_EnoUART_RTS_CONTROL_FLOW)
            {
                /* Enable RX Not Empty interrupt source to continue receiving
                * data into software buffer.
                */
                ENO_EnoUART_INTR_RX_MASK_REG |= ENO_EnoUART_INTR_RX_NOT_EMPTY;
            }
            #endif
            
            /* Release lock */
            ENO_EnoUART_EnableInt();
        }
        #else
        {
            ENO_EnoUART_CLEAR_RX_FIFO;
        }
        #endif
    }

#endif /* (ENO_EnoUART_RX_DIRECTION) */


#if(ENO_EnoUART_TX_DIRECTION)
    /*******************************************************************************
    * Function Name: ENO_EnoUART_SpiEnoUARTWriteTxData
    ****************************************************************************//**
    *
    *  Places a data entry into the transmit buffer to be sent at the next available
    *  bus time.
    *  This function is blocking and waits until there is space available to put the
    *  requested data in the transmit buffer.
    *
    *  \param txDataByte: the data to be transmitted.
    *   The amount of data bits to be transmitted depends on TX data bits selection 
    *   (the data bit counting starts from LSB of txDataByte).
    *
    * \globalvars
    *  ENO_EnoUART_txBufferHead - the start index to put data into the 
    *  software transmit buffer.
    *  ENO_EnoUART_txBufferTail - start index to get data from the software
    *  transmit buffer.
    *
    *******************************************************************************/
    void ENO_EnoUART_SpiEnoUARTWriteTxData(uint32 txData)
    {
    #if (ENO_EnoUART_INTERNAL_TX_SW_BUFFER_CONST)
        uint32 locHead;
    #endif /* (ENO_EnoUART_INTERNAL_TX_SW_BUFFER_CONST) */

        #if (ENO_EnoUART_CHECK_TX_SW_BUFFER)
        {
            /* Put data directly into the TX FIFO */
            if ((ENO_EnoUART_txBufferHead == ENO_EnoUART_txBufferTail) &&
                (ENO_EnoUART_SPI_EnoUART_FIFO_SIZE != ENO_EnoUART_GET_TX_FIFO_ENTRIES))
            {
                /* TX software buffer is empty: put data directly in TX FIFO */
                ENO_EnoUART_TX_FIFO_WR_REG = txData;
            }
            /* Put data into TX software buffer */
            else
            {
                /* Head index to put data */
                locHead = (ENO_EnoUART_txBufferHead + 1u);

                /* Adjust TX software buffer index */
                if (ENO_EnoUART_TX_BUFFER_SIZE == locHead)
                {
                    locHead = 0u;
                }

                /* Wait for space in TX software buffer */
                while (locHead == ENO_EnoUART_txBufferTail)
                {
                }

                /* TX software buffer has at least one room */

                /* Clear old status of INTR_TX_NOT_FULL. It sets at the end of transfer when TX FIFO is empty. */
                ENO_EnoUART_ClearTxInterruptSource(ENO_EnoUART_INTR_TX_NOT_FULL);

                ENO_EnoUART_PutWordInTxBuffer(locHead, txData);

                ENO_EnoUART_txBufferHead = locHead;

                /* Check if TX Not Full is disabled in interrupt */
                if (0u == (ENO_EnoUART_INTR_TX_MASK_REG & ENO_EnoUART_INTR_TX_NOT_FULL))
                {
                    /* Enable TX Not Full interrupt source to transmit from software buffer */
                    ENO_EnoUART_INTR_TX_MASK_REG |= (uint32) ENO_EnoUART_INTR_TX_NOT_FULL;
                }
            }
        }
        #else
        {
            /* Wait until TX FIFO has space to put data element */
            while (ENO_EnoUART_SPI_EnoUART_FIFO_SIZE == ENO_EnoUART_GET_TX_FIFO_ENTRIES)
            {
            }

            ENO_EnoUART_TX_FIFO_WR_REG = txData;
        }
        #endif
    }


    /*******************************************************************************
    * Function Name: ENO_EnoUART_SpiEnoUARTPutArray
    ****************************************************************************//**
    *
    *  Places an array of data into the transmit buffer to be sent.
    *  This function is blocking and waits until there is a space available to put
    *  all the requested data in the transmit buffer. The array size can be greater
    *  than transmit buffer size.
    *
    * \param wrBuf: pointer to an array of data to be placed in transmit buffer. 
    *  The width of the data to be transmitted depends on TX data width selection 
    *  (the data bit counting starts from LSB for each array element).
    * \param count: number of data elements to be placed in the transmit buffer.
    *
    * \globalvars
    *  ENO_EnoUART_txBufferHead - the start index to put data into the 
    *  software transmit buffer.
    *  ENO_EnoUART_txBufferTail - start index to get data from the software
    *  transmit buffer.
    *
    *******************************************************************************/
    void ENO_EnoUART_SpiEnoUARTPutArray(const uint8 wrBuf[], uint32 count)
    {
        uint32 i;

        for (i=0u; i < count; i++)
        {
            ENO_EnoUART_SpiEnoUARTWriteTxData((uint32) wrBuf[i]);
        }
    }


    /*******************************************************************************
    * Function Name: ENO_EnoUART_SpiEnoUARTGetTxBufferSize
    ****************************************************************************//**
    *
    *  Returns the number of elements currently in the transmit buffer.
    *   - TX software buffer is disabled: returns the number of used entries in
    *     TX FIFO.
    *   - TX software buffer is enabled: returns the number of elements currently
    *     used in the transmit buffer. This number does not include used entries in
    *     the TX FIFO. The transmit buffer size is zero until the TX FIFO is
    *     not full.
    *
    * \return
    *  Number of data elements ready to transmit.
    *
    * \globalvars
    *  ENO_EnoUART_txBufferHead - the start index to put data into the 
    *  software transmit buffer.
    *  ENO_EnoUART_txBufferTail - start index to get data from the software
    *  transmit buffer.
    *
    *******************************************************************************/
    uint32 ENO_EnoUART_SpiEnoUARTGetTxBufferSize(void)
    {
        uint32 size;
    #if (ENO_EnoUART_INTERNAL_TX_SW_BUFFER_CONST)
        uint32 locTail;
    #endif /* (ENO_EnoUART_INTERNAL_TX_SW_BUFFER_CONST) */

        #if (ENO_EnoUART_CHECK_TX_SW_BUFFER)
        {
            /* Get current Tail index */
            locTail = ENO_EnoUART_txBufferTail;

            if (ENO_EnoUART_txBufferHead >= locTail)
            {
                size = (ENO_EnoUART_txBufferHead - locTail);
            }
            else
            {
                size = (ENO_EnoUART_txBufferHead + (ENO_EnoUART_TX_BUFFER_SIZE - locTail));
            }
        }
        #else
        {
            size = ENO_EnoUART_GET_TX_FIFO_ENTRIES;
        }
        #endif

        return (size);
    }


    /*******************************************************************************
    * Function Name: ENO_EnoUART_SpiEnoUARTClearTxBuffer
    ****************************************************************************//**
    *
    *  Clears the transmit buffer and TX FIFO.
    *
    * \globalvars
    *  ENO_EnoUART_txBufferHead - the start index to put data into the 
    *  software transmit buffer.
    *  ENO_EnoUART_txBufferTail - start index to get data from the software
    *  transmit buffer.
    *
    *******************************************************************************/
    void ENO_EnoUART_SpiEnoUARTClearTxBuffer(void)
    {
        #if (ENO_EnoUART_CHECK_TX_SW_BUFFER)
        {
            /* Lock from component interruption */
            ENO_EnoUART_DisableInt();

            /* Flush TX software buffer */
            ENO_EnoUART_txBufferHead = ENO_EnoUART_txBufferTail;

            ENO_EnoUART_INTR_TX_MASK_REG &= (uint32) ~ENO_EnoUART_INTR_TX_NOT_FULL;
            ENO_EnoUART_CLEAR_TX_FIFO;
            ENO_EnoUART_ClearTxInterruptSource(ENO_EnoUART_INTR_TX_ALL);

            /* Release lock */
            ENO_EnoUART_EnableInt();
        }
        #else
        {
            ENO_EnoUART_CLEAR_TX_FIFO;
        }
        #endif
    }

#endif /* (ENO_EnoUART_TX_DIRECTION) */


/*******************************************************************************
* Function Name: ENO_EnoUART_SpiEnoUARTDisableIntRx
****************************************************************************//**
*
*  Disables the RX interrupt sources.
*
*  \return
*   Returns the RX interrupt sources enabled before the function call.
*
*******************************************************************************/
uint32 ENO_EnoUART_SpiEnoUARTDisableIntRx(void)
{
    uint32 intSource;

    intSource = ENO_EnoUART_GetRxInterruptMode();

    ENO_EnoUART_SetRxInterruptMode(ENO_EnoUART_NO_INTR_SOURCES);

    return (intSource);
}


/*******************************************************************************
* Function Name: ENO_EnoUART_SpiEnoUARTDisableIntTx
****************************************************************************//**
*
*  Disables TX interrupt sources.
*
*  \return
*   Returns TX interrupt sources enabled before function call.
*
*******************************************************************************/
uint32 ENO_EnoUART_SpiEnoUARTDisableIntTx(void)
{
    uint32 intSourceMask;

    intSourceMask = ENO_EnoUART_GetTxInterruptMode();

    ENO_EnoUART_SetTxInterruptMode(ENO_EnoUART_NO_INTR_SOURCES);

    return (intSourceMask);
}


#if(ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    /*******************************************************************************
    * Function Name: ENO_EnoUART_PutWordInRxBuffer
    ****************************************************************************//**
    *
    *  Stores a byte/word into the RX buffer.
    *  Only available in the Unconfigured operation mode.
    *
    *  \param index:      index to store data byte/word in the RX buffer.
    *  \param rxDataByte: byte/word to store.
    *
    *******************************************************************************/
    void ENO_EnoUART_PutWordInRxBuffer(uint32 idx, uint32 rxDataByte)
    {
        /* Put data in buffer */
        if (ENO_EnoUART_ONE_BYTE_WIDTH == ENO_EnoUART_rxDataBits)
        {
            ENO_EnoUART_rxBuffer[idx] = ((uint8) rxDataByte);
        }
        else
        {
            ENO_EnoUART_rxBuffer[(uint32)(idx << 1u)]      = LO8(LO16(rxDataByte));
            ENO_EnoUART_rxBuffer[(uint32)(idx << 1u) + 1u] = HI8(LO16(rxDataByte));
        }
    }


    /*******************************************************************************
    * Function Name: ENO_EnoUART_GetWordFromRxBuffer
    ****************************************************************************//**
    *
    *  Reads byte/word from RX buffer.
    *  Only available in the Unconfigured operation mode.
    *
    *  \return
    *   Returns byte/word read from RX buffer.
    *
    *******************************************************************************/
    uint32 ENO_EnoUART_GetWordFromRxBuffer(uint32 idx)
    {
        uint32 value;

        if (ENO_EnoUART_ONE_BYTE_WIDTH == ENO_EnoUART_rxDataBits)
        {
            value = ENO_EnoUART_rxBuffer[idx];
        }
        else
        {
            value  = (uint32) ENO_EnoUART_rxBuffer[(uint32)(idx << 1u)];
            value |= (uint32) ((uint32)ENO_EnoUART_rxBuffer[(uint32)(idx << 1u) + 1u] << 8u);
        }

        return (value);
    }


    /*******************************************************************************
    * Function Name: ENO_EnoUART_PutWordInTxBuffer
    ****************************************************************************//**
    *
    *  Stores byte/word into the TX buffer.
    *  Only available in the Unconfigured operation mode.
    *
    *  \param idx:        index to store data byte/word in the TX buffer.
    *  \param txDataByte: byte/word to store.
    *
    *******************************************************************************/
    void ENO_EnoUART_PutWordInTxBuffer(uint32 idx, uint32 txDataByte)
    {
        /* Put data in buffer */
        if (ENO_EnoUART_ONE_BYTE_WIDTH == ENO_EnoUART_txDataBits)
        {
            ENO_EnoUART_txBuffer[idx] = ((uint8) txDataByte);
        }
        else
        {
            ENO_EnoUART_txBuffer[(uint32)(idx << 1u)]      = LO8(LO16(txDataByte));
            ENO_EnoUART_txBuffer[(uint32)(idx << 1u) + 1u] = HI8(LO16(txDataByte));
        }
    }


    /*******************************************************************************
    * Function Name: ENO_EnoUART_GetWordFromTxBuffer
    ****************************************************************************//**
    *
    *  Reads byte/word from the TX buffer.
    *  Only available in the Unconfigured operation mode.
    *
    *  \param idx: index to get data byte/word from the TX buffer.
    *
    *  \return
    *   Returns byte/word read from the TX buffer.
    *
    *******************************************************************************/
    uint32 ENO_EnoUART_GetWordFromTxBuffer(uint32 idx)
    {
        uint32 value;

        if (ENO_EnoUART_ONE_BYTE_WIDTH == ENO_EnoUART_txDataBits)
        {
            value = (uint32) ENO_EnoUART_txBuffer[idx];
        }
        else
        {
            value  = (uint32) ENO_EnoUART_txBuffer[(uint32)(idx << 1u)];
            value |= (uint32) ((uint32) ENO_EnoUART_txBuffer[(uint32)(idx << 1u) + 1u] << 8u);
        }

        return (value);
    }

#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */


/* [] END OF FILE */
