/***************************************************************************//**
* \file ENO_EnoUART.c
* \version 3.20
*
* \brief
*  This file provides the source code to the API for the SCB Component.
*
* Note:
*
*******************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ENO_EnoUART_PVT.h"

#if (ENO_EnoUART_SCB_MODE_I2C_INC)
    #include "ENO_EnoUART_I2C_PVT.h"
#endif /* (ENO_EnoUART_SCB_MODE_I2C_INC) */

#if (ENO_EnoUART_SCB_MODE_EZI2C_INC)
    #include "ENO_EnoUART_EZI2C_PVT.h"
#endif /* (ENO_EnoUART_SCB_MODE_EZI2C_INC) */

#if (ENO_EnoUART_SCB_MODE_SPI_INC || ENO_EnoUART_SCB_MODE_EnoUART_INC)
    #include "ENO_EnoUART_SPI_EnoUART_PVT.h"
#endif /* (ENO_EnoUART_SCB_MODE_SPI_INC || ENO_EnoUART_SCB_MODE_EnoUART_INC) */


/***************************************
*    Run Time Configuration Vars
***************************************/

/* Stores internal component configuration for Unconfigured mode */
#if (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Common configuration variables */
    uint8 ENO_EnoUART_scbMode = ENO_EnoUART_SCB_MODE_UNCONFIG;
    uint8 ENO_EnoUART_scbEnableWake;
    uint8 ENO_EnoUART_scbEnableIntr;

    /* I2C configuration variables */
    uint8 ENO_EnoUART_mode;
    uint8 ENO_EnoUART_acceptAddr;

    /* SPI/EnoUART configuration variables */
    volatile uint8 * ENO_EnoUART_rxBuffer;
    uint8  ENO_EnoUART_rxDataBits;
    uint32 ENO_EnoUART_rxBufferSize;

    volatile uint8 * ENO_EnoUART_txBuffer;
    uint8  ENO_EnoUART_txDataBits;
    uint32 ENO_EnoUART_txBufferSize;

    /* EZI2C configuration variables */
    uint8 ENO_EnoUART_numberOfAddr;
    uint8 ENO_EnoUART_subAddrSize;
#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*     Common SCB Vars
***************************************/
/**
* \addtogroup group_general
* \{
*/

/** ENO_EnoUART_initVar indicates whether the ENO_EnoUART 
*  component has been initialized. The variable is initialized to 0 
*  and set to 1 the first time SCB_Start() is called. This allows 
*  the component to restart without reinitialization after the first 
*  call to the ENO_EnoUART_Start() routine.
*
*  If re-initialization of the component is required, then the 
*  ENO_EnoUART_Init() function can be called before the 
*  ENO_EnoUART_Start() or ENO_EnoUART_Enable() function.
*/
uint8 ENO_EnoUART_initVar = 0u;


#if (! (ENO_EnoUART_SCB_MODE_I2C_CONST_CFG || \
        ENO_EnoUART_SCB_MODE_EZI2C_CONST_CFG))
    /** This global variable stores TX interrupt sources after 
    * ENO_EnoUART_Stop() is called. Only these TX interrupt sources 
    * will be restored on a subsequent ENO_EnoUART_Enable() call.
    */
    uint16 ENO_EnoUART_IntrTxMask = 0u;
#endif /* (! (ENO_EnoUART_SCB_MODE_I2C_CONST_CFG || \
              ENO_EnoUART_SCB_MODE_EZI2C_CONST_CFG)) */
/** \} globals */

#if (ENO_EnoUART_SCB_IRQ_INTERNAL)
#if !defined (CY_REMOVE_ENO_EnoUART_CUSTOM_INTR_HANDLER)
    void (*ENO_EnoUART_customIntrHandler)(void) = NULL;
#endif /* !defined (CY_REMOVE_ENO_EnoUART_CUSTOM_INTR_HANDLER) */
#endif /* (ENO_EnoUART_SCB_IRQ_INTERNAL) */


/***************************************
*    Private Function Prototypes
***************************************/

static void ENO_EnoUART_ScbEnableIntr(void);
static void ENO_EnoUART_ScbModeStop(void);
static void ENO_EnoUART_ScbModePostEnable(void);


/*******************************************************************************
* Function Name: ENO_EnoUART_Init
****************************************************************************//**
*
*  Initializes the ENO_EnoUART component to operate in one of the selected
*  configurations: I2C, SPI, EnoUART or EZI2C.
*  When the configuration is set to "Unconfigured SCB", this function does
*  not do any initialization. Use mode-specific initialization APIs instead:
*  ENO_EnoUART_I2CInit, ENO_EnoUART_SpiInit, 
*  ENO_EnoUART_EnoUARTInit or ENO_EnoUART_EzI2CInit.
*
*******************************************************************************/
void ENO_EnoUART_Init(void)
{
#if (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    if (ENO_EnoUART_SCB_MODE_UNCONFIG_RUNTM_CFG)
    {
        ENO_EnoUART_initVar = 0u;
    }
    else
    {
        /* Initialization was done before this function call */
    }

#elif (ENO_EnoUART_SCB_MODE_I2C_CONST_CFG)
    ENO_EnoUART_I2CInit();

#elif (ENO_EnoUART_SCB_MODE_SPI_CONST_CFG)
    ENO_EnoUART_SpiInit();

#elif (ENO_EnoUART_SCB_MODE_EnoUART_CONST_CFG)
    ENO_EnoUART_EnoUARTInit();

#elif (ENO_EnoUART_SCB_MODE_EZI2C_CONST_CFG)
    ENO_EnoUART_EzI2CInit();

#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: ENO_EnoUART_Enable
****************************************************************************//**
*
*  Enables ENO_EnoUART component operation: activates the hardware and 
*  internal interrupt. It also restores TX interrupt sources disabled after the 
*  ENO_EnoUART_Stop() function was called (note that level-triggered TX 
*  interrupt sources remain disabled to not cause code lock-up).
*  For I2C and EZI2C modes the interrupt is internal and mandatory for 
*  operation. For SPI and EnoUART modes the interrupt can be configured as none, 
*  internal or external.
*  The ENO_EnoUART configuration should be not changed when the component
*  is enabled. Any configuration changes should be made after disabling the 
*  component.
*  When configuration is set to �Unconfigured ENO_EnoUART�, the component 
*  must first be initialized to operate in one of the following configurations: 
*  I2C, SPI, EnoUART or EZ I2C, using the mode-specific initialization API. 
*  Otherwise this function does not enable the component.
*
*******************************************************************************/
void ENO_EnoUART_Enable(void)
{
#if (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Enable SCB block, only if it is already configured */
    if (!ENO_EnoUART_SCB_MODE_UNCONFIG_RUNTM_CFG)
    {
        ENO_EnoUART_CTRL_REG |= ENO_EnoUART_CTRL_ENABLED;

        ENO_EnoUART_ScbEnableIntr();

        /* Call PostEnable function specific to current operation mode */
        ENO_EnoUART_ScbModePostEnable();
    }
#else
    ENO_EnoUART_CTRL_REG |= ENO_EnoUART_CTRL_ENABLED;

    ENO_EnoUART_ScbEnableIntr();

    /* Call PostEnable function specific to current operation mode */
    ENO_EnoUART_ScbModePostEnable();
#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: ENO_EnoUART_Start
****************************************************************************//**
*
*  Invokes ENO_EnoUART_Init() and ENO_EnoUART_Enable().
*  After this function call, the component is enabled and ready for operation.
*  When configuration is set to "Unconfigured SCB", the component must first be
*  initialized to operate in one of the following configurations: I2C, SPI, EnoUART
*  or EZI2C. Otherwise this function does not enable the component.
*
* \globalvars
*  ENO_EnoUART_initVar - used to check initial configuration, modified
*  on first function call.
*
*******************************************************************************/
void ENO_EnoUART_Start(void)
{
    if (0u == ENO_EnoUART_initVar)
    {
        ENO_EnoUART_Init();
        ENO_EnoUART_initVar = 1u; /* Component was initialized */
    }

    ENO_EnoUART_Enable();
}


/*******************************************************************************
* Function Name: ENO_EnoUART_Stop
****************************************************************************//**
*
*  Disables the ENO_EnoUART component: disable the hardware and internal 
*  interrupt. It also disables all TX interrupt sources so as not to cause an 
*  unexpected interrupt trigger because after the component is enabled, the 
*  TX FIFO is empty.
*  Refer to the function ENO_EnoUART_Enable() for the interrupt 
*  configuration details.
*  This function disables the SCB component without checking to see if 
*  communication is in progress. Before calling this function it may be 
*  necessary to check the status of communication to make sure communication 
*  is complete. If this is not done then communication could be stopped mid 
*  byte and corrupted data could result.
*
*******************************************************************************/
void ENO_EnoUART_Stop(void)
{
#if (ENO_EnoUART_SCB_IRQ_INTERNAL)
    ENO_EnoUART_DisableInt();
#endif /* (ENO_EnoUART_SCB_IRQ_INTERNAL) */

    /* Call Stop function specific to current operation mode */
    ENO_EnoUART_ScbModeStop();

    /* Disable SCB IP */
    ENO_EnoUART_CTRL_REG &= (uint32) ~ENO_EnoUART_CTRL_ENABLED;

    /* Disable all TX interrupt sources so as not to cause an unexpected
    * interrupt trigger after the component will be enabled because the 
    * TX FIFO is empty.
    * For SCB IP v0, it is critical as it does not mask-out interrupt
    * sources when it is disabled. This can cause a code lock-up in the
    * interrupt handler because TX FIFO cannot be loaded after the block
    * is disabled.
    */
    ENO_EnoUART_SetTxInterruptMode(ENO_EnoUART_NO_INTR_SOURCES);

#if (ENO_EnoUART_SCB_IRQ_INTERNAL)
    ENO_EnoUART_ClearPendingInt();
#endif /* (ENO_EnoUART_SCB_IRQ_INTERNAL) */
}


/*******************************************************************************
* Function Name: ENO_EnoUART_SetRxFifoLevel
****************************************************************************//**
*
*  Sets level in the RX FIFO to generate a RX level interrupt.
*  When the RX FIFO has more entries than the RX FIFO level an RX level
*  interrupt request is generated.
*
*  \param level: Level in the RX FIFO to generate RX level interrupt.
*   The range of valid level values is between 0 and RX FIFO depth - 1.
*
*******************************************************************************/
void ENO_EnoUART_SetRxFifoLevel(uint32 level)
{
    uint32 rxFifoCtrl;

    rxFifoCtrl = ENO_EnoUART_RX_FIFO_CTRL_REG;

    rxFifoCtrl &= ((uint32) ~ENO_EnoUART_RX_FIFO_CTRL_TRIGGER_LEVEL_MASK); /* Clear level mask bits */
    rxFifoCtrl |= ((uint32) (ENO_EnoUART_RX_FIFO_CTRL_TRIGGER_LEVEL_MASK & level));

    ENO_EnoUART_RX_FIFO_CTRL_REG = rxFifoCtrl;
}


/*******************************************************************************
* Function Name: ENO_EnoUART_SetTxFifoLevel
****************************************************************************//**
*
*  Sets level in the TX FIFO to generate a TX level interrupt.
*  When the TX FIFO has less entries than the TX FIFO level an TX level
*  interrupt request is generated.
*
*  \param level: Level in the TX FIFO to generate TX level interrupt.
*   The range of valid level values is between 0 and TX FIFO depth - 1.
*
*******************************************************************************/
void ENO_EnoUART_SetTxFifoLevel(uint32 level)
{
    uint32 txFifoCtrl;

    txFifoCtrl = ENO_EnoUART_TX_FIFO_CTRL_REG;

    txFifoCtrl &= ((uint32) ~ENO_EnoUART_TX_FIFO_CTRL_TRIGGER_LEVEL_MASK); /* Clear level mask bits */
    txFifoCtrl |= ((uint32) (ENO_EnoUART_TX_FIFO_CTRL_TRIGGER_LEVEL_MASK & level));

    ENO_EnoUART_TX_FIFO_CTRL_REG = txFifoCtrl;
}


#if (ENO_EnoUART_SCB_IRQ_INTERNAL)
    /*******************************************************************************
    * Function Name: ENO_EnoUART_SetCustomInterruptHandler
    ****************************************************************************//**
    *
    *  Registers a function to be called by the internal interrupt handler.
    *  First the function that is registered is called, then the internal interrupt
    *  handler performs any operation such as software buffer management functions
    *  before the interrupt returns.  It is the user's responsibility not to break
    *  the software buffer operations. Only one custom handler is supported, which
    *  is the function provided by the most recent call.
    *  At the initialization time no custom handler is registered.
    *
    *  \param func: Pointer to the function to register.
    *        The value NULL indicates to remove the current custom interrupt
    *        handler.
    *
    *******************************************************************************/
    void ENO_EnoUART_SetCustomInterruptHandler(void (*func)(void))
    {
    #if !defined (CY_REMOVE_ENO_EnoUART_CUSTOM_INTR_HANDLER)
        ENO_EnoUART_customIntrHandler = func; /* Register interrupt handler */
    #else
        if (NULL != func)
        {
            /* Suppress compiler warning */
        }
    #endif /* !defined (CY_REMOVE_ENO_EnoUART_CUSTOM_INTR_HANDLER) */
    }
#endif /* (ENO_EnoUART_SCB_IRQ_INTERNAL) */


/*******************************************************************************
* Function Name: ENO_EnoUART_ScbModeEnableIntr
****************************************************************************//**
*
*  Enables an interrupt for a specific mode.
*
*******************************************************************************/
static void ENO_EnoUART_ScbEnableIntr(void)
{
#if (ENO_EnoUART_SCB_IRQ_INTERNAL)
    /* Enable interrupt in NVIC */
    #if (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
        if (0u != ENO_EnoUART_scbEnableIntr)
        {
            ENO_EnoUART_EnableInt();
        }

    #else
        ENO_EnoUART_EnableInt();

    #endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */
#endif /* (ENO_EnoUART_SCB_IRQ_INTERNAL) */
}


/*******************************************************************************
* Function Name: ENO_EnoUART_ScbModePostEnable
****************************************************************************//**
*
*  Calls the PostEnable function for a specific operation mode.
*
*******************************************************************************/
static void ENO_EnoUART_ScbModePostEnable(void)
{
#if (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
#if (!ENO_EnoUART_CY_SCBIP_V1)
    if (ENO_EnoUART_SCB_MODE_SPI_RUNTM_CFG)
    {
        ENO_EnoUART_SpiPostEnable();
    }
    else if (ENO_EnoUART_SCB_MODE_EnoUART_RUNTM_CFG)
    {
        ENO_EnoUART_EnoUARTPostEnable();
    }
    else
    {
        /* Unknown mode: do nothing */
    }
#endif /* (!ENO_EnoUART_CY_SCBIP_V1) */

#elif (ENO_EnoUART_SCB_MODE_SPI_CONST_CFG)
    ENO_EnoUART_SpiPostEnable();

#elif (ENO_EnoUART_SCB_MODE_EnoUART_CONST_CFG)
    ENO_EnoUART_EnoUARTPostEnable();

#else
    /* Unknown mode: do nothing */
#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: ENO_EnoUART_ScbModeStop
****************************************************************************//**
*
*  Calls the Stop function for a specific operation mode.
*
*******************************************************************************/
static void ENO_EnoUART_ScbModeStop(void)
{
#if (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    if (ENO_EnoUART_SCB_MODE_I2C_RUNTM_CFG)
    {
        ENO_EnoUART_I2CStop();
    }
    else if (ENO_EnoUART_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        ENO_EnoUART_EzI2CStop();
    }
#if (!ENO_EnoUART_CY_SCBIP_V1)
    else if (ENO_EnoUART_SCB_MODE_SPI_RUNTM_CFG)
    {
        ENO_EnoUART_SpiStop();
    }
    else if (ENO_EnoUART_SCB_MODE_EnoUART_RUNTM_CFG)
    {
        ENO_EnoUART_EnoUARTStop();
    }
#endif /* (!ENO_EnoUART_CY_SCBIP_V1) */
    else
    {
        /* Unknown mode: do nothing */
    }
#elif (ENO_EnoUART_SCB_MODE_I2C_CONST_CFG)
    ENO_EnoUART_I2CStop();

#elif (ENO_EnoUART_SCB_MODE_EZI2C_CONST_CFG)
    ENO_EnoUART_EzI2CStop();

#elif (ENO_EnoUART_SCB_MODE_SPI_CONST_CFG)
    ENO_EnoUART_SpiStop();

#elif (ENO_EnoUART_SCB_MODE_EnoUART_CONST_CFG)
    ENO_EnoUART_EnoUARTStop();

#else
    /* Unknown mode: do nothing */
#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */
}


#if (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    /*******************************************************************************
    * Function Name: ENO_EnoUART_SetPins
    ****************************************************************************//**
    *
    *  Sets the pins settings accordingly to the selected operation mode.
    *  Only available in the Unconfigured operation mode. The mode specific
    *  initialization function calls it.
    *  Pins configuration is set by PSoC Creator when a specific mode of operation
    *  is selected in design time.
    *
    *  \param mode:      Mode of SCB operation.
    *  \param subMode:   Sub-mode of SCB operation. It is only required for SPI and EnoUART
    *             modes.
    *  \param EnoUARTEnableMask: enables TX or RX direction and RTS and CTS signals.
    *
    *******************************************************************************/
    void ENO_EnoUART_SetPins(uint32 mode, uint32 subMode, uint32 EnoUARTEnableMask)
    {
        uint32 pinsDm[ENO_EnoUART_SCB_PINS_NUMBER];
        uint32 i;
        
    #if (!ENO_EnoUART_CY_SCBIP_V1)
        uint32 pinsInBuf = 0u;
    #endif /* (!ENO_EnoUART_CY_SCBIP_V1) */
        
        uint32 hsiomSel[ENO_EnoUART_SCB_PINS_NUMBER] = 
        {
            ENO_EnoUART_RX_SCL_MOSI_HSIOM_SEL_GPIO,
            ENO_EnoUART_TX_SDA_MISO_HSIOM_SEL_GPIO,
            0u,
            0u,
            0u,
            0u,
            0u,
        };

    #if (ENO_EnoUART_CY_SCBIP_V1)
        /* Supress compiler warning. */
        if ((0u == subMode) || (0u == EnoUARTEnableMask))
        {
        }
    #endif /* (ENO_EnoUART_CY_SCBIP_V1) */

        /* Set default HSIOM to GPIO and Drive Mode to Analog Hi-Z */
        for (i = 0u; i < ENO_EnoUART_SCB_PINS_NUMBER; i++)
        {
            pinsDm[i] = ENO_EnoUART_PIN_DM_ALG_HIZ;
        }

        if ((ENO_EnoUART_SCB_MODE_I2C   == mode) ||
            (ENO_EnoUART_SCB_MODE_EZI2C == mode))
        {
        #if (ENO_EnoUART_RX_SCL_MOSI_PIN)
            hsiomSel[ENO_EnoUART_RX_SCL_MOSI_PIN_INDEX] = ENO_EnoUART_RX_SCL_MOSI_HSIOM_SEL_I2C;
            pinsDm  [ENO_EnoUART_RX_SCL_MOSI_PIN_INDEX] = ENO_EnoUART_PIN_DM_OD_LO;
        #elif (ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN)
            hsiomSel[ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN_INDEX] = ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_SEL_I2C;
            pinsDm  [ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN_INDEX] = ENO_EnoUART_PIN_DM_OD_LO;
        #else
        #endif /* (ENO_EnoUART_RX_SCL_MOSI_PIN) */
        
        #if (ENO_EnoUART_TX_SDA_MISO_PIN)
            hsiomSel[ENO_EnoUART_TX_SDA_MISO_PIN_INDEX] = ENO_EnoUART_TX_SDA_MISO_HSIOM_SEL_I2C;
            pinsDm  [ENO_EnoUART_TX_SDA_MISO_PIN_INDEX] = ENO_EnoUART_PIN_DM_OD_LO;
        #endif /* (ENO_EnoUART_TX_SDA_MISO_PIN) */
        }
    #if (!ENO_EnoUART_CY_SCBIP_V1)
        else if (ENO_EnoUART_SCB_MODE_SPI == mode)
        {
        #if (ENO_EnoUART_RX_SCL_MOSI_PIN)
            hsiomSel[ENO_EnoUART_RX_SCL_MOSI_PIN_INDEX] = ENO_EnoUART_RX_SCL_MOSI_HSIOM_SEL_SPI;
        #elif (ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN)
            hsiomSel[ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN_INDEX] = ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_SEL_SPI;
        #else
        #endif /* (ENO_EnoUART_RX_SCL_MOSI_PIN) */
        
        #if (ENO_EnoUART_TX_SDA_MISO_PIN)
            hsiomSel[ENO_EnoUART_TX_SDA_MISO_PIN_INDEX] = ENO_EnoUART_TX_SDA_MISO_HSIOM_SEL_SPI;
        #endif /* (ENO_EnoUART_TX_SDA_MISO_PIN) */
        
        #if (ENO_EnoUART_SCLK_PIN)
            hsiomSel[ENO_EnoUART_SCLK_PIN_INDEX] = ENO_EnoUART_SCLK_HSIOM_SEL_SPI;
        #endif /* (ENO_EnoUART_SCLK_PIN) */

            if (ENO_EnoUART_SPI_SLAVE == subMode)
            {
                /* Slave */
                pinsDm[ENO_EnoUART_RX_SCL_MOSI_PIN_INDEX] = ENO_EnoUART_PIN_DM_DIG_HIZ;
                pinsDm[ENO_EnoUART_TX_SDA_MISO_PIN_INDEX] = ENO_EnoUART_PIN_DM_STRONG;
                pinsDm[ENO_EnoUART_SCLK_PIN_INDEX] = ENO_EnoUART_PIN_DM_DIG_HIZ;

            #if (ENO_EnoUART_SS0_PIN)
                /* Only SS0 is valid choice for Slave */
                hsiomSel[ENO_EnoUART_SS0_PIN_INDEX] = ENO_EnoUART_SS0_HSIOM_SEL_SPI;
                pinsDm  [ENO_EnoUART_SS0_PIN_INDEX] = ENO_EnoUART_PIN_DM_DIG_HIZ;
            #endif /* (ENO_EnoUART_SS0_PIN) */

            #if (ENO_EnoUART_TX_SDA_MISO_PIN)
                /* Disable input buffer */
                 pinsInBuf |= ENO_EnoUART_TX_SDA_MISO_PIN_MASK;
            #endif /* (ENO_EnoUART_TX_SDA_MISO_PIN) */
            }
            else 
            {
                /* (Master) */
                pinsDm[ENO_EnoUART_RX_SCL_MOSI_PIN_INDEX] = ENO_EnoUART_PIN_DM_STRONG;
                pinsDm[ENO_EnoUART_TX_SDA_MISO_PIN_INDEX] = ENO_EnoUART_PIN_DM_DIG_HIZ;
                pinsDm[ENO_EnoUART_SCLK_PIN_INDEX] = ENO_EnoUART_PIN_DM_STRONG;

            #if (ENO_EnoUART_SS0_PIN)
                hsiomSel [ENO_EnoUART_SS0_PIN_INDEX] = ENO_EnoUART_SS0_HSIOM_SEL_SPI;
                pinsDm   [ENO_EnoUART_SS0_PIN_INDEX] = ENO_EnoUART_PIN_DM_STRONG;
                pinsInBuf |= ENO_EnoUART_SS0_PIN_MASK;
            #endif /* (ENO_EnoUART_SS0_PIN) */

            #if (ENO_EnoUART_SS1_PIN)
                hsiomSel [ENO_EnoUART_SS1_PIN_INDEX] = ENO_EnoUART_SS1_HSIOM_SEL_SPI;
                pinsDm   [ENO_EnoUART_SS1_PIN_INDEX] = ENO_EnoUART_PIN_DM_STRONG;
                pinsInBuf |= ENO_EnoUART_SS1_PIN_MASK;
            #endif /* (ENO_EnoUART_SS1_PIN) */

            #if (ENO_EnoUART_SS2_PIN)
                hsiomSel [ENO_EnoUART_SS2_PIN_INDEX] = ENO_EnoUART_SS2_HSIOM_SEL_SPI;
                pinsDm   [ENO_EnoUART_SS2_PIN_INDEX] = ENO_EnoUART_PIN_DM_STRONG;
                pinsInBuf |= ENO_EnoUART_SS2_PIN_MASK;
            #endif /* (ENO_EnoUART_SS2_PIN) */

            #if (ENO_EnoUART_SS3_PIN)
                hsiomSel [ENO_EnoUART_SS3_PIN_INDEX] = ENO_EnoUART_SS3_HSIOM_SEL_SPI;
                pinsDm   [ENO_EnoUART_SS3_PIN_INDEX] = ENO_EnoUART_PIN_DM_STRONG;
                pinsInBuf |= ENO_EnoUART_SS3_PIN_MASK;
            #endif /* (ENO_EnoUART_SS3_PIN) */

                /* Disable input buffers */
            #if (ENO_EnoUART_RX_SCL_MOSI_PIN)
                pinsInBuf |= ENO_EnoUART_RX_SCL_MOSI_PIN_MASK;
            #elif (ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN)
                pinsInBuf |= ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN_MASK;
            #else
            #endif /* (ENO_EnoUART_RX_SCL_MOSI_PIN) */

            #if (ENO_EnoUART_SCLK_PIN)
                pinsInBuf |= ENO_EnoUART_SCLK_PIN_MASK;
            #endif /* (ENO_EnoUART_SCLK_PIN) */
            }
        }
        else /* EnoUART */
        {
            if (ENO_EnoUART_EnoUART_MODE_SMARTCARD == subMode)
            {
                /* SmartCard */
            #if (ENO_EnoUART_TX_SDA_MISO_PIN)
                hsiomSel[ENO_EnoUART_TX_SDA_MISO_PIN_INDEX] = ENO_EnoUART_TX_SDA_MISO_HSIOM_SEL_EnoUART;
                pinsDm  [ENO_EnoUART_TX_SDA_MISO_PIN_INDEX] = ENO_EnoUART_PIN_DM_OD_LO;
            #endif /* (ENO_EnoUART_TX_SDA_MISO_PIN) */
            }
            else /* Standard or IrDA */
            {
                if (0u != (ENO_EnoUART_EnoUART_RX_PIN_ENABLE & EnoUARTEnableMask))
                {
                #if (ENO_EnoUART_RX_SCL_MOSI_PIN)
                    hsiomSel[ENO_EnoUART_RX_SCL_MOSI_PIN_INDEX] = ENO_EnoUART_RX_SCL_MOSI_HSIOM_SEL_EnoUART;
                    pinsDm  [ENO_EnoUART_RX_SCL_MOSI_PIN_INDEX] = ENO_EnoUART_PIN_DM_DIG_HIZ;
                #elif (ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN)
                    hsiomSel[ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN_INDEX] = ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_SEL_EnoUART;
                    pinsDm  [ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN_INDEX] = ENO_EnoUART_PIN_DM_DIG_HIZ;
                #else
                #endif /* (ENO_EnoUART_RX_SCL_MOSI_PIN) */
                }

                if (0u != (ENO_EnoUART_EnoUART_TX_PIN_ENABLE & EnoUARTEnableMask))
                {
                #if (ENO_EnoUART_TX_SDA_MISO_PIN)
                    hsiomSel[ENO_EnoUART_TX_SDA_MISO_PIN_INDEX] = ENO_EnoUART_TX_SDA_MISO_HSIOM_SEL_EnoUART;
                    pinsDm  [ENO_EnoUART_TX_SDA_MISO_PIN_INDEX] = ENO_EnoUART_PIN_DM_STRONG;
                    
                    /* Disable input buffer */
                    pinsInBuf |= ENO_EnoUART_TX_SDA_MISO_PIN_MASK;
                #endif /* (ENO_EnoUART_TX_SDA_MISO_PIN) */
                }

            #if !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1)
                if (ENO_EnoUART_EnoUART_MODE_STD == subMode)
                {
                    if (0u != (ENO_EnoUART_EnoUART_CTS_PIN_ENABLE & EnoUARTEnableMask))
                    {
                        /* CTS input is multiplexed with SCLK */
                    #if (ENO_EnoUART_SCLK_PIN)
                        hsiomSel[ENO_EnoUART_SCLK_PIN_INDEX] = ENO_EnoUART_SCLK_HSIOM_SEL_EnoUART;
                        pinsDm  [ENO_EnoUART_SCLK_PIN_INDEX] = ENO_EnoUART_PIN_DM_DIG_HIZ;
                    #endif /* (ENO_EnoUART_SCLK_PIN) */
                    }

                    if (0u != (ENO_EnoUART_EnoUART_RTS_PIN_ENABLE & EnoUARTEnableMask))
                    {
                        /* RTS output is multiplexed with SS0 */
                    #if (ENO_EnoUART_SS0_PIN)
                        hsiomSel[ENO_EnoUART_SS0_PIN_INDEX] = ENO_EnoUART_SS0_HSIOM_SEL_EnoUART;
                        pinsDm  [ENO_EnoUART_SS0_PIN_INDEX] = ENO_EnoUART_PIN_DM_STRONG;
                        
                        /* Disable input buffer */
                        pinsInBuf |= ENO_EnoUART_SS0_PIN_MASK;
                    #endif /* (ENO_EnoUART_SS0_PIN) */
                    }
                }
            #endif /* !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1) */
            }
        }
    #endif /* (!ENO_EnoUART_CY_SCBIP_V1) */

    /* Configure pins: set HSIOM, DM and InputBufEnable */
    /* Note: the DR register settings do not effect the pin output if HSIOM is other than GPIO */

    #if (ENO_EnoUART_RX_SCL_MOSI_PIN)
        ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_RX_SCL_MOSI_HSIOM_REG,
                                       ENO_EnoUART_RX_SCL_MOSI_HSIOM_MASK,
                                       ENO_EnoUART_RX_SCL_MOSI_HSIOM_POS,
                                        hsiomSel[ENO_EnoUART_RX_SCL_MOSI_PIN_INDEX]);

        ENO_EnoUART_EnoUART_rx_i2c_scl_spi_mosi_SetDriveMode((uint8) pinsDm[ENO_EnoUART_RX_SCL_MOSI_PIN_INDEX]);

        #if (!ENO_EnoUART_CY_SCBIP_V1)
            ENO_EnoUART_SET_INP_DIS(ENO_EnoUART_EnoUART_rx_i2c_scl_spi_mosi_INP_DIS,
                                         ENO_EnoUART_EnoUART_rx_i2c_scl_spi_mosi_MASK,
                                         (0u != (pinsInBuf & ENO_EnoUART_RX_SCL_MOSI_PIN_MASK)));
        #endif /* (!ENO_EnoUART_CY_SCBIP_V1) */
    
    #elif (ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN)
        ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG,
                                       ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_MASK,
                                       ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_POS,
                                       hsiomSel[ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN_INDEX]);

        ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi_SetDriveMode((uint8)
                                                               pinsDm[ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN_INDEX]);

        ENO_EnoUART_SET_INP_DIS(ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi_INP_DIS,
                                     ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi_MASK,
                                     (0u != (pinsInBuf & ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN_MASK)));

         /* Set interrupt on falling edge */
        ENO_EnoUART_SET_INCFG_TYPE(ENO_EnoUART_RX_WAKE_SCL_MOSI_INTCFG_REG,
                                        ENO_EnoUART_RX_WAKE_SCL_MOSI_INTCFG_TYPE_MASK,
                                        ENO_EnoUART_RX_WAKE_SCL_MOSI_INTCFG_TYPE_POS,
                                        ENO_EnoUART_INTCFG_TYPE_FALLING_EDGE);
    #else
    #endif /* (ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN) */

    #if (ENO_EnoUART_TX_SDA_MISO_PIN)
        ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_TX_SDA_MISO_HSIOM_REG,
                                       ENO_EnoUART_TX_SDA_MISO_HSIOM_MASK,
                                       ENO_EnoUART_TX_SDA_MISO_HSIOM_POS,
                                        hsiomSel[ENO_EnoUART_TX_SDA_MISO_PIN_INDEX]);

        ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso_SetDriveMode((uint8) pinsDm[ENO_EnoUART_TX_SDA_MISO_PIN_INDEX]);

    #if (!ENO_EnoUART_CY_SCBIP_V1)
        ENO_EnoUART_SET_INP_DIS(ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso_INP_DIS,
                                     ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso_MASK,
                                    (0u != (pinsInBuf & ENO_EnoUART_TX_SDA_MISO_PIN_MASK)));
    #endif /* (!ENO_EnoUART_CY_SCBIP_V1) */
    #endif /* (ENO_EnoUART_RX_SCL_MOSI_PIN) */

    #if (ENO_EnoUART_SCLK_PIN)
        ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_SCLK_HSIOM_REG,
                                       ENO_EnoUART_SCLK_HSIOM_MASK,
                                       ENO_EnoUART_SCLK_HSIOM_POS,
                                       hsiomSel[ENO_EnoUART_SCLK_PIN_INDEX]);

        ENO_EnoUART_spi_sclk_SetDriveMode((uint8) pinsDm[ENO_EnoUART_SCLK_PIN_INDEX]);

        ENO_EnoUART_SET_INP_DIS(ENO_EnoUART_spi_sclk_INP_DIS,
                                     ENO_EnoUART_spi_sclk_MASK,
                                     (0u != (pinsInBuf & ENO_EnoUART_SCLK_PIN_MASK)));
    #endif /* (ENO_EnoUART_SCLK_PIN) */

    #if (ENO_EnoUART_SS0_PIN)
        ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_SS0_HSIOM_REG,
                                       ENO_EnoUART_SS0_HSIOM_MASK,
                                       ENO_EnoUART_SS0_HSIOM_POS,
                                       hsiomSel[ENO_EnoUART_SS0_PIN_INDEX]);

        ENO_EnoUART_spi_ss0_SetDriveMode((uint8) pinsDm[ENO_EnoUART_SS0_PIN_INDEX]);

        ENO_EnoUART_SET_INP_DIS(ENO_EnoUART_spi_ss0_INP_DIS,
                                     ENO_EnoUART_spi_ss0_MASK,
                                     (0u != (pinsInBuf & ENO_EnoUART_SS0_PIN_MASK)));
    #endif /* (ENO_EnoUART_SS0_PIN) */

    #if (ENO_EnoUART_SS1_PIN)
        ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_SS1_HSIOM_REG,
                                       ENO_EnoUART_SS1_HSIOM_MASK,
                                       ENO_EnoUART_SS1_HSIOM_POS,
                                       hsiomSel[ENO_EnoUART_SS1_PIN_INDEX]);

        ENO_EnoUART_spi_ss1_SetDriveMode((uint8) pinsDm[ENO_EnoUART_SS1_PIN_INDEX]);

        ENO_EnoUART_SET_INP_DIS(ENO_EnoUART_spi_ss1_INP_DIS,
                                     ENO_EnoUART_spi_ss1_MASK,
                                     (0u != (pinsInBuf & ENO_EnoUART_SS1_PIN_MASK)));
    #endif /* (ENO_EnoUART_SS1_PIN) */

    #if (ENO_EnoUART_SS2_PIN)
        ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_SS2_HSIOM_REG,
                                       ENO_EnoUART_SS2_HSIOM_MASK,
                                       ENO_EnoUART_SS2_HSIOM_POS,
                                       hsiomSel[ENO_EnoUART_SS2_PIN_INDEX]);

        ENO_EnoUART_spi_ss2_SetDriveMode((uint8) pinsDm[ENO_EnoUART_SS2_PIN_INDEX]);

        ENO_EnoUART_SET_INP_DIS(ENO_EnoUART_spi_ss2_INP_DIS,
                                     ENO_EnoUART_spi_ss2_MASK,
                                     (0u != (pinsInBuf & ENO_EnoUART_SS2_PIN_MASK)));
    #endif /* (ENO_EnoUART_SS2_PIN) */

    #if (ENO_EnoUART_SS3_PIN)
        ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_SS3_HSIOM_REG,
                                       ENO_EnoUART_SS3_HSIOM_MASK,
                                       ENO_EnoUART_SS3_HSIOM_POS,
                                       hsiomSel[ENO_EnoUART_SS3_PIN_INDEX]);

        ENO_EnoUART_spi_ss3_SetDriveMode((uint8) pinsDm[ENO_EnoUART_SS3_PIN_INDEX]);

        ENO_EnoUART_SET_INP_DIS(ENO_EnoUART_spi_ss3_INP_DIS,
                                     ENO_EnoUART_spi_ss3_MASK,
                                     (0u != (pinsInBuf & ENO_EnoUART_SS3_PIN_MASK)));
    #endif /* (ENO_EnoUART_SS3_PIN) */
    }

#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */


#if (ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1)
    /*******************************************************************************
    * Function Name: ENO_EnoUART_I2CSlaveNackGeneration
    ****************************************************************************//**
    *
    *  Sets command to generate NACK to the address or data.
    *
    *******************************************************************************/
    void ENO_EnoUART_I2CSlaveNackGeneration(void)
    {
        /* Check for EC_AM toggle condition: EC_AM and clock stretching for address are enabled */
        if ((0u != (ENO_EnoUART_CTRL_REG & ENO_EnoUART_CTRL_EC_AM_MODE)) &&
            (0u == (ENO_EnoUART_I2C_CTRL_REG & ENO_EnoUART_I2C_CTRL_S_NOT_READY_ADDR_NACK)))
        {
            /* Toggle EC_AM before NACK generation */
            ENO_EnoUART_CTRL_REG &= ~ENO_EnoUART_CTRL_EC_AM_MODE;
            ENO_EnoUART_CTRL_REG |=  ENO_EnoUART_CTRL_EC_AM_MODE;
        }

        ENO_EnoUART_I2C_SLAVE_CMD_REG = ENO_EnoUART_I2C_SLAVE_CMD_S_NACK;
    }
#endif /* (ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1) */


/* [] END OF FILE */
