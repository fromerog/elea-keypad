/*******************************************************************************
* File Name: EnoUART_rx_wake.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_EnoUART_rx_wake_ALIASES_H) /* Pins EnoUART_rx_wake_ALIASES_H */
#define CY_PINS_EnoUART_rx_wake_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define EnoUART_rx_wake_0			(EnoUART_rx_wake__0__PC)
#define EnoUART_rx_wake_0_PS		(EnoUART_rx_wake__0__PS)
#define EnoUART_rx_wake_0_PC		(EnoUART_rx_wake__0__PC)
#define EnoUART_rx_wake_0_DR		(EnoUART_rx_wake__0__DR)
#define EnoUART_rx_wake_0_SHIFT	(EnoUART_rx_wake__0__SHIFT)
#define EnoUART_rx_wake_0_INTR	((uint16)((uint16)0x0003u << (EnoUART_rx_wake__0__SHIFT*2u)))

#define EnoUART_rx_wake_INTR_ALL	 ((uint16)(EnoUART_rx_wake_0_INTR))


#endif /* End Pins EnoUART_rx_wake_ALIASES_H */


/* [] END OF FILE */
