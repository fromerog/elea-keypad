/***************************************************************************//**
* \file ENO_EnoUART_PM.c
* \version 3.20
*
* \brief
*  This file provides the source code to the Power Management support for
*  the SCB Component.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ENO_EnoUART.h"
#include "ENO_EnoUART_PVT.h"

#if(ENO_EnoUART_SCB_MODE_I2C_INC)
    #include "ENO_EnoUART_I2C_PVT.h"
#endif /* (ENO_EnoUART_SCB_MODE_I2C_INC) */

#if(ENO_EnoUART_SCB_MODE_EZI2C_INC)
    #include "ENO_EnoUART_EZI2C_PVT.h"
#endif /* (ENO_EnoUART_SCB_MODE_EZI2C_INC) */

#if(ENO_EnoUART_SCB_MODE_SPI_INC || ENO_EnoUART_SCB_MODE_EnoUART_INC)
    #include "ENO_EnoUART_SPI_EnoUART_PVT.h"
#endif /* (ENO_EnoUART_SCB_MODE_SPI_INC || ENO_EnoUART_SCB_MODE_EnoUART_INC) */


/***************************************
*   Backup Structure declaration
***************************************/

#if(ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG || \
   (ENO_EnoUART_SCB_MODE_I2C_CONST_CFG   && (!ENO_EnoUART_I2C_WAKE_ENABLE_CONST))   || \
   (ENO_EnoUART_SCB_MODE_EZI2C_CONST_CFG && (!ENO_EnoUART_EZI2C_WAKE_ENABLE_CONST)) || \
   (ENO_EnoUART_SCB_MODE_SPI_CONST_CFG   && (!ENO_EnoUART_SPI_WAKE_ENABLE_CONST))   || \
   (ENO_EnoUART_SCB_MODE_EnoUART_CONST_CFG  && (!ENO_EnoUART_EnoUART_WAKE_ENABLE_CONST)))

    ENO_EnoUART_BACKUP_STRUCT ENO_EnoUART_backup =
    {
        0u, /* enableState */
    };
#endif


/*******************************************************************************
* Function Name: ENO_EnoUART_Sleep
****************************************************************************//**
*
*  Prepares the ENO_EnoUART component to enter Deep Sleep.
*  The �Enable wakeup from Deep Sleep Mode� selection has an influence on this 
*  function implementation:
*  - Checked: configures the component to be wakeup source from Deep Sleep.
*  - Unchecked: stores the current component state (enabled or disabled) and 
*    disables the component. See SCB_Stop() function for details about component 
*    disabling.
*
*  Call the ENO_EnoUART_Sleep() function before calling the 
*  CyPmSysDeepSleep() function. 
*  Refer to the PSoC Creator System Reference Guide for more information about 
*  power management functions and Low power section of this document for the 
*  selected mode.
*
*  This function should not be called before entering Sleep.
*
*******************************************************************************/
void ENO_EnoUART_Sleep(void)
{
#if(ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)

    if(ENO_EnoUART_SCB_WAKE_ENABLE_CHECK)
    {
        if(ENO_EnoUART_SCB_MODE_I2C_RUNTM_CFG)
        {
            ENO_EnoUART_I2CSaveConfig();
        }
        else if(ENO_EnoUART_SCB_MODE_EZI2C_RUNTM_CFG)
        {
            ENO_EnoUART_EzI2CSaveConfig();
        }
    #if(!ENO_EnoUART_CY_SCBIP_V1)
        else if(ENO_EnoUART_SCB_MODE_SPI_RUNTM_CFG)
        {
            ENO_EnoUART_SpiSaveConfig();
        }
        else if(ENO_EnoUART_SCB_MODE_EnoUART_RUNTM_CFG)
        {
            ENO_EnoUART_EnoUARTSaveConfig();
        }
    #endif /* (!ENO_EnoUART_CY_SCBIP_V1) */
        else
        {
            /* Unknown mode */
        }
    }
    else
    {
        ENO_EnoUART_backup.enableState = (uint8) ENO_EnoUART_GET_CTRL_ENABLED;

        if(0u != ENO_EnoUART_backup.enableState)
        {
            ENO_EnoUART_Stop();
        }
    }

#else

    #if (ENO_EnoUART_SCB_MODE_I2C_CONST_CFG && ENO_EnoUART_I2C_WAKE_ENABLE_CONST)
        ENO_EnoUART_I2CSaveConfig();

    #elif (ENO_EnoUART_SCB_MODE_EZI2C_CONST_CFG && ENO_EnoUART_EZI2C_WAKE_ENABLE_CONST)
        ENO_EnoUART_EzI2CSaveConfig();

    #elif (ENO_EnoUART_SCB_MODE_SPI_CONST_CFG && ENO_EnoUART_SPI_WAKE_ENABLE_CONST)
        ENO_EnoUART_SpiSaveConfig();

    #elif (ENO_EnoUART_SCB_MODE_EnoUART_CONST_CFG && ENO_EnoUART_EnoUART_WAKE_ENABLE_CONST)
        ENO_EnoUART_EnoUARTSaveConfig();

    #else

        ENO_EnoUART_backup.enableState = (uint8) ENO_EnoUART_GET_CTRL_ENABLED;

        if(0u != ENO_EnoUART_backup.enableState)
        {
            ENO_EnoUART_Stop();
        }

    #endif /* defined (ENO_EnoUART_SCB_MODE_I2C_CONST_CFG) && (ENO_EnoUART_I2C_WAKE_ENABLE_CONST) */

#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: ENO_EnoUART_Wakeup
****************************************************************************//**
*
*  Prepares the ENO_EnoUART component for Active mode operation after 
*  Deep Sleep.
*  The �Enable wakeup from Deep Sleep Mode� selection has influence on this 
*  function implementation:
*  - Checked: restores the component Active mode configuration.
*  - Unchecked: enables the component if it was enabled before enter Deep Sleep.
*
*  This function should not be called after exiting Sleep.
*
*  \sideeffect
*   Calling the ENO_EnoUART_Wakeup() function without first calling the 
*   ENO_EnoUART_Sleep() function may produce unexpected behavior.
*
*******************************************************************************/
void ENO_EnoUART_Wakeup(void)
{
#if(ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)

    if(ENO_EnoUART_SCB_WAKE_ENABLE_CHECK)
    {
        if(ENO_EnoUART_SCB_MODE_I2C_RUNTM_CFG)
        {
            ENO_EnoUART_I2CRestoreConfig();
        }
        else if(ENO_EnoUART_SCB_MODE_EZI2C_RUNTM_CFG)
        {
            ENO_EnoUART_EzI2CRestoreConfig();
        }
    #if(!ENO_EnoUART_CY_SCBIP_V1)
        else if(ENO_EnoUART_SCB_MODE_SPI_RUNTM_CFG)
        {
            ENO_EnoUART_SpiRestoreConfig();
        }
        else if(ENO_EnoUART_SCB_MODE_EnoUART_RUNTM_CFG)
        {
            ENO_EnoUART_EnoUARTRestoreConfig();
        }
    #endif /* (!ENO_EnoUART_CY_SCBIP_V1) */
        else
        {
            /* Unknown mode */
        }
    }
    else
    {
        if(0u != ENO_EnoUART_backup.enableState)
        {
            ENO_EnoUART_Enable();
        }
    }

#else

    #if (ENO_EnoUART_SCB_MODE_I2C_CONST_CFG  && ENO_EnoUART_I2C_WAKE_ENABLE_CONST)
        ENO_EnoUART_I2CRestoreConfig();

    #elif (ENO_EnoUART_SCB_MODE_EZI2C_CONST_CFG && ENO_EnoUART_EZI2C_WAKE_ENABLE_CONST)
        ENO_EnoUART_EzI2CRestoreConfig();

    #elif (ENO_EnoUART_SCB_MODE_SPI_CONST_CFG && ENO_EnoUART_SPI_WAKE_ENABLE_CONST)
        ENO_EnoUART_SpiRestoreConfig();

    #elif (ENO_EnoUART_SCB_MODE_EnoUART_CONST_CFG && ENO_EnoUART_EnoUART_WAKE_ENABLE_CONST)
        ENO_EnoUART_EnoUARTRestoreConfig();

    #else

        if(0u != ENO_EnoUART_backup.enableState)
        {
            ENO_EnoUART_Enable();
        }

    #endif /* (ENO_EnoUART_I2C_WAKE_ENABLE_CONST) */

#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/* [] END OF FILE */
