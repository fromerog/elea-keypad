/*******************************************************************************
* File Name: EnoUART_EON_PINS.h
* Version 3.0
*
* Description:
*  This file provides constants and parameter values for the pin components
*  buried into SCB Component.
*
* Note:
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PINS_EnoUART_EON_H)
#define CY_SCB_PINS_EnoUART_EON_H

#include "cydevice_trm.h"
#include "cyfitter.h"
#include "cytypes.h"


/***************************************
*   Conditional Compilation Parameters
****************************************/

/* Unconfigured pins */
#define EnoUART_EON_REMOVE_RX_WAKE_SCL_MOSI_PIN  (1u)
#define EnoUART_EON_REMOVE_RX_SCL_MOSI_PIN      (1u)
#define EnoUART_EON_REMOVE_TX_SDA_MISO_PIN      (1u)
#define EnoUART_EON_REMOVE_SCLK_PIN      (1u)
#define EnoUART_EON_REMOVE_SS0_PIN      (1u)
#define EnoUART_EON_REMOVE_SS1_PIN                 (1u)
#define EnoUART_EON_REMOVE_SS2_PIN                 (1u)
#define EnoUART_EON_REMOVE_SS3_PIN                 (1u)

/* Mode defined pins */
#define EnoUART_EON_REMOVE_I2C_PINS                (1u)
#define EnoUART_EON_REMOVE_SPI_MASTER_PINS         (1u)
#define EnoUART_EON_REMOVE_SPI_MASTER_SCLK_PIN     (1u)
#define EnoUART_EON_REMOVE_SPI_MASTER_MOSI_PIN     (1u)
#define EnoUART_EON_REMOVE_SPI_MASTER_MISO_PIN     (1u)
#define EnoUART_EON_REMOVE_SPI_MASTER_SS0_PIN      (1u)
#define EnoUART_EON_REMOVE_SPI_MASTER_SS1_PIN      (1u)
#define EnoUART_EON_REMOVE_SPI_MASTER_SS2_PIN      (1u)
#define EnoUART_EON_REMOVE_SPI_MASTER_SS3_PIN      (1u)
#define EnoUART_EON_REMOVE_SPI_SLAVE_PINS          (1u)
#define EnoUART_EON_REMOVE_SPI_SLAVE_MOSI_PIN      (1u)
#define EnoUART_EON_REMOVE_SPI_SLAVE_MISO_PIN      (1u)
#define EnoUART_EON_REMOVE_EnoUART_TX_PIN             (0u)
#define EnoUART_EON_REMOVE_EnoUART_RX_TX_PIN          (1u)
#define EnoUART_EON_REMOVE_EnoUART_RX_PIN             (0u)
#define EnoUART_EON_REMOVE_EnoUART_RX_WAKE_PIN        (1u)
#define EnoUART_EON_REMOVE_EnoUART_RTS_PIN            (1u)
#define EnoUART_EON_REMOVE_EnoUART_CTS_PIN            (1u)

/* Unconfigured pins */
#define EnoUART_EON_RX_WAKE_SCL_MOSI_PIN (0u == EnoUART_EON_REMOVE_RX_WAKE_SCL_MOSI_PIN)
#define EnoUART_EON_RX_SCL_MOSI_PIN     (0u == EnoUART_EON_REMOVE_RX_SCL_MOSI_PIN)
#define EnoUART_EON_TX_SDA_MISO_PIN     (0u == EnoUART_EON_REMOVE_TX_SDA_MISO_PIN)
#define EnoUART_EON_SCLK_PIN     (0u == EnoUART_EON_REMOVE_SCLK_PIN)
#define EnoUART_EON_SS0_PIN     (0u == EnoUART_EON_REMOVE_SS0_PIN)
#define EnoUART_EON_SS1_PIN                (0u == EnoUART_EON_REMOVE_SS1_PIN)
#define EnoUART_EON_SS2_PIN                (0u == EnoUART_EON_REMOVE_SS2_PIN)
#define EnoUART_EON_SS3_PIN                (0u == EnoUART_EON_REMOVE_SS3_PIN)

/* Mode defined pins */
#define EnoUART_EON_I2C_PINS               (0u == EnoUART_EON_REMOVE_I2C_PINS)
#define EnoUART_EON_SPI_MASTER_PINS        (0u == EnoUART_EON_REMOVE_SPI_MASTER_PINS)
#define EnoUART_EON_SPI_MASTER_SCLK_PIN    (0u == EnoUART_EON_REMOVE_SPI_MASTER_SCLK_PIN)
#define EnoUART_EON_SPI_MASTER_MOSI_PIN    (0u == EnoUART_EON_REMOVE_SPI_MASTER_MOSI_PIN)
#define EnoUART_EON_SPI_MASTER_MISO_PIN    (0u == EnoUART_EON_REMOVE_SPI_MASTER_MISO_PIN)
#define EnoUART_EON_SPI_MASTER_SS0_PIN     (0u == EnoUART_EON_REMOVE_SPI_MASTER_SS0_PIN)
#define EnoUART_EON_SPI_MASTER_SS1_PIN     (0u == EnoUART_EON_REMOVE_SPI_MASTER_SS1_PIN)
#define EnoUART_EON_SPI_MASTER_SS2_PIN     (0u == EnoUART_EON_REMOVE_SPI_MASTER_SS2_PIN)
#define EnoUART_EON_SPI_MASTER_SS3_PIN     (0u == EnoUART_EON_REMOVE_SPI_MASTER_SS3_PIN)
#define EnoUART_EON_SPI_SLAVE_PINS         (0u == EnoUART_EON_REMOVE_SPI_SLAVE_PINS)
#define EnoUART_EON_SPI_SLAVE_MOSI_PIN     (0u == EnoUART_EON_REMOVE_SPI_SLAVE_MOSI_PIN)
#define EnoUART_EON_SPI_SLAVE_MISO_PIN     (0u == EnoUART_EON_REMOVE_SPI_SLAVE_MISO_PIN)
#define EnoUART_EON_EnoUART_TX_PIN            (0u == EnoUART_EON_REMOVE_EnoUART_TX_PIN)
#define EnoUART_EON_EnoUART_RX_TX_PIN         (0u == EnoUART_EON_REMOVE_EnoUART_RX_TX_PIN)
#define EnoUART_EON_EnoUART_RX_PIN            (0u == EnoUART_EON_REMOVE_EnoUART_RX_PIN)
#define EnoUART_EON_EnoUART_RX_WAKE_PIN       (0u == EnoUART_EON_REMOVE_EnoUART_RX_WAKE_PIN)
#define EnoUART_EON_EnoUART_RTS_PIN           (0u == EnoUART_EON_REMOVE_EnoUART_RTS_PIN)
#define EnoUART_EON_EnoUART_CTS_PIN           (0u == EnoUART_EON_REMOVE_EnoUART_CTS_PIN)


/***************************************
*             Includes
****************************************/

#if (EnoUART_EON_RX_WAKE_SCL_MOSI_PIN)
    #include "EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi.h"
#endif /* (EnoUART_EON_RX_SCL_MOSI) */

#if (EnoUART_EON_RX_SCL_MOSI_PIN)
    #include "EnoUART_EON_EnoUART_rx_i2c_scl_spi_mosi.h"
#endif /* (EnoUART_EON_RX_SCL_MOSI) */

#if (EnoUART_EON_TX_SDA_MISO_PIN)
    #include "EnoUART_EON_EnoUART_tx_i2c_sda_spi_miso.h"
#endif /* (EnoUART_EON_TX_SDA_MISO) */

#if (EnoUART_EON_SCLK_PIN)
    #include "EnoUART_EON_spi_sclk.h"
#endif /* (EnoUART_EON_SCLK) */

#if (EnoUART_EON_SS0_PIN)
    #include "EnoUART_EON_spi_ss0.h"
#endif /* (EnoUART_EON_SS0_PIN) */

#if (EnoUART_EON_SS1_PIN)
    #include "EnoUART_EON_spi_ss1.h"
#endif /* (EnoUART_EON_SS1_PIN) */

#if (EnoUART_EON_SS2_PIN)
    #include "EnoUART_EON_spi_ss2.h"
#endif /* (EnoUART_EON_SS2_PIN) */

#if (EnoUART_EON_SS3_PIN)
    #include "EnoUART_EON_spi_ss3.h"
#endif /* (EnoUART_EON_SS3_PIN) */

#if (EnoUART_EON_I2C_PINS)
    #include "EnoUART_EON_scl.h"
    #include "EnoUART_EON_sda.h"
#endif /* (EnoUART_EON_I2C_PINS) */

#if (EnoUART_EON_SPI_MASTER_PINS)
#if (EnoUART_EON_SPI_MASTER_SCLK_PIN)
    #include "EnoUART_EON_sclk_m.h"
#endif /* (EnoUART_EON_SPI_MASTER_SCLK_PIN) */

#if (EnoUART_EON_SPI_MASTER_MOSI_PIN)
    #include "EnoUART_EON_mosi_m.h"
#endif /* (EnoUART_EON_SPI_MASTER_MOSI_PIN) */

#if (EnoUART_EON_SPI_MASTER_MISO_PIN)
    #include "EnoUART_EON_miso_m.h"
#endif /*(EnoUART_EON_SPI_MASTER_MISO_PIN) */
#endif /* (EnoUART_EON_SPI_MASTER_PINS) */

#if (EnoUART_EON_SPI_SLAVE_PINS)
    #include "EnoUART_EON_sclk_s.h"
    #include "EnoUART_EON_ss_s.h"

#if (EnoUART_EON_SPI_SLAVE_MOSI_PIN)
    #include "EnoUART_EON_mosi_s.h"
#endif /* (EnoUART_EON_SPI_SLAVE_MOSI_PIN) */

#if (EnoUART_EON_SPI_SLAVE_MISO_PIN)
    #include "EnoUART_EON_miso_s.h"
#endif /*(EnoUART_EON_SPI_SLAVE_MISO_PIN) */
#endif /* (EnoUART_EON_SPI_SLAVE_PINS) */

#if (EnoUART_EON_SPI_MASTER_SS0_PIN)
    #include "EnoUART_EON_ss0_m.h"
#endif /* (EnoUART_EON_SPI_MASTER_SS0_PIN) */

#if (EnoUART_EON_SPI_MASTER_SS1_PIN)
    #include "EnoUART_EON_ss1_m.h"
#endif /* (EnoUART_EON_SPI_MASTER_SS1_PIN) */

#if (EnoUART_EON_SPI_MASTER_SS2_PIN)
    #include "EnoUART_EON_ss2_m.h"
#endif /* (EnoUART_EON_SPI_MASTER_SS2_PIN) */

#if (EnoUART_EON_SPI_MASTER_SS3_PIN)
    #include "EnoUART_EON_ss3_m.h"
#endif /* (EnoUART_EON_SPI_MASTER_SS3_PIN) */

#if (EnoUART_EON_EnoUART_TX_PIN)
    #include "EnoUART_EON_tx.h"
#endif /* (EnoUART_EON_EnoUART_TX_PIN) */

#if (EnoUART_EON_EnoUART_RX_TX_PIN)
    #include "EnoUART_EON_rx_tx.h"
#endif /* (EnoUART_EON_EnoUART_RX_TX_PIN) */

#if (EnoUART_EON_EnoUART_RX_PIN)
    #include "EnoUART_EON_rx.h"
#endif /* (EnoUART_EON_EnoUART_RX_PIN) */

#if (EnoUART_EON_EnoUART_RX_WAKE_PIN)
    #include "EnoUART_EON_rx_wake.h"
#endif /* (EnoUART_EON_EnoUART_RX_WAKE_PIN) */

#if (EnoUART_EON_EnoUART_RTS_PIN)
    #include "EnoUART_EON_rts.h"
#endif /* (EnoUART_EON_EnoUART_RTS_PIN) */

#if (EnoUART_EON_EnoUART_CTS_PIN)
    #include "EnoUART_EON_cts.h"
#endif /* (EnoUART_EON_EnoUART_CTS_PIN) */


/***************************************
*              Registers
***************************************/

#if (EnoUART_EON_RX_WAKE_SCL_MOSI_PIN)
    #define EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_REG \
                            (*(reg32 *) EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi__0__HSIOM)
    #define EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_PTR \
                            ( (reg32 *) EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi__0__HSIOM)
    #define EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_MASK \
                            (EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi__0__HSIOM_MASK)
    #define EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_POS \
                            (EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi__0__HSIOM_SHIFT)

    #define EnoUART_EON_RX_WAKE_SCL_MOSI_INTCFG_REG \
                            (*(reg32 *) EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi__0__INTCFG)
    #define EnoUART_EON_RX_WAKE_SCL_MOSI_INTCFG_PTR \
                            ( (reg32 *) EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi__0__INTCFG)
    #define EnoUART_EON_RX_WAKE_SCL_MOSI_INTCFG_TYPE_POS  (EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi__SHIFT)
    #define EnoUART_EON_RX_WAKE_SCL_MOSI_INTCFG_TYPE_MASK \
                            ((uint32) EnoUART_EON_INTCFG_TYPE_MASK << \
                                      EnoUART_EON_RX_WAKE_SCL_MOSI_INTCFG_TYPE_POS)
#endif /* (EnoUART_EON_RX_WAKE_SCL_MOSI_PIN) */

#if (EnoUART_EON_RX_SCL_MOSI_PIN)
    #define EnoUART_EON_RX_SCL_MOSI_HSIOM_REG   (*(reg32 *) EnoUART_EON_EnoUART_rx_i2c_scl_spi_mosi__0__HSIOM)
    #define EnoUART_EON_RX_SCL_MOSI_HSIOM_PTR   ( (reg32 *) EnoUART_EON_EnoUART_rx_i2c_scl_spi_mosi__0__HSIOM)
    #define EnoUART_EON_RX_SCL_MOSI_HSIOM_MASK  (EnoUART_EON_EnoUART_rx_i2c_scl_spi_mosi__0__HSIOM_MASK)
    #define EnoUART_EON_RX_SCL_MOSI_HSIOM_POS   (EnoUART_EON_EnoUART_rx_i2c_scl_spi_mosi__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_RX_SCL_MOSI_PIN) */

#if (EnoUART_EON_TX_SDA_MISO_PIN)
    #define EnoUART_EON_TX_SDA_MISO_HSIOM_REG   (*(reg32 *) EnoUART_EON_EnoUART_tx_i2c_sda_spi_miso__0__HSIOM)
    #define EnoUART_EON_TX_SDA_MISO_HSIOM_PTR   ( (reg32 *) EnoUART_EON_EnoUART_tx_i2c_sda_spi_miso__0__HSIOM)
    #define EnoUART_EON_TX_SDA_MISO_HSIOM_MASK  (EnoUART_EON_EnoUART_tx_i2c_sda_spi_miso__0__HSIOM_MASK)
    #define EnoUART_EON_TX_SDA_MISO_HSIOM_POS   (EnoUART_EON_EnoUART_tx_i2c_sda_spi_miso__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_TX_SDA_MISO_PIN) */

#if (EnoUART_EON_SCLK_PIN)
    #define EnoUART_EON_SCLK_HSIOM_REG   (*(reg32 *) EnoUART_EON_spi_sclk__0__HSIOM)
    #define EnoUART_EON_SCLK_HSIOM_PTR   ( (reg32 *) EnoUART_EON_spi_sclk__0__HSIOM)
    #define EnoUART_EON_SCLK_HSIOM_MASK  (EnoUART_EON_spi_sclk__0__HSIOM_MASK)
    #define EnoUART_EON_SCLK_HSIOM_POS   (EnoUART_EON_spi_sclk__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_SCLK_PIN) */

#if (EnoUART_EON_SS0_PIN)
    #define EnoUART_EON_SS0_HSIOM_REG   (*(reg32 *) EnoUART_EON_spi_ss0__0__HSIOM)
    #define EnoUART_EON_SS0_HSIOM_PTR   ( (reg32 *) EnoUART_EON_spi_ss0__0__HSIOM)
    #define EnoUART_EON_SS0_HSIOM_MASK  (EnoUART_EON_spi_ss0__0__HSIOM_MASK)
    #define EnoUART_EON_SS0_HSIOM_POS   (EnoUART_EON_spi_ss0__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_SS0_PIN) */

#if (EnoUART_EON_SS1_PIN)
    #define EnoUART_EON_SS1_HSIOM_REG      (*(reg32 *) EnoUART_EON_spi_ss1__0__HSIOM)
    #define EnoUART_EON_SS1_HSIOM_PTR      ( (reg32 *) EnoUART_EON_spi_ss1__0__HSIOM)
    #define EnoUART_EON_SS1_HSIOM_MASK     (EnoUART_EON_spi_ss1__0__HSIOM_MASK)
    #define EnoUART_EON_SS1_HSIOM_POS      (EnoUART_EON_spi_ss1__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_SS1_PIN) */

#if (EnoUART_EON_SS2_PIN)
    #define EnoUART_EON_SS2_HSIOM_REG     (*(reg32 *) EnoUART_EON_spi_ss2__0__HSIOM)
    #define EnoUART_EON_SS2_HSIOM_PTR     ( (reg32 *) EnoUART_EON_spi_ss2__0__HSIOM)
    #define EnoUART_EON_SS2_HSIOM_MASK    (EnoUART_EON_spi_ss2__0__HSIOM_MASK)
    #define EnoUART_EON_SS2_HSIOM_POS     (EnoUART_EON_spi_ss2__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_SS2_PIN) */

#if (EnoUART_EON_SS3_PIN)
    #define EnoUART_EON_SS3_HSIOM_REG     (*(reg32 *) EnoUART_EON_spi_ss3__0__HSIOM)
    #define EnoUART_EON_SS3_HSIOM_PTR     ( (reg32 *) EnoUART_EON_spi_ss3__0__HSIOM)
    #define EnoUART_EON_SS3_HSIOM_MASK    (EnoUART_EON_spi_ss3__0__HSIOM_MASK)
    #define EnoUART_EON_SS3_HSIOM_POS     (EnoUART_EON_spi_ss3__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_SS3_PIN) */

#if (EnoUART_EON_I2C_PINS)
    #define EnoUART_EON_SCL_HSIOM_REG     (*(reg32 *) EnoUART_EON_scl__0__HSIOM)
    #define EnoUART_EON_SCL_HSIOM_PTR     ( (reg32 *) EnoUART_EON_scl__0__HSIOM)
    #define EnoUART_EON_SCL_HSIOM_MASK    (EnoUART_EON_scl__0__HSIOM_MASK)
    #define EnoUART_EON_SCL_HSIOM_POS     (EnoUART_EON_scl__0__HSIOM_SHIFT)

    #define EnoUART_EON_SDA_HSIOM_REG     (*(reg32 *) EnoUART_EON_sda__0__HSIOM)
    #define EnoUART_EON_SDA_HSIOM_PTR     ( (reg32 *) EnoUART_EON_sda__0__HSIOM)
    #define EnoUART_EON_SDA_HSIOM_MASK    (EnoUART_EON_sda__0__HSIOM_MASK)
    #define EnoUART_EON_SDA_HSIOM_POS     (EnoUART_EON_sda__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_I2C_PINS) */

#if (EnoUART_EON_SPI_MASTER_SCLK_PIN)
    #define EnoUART_EON_SCLK_M_HSIOM_REG   (*(reg32 *) EnoUART_EON_sclk_m__0__HSIOM)
    #define EnoUART_EON_SCLK_M_HSIOM_PTR   ( (reg32 *) EnoUART_EON_sclk_m__0__HSIOM)
    #define EnoUART_EON_SCLK_M_HSIOM_MASK  (EnoUART_EON_sclk_m__0__HSIOM_MASK)
    #define EnoUART_EON_SCLK_M_HSIOM_POS   (EnoUART_EON_sclk_m__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_SPI_MASTER_SCLK_PIN) */

#if (EnoUART_EON_SPI_MASTER_SS0_PIN)
    #define EnoUART_EON_SS0_M_HSIOM_REG    (*(reg32 *) EnoUART_EON_ss0_m__0__HSIOM)
    #define EnoUART_EON_SS0_M_HSIOM_PTR    ( (reg32 *) EnoUART_EON_ss0_m__0__HSIOM)
    #define EnoUART_EON_SS0_M_HSIOM_MASK   (EnoUART_EON_ss0_m__0__HSIOM_MASK)
    #define EnoUART_EON_SS0_M_HSIOM_POS    (EnoUART_EON_ss0_m__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_SPI_MASTER_SS0_PIN) */

#if (EnoUART_EON_SPI_MASTER_SS1_PIN)
    #define EnoUART_EON_SS1_M_HSIOM_REG    (*(reg32 *) EnoUART_EON_ss1_m__0__HSIOM)
    #define EnoUART_EON_SS1_M_HSIOM_PTR    ( (reg32 *) EnoUART_EON_ss1_m__0__HSIOM)
    #define EnoUART_EON_SS1_M_HSIOM_MASK   (EnoUART_EON_ss1_m__0__HSIOM_MASK)
    #define EnoUART_EON_SS1_M_HSIOM_POS    (EnoUART_EON_ss1_m__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_SPI_MASTER_SS1_PIN) */

#if (EnoUART_EON_SPI_MASTER_SS2_PIN)
    #define EnoUART_EON_SS2_M_HSIOM_REG    (*(reg32 *) EnoUART_EON_ss2_m__0__HSIOM)
    #define EnoUART_EON_SS2_M_HSIOM_PTR    ( (reg32 *) EnoUART_EON_ss2_m__0__HSIOM)
    #define EnoUART_EON_SS2_M_HSIOM_MASK   (EnoUART_EON_ss2_m__0__HSIOM_MASK)
    #define EnoUART_EON_SS2_M_HSIOM_POS    (EnoUART_EON_ss2_m__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_SPI_MASTER_SS2_PIN) */

#if (EnoUART_EON_SPI_MASTER_SS3_PIN)
    #define EnoUART_EON_SS3_M_HSIOM_REG    (*(reg32 *) EnoUART_EON_ss3_m__0__HSIOM)
    #define EnoUART_EON_SS3_M_HSIOM_PTR    ( (reg32 *) EnoUART_EON_ss3_m__0__HSIOM)
    #define EnoUART_EON_SS3_M_HSIOM_MASK   (EnoUART_EON_ss3_m__0__HSIOM_MASK)
    #define EnoUART_EON_SS3_M_HSIOM_POS    (EnoUART_EON_ss3_m__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_SPI_MASTER_SS3_PIN) */

#if (EnoUART_EON_EnoUART_TX_PIN)
    #define EnoUART_EON_TX_HSIOM_REG   (*(reg32 *) EnoUART_EON_tx__0__HSIOM)
    #define EnoUART_EON_TX_HSIOM_PTR   ( (reg32 *) EnoUART_EON_tx_0__HSIOM)
    #define EnoUART_EON_TX_HSIOM_MASK  (EnoUART_EON_tx__0__HSIOM_MASK)
    #define EnoUART_EON_TX_HSIOM_POS   (EnoUART_EON_tx__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_EnoUART_TX_PIN) */

#if (EnoUART_EON_EnoUART_RTS_PIN)
    #define EnoUART_EON_RTS_HSIOM_REG  (*(reg32 *) EnoUART_EON_rts__0__HSIOM)
    #define EnoUART_EON_RTS_HSIOM_PTR  ( (reg32 *) EnoUART_EON_rts__0__HSIOM)
    #define EnoUART_EON_RTS_HSIOM_MASK (EnoUART_EON_rts__0__HSIOM_MASK)
    #define EnoUART_EON_RTS_HSIOM_POS  (EnoUART_EON_rts__0__HSIOM_SHIFT)
#endif /* (EnoUART_EON_EnoUART_RTS_PIN) */


/***************************************
*        Registers Constants
***************************************/

/* Pins constants */
#define EnoUART_EON_HSIOM_DEF_SEL      (0x00u)
#define EnoUART_EON_HSIOM_GPIO_SEL     (0x00u)
#define EnoUART_EON_HSIOM_EnoUART_SEL     (0x09u)
#define EnoUART_EON_HSIOM_I2C_SEL      (0x0Eu)
#define EnoUART_EON_HSIOM_SPI_SEL      (0x0Fu)

#define EnoUART_EON_RX_WAKE_SCL_MOSI_PIN_INDEX   (0u)
#define EnoUART_EON_RX_SCL_MOSI_PIN_INDEX       (0u)
#define EnoUART_EON_TX_SDA_MISO_PIN_INDEX       (1u)
#define EnoUART_EON_SCLK_PIN_INDEX       (2u)
#define EnoUART_EON_SS0_PIN_INDEX       (3u)
#define EnoUART_EON_SS1_PIN_INDEX                  (4u)
#define EnoUART_EON_SS2_PIN_INDEX                  (5u)
#define EnoUART_EON_SS3_PIN_INDEX                  (6u)

#define EnoUART_EON_RX_WAKE_SCL_MOSI_PIN_MASK ((uint32) 0x01u << EnoUART_EON_RX_WAKE_SCL_MOSI_PIN_INDEX)
#define EnoUART_EON_RX_SCL_MOSI_PIN_MASK     ((uint32) 0x01u << EnoUART_EON_RX_SCL_MOSI_PIN_INDEX)
#define EnoUART_EON_TX_SDA_MISO_PIN_MASK     ((uint32) 0x01u << EnoUART_EON_TX_SDA_MISO_PIN_INDEX)
#define EnoUART_EON_SCLK_PIN_MASK     ((uint32) 0x01u << EnoUART_EON_SCLK_PIN_INDEX)
#define EnoUART_EON_SS0_PIN_MASK     ((uint32) 0x01u << EnoUART_EON_SS0_PIN_INDEX)
#define EnoUART_EON_SS1_PIN_MASK                ((uint32) 0x01u << EnoUART_EON_SS1_PIN_INDEX)
#define EnoUART_EON_SS2_PIN_MASK                ((uint32) 0x01u << EnoUART_EON_SS2_PIN_INDEX)
#define EnoUART_EON_SS3_PIN_MASK                ((uint32) 0x01u << EnoUART_EON_SS3_PIN_INDEX)

/* Pin interrupt constants */
#define EnoUART_EON_INTCFG_TYPE_MASK           (0x03u)
#define EnoUART_EON_INTCFG_TYPE_FALLING_EDGE   (0x02u)

/* Pin Drive Mode constants */
#define EnoUART_EON_PIN_DM_ALG_HIZ  (0u)
#define EnoUART_EON_PIN_DM_DIG_HIZ  (1u)
#define EnoUART_EON_PIN_DM_OD_LO    (4u)
#define EnoUART_EON_PIN_DM_STRONG   (6u)


/***************************************
*          Macro Definitions
***************************************/

/* Return drive mode of the pin */
#define EnoUART_EON_DM_MASK    (0x7u)
#define EnoUART_EON_DM_SIZE    (3)
#define EnoUART_EON_GET_P4_PIN_DM(reg, pos) \
    ( ((reg) & (uint32) ((uint32) EnoUART_EON_DM_MASK << (EnoUART_EON_DM_SIZE * (pos)))) >> \
                                                              (EnoUART_EON_DM_SIZE * (pos)) )

#if (EnoUART_EON_TX_SDA_MISO_PIN)
    #define EnoUART_EON_CHECK_TX_SDA_MISO_PIN_USED \
                (EnoUART_EON_PIN_DM_ALG_HIZ != \
                    EnoUART_EON_GET_P4_PIN_DM(EnoUART_EON_EnoUART_tx_i2c_sda_spi_miso_PC, \
                                                   EnoUART_EON_EnoUART_tx_i2c_sda_spi_miso_SHIFT))
#endif /* (EnoUART_EON_TX_SDA_MISO_PIN) */

#if (EnoUART_EON_SS0_PIN)
    #define EnoUART_EON_CHECK_SS0_PIN_USED \
                (EnoUART_EON_PIN_DM_ALG_HIZ != \
                    EnoUART_EON_GET_P4_PIN_DM(EnoUART_EON_spi_ss0_PC, \
                                                   EnoUART_EON_spi_ss0_SHIFT))
#endif /* (EnoUART_EON_SS0_PIN) */

/* Set bits-mask in register */
#define EnoUART_EON_SET_REGISTER_BITS(reg, mask, pos, mode) \
                    do                                           \
                    {                                            \
                        (reg) = (((reg) & ((uint32) ~(uint32) (mask))) | ((uint32) ((uint32) (mode) << (pos)))); \
                    }while(0)

/* Set bit in the register */
#define EnoUART_EON_SET_REGISTER_BIT(reg, mask, val) \
                    ((val) ? ((reg) |= (mask)) : ((reg) &= ((uint32) ~((uint32) (mask)))))

#define EnoUART_EON_SET_HSIOM_SEL(reg, mask, pos, sel) EnoUART_EON_SET_REGISTER_BITS(reg, mask, pos, sel)
#define EnoUART_EON_SET_INCFG_TYPE(reg, mask, pos, intType) \
                                                        EnoUART_EON_SET_REGISTER_BITS(reg, mask, pos, intType)
#define EnoUART_EON_SET_INP_DIS(reg, mask, val) EnoUART_EON_SET_REGISTER_BIT(reg, mask, val)

/* EnoUART_EON_SET_I2C_SCL_DR(val) - Sets I2C SCL DR register.
*  EnoUART_EON_SET_I2C_SCL_HSIOM_SEL(sel) - Sets I2C SCL HSIOM settings.
*/
/* SCB I2C: scl signal */
#if (EnoUART_EON_CY_SCBIP_V0)
#if (EnoUART_EON_I2C_PINS)
    #define EnoUART_EON_SET_I2C_SCL_DR(val) EnoUART_EON_scl_Write(val)

    #define EnoUART_EON_SET_I2C_SCL_HSIOM_SEL(sel) \
                          EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_SCL_HSIOM_REG,  \
                                                         EnoUART_EON_SCL_HSIOM_MASK, \
                                                         EnoUART_EON_SCL_HSIOM_POS,  \
                                                         (sel))
    #define EnoUART_EON_WAIT_SCL_SET_HIGH  (0u == EnoUART_EON_scl_Read())

/* Unconfigured SCB: scl signal */
#elif (EnoUART_EON_RX_WAKE_SCL_MOSI_PIN)
    #define EnoUART_EON_SET_I2C_SCL_DR(val) \
                            EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi_Write(val)

    #define EnoUART_EON_SET_I2C_SCL_HSIOM_SEL(sel) \
                    EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_REG,  \
                                                   EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_MASK, \
                                                   EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_POS,  \
                                                   (sel))

    #define EnoUART_EON_WAIT_SCL_SET_HIGH  (0u == EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi_Read())

#elif (EnoUART_EON_RX_SCL_MOSI_PIN)
    #define EnoUART_EON_SET_I2C_SCL_DR(val) \
                            EnoUART_EON_EnoUART_rx_i2c_scl_spi_mosi_Write(val)


    #define EnoUART_EON_SET_I2C_SCL_HSIOM_SEL(sel) \
                            EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_RX_SCL_MOSI_HSIOM_REG,  \
                                                           EnoUART_EON_RX_SCL_MOSI_HSIOM_MASK, \
                                                           EnoUART_EON_RX_SCL_MOSI_HSIOM_POS,  \
                                                           (sel))

    #define EnoUART_EON_WAIT_SCL_SET_HIGH  (0u == EnoUART_EON_EnoUART_rx_i2c_scl_spi_mosi_Read())

#else
    #define EnoUART_EON_SET_I2C_SCL_DR(val) \
                                                    do{ /* Does nothing */ }while(0)
    #define EnoUART_EON_SET_I2C_SCL_HSIOM_SEL(sel) \
                                                    do{ /* Does nothing */ }while(0)

    #define EnoUART_EON_WAIT_SCL_SET_HIGH  (0u)
#endif /* (EnoUART_EON_I2C_PINS) */

/* SCB I2C: sda signal */
#if (EnoUART_EON_I2C_PINS)
    #define EnoUART_EON_WAIT_SDA_SET_HIGH  (0u == EnoUART_EON_sda_Read())
/* Unconfigured SCB: sda signal */
#elif (EnoUART_EON_TX_SDA_MISO_PIN)
    #define EnoUART_EON_WAIT_SDA_SET_HIGH  (0u == EnoUART_EON_EnoUART_tx_i2c_sda_spi_miso_Read())
#else
    #define EnoUART_EON_WAIT_SDA_SET_HIGH  (0u)
#endif /* (EnoUART_EON_MOSI_SCL_RX_PIN) */
#endif /* (EnoUART_EON_CY_SCBIP_V0) */


/***************************************
* The following code is DEPRECATED and
* must not be used.
***************************************/

/* Unconfigured pins */
#define EnoUART_EON_REMOVE_MOSI_SCL_RX_WAKE_PIN    EnoUART_EON_REMOVE_RX_WAKE_SCL_MOSI_PIN
#define EnoUART_EON_REMOVE_MOSI_SCL_RX_PIN         EnoUART_EON_REMOVE_RX_SCL_MOSI_PIN
#define EnoUART_EON_REMOVE_MISO_SDA_TX_PIN         EnoUART_EON_REMOVE_TX_SDA_MISO_PIN
#ifndef EnoUART_EON_REMOVE_SCLK_PIN
#define EnoUART_EON_REMOVE_SCLK_PIN                EnoUART_EON_REMOVE_SCLK_PIN
#endif /* EnoUART_EON_REMOVE_SCLK_PIN */
#ifndef EnoUART_EON_REMOVE_SS0_PIN
#define EnoUART_EON_REMOVE_SS0_PIN                 EnoUART_EON_REMOVE_SS0_PIN
#endif /* EnoUART_EON_REMOVE_SS0_PIN */

/* Unconfigured pins */
#define EnoUART_EON_MOSI_SCL_RX_WAKE_PIN   EnoUART_EON_RX_WAKE_SCL_MOSI_PIN
#define EnoUART_EON_MOSI_SCL_RX_PIN        EnoUART_EON_RX_SCL_MOSI_PIN
#define EnoUART_EON_MISO_SDA_TX_PIN        EnoUART_EON_TX_SDA_MISO_PIN
#ifndef EnoUART_EON_SCLK_PIN
#define EnoUART_EON_SCLK_PIN               EnoUART_EON_SCLK_PIN
#endif /* EnoUART_EON_SCLK_PIN */
#ifndef EnoUART_EON_SS0_PIN
#define EnoUART_EON_SS0_PIN                EnoUART_EON_SS0_PIN
#endif /* EnoUART_EON_SS0_PIN */

#if (EnoUART_EON_MOSI_SCL_RX_WAKE_PIN)
    #define EnoUART_EON_MOSI_SCL_RX_WAKE_HSIOM_REG     EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define EnoUART_EON_MOSI_SCL_RX_WAKE_HSIOM_PTR     EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define EnoUART_EON_MOSI_SCL_RX_WAKE_HSIOM_MASK    EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define EnoUART_EON_MOSI_SCL_RX_WAKE_HSIOM_POS     EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_REG

    #define EnoUART_EON_MOSI_SCL_RX_WAKE_INTCFG_REG    EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define EnoUART_EON_MOSI_SCL_RX_WAKE_INTCFG_PTR    EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_REG

    #define EnoUART_EON_MOSI_SCL_RX_WAKE_INTCFG_TYPE_POS   EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define EnoUART_EON_MOSI_SCL_RX_WAKE_INTCFG_TYPE_MASK  EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_REG
#endif /* (EnoUART_EON_RX_WAKE_SCL_MOSI_PIN) */

#if (EnoUART_EON_MOSI_SCL_RX_PIN)
    #define EnoUART_EON_MOSI_SCL_RX_HSIOM_REG      EnoUART_EON_RX_SCL_MOSI_HSIOM_REG
    #define EnoUART_EON_MOSI_SCL_RX_HSIOM_PTR      EnoUART_EON_RX_SCL_MOSI_HSIOM_PTR
    #define EnoUART_EON_MOSI_SCL_RX_HSIOM_MASK     EnoUART_EON_RX_SCL_MOSI_HSIOM_MASK
    #define EnoUART_EON_MOSI_SCL_RX_HSIOM_POS      EnoUART_EON_RX_SCL_MOSI_HSIOM_POS
#endif /* (EnoUART_EON_MOSI_SCL_RX_PIN) */

#if (EnoUART_EON_MISO_SDA_TX_PIN)
    #define EnoUART_EON_MISO_SDA_TX_HSIOM_REG      EnoUART_EON_TX_SDA_MISO_HSIOM_REG
    #define EnoUART_EON_MISO_SDA_TX_HSIOM_PTR      EnoUART_EON_TX_SDA_MISO_HSIOM_REG
    #define EnoUART_EON_MISO_SDA_TX_HSIOM_MASK     EnoUART_EON_TX_SDA_MISO_HSIOM_REG
    #define EnoUART_EON_MISO_SDA_TX_HSIOM_POS      EnoUART_EON_TX_SDA_MISO_HSIOM_REG
#endif /* (EnoUART_EON_MISO_SDA_TX_PIN_PIN) */

#if (EnoUART_EON_SCLK_PIN)
    #ifndef EnoUART_EON_SCLK_HSIOM_REG
    #define EnoUART_EON_SCLK_HSIOM_REG     EnoUART_EON_SCLK_HSIOM_REG
    #define EnoUART_EON_SCLK_HSIOM_PTR     EnoUART_EON_SCLK_HSIOM_PTR
    #define EnoUART_EON_SCLK_HSIOM_MASK    EnoUART_EON_SCLK_HSIOM_MASK
    #define EnoUART_EON_SCLK_HSIOM_POS     EnoUART_EON_SCLK_HSIOM_POS
    #endif /* EnoUART_EON_SCLK_HSIOM_REG */
#endif /* (EnoUART_EON_SCLK_PIN) */

#if (EnoUART_EON_SS0_PIN)
    #ifndef EnoUART_EON_SS0_HSIOM_REG
    #define EnoUART_EON_SS0_HSIOM_REG      EnoUART_EON_SS0_HSIOM_REG
    #define EnoUART_EON_SS0_HSIOM_PTR      EnoUART_EON_SS0_HSIOM_PTR
    #define EnoUART_EON_SS0_HSIOM_MASK     EnoUART_EON_SS0_HSIOM_MASK
    #define EnoUART_EON_SS0_HSIOM_POS      EnoUART_EON_SS0_HSIOM_POS
    #endif /* EnoUART_EON_SS0_HSIOM_REG */
#endif /* (EnoUART_EON_SS0_PIN) */

#define EnoUART_EON_MOSI_SCL_RX_WAKE_PIN_INDEX EnoUART_EON_RX_WAKE_SCL_MOSI_PIN_INDEX
#define EnoUART_EON_MOSI_SCL_RX_PIN_INDEX      EnoUART_EON_RX_SCL_MOSI_PIN_INDEX
#define EnoUART_EON_MISO_SDA_TX_PIN_INDEX      EnoUART_EON_TX_SDA_MISO_PIN_INDEX
#ifndef EnoUART_EON_SCLK_PIN_INDEX
#define EnoUART_EON_SCLK_PIN_INDEX             EnoUART_EON_SCLK_PIN_INDEX
#endif /* EnoUART_EON_SCLK_PIN_INDEX */
#ifndef EnoUART_EON_SS0_PIN_INDEX
#define EnoUART_EON_SS0_PIN_INDEX              EnoUART_EON_SS0_PIN_INDEX
#endif /* EnoUART_EON_SS0_PIN_INDEX */

#define EnoUART_EON_MOSI_SCL_RX_WAKE_PIN_MASK EnoUART_EON_RX_WAKE_SCL_MOSI_PIN_MASK
#define EnoUART_EON_MOSI_SCL_RX_PIN_MASK      EnoUART_EON_RX_SCL_MOSI_PIN_MASK
#define EnoUART_EON_MISO_SDA_TX_PIN_MASK      EnoUART_EON_TX_SDA_MISO_PIN_MASK
#ifndef EnoUART_EON_SCLK_PIN_MASK
#define EnoUART_EON_SCLK_PIN_MASK             EnoUART_EON_SCLK_PIN_MASK
#endif /* EnoUART_EON_SCLK_PIN_MASK */
#ifndef EnoUART_EON_SS0_PIN_MASK
#define EnoUART_EON_SS0_PIN_MASK              EnoUART_EON_SS0_PIN_MASK
#endif /* EnoUART_EON_SS0_PIN_MASK */

#endif /* (CY_SCB_PINS_EnoUART_EON_H) */


/* [] END OF FILE */
