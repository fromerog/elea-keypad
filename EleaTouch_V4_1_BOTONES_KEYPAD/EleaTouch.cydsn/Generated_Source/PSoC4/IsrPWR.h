/*******************************************************************************
* File Name: IsrPWR.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_IsrPWR_H)
#define CY_ISR_IsrPWR_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void IsrPWR_Start(void);
void IsrPWR_StartEx(cyisraddress address);
void IsrPWR_Stop(void);

CY_ISR_PROTO(IsrPWR_Interrupt);

void IsrPWR_SetVector(cyisraddress address);
cyisraddress IsrPWR_GetVector(void);

void IsrPWR_SetPriority(uint8 priority);
uint8 IsrPWR_GetPriority(void);

void IsrPWR_Enable(void);
uint8 IsrPWR_GetState(void);
void IsrPWR_Disable(void);

void IsrPWR_SetPending(void);
void IsrPWR_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the IsrPWR ISR. */
#define IsrPWR_INTC_VECTOR            ((reg32 *) IsrPWR__INTC_VECT)

/* Address of the IsrPWR ISR priority. */
#define IsrPWR_INTC_PRIOR             ((reg32 *) IsrPWR__INTC_PRIOR_REG)

/* Priority of the IsrPWR interrupt. */
#define IsrPWR_INTC_PRIOR_NUMBER      IsrPWR__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable IsrPWR interrupt. */
#define IsrPWR_INTC_SET_EN            ((reg32 *) IsrPWR__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the IsrPWR interrupt. */
#define IsrPWR_INTC_CLR_EN            ((reg32 *) IsrPWR__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the IsrPWR interrupt state to pending. */
#define IsrPWR_INTC_SET_PD            ((reg32 *) IsrPWR__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the IsrPWR interrupt. */
#define IsrPWR_INTC_CLR_PD            ((reg32 *) IsrPWR__INTC_CLR_PD_REG)



#endif /* CY_ISR_IsrPWR_H */


/* [] END OF FILE */
