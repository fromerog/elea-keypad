/***************************************************************************//**
* \file ENO_EnoUART_PINS.h
* \version 3.20
*
* \brief
*  This file provides constants and parameter values for the pin components
*  buried into SCB Component.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PINS_ENO_EnoUART_H)
#define CY_SCB_PINS_ENO_EnoUART_H

#include "cydevice_trm.h"
#include "cyfitter.h"
#include "cytypes.h"


/***************************************
*   Conditional Compilation Parameters
****************************************/

/* Unconfigured pins */
#define ENO_EnoUART_REMOVE_RX_WAKE_SCL_MOSI_PIN  (1u)
#define ENO_EnoUART_REMOVE_RX_SCL_MOSI_PIN      (1u)
#define ENO_EnoUART_REMOVE_TX_SDA_MISO_PIN      (1u)
#define ENO_EnoUART_REMOVE_SCLK_PIN      (1u)
#define ENO_EnoUART_REMOVE_SS0_PIN      (1u)
#define ENO_EnoUART_REMOVE_SS1_PIN                 (1u)
#define ENO_EnoUART_REMOVE_SS2_PIN                 (1u)
#define ENO_EnoUART_REMOVE_SS3_PIN                 (1u)

/* Mode defined pins */
#define ENO_EnoUART_REMOVE_I2C_PINS                (1u)
#define ENO_EnoUART_REMOVE_SPI_MASTER_PINS         (1u)
#define ENO_EnoUART_REMOVE_SPI_MASTER_SCLK_PIN     (1u)
#define ENO_EnoUART_REMOVE_SPI_MASTER_MOSI_PIN     (1u)
#define ENO_EnoUART_REMOVE_SPI_MASTER_MISO_PIN     (1u)
#define ENO_EnoUART_REMOVE_SPI_MASTER_SS0_PIN      (1u)
#define ENO_EnoUART_REMOVE_SPI_MASTER_SS1_PIN      (1u)
#define ENO_EnoUART_REMOVE_SPI_MASTER_SS2_PIN      (1u)
#define ENO_EnoUART_REMOVE_SPI_MASTER_SS3_PIN      (1u)
#define ENO_EnoUART_REMOVE_SPI_SLAVE_PINS          (1u)
#define ENO_EnoUART_REMOVE_SPI_SLAVE_MOSI_PIN      (1u)
#define ENO_EnoUART_REMOVE_SPI_SLAVE_MISO_PIN      (1u)
#define ENO_EnoUART_REMOVE_EnoUART_TX_PIN             (0u)
#define ENO_EnoUART_REMOVE_EnoUART_RX_TX_PIN          (1u)
#define ENO_EnoUART_REMOVE_EnoUART_RX_PIN             (0u)
#define ENO_EnoUART_REMOVE_EnoUART_RX_WAKE_PIN        (1u)
#define ENO_EnoUART_REMOVE_EnoUART_RTS_PIN            (1u)
#define ENO_EnoUART_REMOVE_EnoUART_CTS_PIN            (1u)

/* Unconfigured pins */
#define ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN (0u == ENO_EnoUART_REMOVE_RX_WAKE_SCL_MOSI_PIN)
#define ENO_EnoUART_RX_SCL_MOSI_PIN     (0u == ENO_EnoUART_REMOVE_RX_SCL_MOSI_PIN)
#define ENO_EnoUART_TX_SDA_MISO_PIN     (0u == ENO_EnoUART_REMOVE_TX_SDA_MISO_PIN)
#define ENO_EnoUART_SCLK_PIN     (0u == ENO_EnoUART_REMOVE_SCLK_PIN)
#define ENO_EnoUART_SS0_PIN     (0u == ENO_EnoUART_REMOVE_SS0_PIN)
#define ENO_EnoUART_SS1_PIN                (0u == ENO_EnoUART_REMOVE_SS1_PIN)
#define ENO_EnoUART_SS2_PIN                (0u == ENO_EnoUART_REMOVE_SS2_PIN)
#define ENO_EnoUART_SS3_PIN                (0u == ENO_EnoUART_REMOVE_SS3_PIN)

/* Mode defined pins */
#define ENO_EnoUART_I2C_PINS               (0u == ENO_EnoUART_REMOVE_I2C_PINS)
#define ENO_EnoUART_SPI_MASTER_PINS        (0u == ENO_EnoUART_REMOVE_SPI_MASTER_PINS)
#define ENO_EnoUART_SPI_MASTER_SCLK_PIN    (0u == ENO_EnoUART_REMOVE_SPI_MASTER_SCLK_PIN)
#define ENO_EnoUART_SPI_MASTER_MOSI_PIN    (0u == ENO_EnoUART_REMOVE_SPI_MASTER_MOSI_PIN)
#define ENO_EnoUART_SPI_MASTER_MISO_PIN    (0u == ENO_EnoUART_REMOVE_SPI_MASTER_MISO_PIN)
#define ENO_EnoUART_SPI_MASTER_SS0_PIN     (0u == ENO_EnoUART_REMOVE_SPI_MASTER_SS0_PIN)
#define ENO_EnoUART_SPI_MASTER_SS1_PIN     (0u == ENO_EnoUART_REMOVE_SPI_MASTER_SS1_PIN)
#define ENO_EnoUART_SPI_MASTER_SS2_PIN     (0u == ENO_EnoUART_REMOVE_SPI_MASTER_SS2_PIN)
#define ENO_EnoUART_SPI_MASTER_SS3_PIN     (0u == ENO_EnoUART_REMOVE_SPI_MASTER_SS3_PIN)
#define ENO_EnoUART_SPI_SLAVE_PINS         (0u == ENO_EnoUART_REMOVE_SPI_SLAVE_PINS)
#define ENO_EnoUART_SPI_SLAVE_MOSI_PIN     (0u == ENO_EnoUART_REMOVE_SPI_SLAVE_MOSI_PIN)
#define ENO_EnoUART_SPI_SLAVE_MISO_PIN     (0u == ENO_EnoUART_REMOVE_SPI_SLAVE_MISO_PIN)
#define ENO_EnoUART_EnoUART_TX_PIN            (0u == ENO_EnoUART_REMOVE_EnoUART_TX_PIN)
#define ENO_EnoUART_EnoUART_RX_TX_PIN         (0u == ENO_EnoUART_REMOVE_EnoUART_RX_TX_PIN)
#define ENO_EnoUART_EnoUART_RX_PIN            (0u == ENO_EnoUART_REMOVE_EnoUART_RX_PIN)
#define ENO_EnoUART_EnoUART_RX_WAKE_PIN       (0u == ENO_EnoUART_REMOVE_EnoUART_RX_WAKE_PIN)
#define ENO_EnoUART_EnoUART_RTS_PIN           (0u == ENO_EnoUART_REMOVE_EnoUART_RTS_PIN)
#define ENO_EnoUART_EnoUART_CTS_PIN           (0u == ENO_EnoUART_REMOVE_EnoUART_CTS_PIN)


/***************************************
*             Includes
****************************************/

#if (ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN)
    #include "ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi.h"
#endif /* (ENO_EnoUART_RX_SCL_MOSI) */

#if (ENO_EnoUART_RX_SCL_MOSI_PIN)
    #include "ENO_EnoUART_EnoUART_rx_i2c_scl_spi_mosi.h"
#endif /* (ENO_EnoUART_RX_SCL_MOSI) */

#if (ENO_EnoUART_TX_SDA_MISO_PIN)
    #include "ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso.h"
#endif /* (ENO_EnoUART_TX_SDA_MISO) */

#if (ENO_EnoUART_SCLK_PIN)
    #include "ENO_EnoUART_spi_sclk.h"
#endif /* (ENO_EnoUART_SCLK) */

#if (ENO_EnoUART_SS0_PIN)
    #include "ENO_EnoUART_spi_ss0.h"
#endif /* (ENO_EnoUART_SS0_PIN) */

#if (ENO_EnoUART_SS1_PIN)
    #include "ENO_EnoUART_spi_ss1.h"
#endif /* (ENO_EnoUART_SS1_PIN) */

#if (ENO_EnoUART_SS2_PIN)
    #include "ENO_EnoUART_spi_ss2.h"
#endif /* (ENO_EnoUART_SS2_PIN) */

#if (ENO_EnoUART_SS3_PIN)
    #include "ENO_EnoUART_spi_ss3.h"
#endif /* (ENO_EnoUART_SS3_PIN) */

#if (ENO_EnoUART_I2C_PINS)
    #include "ENO_EnoUART_scl.h"
    #include "ENO_EnoUART_sda.h"
#endif /* (ENO_EnoUART_I2C_PINS) */

#if (ENO_EnoUART_SPI_MASTER_PINS)
#if (ENO_EnoUART_SPI_MASTER_SCLK_PIN)
    #include "ENO_EnoUART_sclk_m.h"
#endif /* (ENO_EnoUART_SPI_MASTER_SCLK_PIN) */

#if (ENO_EnoUART_SPI_MASTER_MOSI_PIN)
    #include "ENO_EnoUART_mosi_m.h"
#endif /* (ENO_EnoUART_SPI_MASTER_MOSI_PIN) */

#if (ENO_EnoUART_SPI_MASTER_MISO_PIN)
    #include "ENO_EnoUART_miso_m.h"
#endif /*(ENO_EnoUART_SPI_MASTER_MISO_PIN) */
#endif /* (ENO_EnoUART_SPI_MASTER_PINS) */

#if (ENO_EnoUART_SPI_SLAVE_PINS)
    #include "ENO_EnoUART_sclk_s.h"
    #include "ENO_EnoUART_ss_s.h"

#if (ENO_EnoUART_SPI_SLAVE_MOSI_PIN)
    #include "ENO_EnoUART_mosi_s.h"
#endif /* (ENO_EnoUART_SPI_SLAVE_MOSI_PIN) */

#if (ENO_EnoUART_SPI_SLAVE_MISO_PIN)
    #include "ENO_EnoUART_miso_s.h"
#endif /*(ENO_EnoUART_SPI_SLAVE_MISO_PIN) */
#endif /* (ENO_EnoUART_SPI_SLAVE_PINS) */

#if (ENO_EnoUART_SPI_MASTER_SS0_PIN)
    #include "ENO_EnoUART_ss0_m.h"
#endif /* (ENO_EnoUART_SPI_MASTER_SS0_PIN) */

#if (ENO_EnoUART_SPI_MASTER_SS1_PIN)
    #include "ENO_EnoUART_ss1_m.h"
#endif /* (ENO_EnoUART_SPI_MASTER_SS1_PIN) */

#if (ENO_EnoUART_SPI_MASTER_SS2_PIN)
    #include "ENO_EnoUART_ss2_m.h"
#endif /* (ENO_EnoUART_SPI_MASTER_SS2_PIN) */

#if (ENO_EnoUART_SPI_MASTER_SS3_PIN)
    #include "ENO_EnoUART_ss3_m.h"
#endif /* (ENO_EnoUART_SPI_MASTER_SS3_PIN) */

#if (ENO_EnoUART_EnoUART_TX_PIN)
    #include "ENO_EnoUART_tx.h"
#endif /* (ENO_EnoUART_EnoUART_TX_PIN) */

#if (ENO_EnoUART_EnoUART_RX_TX_PIN)
    #include "ENO_EnoUART_rx_tx.h"
#endif /* (ENO_EnoUART_EnoUART_RX_TX_PIN) */

#if (ENO_EnoUART_EnoUART_RX_PIN)
    #include "ENO_EnoUART_rx.h"
#endif /* (ENO_EnoUART_EnoUART_RX_PIN) */

#if (ENO_EnoUART_EnoUART_RX_WAKE_PIN)
    #include "ENO_EnoUART_rx_wake.h"
#endif /* (ENO_EnoUART_EnoUART_RX_WAKE_PIN) */

#if (ENO_EnoUART_EnoUART_RTS_PIN)
    #include "ENO_EnoUART_rts.h"
#endif /* (ENO_EnoUART_EnoUART_RTS_PIN) */

#if (ENO_EnoUART_EnoUART_CTS_PIN)
    #include "ENO_EnoUART_cts.h"
#endif /* (ENO_EnoUART_EnoUART_CTS_PIN) */


/***************************************
*              Registers
***************************************/

#if (ENO_EnoUART_RX_SCL_MOSI_PIN)
    #define ENO_EnoUART_RX_SCL_MOSI_HSIOM_REG   (*(reg32 *) ENO_EnoUART_EnoUART_rx_i2c_scl_spi_mosi__0__HSIOM)
    #define ENO_EnoUART_RX_SCL_MOSI_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_EnoUART_rx_i2c_scl_spi_mosi__0__HSIOM)
    
    #define ENO_EnoUART_RX_SCL_MOSI_HSIOM_MASK      (ENO_EnoUART_EnoUART_rx_i2c_scl_spi_mosi__0__HSIOM_MASK)
    #define ENO_EnoUART_RX_SCL_MOSI_HSIOM_POS       (ENO_EnoUART_EnoUART_rx_i2c_scl_spi_mosi__0__HSIOM_SHIFT)
    #define ENO_EnoUART_RX_SCL_MOSI_HSIOM_SEL_GPIO  (ENO_EnoUART_EnoUART_rx_i2c_scl_spi_mosi__0__HSIOM_GPIO)
    #define ENO_EnoUART_RX_SCL_MOSI_HSIOM_SEL_I2C   (ENO_EnoUART_EnoUART_rx_i2c_scl_spi_mosi__0__HSIOM_I2C)
    #define ENO_EnoUART_RX_SCL_MOSI_HSIOM_SEL_SPI   (ENO_EnoUART_EnoUART_rx_i2c_scl_spi_mosi__0__HSIOM_SPI)
    #define ENO_EnoUART_RX_SCL_MOSI_HSIOM_SEL_EnoUART  (ENO_EnoUART_EnoUART_rx_i2c_scl_spi_mosi__0__HSIOM_EnoUART)
    
#elif (ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN)
    #define ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG   (*(reg32 *) ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi__0__HSIOM)
    #define ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi__0__HSIOM)
    
    #define ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_MASK      (ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi__0__HSIOM_MASK)
    #define ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_POS       (ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi__0__HSIOM_SHIFT)
    #define ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_SEL_GPIO  (ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi__0__HSIOM_GPIO)
    #define ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_SEL_I2C   (ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi__0__HSIOM_I2C)
    #define ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_SEL_SPI   (ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi__0__HSIOM_SPI)
    #define ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_SEL_EnoUART  (ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi__0__HSIOM_EnoUART)    
   
    #define ENO_EnoUART_RX_WAKE_SCL_MOSI_INTCFG_REG (*(reg32 *) ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi__0__INTCFG)
    #define ENO_EnoUART_RX_WAKE_SCL_MOSI_INTCFG_PTR ( (reg32 *) ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi__0__INTCFG)
    #define ENO_EnoUART_RX_WAKE_SCL_MOSI_INTCFG_TYPE_POS  (ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi__SHIFT)
    #define ENO_EnoUART_RX_WAKE_SCL_MOSI_INTCFG_TYPE_MASK ((uint32) ENO_EnoUART_INTCFG_TYPE_MASK << \
                                                                           ENO_EnoUART_RX_WAKE_SCL_MOSI_INTCFG_TYPE_POS)
#else
    /* None of pins ENO_EnoUART_RX_SCL_MOSI_PIN or ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN present.*/
#endif /* (ENO_EnoUART_RX_SCL_MOSI_PIN) */

#if (ENO_EnoUART_TX_SDA_MISO_PIN)
    #define ENO_EnoUART_TX_SDA_MISO_HSIOM_REG   (*(reg32 *) ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso__0__HSIOM)
    #define ENO_EnoUART_TX_SDA_MISO_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso__0__HSIOM)
    
    #define ENO_EnoUART_TX_SDA_MISO_HSIOM_MASK      (ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso__0__HSIOM_MASK)
    #define ENO_EnoUART_TX_SDA_MISO_HSIOM_POS       (ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso__0__HSIOM_SHIFT)
    #define ENO_EnoUART_TX_SDA_MISO_HSIOM_SEL_GPIO  (ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso__0__HSIOM_GPIO)
    #define ENO_EnoUART_TX_SDA_MISO_HSIOM_SEL_I2C   (ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso__0__HSIOM_I2C)
    #define ENO_EnoUART_TX_SDA_MISO_HSIOM_SEL_SPI   (ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso__0__HSIOM_SPI)
    #define ENO_EnoUART_TX_SDA_MISO_HSIOM_SEL_EnoUART  (ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso__0__HSIOM_EnoUART)
#endif /* (ENO_EnoUART_TX_SDA_MISO_PIN) */

#if (ENO_EnoUART_SCLK_PIN)
    #define ENO_EnoUART_SCLK_HSIOM_REG   (*(reg32 *) ENO_EnoUART_spi_sclk__0__HSIOM)
    #define ENO_EnoUART_SCLK_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_spi_sclk__0__HSIOM)
    
    #define ENO_EnoUART_SCLK_HSIOM_MASK      (ENO_EnoUART_spi_sclk__0__HSIOM_MASK)
    #define ENO_EnoUART_SCLK_HSIOM_POS       (ENO_EnoUART_spi_sclk__0__HSIOM_SHIFT)
    #define ENO_EnoUART_SCLK_HSIOM_SEL_GPIO  (ENO_EnoUART_spi_sclk__0__HSIOM_GPIO)
    #define ENO_EnoUART_SCLK_HSIOM_SEL_I2C   (ENO_EnoUART_spi_sclk__0__HSIOM_I2C)
    #define ENO_EnoUART_SCLK_HSIOM_SEL_SPI   (ENO_EnoUART_spi_sclk__0__HSIOM_SPI)
    #define ENO_EnoUART_SCLK_HSIOM_SEL_EnoUART  (ENO_EnoUART_spi_sclk__0__HSIOM_EnoUART)
#endif /* (ENO_EnoUART_SCLK_PIN) */

#if (ENO_EnoUART_SS0_PIN)
    #define ENO_EnoUART_SS0_HSIOM_REG   (*(reg32 *) ENO_EnoUART_spi_ss0__0__HSIOM)
    #define ENO_EnoUART_SS0_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_spi_ss0__0__HSIOM)
    
    #define ENO_EnoUART_SS0_HSIOM_MASK      (ENO_EnoUART_spi_ss0__0__HSIOM_MASK)
    #define ENO_EnoUART_SS0_HSIOM_POS       (ENO_EnoUART_spi_ss0__0__HSIOM_SHIFT)
    #define ENO_EnoUART_SS0_HSIOM_SEL_GPIO  (ENO_EnoUART_spi_ss0__0__HSIOM_GPIO)
    #define ENO_EnoUART_SS0_HSIOM_SEL_I2C   (ENO_EnoUART_spi_ss0__0__HSIOM_I2C)
    #define ENO_EnoUART_SS0_HSIOM_SEL_SPI   (ENO_EnoUART_spi_ss0__0__HSIOM_SPI)
#if !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1)
    #define ENO_EnoUART_SS0_HSIOM_SEL_EnoUART  (ENO_EnoUART_spi_ss0__0__HSIOM_EnoUART)
#endif /* !(ENO_EnoUART_CY_SCBIP_V0 || ENO_EnoUART_CY_SCBIP_V1) */
#endif /* (ENO_EnoUART_SS0_PIN) */

#if (ENO_EnoUART_SS1_PIN)
    #define ENO_EnoUART_SS1_HSIOM_REG  (*(reg32 *) ENO_EnoUART_spi_ss1__0__HSIOM)
    #define ENO_EnoUART_SS1_HSIOM_PTR  ( (reg32 *) ENO_EnoUART_spi_ss1__0__HSIOM)
    
    #define ENO_EnoUART_SS1_HSIOM_MASK     (ENO_EnoUART_spi_ss1__0__HSIOM_MASK)
    #define ENO_EnoUART_SS1_HSIOM_POS      (ENO_EnoUART_spi_ss1__0__HSIOM_SHIFT)
    #define ENO_EnoUART_SS1_HSIOM_SEL_GPIO (ENO_EnoUART_spi_ss1__0__HSIOM_GPIO)
    #define ENO_EnoUART_SS1_HSIOM_SEL_I2C  (ENO_EnoUART_spi_ss1__0__HSIOM_I2C)
    #define ENO_EnoUART_SS1_HSIOM_SEL_SPI  (ENO_EnoUART_spi_ss1__0__HSIOM_SPI)
#endif /* (ENO_EnoUART_SS1_PIN) */

#if (ENO_EnoUART_SS2_PIN)
    #define ENO_EnoUART_SS2_HSIOM_REG     (*(reg32 *) ENO_EnoUART_spi_ss2__0__HSIOM)
    #define ENO_EnoUART_SS2_HSIOM_PTR     ( (reg32 *) ENO_EnoUART_spi_ss2__0__HSIOM)
    
    #define ENO_EnoUART_SS2_HSIOM_MASK     (ENO_EnoUART_spi_ss2__0__HSIOM_MASK)
    #define ENO_EnoUART_SS2_HSIOM_POS      (ENO_EnoUART_spi_ss2__0__HSIOM_SHIFT)
    #define ENO_EnoUART_SS2_HSIOM_SEL_GPIO (ENO_EnoUART_spi_ss2__0__HSIOM_GPIO)
    #define ENO_EnoUART_SS2_HSIOM_SEL_I2C  (ENO_EnoUART_spi_ss2__0__HSIOM_I2C)
    #define ENO_EnoUART_SS2_HSIOM_SEL_SPI  (ENO_EnoUART_spi_ss2__0__HSIOM_SPI)
#endif /* (ENO_EnoUART_SS2_PIN) */

#if (ENO_EnoUART_SS3_PIN)
    #define ENO_EnoUART_SS3_HSIOM_REG     (*(reg32 *) ENO_EnoUART_spi_ss3__0__HSIOM)
    #define ENO_EnoUART_SS3_HSIOM_PTR     ( (reg32 *) ENO_EnoUART_spi_ss3__0__HSIOM)
    
    #define ENO_EnoUART_SS3_HSIOM_MASK     (ENO_EnoUART_spi_ss3__0__HSIOM_MASK)
    #define ENO_EnoUART_SS3_HSIOM_POS      (ENO_EnoUART_spi_ss3__0__HSIOM_SHIFT)
    #define ENO_EnoUART_SS3_HSIOM_SEL_GPIO (ENO_EnoUART_spi_ss3__0__HSIOM_GPIO)
    #define ENO_EnoUART_SS3_HSIOM_SEL_I2C  (ENO_EnoUART_spi_ss3__0__HSIOM_I2C)
    #define ENO_EnoUART_SS3_HSIOM_SEL_SPI  (ENO_EnoUART_spi_ss3__0__HSIOM_SPI)
#endif /* (ENO_EnoUART_SS3_PIN) */

#if (ENO_EnoUART_I2C_PINS)
    #define ENO_EnoUART_SCL_HSIOM_REG  (*(reg32 *) ENO_EnoUART_scl__0__HSIOM)
    #define ENO_EnoUART_SCL_HSIOM_PTR  ( (reg32 *) ENO_EnoUART_scl__0__HSIOM)
    
    #define ENO_EnoUART_SCL_HSIOM_MASK     (ENO_EnoUART_scl__0__HSIOM_MASK)
    #define ENO_EnoUART_SCL_HSIOM_POS      (ENO_EnoUART_scl__0__HSIOM_SHIFT)
    #define ENO_EnoUART_SCL_HSIOM_SEL_GPIO (ENO_EnoUART_sda__0__HSIOM_GPIO)
    #define ENO_EnoUART_SCL_HSIOM_SEL_I2C  (ENO_EnoUART_sda__0__HSIOM_I2C)
    
    #define ENO_EnoUART_SDA_HSIOM_REG  (*(reg32 *) ENO_EnoUART_sda__0__HSIOM)
    #define ENO_EnoUART_SDA_HSIOM_PTR  ( (reg32 *) ENO_EnoUART_sda__0__HSIOM)
    
    #define ENO_EnoUART_SDA_HSIOM_MASK     (ENO_EnoUART_sda__0__HSIOM_MASK)
    #define ENO_EnoUART_SDA_HSIOM_POS      (ENO_EnoUART_sda__0__HSIOM_SHIFT)
    #define ENO_EnoUART_SDA_HSIOM_SEL_GPIO (ENO_EnoUART_sda__0__HSIOM_GPIO)
    #define ENO_EnoUART_SDA_HSIOM_SEL_I2C  (ENO_EnoUART_sda__0__HSIOM_I2C)
#endif /* (ENO_EnoUART_I2C_PINS) */

#if (ENO_EnoUART_SPI_SLAVE_PINS)
    #define ENO_EnoUART_SCLK_S_HSIOM_REG   (*(reg32 *) ENO_EnoUART_sclk_s__0__HSIOM)
    #define ENO_EnoUART_SCLK_S_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_sclk_s__0__HSIOM)
    
    #define ENO_EnoUART_SCLK_S_HSIOM_MASK      (ENO_EnoUART_sclk_s__0__HSIOM_MASK)
    #define ENO_EnoUART_SCLK_S_HSIOM_POS       (ENO_EnoUART_sclk_s__0__HSIOM_SHIFT)
    #define ENO_EnoUART_SCLK_S_HSIOM_SEL_GPIO  (ENO_EnoUART_sclk_s__0__HSIOM_GPIO)
    #define ENO_EnoUART_SCLK_S_HSIOM_SEL_SPI   (ENO_EnoUART_sclk_s__0__HSIOM_SPI)
    
    #define ENO_EnoUART_SS0_S_HSIOM_REG    (*(reg32 *) ENO_EnoUART_ss0_s__0__HSIOM)
    #define ENO_EnoUART_SS0_S_HSIOM_PTR    ( (reg32 *) ENO_EnoUART_ss0_s__0__HSIOM)
    
    #define ENO_EnoUART_SS0_S_HSIOM_MASK       (ENO_EnoUART_ss0_s__0__HSIOM_MASK)
    #define ENO_EnoUART_SS0_S_HSIOM_POS        (ENO_EnoUART_ss0_s__0__HSIOM_SHIFT)
    #define ENO_EnoUART_SS0_S_HSIOM_SEL_GPIO   (ENO_EnoUART_ss0_s__0__HSIOM_GPIO)  
    #define ENO_EnoUART_SS0_S_HSIOM_SEL_SPI    (ENO_EnoUART_ss0_s__0__HSIOM_SPI)
#endif /* (ENO_EnoUART_SPI_SLAVE_PINS) */

#if (ENO_EnoUART_SPI_SLAVE_MOSI_PIN)
    #define ENO_EnoUART_MOSI_S_HSIOM_REG   (*(reg32 *) ENO_EnoUART_mosi_s__0__HSIOM)
    #define ENO_EnoUART_MOSI_S_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_mosi_s__0__HSIOM)
    
    #define ENO_EnoUART_MOSI_S_HSIOM_MASK      (ENO_EnoUART_mosi_s__0__HSIOM_MASK)
    #define ENO_EnoUART_MOSI_S_HSIOM_POS       (ENO_EnoUART_mosi_s__0__HSIOM_SHIFT)
    #define ENO_EnoUART_MOSI_S_HSIOM_SEL_GPIO  (ENO_EnoUART_mosi_s__0__HSIOM_GPIO)
    #define ENO_EnoUART_MOSI_S_HSIOM_SEL_SPI   (ENO_EnoUART_mosi_s__0__HSIOM_SPI)
#endif /* (ENO_EnoUART_SPI_SLAVE_MOSI_PIN) */

#if (ENO_EnoUART_SPI_SLAVE_MISO_PIN)
    #define ENO_EnoUART_MISO_S_HSIOM_REG   (*(reg32 *) ENO_EnoUART_miso_s__0__HSIOM)
    #define ENO_EnoUART_MISO_S_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_miso_s__0__HSIOM)
    
    #define ENO_EnoUART_MISO_S_HSIOM_MASK      (ENO_EnoUART_miso_s__0__HSIOM_MASK)
    #define ENO_EnoUART_MISO_S_HSIOM_POS       (ENO_EnoUART_miso_s__0__HSIOM_SHIFT)
    #define ENO_EnoUART_MISO_S_HSIOM_SEL_GPIO  (ENO_EnoUART_miso_s__0__HSIOM_GPIO)
    #define ENO_EnoUART_MISO_S_HSIOM_SEL_SPI   (ENO_EnoUART_miso_s__0__HSIOM_SPI)
#endif /* (ENO_EnoUART_SPI_SLAVE_MISO_PIN) */

#if (ENO_EnoUART_SPI_MASTER_MISO_PIN)
    #define ENO_EnoUART_MISO_M_HSIOM_REG   (*(reg32 *) ENO_EnoUART_miso_m__0__HSIOM)
    #define ENO_EnoUART_MISO_M_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_miso_m__0__HSIOM)
    
    #define ENO_EnoUART_MISO_M_HSIOM_MASK      (ENO_EnoUART_miso_m__0__HSIOM_MASK)
    #define ENO_EnoUART_MISO_M_HSIOM_POS       (ENO_EnoUART_miso_m__0__HSIOM_SHIFT)
    #define ENO_EnoUART_MISO_M_HSIOM_SEL_GPIO  (ENO_EnoUART_miso_m__0__HSIOM_GPIO)
    #define ENO_EnoUART_MISO_M_HSIOM_SEL_SPI   (ENO_EnoUART_miso_m__0__HSIOM_SPI)
#endif /* (ENO_EnoUART_SPI_MASTER_MISO_PIN) */

#if (ENO_EnoUART_SPI_MASTER_MOSI_PIN)
    #define ENO_EnoUART_MOSI_M_HSIOM_REG   (*(reg32 *) ENO_EnoUART_mosi_m__0__HSIOM)
    #define ENO_EnoUART_MOSI_M_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_mosi_m__0__HSIOM)
    
    #define ENO_EnoUART_MOSI_M_HSIOM_MASK      (ENO_EnoUART_mosi_m__0__HSIOM_MASK)
    #define ENO_EnoUART_MOSI_M_HSIOM_POS       (ENO_EnoUART_mosi_m__0__HSIOM_SHIFT)
    #define ENO_EnoUART_MOSI_M_HSIOM_SEL_GPIO  (ENO_EnoUART_mosi_m__0__HSIOM_GPIO)
    #define ENO_EnoUART_MOSI_M_HSIOM_SEL_SPI   (ENO_EnoUART_mosi_m__0__HSIOM_SPI)
#endif /* (ENO_EnoUART_SPI_MASTER_MOSI_PIN) */

#if (ENO_EnoUART_SPI_MASTER_SCLK_PIN)
    #define ENO_EnoUART_SCLK_M_HSIOM_REG   (*(reg32 *) ENO_EnoUART_sclk_m__0__HSIOM)
    #define ENO_EnoUART_SCLK_M_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_sclk_m__0__HSIOM)
    
    #define ENO_EnoUART_SCLK_M_HSIOM_MASK      (ENO_EnoUART_sclk_m__0__HSIOM_MASK)
    #define ENO_EnoUART_SCLK_M_HSIOM_POS       (ENO_EnoUART_sclk_m__0__HSIOM_SHIFT)
    #define ENO_EnoUART_SCLK_M_HSIOM_SEL_GPIO  (ENO_EnoUART_sclk_m__0__HSIOM_GPIO)
    #define ENO_EnoUART_SCLK_M_HSIOM_SEL_SPI   (ENO_EnoUART_sclk_m__0__HSIOM_SPI)
#endif /* (ENO_EnoUART_SPI_MASTER_SCLK_PIN) */

#if (ENO_EnoUART_SPI_MASTER_SS0_PIN)
    #define ENO_EnoUART_SS0_M_HSIOM_REG    (*(reg32 *) ENO_EnoUART_ss0_m__0__HSIOM)
    #define ENO_EnoUART_SS0_M_HSIOM_PTR    ( (reg32 *) ENO_EnoUART_ss0_m__0__HSIOM)
    
    #define ENO_EnoUART_SS0_M_HSIOM_MASK       (ENO_EnoUART_ss0_m__0__HSIOM_MASK)
    #define ENO_EnoUART_SS0_M_HSIOM_POS        (ENO_EnoUART_ss0_m__0__HSIOM_SHIFT)
    #define ENO_EnoUART_SS0_M_HSIOM_SEL_GPIO   (ENO_EnoUART_ss0_m__0__HSIOM_GPIO)
    #define ENO_EnoUART_SS0_M_HSIOM_SEL_SPI    (ENO_EnoUART_ss0_m__0__HSIOM_SPI)
#endif /* (ENO_EnoUART_SPI_MASTER_SS0_PIN) */

#if (ENO_EnoUART_SPI_MASTER_SS1_PIN)
    #define ENO_EnoUART_SS1_M_HSIOM_REG    (*(reg32 *) ENO_EnoUART_ss1_m__0__HSIOM)
    #define ENO_EnoUART_SS1_M_HSIOM_PTR    ( (reg32 *) ENO_EnoUART_ss1_m__0__HSIOM)
    
    #define ENO_EnoUART_SS1_M_HSIOM_MASK       (ENO_EnoUART_ss1_m__0__HSIOM_MASK)
    #define ENO_EnoUART_SS1_M_HSIOM_POS        (ENO_EnoUART_ss1_m__0__HSIOM_SHIFT)
    #define ENO_EnoUART_SS1_M_HSIOM_SEL_GPIO   (ENO_EnoUART_ss1_m__0__HSIOM_GPIO)
    #define ENO_EnoUART_SS1_M_HSIOM_SEL_SPI    (ENO_EnoUART_ss1_m__0__HSIOM_SPI)
#endif /* (ENO_EnoUART_SPI_MASTER_SS1_PIN) */

#if (ENO_EnoUART_SPI_MASTER_SS2_PIN)
    #define ENO_EnoUART_SS2_M_HSIOM_REG    (*(reg32 *) ENO_EnoUART_ss2_m__0__HSIOM)
    #define ENO_EnoUART_SS2_M_HSIOM_PTR    ( (reg32 *) ENO_EnoUART_ss2_m__0__HSIOM)
    
    #define ENO_EnoUART_SS2_M_HSIOM_MASK       (ENO_EnoUART_ss2_m__0__HSIOM_MASK)
    #define ENO_EnoUART_SS2_M_HSIOM_POS        (ENO_EnoUART_ss2_m__0__HSIOM_SHIFT)
    #define ENO_EnoUART_SS2_M_HSIOM_SEL_GPIO   (ENO_EnoUART_ss2_m__0__HSIOM_GPIO)
    #define ENO_EnoUART_SS2_M_HSIOM_SEL_SPI    (ENO_EnoUART_ss2_m__0__HSIOM_SPI)
#endif /* (ENO_EnoUART_SPI_MASTER_SS2_PIN) */

#if (ENO_EnoUART_SPI_MASTER_SS3_PIN)
    #define ENO_EnoUART_SS3_M_HSIOM_REG    (*(reg32 *) ENO_EnoUART_ss3_m__0__HSIOM)
    #define ENO_EnoUART_SS3_M_HSIOM_PTR    ( (reg32 *) ENO_EnoUART_ss3_m__0__HSIOM)
    
    #define ENO_EnoUART_SS3_M_HSIOM_MASK      (ENO_EnoUART_ss3_m__0__HSIOM_MASK)
    #define ENO_EnoUART_SS3_M_HSIOM_POS       (ENO_EnoUART_ss3_m__0__HSIOM_SHIFT)
    #define ENO_EnoUART_SS3_M_HSIOM_SEL_GPIO  (ENO_EnoUART_ss3_m__0__HSIOM_GPIO)
    #define ENO_EnoUART_SS3_M_HSIOM_SEL_SPI   (ENO_EnoUART_ss3_m__0__HSIOM_SPI)
#endif /* (ENO_EnoUART_SPI_MASTER_SS3_PIN) */

#if (ENO_EnoUART_EnoUART_RX_PIN)
    #define ENO_EnoUART_RX_HSIOM_REG   (*(reg32 *) ENO_EnoUART_rx__0__HSIOM)
    #define ENO_EnoUART_RX_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_rx__0__HSIOM)
    
    #define ENO_EnoUART_RX_HSIOM_MASK      (ENO_EnoUART_rx__0__HSIOM_MASK)
    #define ENO_EnoUART_RX_HSIOM_POS       (ENO_EnoUART_rx__0__HSIOM_SHIFT)
    #define ENO_EnoUART_RX_HSIOM_SEL_GPIO  (ENO_EnoUART_rx__0__HSIOM_GPIO)
    #define ENO_EnoUART_RX_HSIOM_SEL_EnoUART  (ENO_EnoUART_rx__0__HSIOM_EnoUART)
#endif /* (ENO_EnoUART_EnoUART_RX_PIN) */

#if (ENO_EnoUART_EnoUART_RX_WAKE_PIN)
    #define ENO_EnoUART_RX_WAKE_HSIOM_REG   (*(reg32 *) ENO_EnoUART_rx_wake__0__HSIOM)
    #define ENO_EnoUART_RX_WAKE_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_rx_wake__0__HSIOM)
    
    #define ENO_EnoUART_RX_WAKE_HSIOM_MASK      (ENO_EnoUART_rx_wake__0__HSIOM_MASK)
    #define ENO_EnoUART_RX_WAKE_HSIOM_POS       (ENO_EnoUART_rx_wake__0__HSIOM_SHIFT)
    #define ENO_EnoUART_RX_WAKE_HSIOM_SEL_GPIO  (ENO_EnoUART_rx_wake__0__HSIOM_GPIO)
    #define ENO_EnoUART_RX_WAKE_HSIOM_SEL_EnoUART  (ENO_EnoUART_rx_wake__0__HSIOM_EnoUART)
#endif /* (ENO_EnoUART_EnoUART_WAKE_RX_PIN) */

#if (ENO_EnoUART_EnoUART_CTS_PIN)
    #define ENO_EnoUART_CTS_HSIOM_REG   (*(reg32 *) ENO_EnoUART_cts__0__HSIOM)
    #define ENO_EnoUART_CTS_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_cts__0__HSIOM)
    
    #define ENO_EnoUART_CTS_HSIOM_MASK      (ENO_EnoUART_cts__0__HSIOM_MASK)
    #define ENO_EnoUART_CTS_HSIOM_POS       (ENO_EnoUART_cts__0__HSIOM_SHIFT)
    #define ENO_EnoUART_CTS_HSIOM_SEL_GPIO  (ENO_EnoUART_cts__0__HSIOM_GPIO)
    #define ENO_EnoUART_CTS_HSIOM_SEL_EnoUART  (ENO_EnoUART_cts__0__HSIOM_EnoUART)
#endif /* (ENO_EnoUART_EnoUART_CTS_PIN) */

#if (ENO_EnoUART_EnoUART_TX_PIN)
    #define ENO_EnoUART_TX_HSIOM_REG   (*(reg32 *) ENO_EnoUART_tx__0__HSIOM)
    #define ENO_EnoUART_TX_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_tx__0__HSIOM)
    
    #define ENO_EnoUART_TX_HSIOM_MASK      (ENO_EnoUART_tx__0__HSIOM_MASK)
    #define ENO_EnoUART_TX_HSIOM_POS       (ENO_EnoUART_tx__0__HSIOM_SHIFT)
    #define ENO_EnoUART_TX_HSIOM_SEL_GPIO  (ENO_EnoUART_tx__0__HSIOM_GPIO)
    #define ENO_EnoUART_TX_HSIOM_SEL_EnoUART  (ENO_EnoUART_tx__0__HSIOM_EnoUART)
#endif /* (ENO_EnoUART_EnoUART_TX_PIN) */

#if (ENO_EnoUART_EnoUART_RX_TX_PIN)
    #define ENO_EnoUART_RX_TX_HSIOM_REG   (*(reg32 *) ENO_EnoUART_rx_tx__0__HSIOM)
    #define ENO_EnoUART_RX_TX_HSIOM_PTR   ( (reg32 *) ENO_EnoUART_rx_tx__0__HSIOM)
    
    #define ENO_EnoUART_RX_TX_HSIOM_MASK      (ENO_EnoUART_rx_tx__0__HSIOM_MASK)
    #define ENO_EnoUART_RX_TX_HSIOM_POS       (ENO_EnoUART_rx_tx__0__HSIOM_SHIFT)
    #define ENO_EnoUART_RX_TX_HSIOM_SEL_GPIO  (ENO_EnoUART_rx_tx__0__HSIOM_GPIO)
    #define ENO_EnoUART_RX_TX_HSIOM_SEL_EnoUART  (ENO_EnoUART_rx_tx__0__HSIOM_EnoUART)
#endif /* (ENO_EnoUART_EnoUART_RX_TX_PIN) */

#if (ENO_EnoUART_EnoUART_RTS_PIN)
    #define ENO_EnoUART_RTS_HSIOM_REG      (*(reg32 *) ENO_EnoUART_rts__0__HSIOM)
    #define ENO_EnoUART_RTS_HSIOM_PTR      ( (reg32 *) ENO_EnoUART_rts__0__HSIOM)
    
    #define ENO_EnoUART_RTS_HSIOM_MASK     (ENO_EnoUART_rts__0__HSIOM_MASK)
    #define ENO_EnoUART_RTS_HSIOM_POS      (ENO_EnoUART_rts__0__HSIOM_SHIFT)    
    #define ENO_EnoUART_RTS_HSIOM_SEL_GPIO (ENO_EnoUART_rts__0__HSIOM_GPIO)
    #define ENO_EnoUART_RTS_HSIOM_SEL_EnoUART (ENO_EnoUART_rts__0__HSIOM_EnoUART)    
#endif /* (ENO_EnoUART_EnoUART_RTS_PIN) */


/***************************************
*        Registers Constants
***************************************/

/* HSIOM switch values. */ 
#define ENO_EnoUART_HSIOM_DEF_SEL      (0x00u)
#define ENO_EnoUART_HSIOM_GPIO_SEL     (0x00u)
/* The HSIOM values provided below are valid only for ENO_EnoUART_CY_SCBIP_V0 
* and ENO_EnoUART_CY_SCBIP_V1. It is not recommended to use them for 
* ENO_EnoUART_CY_SCBIP_V2. Use pin name specific HSIOM constants provided 
* above instead for any SCB IP block version.
*/
#define ENO_EnoUART_HSIOM_EnoUART_SEL     (0x09u)
#define ENO_EnoUART_HSIOM_I2C_SEL      (0x0Eu)
#define ENO_EnoUART_HSIOM_SPI_SEL      (0x0Fu)

/* Pins settings index. */
#define ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN_INDEX   (0u)
#define ENO_EnoUART_RX_SCL_MOSI_PIN_INDEX       (0u)
#define ENO_EnoUART_TX_SDA_MISO_PIN_INDEX       (1u)
#define ENO_EnoUART_SCLK_PIN_INDEX       (2u)
#define ENO_EnoUART_SS0_PIN_INDEX       (3u)
#define ENO_EnoUART_SS1_PIN_INDEX                  (4u)
#define ENO_EnoUART_SS2_PIN_INDEX                  (5u)
#define ENO_EnoUART_SS3_PIN_INDEX                  (6u)

/* Pins settings mask. */
#define ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN_MASK ((uint32) 0x01u << ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN_INDEX)
#define ENO_EnoUART_RX_SCL_MOSI_PIN_MASK     ((uint32) 0x01u << ENO_EnoUART_RX_SCL_MOSI_PIN_INDEX)
#define ENO_EnoUART_TX_SDA_MISO_PIN_MASK     ((uint32) 0x01u << ENO_EnoUART_TX_SDA_MISO_PIN_INDEX)
#define ENO_EnoUART_SCLK_PIN_MASK     ((uint32) 0x01u << ENO_EnoUART_SCLK_PIN_INDEX)
#define ENO_EnoUART_SS0_PIN_MASK     ((uint32) 0x01u << ENO_EnoUART_SS0_PIN_INDEX)
#define ENO_EnoUART_SS1_PIN_MASK                ((uint32) 0x01u << ENO_EnoUART_SS1_PIN_INDEX)
#define ENO_EnoUART_SS2_PIN_MASK                ((uint32) 0x01u << ENO_EnoUART_SS2_PIN_INDEX)
#define ENO_EnoUART_SS3_PIN_MASK                ((uint32) 0x01u << ENO_EnoUART_SS3_PIN_INDEX)

/* Pin interrupt constants. */
#define ENO_EnoUART_INTCFG_TYPE_MASK           (0x03u)
#define ENO_EnoUART_INTCFG_TYPE_FALLING_EDGE   (0x02u)

/* Pin Drive Mode constants. */
#define ENO_EnoUART_PIN_DM_ALG_HIZ  (0u)
#define ENO_EnoUART_PIN_DM_DIG_HIZ  (1u)
#define ENO_EnoUART_PIN_DM_OD_LO    (4u)
#define ENO_EnoUART_PIN_DM_STRONG   (6u)


/***************************************
*          Macro Definitions
***************************************/

/* Return drive mode of the pin */
#define ENO_EnoUART_DM_MASK    (0x7u)
#define ENO_EnoUART_DM_SIZE    (3u)
#define ENO_EnoUART_GET_P4_PIN_DM(reg, pos) \
    ( ((reg) & (uint32) ((uint32) ENO_EnoUART_DM_MASK << (ENO_EnoUART_DM_SIZE * (pos)))) >> \
                                                              (ENO_EnoUART_DM_SIZE * (pos)) )

#if (ENO_EnoUART_TX_SDA_MISO_PIN)
    #define ENO_EnoUART_CHECK_TX_SDA_MISO_PIN_USED \
                (ENO_EnoUART_PIN_DM_ALG_HIZ != \
                    ENO_EnoUART_GET_P4_PIN_DM(ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso_PC, \
                                                   ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso_SHIFT))
#endif /* (ENO_EnoUART_TX_SDA_MISO_PIN) */

#if (ENO_EnoUART_SS0_PIN)
    #define ENO_EnoUART_CHECK_SS0_PIN_USED \
                (ENO_EnoUART_PIN_DM_ALG_HIZ != \
                    ENO_EnoUART_GET_P4_PIN_DM(ENO_EnoUART_spi_ss0_PC, \
                                                   ENO_EnoUART_spi_ss0_SHIFT))
#endif /* (ENO_EnoUART_SS0_PIN) */

/* Set bits-mask in register */
#define ENO_EnoUART_SET_REGISTER_BITS(reg, mask, pos, mode) \
                    do                                           \
                    {                                            \
                        (reg) = (((reg) & ((uint32) ~(uint32) (mask))) | ((uint32) ((uint32) (mode) << (pos)))); \
                    }while(0)

/* Set bit in the register */
#define ENO_EnoUART_SET_REGISTER_BIT(reg, mask, val) \
                    ((val) ? ((reg) |= (mask)) : ((reg) &= ((uint32) ~((uint32) (mask)))))

#define ENO_EnoUART_SET_HSIOM_SEL(reg, mask, pos, sel) ENO_EnoUART_SET_REGISTER_BITS(reg, mask, pos, sel)
#define ENO_EnoUART_SET_INCFG_TYPE(reg, mask, pos, intType) \
                                                        ENO_EnoUART_SET_REGISTER_BITS(reg, mask, pos, intType)
#define ENO_EnoUART_SET_INP_DIS(reg, mask, val) ENO_EnoUART_SET_REGISTER_BIT(reg, mask, val)

/* ENO_EnoUART_SET_I2C_SCL_DR(val) - Sets I2C SCL DR register.
*  ENO_EnoUART_SET_I2C_SCL_HSIOM_SEL(sel) - Sets I2C SCL HSIOM settings.
*/
/* SCB I2C: scl signal */
#if (ENO_EnoUART_CY_SCBIP_V0)
#if (ENO_EnoUART_I2C_PINS)
    #define ENO_EnoUART_SET_I2C_SCL_DR(val) ENO_EnoUART_scl_Write(val)

    #define ENO_EnoUART_SET_I2C_SCL_HSIOM_SEL(sel) \
                          ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_SCL_HSIOM_REG,  \
                                                         ENO_EnoUART_SCL_HSIOM_MASK, \
                                                         ENO_EnoUART_SCL_HSIOM_POS,  \
                                                         (sel))
    #define ENO_EnoUART_WAIT_SCL_SET_HIGH  (0u == ENO_EnoUART_scl_Read())

/* Unconfigured SCB: scl signal */
#elif (ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN)
    #define ENO_EnoUART_SET_I2C_SCL_DR(val) \
                            ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi_Write(val)

    #define ENO_EnoUART_SET_I2C_SCL_HSIOM_SEL(sel) \
                    ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG,  \
                                                   ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_MASK, \
                                                   ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_POS,  \
                                                   (sel))

    #define ENO_EnoUART_WAIT_SCL_SET_HIGH  (0u == ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi_Read())

#elif (ENO_EnoUART_RX_SCL_MOSI_PIN)
    #define ENO_EnoUART_SET_I2C_SCL_DR(val) \
                            ENO_EnoUART_EnoUART_rx_i2c_scl_spi_mosi_Write(val)


    #define ENO_EnoUART_SET_I2C_SCL_HSIOM_SEL(sel) \
                            ENO_EnoUART_SET_HSIOM_SEL(ENO_EnoUART_RX_SCL_MOSI_HSIOM_REG,  \
                                                           ENO_EnoUART_RX_SCL_MOSI_HSIOM_MASK, \
                                                           ENO_EnoUART_RX_SCL_MOSI_HSIOM_POS,  \
                                                           (sel))

    #define ENO_EnoUART_WAIT_SCL_SET_HIGH  (0u == ENO_EnoUART_EnoUART_rx_i2c_scl_spi_mosi_Read())

#else
    #define ENO_EnoUART_SET_I2C_SCL_DR(val)        do{ /* Does nothing */ }while(0)
    #define ENO_EnoUART_SET_I2C_SCL_HSIOM_SEL(sel) do{ /* Does nothing */ }while(0)

    #define ENO_EnoUART_WAIT_SCL_SET_HIGH  (0u)
#endif /* (ENO_EnoUART_I2C_PINS) */

/* SCB I2C: sda signal */
#if (ENO_EnoUART_I2C_PINS)
    #define ENO_EnoUART_WAIT_SDA_SET_HIGH  (0u == ENO_EnoUART_sda_Read())
/* Unconfigured SCB: sda signal */
#elif (ENO_EnoUART_TX_SDA_MISO_PIN)
    #define ENO_EnoUART_WAIT_SDA_SET_HIGH  (0u == ENO_EnoUART_EnoUART_tx_i2c_sda_spi_miso_Read())
#else
    #define ENO_EnoUART_WAIT_SDA_SET_HIGH  (0u)
#endif /* (ENO_EnoUART_MOSI_SCL_RX_PIN) */
#endif /* (ENO_EnoUART_CY_SCBIP_V0) */

/* Clear EnoUART wakeup source */
#if (ENO_EnoUART_RX_SCL_MOSI_PIN)
    #define ENO_EnoUART_CLEAR_EnoUART_RX_WAKE_INTR        do{ /* Does nothing */ }while(0)
    
#elif (ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN)
    #define ENO_EnoUART_CLEAR_EnoUART_RX_WAKE_INTR \
            do{                                      \
                (void) ENO_EnoUART_EnoUART_rx_wake_i2c_scl_spi_mosi_ClearInterrupt(); \
            }while(0)

#elif(ENO_EnoUART_EnoUART_RX_WAKE_PIN)
    #define ENO_EnoUART_CLEAR_EnoUART_RX_WAKE_INTR \
            do{                                      \
                (void) ENO_EnoUART_rx_wake_ClearInterrupt(); \
            }while(0)
#else
#endif /* (ENO_EnoUART_RX_SCL_MOSI_PIN) */


/***************************************
* The following code is DEPRECATED and
* must not be used.
***************************************/

/* Unconfigured pins */
#define ENO_EnoUART_REMOVE_MOSI_SCL_RX_WAKE_PIN    ENO_EnoUART_REMOVE_RX_WAKE_SCL_MOSI_PIN
#define ENO_EnoUART_REMOVE_MOSI_SCL_RX_PIN         ENO_EnoUART_REMOVE_RX_SCL_MOSI_PIN
#define ENO_EnoUART_REMOVE_MISO_SDA_TX_PIN         ENO_EnoUART_REMOVE_TX_SDA_MISO_PIN
#ifndef ENO_EnoUART_REMOVE_SCLK_PIN
#define ENO_EnoUART_REMOVE_SCLK_PIN                ENO_EnoUART_REMOVE_SCLK_PIN
#endif /* ENO_EnoUART_REMOVE_SCLK_PIN */
#ifndef ENO_EnoUART_REMOVE_SS0_PIN
#define ENO_EnoUART_REMOVE_SS0_PIN                 ENO_EnoUART_REMOVE_SS0_PIN
#endif /* ENO_EnoUART_REMOVE_SS0_PIN */

/* Unconfigured pins */
#define ENO_EnoUART_MOSI_SCL_RX_WAKE_PIN   ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN
#define ENO_EnoUART_MOSI_SCL_RX_PIN        ENO_EnoUART_RX_SCL_MOSI_PIN
#define ENO_EnoUART_MISO_SDA_TX_PIN        ENO_EnoUART_TX_SDA_MISO_PIN
#ifndef ENO_EnoUART_SCLK_PIN
#define ENO_EnoUART_SCLK_PIN               ENO_EnoUART_SCLK_PIN
#endif /* ENO_EnoUART_SCLK_PIN */
#ifndef ENO_EnoUART_SS0_PIN
#define ENO_EnoUART_SS0_PIN                ENO_EnoUART_SS0_PIN
#endif /* ENO_EnoUART_SS0_PIN */

#if (ENO_EnoUART_MOSI_SCL_RX_WAKE_PIN)
    #define ENO_EnoUART_MOSI_SCL_RX_WAKE_HSIOM_REG     ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define ENO_EnoUART_MOSI_SCL_RX_WAKE_HSIOM_PTR     ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define ENO_EnoUART_MOSI_SCL_RX_WAKE_HSIOM_MASK    ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define ENO_EnoUART_MOSI_SCL_RX_WAKE_HSIOM_POS     ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG

    #define ENO_EnoUART_MOSI_SCL_RX_WAKE_INTCFG_REG    ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define ENO_EnoUART_MOSI_SCL_RX_WAKE_INTCFG_PTR    ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG

    #define ENO_EnoUART_MOSI_SCL_RX_WAKE_INTCFG_TYPE_POS   ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG
    #define ENO_EnoUART_MOSI_SCL_RX_WAKE_INTCFG_TYPE_MASK  ENO_EnoUART_RX_WAKE_SCL_MOSI_HSIOM_REG
#endif /* (ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN) */

#if (ENO_EnoUART_MOSI_SCL_RX_PIN)
    #define ENO_EnoUART_MOSI_SCL_RX_HSIOM_REG      ENO_EnoUART_RX_SCL_MOSI_HSIOM_REG
    #define ENO_EnoUART_MOSI_SCL_RX_HSIOM_PTR      ENO_EnoUART_RX_SCL_MOSI_HSIOM_PTR
    #define ENO_EnoUART_MOSI_SCL_RX_HSIOM_MASK     ENO_EnoUART_RX_SCL_MOSI_HSIOM_MASK
    #define ENO_EnoUART_MOSI_SCL_RX_HSIOM_POS      ENO_EnoUART_RX_SCL_MOSI_HSIOM_POS
#endif /* (ENO_EnoUART_MOSI_SCL_RX_PIN) */

#if (ENO_EnoUART_MISO_SDA_TX_PIN)
    #define ENO_EnoUART_MISO_SDA_TX_HSIOM_REG      ENO_EnoUART_TX_SDA_MISO_HSIOM_REG
    #define ENO_EnoUART_MISO_SDA_TX_HSIOM_PTR      ENO_EnoUART_TX_SDA_MISO_HSIOM_REG
    #define ENO_EnoUART_MISO_SDA_TX_HSIOM_MASK     ENO_EnoUART_TX_SDA_MISO_HSIOM_REG
    #define ENO_EnoUART_MISO_SDA_TX_HSIOM_POS      ENO_EnoUART_TX_SDA_MISO_HSIOM_REG
#endif /* (ENO_EnoUART_MISO_SDA_TX_PIN_PIN) */

#if (ENO_EnoUART_SCLK_PIN)
    #ifndef ENO_EnoUART_SCLK_HSIOM_REG
    #define ENO_EnoUART_SCLK_HSIOM_REG     ENO_EnoUART_SCLK_HSIOM_REG
    #define ENO_EnoUART_SCLK_HSIOM_PTR     ENO_EnoUART_SCLK_HSIOM_PTR
    #define ENO_EnoUART_SCLK_HSIOM_MASK    ENO_EnoUART_SCLK_HSIOM_MASK
    #define ENO_EnoUART_SCLK_HSIOM_POS     ENO_EnoUART_SCLK_HSIOM_POS
    #endif /* ENO_EnoUART_SCLK_HSIOM_REG */
#endif /* (ENO_EnoUART_SCLK_PIN) */

#if (ENO_EnoUART_SS0_PIN)
    #ifndef ENO_EnoUART_SS0_HSIOM_REG
    #define ENO_EnoUART_SS0_HSIOM_REG      ENO_EnoUART_SS0_HSIOM_REG
    #define ENO_EnoUART_SS0_HSIOM_PTR      ENO_EnoUART_SS0_HSIOM_PTR
    #define ENO_EnoUART_SS0_HSIOM_MASK     ENO_EnoUART_SS0_HSIOM_MASK
    #define ENO_EnoUART_SS0_HSIOM_POS      ENO_EnoUART_SS0_HSIOM_POS
    #endif /* ENO_EnoUART_SS0_HSIOM_REG */
#endif /* (ENO_EnoUART_SS0_PIN) */

#define ENO_EnoUART_MOSI_SCL_RX_WAKE_PIN_INDEX ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN_INDEX
#define ENO_EnoUART_MOSI_SCL_RX_PIN_INDEX      ENO_EnoUART_RX_SCL_MOSI_PIN_INDEX
#define ENO_EnoUART_MISO_SDA_TX_PIN_INDEX      ENO_EnoUART_TX_SDA_MISO_PIN_INDEX
#ifndef ENO_EnoUART_SCLK_PIN_INDEX
#define ENO_EnoUART_SCLK_PIN_INDEX             ENO_EnoUART_SCLK_PIN_INDEX
#endif /* ENO_EnoUART_SCLK_PIN_INDEX */
#ifndef ENO_EnoUART_SS0_PIN_INDEX
#define ENO_EnoUART_SS0_PIN_INDEX              ENO_EnoUART_SS0_PIN_INDEX
#endif /* ENO_EnoUART_SS0_PIN_INDEX */

#define ENO_EnoUART_MOSI_SCL_RX_WAKE_PIN_MASK ENO_EnoUART_RX_WAKE_SCL_MOSI_PIN_MASK
#define ENO_EnoUART_MOSI_SCL_RX_PIN_MASK      ENO_EnoUART_RX_SCL_MOSI_PIN_MASK
#define ENO_EnoUART_MISO_SDA_TX_PIN_MASK      ENO_EnoUART_TX_SDA_MISO_PIN_MASK
#ifndef ENO_EnoUART_SCLK_PIN_MASK
#define ENO_EnoUART_SCLK_PIN_MASK             ENO_EnoUART_SCLK_PIN_MASK
#endif /* ENO_EnoUART_SCLK_PIN_MASK */
#ifndef ENO_EnoUART_SS0_PIN_MASK
#define ENO_EnoUART_SS0_PIN_MASK              ENO_EnoUART_SS0_PIN_MASK
#endif /* ENO_EnoUART_SS0_PIN_MASK */

#endif /* (CY_SCB_PINS_ENO_EnoUART_H) */


/* [] END OF FILE */
