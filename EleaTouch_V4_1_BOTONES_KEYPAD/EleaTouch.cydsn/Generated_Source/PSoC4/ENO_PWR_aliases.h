/*******************************************************************************
* File Name: ENO_PWR.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ENO_PWR_ALIASES_H) /* Pins ENO_PWR_ALIASES_H */
#define CY_PINS_ENO_PWR_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define ENO_PWR_0			(ENO_PWR__0__PC)
#define ENO_PWR_0_PS		(ENO_PWR__0__PS)
#define ENO_PWR_0_PC		(ENO_PWR__0__PC)
#define ENO_PWR_0_DR		(ENO_PWR__0__DR)
#define ENO_PWR_0_SHIFT	(ENO_PWR__0__SHIFT)
#define ENO_PWR_0_INTR	((uint16)((uint16)0x0003u << (ENO_PWR__0__SHIFT*2u)))

#define ENO_PWR_INTR_ALL	 ((uint16)(ENO_PWR_0_INTR))


#endif /* End Pins ENO_PWR_ALIASES_H */


/* [] END OF FILE */
