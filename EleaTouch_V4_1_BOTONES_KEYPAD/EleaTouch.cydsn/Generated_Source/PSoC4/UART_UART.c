/***************************************************************************//**
* \file EnoUART_EnoUART.c
* \version 3.20
*
* \brief
*  This file provides the source code to the API for the SCB Component in
*  EnoUART mode.
*
* Note:
*
*******************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "EnoUART_PVT.h"
#include "EnoUART_SPI_EnoUART_PVT.h"
#include "cyapicallbacks.h"

#if (EnoUART_EnoUART_WAKE_ENABLE_CONST && EnoUART_EnoUART_RX_WAKEUP_IRQ)
    /**
    * \addtogroup group_globals
    * \{
    */
    /** This global variable determines whether to enable Skip Start
    * functionality when EnoUART_Sleep() function is called:
    * 0 – disable, other values – enable. Default value is 1.
    * It is only available when Enable wakeup from Deep Sleep Mode is enabled.
    */
    uint8 EnoUART_skipStart = 1u;
    /** \} globals */
#endif /* (EnoUART_EnoUART_WAKE_ENABLE_CONST && EnoUART_EnoUART_RX_WAKEUP_IRQ) */

#if(EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)

    /***************************************
    *  Configuration Structure Initialization
    ***************************************/

    const EnoUART_EnoUART_INIT_STRUCT EnoUART_configEnoUART =
    {
        EnoUART_EnoUART_SUB_MODE,
        EnoUART_EnoUART_DIRECTION,
        EnoUART_EnoUART_DATA_BITS_NUM,
        EnoUART_EnoUART_PARITY_TYPE,
        EnoUART_EnoUART_STOP_BITS_NUM,
        EnoUART_EnoUART_OVS_FACTOR,
        EnoUART_EnoUART_IRDA_LOW_POWER,
        EnoUART_EnoUART_MEDIAN_FILTER_ENABLE,
        EnoUART_EnoUART_RETRY_ON_NACK,
        EnoUART_EnoUART_IRDA_POLARITY,
        EnoUART_EnoUART_DROP_ON_PARITY_ERR,
        EnoUART_EnoUART_DROP_ON_FRAME_ERR,
        EnoUART_EnoUART_WAKE_ENABLE,
        0u,
        NULL,
        0u,
        NULL,
        EnoUART_EnoUART_MP_MODE_ENABLE,
        EnoUART_EnoUART_MP_ACCEPT_ADDRESS,
        EnoUART_EnoUART_MP_RX_ADDRESS,
        EnoUART_EnoUART_MP_RX_ADDRESS_MASK,
        (uint32) EnoUART_SCB_IRQ_INTERNAL,
        EnoUART_EnoUART_INTR_RX_MASK,
        EnoUART_EnoUART_RX_TRIGGER_LEVEL,
        EnoUART_EnoUART_INTR_TX_MASK,
        EnoUART_EnoUART_TX_TRIGGER_LEVEL,
        (uint8) EnoUART_EnoUART_BYTE_MODE_ENABLE,
        (uint8) EnoUART_EnoUART_CTS_ENABLE,
        (uint8) EnoUART_EnoUART_CTS_POLARITY,
        (uint8) EnoUART_EnoUART_RTS_POLARITY,
        (uint8) EnoUART_EnoUART_RTS_FIFO_LEVEL
    };


    /*******************************************************************************
    * Function Name: EnoUART_EnoUARTInit
    ****************************************************************************//**
    *
    *  Configures the EnoUART for EnoUART operation.
    *
    *  This function is intended specifically to be used when the EnoUART
    *  configuration is set to “Unconfigured EnoUART” in the customizer.
    *  After initializing the EnoUART in EnoUART mode using this function,
    *  the component can be enabled using the EnoUART_Start() or
    * EnoUART_Enable() function.
    *  This function uses a pointer to a structure that provides the configuration
    *  settings. This structure contains the same information that would otherwise
    *  be provided by the customizer settings.
    *
    *  \param config: pointer to a structure that contains the following list of
    *   fields. These fields match the selections available in the customizer.
    *   Refer to the customizer for further description of the settings.
    *
    *******************************************************************************/
    void EnoUART_EnoUARTInit(const EnoUART_EnoUART_INIT_STRUCT *config)
    {
        uint32 pinsConfig;

        if (NULL == config)
        {
            CYASSERT(0u != 0u); /* Halt execution due to bad function parameter */
        }
        else
        {
            /* Get direction to configure EnoUART pins: TX, RX or TX+RX */
            pinsConfig  = config->direction;

        #if !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1)
            /* Add RTS and CTS pins to configure */
            pinsConfig |= (0u != config->rtsRxFifoLevel) ? (EnoUART_EnoUART_RTS_PIN_ENABLE) : (0u);
            pinsConfig |= (0u != config->enableCts)      ? (EnoUART_EnoUART_CTS_PIN_ENABLE) : (0u);
        #endif /* !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1) */

            /* Configure pins */
            EnoUART_SetPins(EnoUART_SCB_MODE_EnoUART, config->mode, pinsConfig);

            /* Store internal configuration */
            EnoUART_scbMode       = (uint8) EnoUART_SCB_MODE_EnoUART;
            EnoUART_scbEnableWake = (uint8) config->enableWake;
            EnoUART_scbEnableIntr = (uint8) config->enableInterrupt;

            /* Set RX direction internal variables */
            EnoUART_rxBuffer      =         config->rxBuffer;
            EnoUART_rxDataBits    = (uint8) config->dataBits;
            EnoUART_rxBufferSize  = (uint8) config->rxBufferSize;

            /* Set TX direction internal variables */
            EnoUART_txBuffer      =         config->txBuffer;
            EnoUART_txDataBits    = (uint8) config->dataBits;
            EnoUART_txBufferSize  = (uint8) config->txBufferSize;

            /* Configure EnoUART interface */
            if(EnoUART_EnoUART_MODE_IRDA == config->mode)
            {
                /* OVS settings: IrDA */
                EnoUART_CTRL_REG  = ((0u != config->enableIrdaLowPower) ?
                                                (EnoUART_EnoUART_GET_CTRL_OVS_IRDA_LP(config->oversample)) :
                                                (EnoUART_CTRL_OVS_IRDA_OVS16));
            }
            else
            {
                /* OVS settings: EnoUART and SmartCard */
                EnoUART_CTRL_REG  = EnoUART_GET_CTRL_OVS(config->oversample);
            }

            EnoUART_CTRL_REG     |= EnoUART_GET_CTRL_BYTE_MODE  (config->enableByteMode)      |
                                             EnoUART_GET_CTRL_ADDR_ACCEPT(config->multiprocAcceptAddr) |
                                             EnoUART_CTRL_EnoUART;

            /* Configure sub-mode: EnoUART, SmartCard or IrDA */
            EnoUART_EnoUART_CTRL_REG = EnoUART_GET_EnoUART_CTRL_MODE(config->mode);

            /* Configure RX direction */
            EnoUART_EnoUART_RX_CTRL_REG = EnoUART_GET_EnoUART_RX_CTRL_MODE(config->stopBits)              |
                                        EnoUART_GET_EnoUART_RX_CTRL_POLARITY(config->enableInvertedRx)          |
                                        EnoUART_GET_EnoUART_RX_CTRL_MP_MODE(config->enableMultiproc)            |
                                        EnoUART_GET_EnoUART_RX_CTRL_DROP_ON_PARITY_ERR(config->dropOnParityErr) |
                                        EnoUART_GET_EnoUART_RX_CTRL_DROP_ON_FRAME_ERR(config->dropOnFrameErr);

            if(EnoUART_EnoUART_PARITY_NONE != config->parity)
            {
               EnoUART_EnoUART_RX_CTRL_REG |= EnoUART_GET_EnoUART_RX_CTRL_PARITY(config->parity) |
                                                    EnoUART_EnoUART_RX_CTRL_PARITY_ENABLED;
            }

            EnoUART_RX_CTRL_REG      = EnoUART_GET_RX_CTRL_DATA_WIDTH(config->dataBits)       |
                                                EnoUART_GET_RX_CTRL_MEDIAN(config->enableMedianFilter) |
                                                EnoUART_GET_EnoUART_RX_CTRL_ENABLED(config->direction);

            EnoUART_RX_FIFO_CTRL_REG = EnoUART_GET_RX_FIFO_CTRL_TRIGGER_LEVEL(config->rxTriggerLevel);

            /* Configure MP address */
            EnoUART_RX_MATCH_REG     = EnoUART_GET_RX_MATCH_ADDR(config->multiprocAddr) |
                                                EnoUART_GET_RX_MATCH_MASK(config->multiprocAddrMask);

            /* Configure RX direction */
            EnoUART_EnoUART_TX_CTRL_REG = EnoUART_GET_EnoUART_TX_CTRL_MODE(config->stopBits) |
                                                EnoUART_GET_EnoUART_TX_CTRL_RETRY_NACK(config->enableRetryNack);

            if(EnoUART_EnoUART_PARITY_NONE != config->parity)
            {
               EnoUART_EnoUART_TX_CTRL_REG |= EnoUART_GET_EnoUART_TX_CTRL_PARITY(config->parity) |
                                                    EnoUART_EnoUART_TX_CTRL_PARITY_ENABLED;
            }

            EnoUART_TX_CTRL_REG      = EnoUART_GET_TX_CTRL_DATA_WIDTH(config->dataBits)    |
                                                EnoUART_GET_EnoUART_TX_CTRL_ENABLED(config->direction);

            EnoUART_TX_FIFO_CTRL_REG = EnoUART_GET_TX_FIFO_CTRL_TRIGGER_LEVEL(config->txTriggerLevel);

        #if !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1)
            EnoUART_EnoUART_FLOW_CTRL_REG = EnoUART_GET_EnoUART_FLOW_CTRL_CTS_ENABLE(config->enableCts) | \
                                            EnoUART_GET_EnoUART_FLOW_CTRL_CTS_POLARITY (config->ctsPolarity)  | \
                                            EnoUART_GET_EnoUART_FLOW_CTRL_RTS_POLARITY (config->rtsPolarity)  | \
                                            EnoUART_GET_EnoUART_FLOW_CTRL_TRIGGER_LEVEL(config->rtsRxFifoLevel);
        #endif /* !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1) */

            /* Configure interrupt with EnoUART handler but do not enable it */
            CyIntDisable    (EnoUART_ISR_NUMBER);
            CyIntSetPriority(EnoUART_ISR_NUMBER, EnoUART_ISR_PRIORITY);
            (void) CyIntSetVector(EnoUART_ISR_NUMBER, &EnoUART_SPI_EnoUART_ISR);

            /* Configure WAKE interrupt */
        #if(EnoUART_EnoUART_RX_WAKEUP_IRQ)
            CyIntDisable    (EnoUART_RX_WAKE_ISR_NUMBER);
            CyIntSetPriority(EnoUART_RX_WAKE_ISR_NUMBER, EnoUART_RX_WAKE_ISR_PRIORITY);
            (void) CyIntSetVector(EnoUART_RX_WAKE_ISR_NUMBER, &EnoUART_EnoUART_WAKEUP_ISR);
        #endif /* (EnoUART_EnoUART_RX_WAKEUP_IRQ) */

            /* Configure interrupt sources */
            EnoUART_INTR_I2C_EC_MASK_REG = EnoUART_NO_INTR_SOURCES;
            EnoUART_INTR_SPI_EC_MASK_REG = EnoUART_NO_INTR_SOURCES;
            EnoUART_INTR_SLAVE_MASK_REG  = EnoUART_NO_INTR_SOURCES;
            EnoUART_INTR_MASTER_MASK_REG = EnoUART_NO_INTR_SOURCES;
            EnoUART_INTR_RX_MASK_REG     = config->rxInterruptMask;
            EnoUART_INTR_TX_MASK_REG     = config->txInterruptMask;
        
            /* Configure TX interrupt sources to restore. */
            EnoUART_IntrTxMask = LO16(EnoUART_INTR_TX_MASK_REG);

            /* Clear RX buffer indexes */
            EnoUART_rxBufferHead     = 0u;
            EnoUART_rxBufferTail     = 0u;
            EnoUART_rxBufferOverflow = 0u;

            /* Clear TX buffer indexes */
            EnoUART_txBufferHead = 0u;
            EnoUART_txBufferTail = 0u;
        }
    }

#else

    /*******************************************************************************
    * Function Name: EnoUART_EnoUARTInit
    ****************************************************************************//**
    *
    *  Configures the SCB for the EnoUART operation.
    *
    *******************************************************************************/
    void EnoUART_EnoUARTInit(void)
    {
        /* Configure EnoUART interface */
        EnoUART_CTRL_REG = EnoUART_EnoUART_DEFAULT_CTRL;

        /* Configure sub-mode: EnoUART, SmartCard or IrDA */
        EnoUART_EnoUART_CTRL_REG = EnoUART_EnoUART_DEFAULT_EnoUART_CTRL;

        /* Configure RX direction */
        EnoUART_EnoUART_RX_CTRL_REG = EnoUART_EnoUART_DEFAULT_EnoUART_RX_CTRL;
        EnoUART_RX_CTRL_REG      = EnoUART_EnoUART_DEFAULT_RX_CTRL;
        EnoUART_RX_FIFO_CTRL_REG = EnoUART_EnoUART_DEFAULT_RX_FIFO_CTRL;
        EnoUART_RX_MATCH_REG     = EnoUART_EnoUART_DEFAULT_RX_MATCH_REG;

        /* Configure TX direction */
        EnoUART_EnoUART_TX_CTRL_REG = EnoUART_EnoUART_DEFAULT_EnoUART_TX_CTRL;
        EnoUART_TX_CTRL_REG      = EnoUART_EnoUART_DEFAULT_TX_CTRL;
        EnoUART_TX_FIFO_CTRL_REG = EnoUART_EnoUART_DEFAULT_TX_FIFO_CTRL;

    #if !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1)
        EnoUART_EnoUART_FLOW_CTRL_REG = EnoUART_EnoUART_DEFAULT_FLOW_CTRL;
    #endif /* !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1) */

        /* Configure interrupt with EnoUART handler but do not enable it */
    #if(EnoUART_SCB_IRQ_INTERNAL)
        CyIntDisable    (EnoUART_ISR_NUMBER);
        CyIntSetPriority(EnoUART_ISR_NUMBER, EnoUART_ISR_PRIORITY);
        (void) CyIntSetVector(EnoUART_ISR_NUMBER, &EnoUART_SPI_EnoUART_ISR);
    #endif /* (EnoUART_SCB_IRQ_INTERNAL) */

        /* Configure WAKE interrupt */
    #if(EnoUART_EnoUART_RX_WAKEUP_IRQ)
        CyIntDisable    (EnoUART_RX_WAKE_ISR_NUMBER);
        CyIntSetPriority(EnoUART_RX_WAKE_ISR_NUMBER, EnoUART_RX_WAKE_ISR_PRIORITY);
        (void) CyIntSetVector(EnoUART_RX_WAKE_ISR_NUMBER, &EnoUART_EnoUART_WAKEUP_ISR);
    #endif /* (EnoUART_EnoUART_RX_WAKEUP_IRQ) */

        /* Configure interrupt sources */
        EnoUART_INTR_I2C_EC_MASK_REG = EnoUART_EnoUART_DEFAULT_INTR_I2C_EC_MASK;
        EnoUART_INTR_SPI_EC_MASK_REG = EnoUART_EnoUART_DEFAULT_INTR_SPI_EC_MASK;
        EnoUART_INTR_SLAVE_MASK_REG  = EnoUART_EnoUART_DEFAULT_INTR_SLAVE_MASK;
        EnoUART_INTR_MASTER_MASK_REG = EnoUART_EnoUART_DEFAULT_INTR_MASTER_MASK;
        EnoUART_INTR_RX_MASK_REG     = EnoUART_EnoUART_DEFAULT_INTR_RX_MASK;
        EnoUART_INTR_TX_MASK_REG     = EnoUART_EnoUART_DEFAULT_INTR_TX_MASK;
    
        /* Configure TX interrupt sources to restore. */
        EnoUART_IntrTxMask = LO16(EnoUART_INTR_TX_MASK_REG);

    #if(EnoUART_INTERNAL_RX_SW_BUFFER_CONST)
        EnoUART_rxBufferHead     = 0u;
        EnoUART_rxBufferTail     = 0u;
        EnoUART_rxBufferOverflow = 0u;
    #endif /* (EnoUART_INTERNAL_RX_SW_BUFFER_CONST) */

    #if(EnoUART_INTERNAL_TX_SW_BUFFER_CONST)
        EnoUART_txBufferHead = 0u;
        EnoUART_txBufferTail = 0u;
    #endif /* (EnoUART_INTERNAL_TX_SW_BUFFER_CONST) */
    }
#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */


/*******************************************************************************
* Function Name: EnoUART_EnoUARTPostEnable
****************************************************************************//**
*
*  Restores HSIOM settings for the EnoUART output pins (TX and/or RTS) to be
*  controlled by the SCB EnoUART.
*
*******************************************************************************/
void EnoUART_EnoUARTPostEnable(void)
{
#if (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    #if (EnoUART_TX_SDA_MISO_PIN)
        if (EnoUART_CHECK_TX_SDA_MISO_PIN_USED)
        {
            /* Set SCB EnoUART to drive the output pin */
            EnoUART_SET_HSIOM_SEL(EnoUART_TX_SDA_MISO_HSIOM_REG, EnoUART_TX_SDA_MISO_HSIOM_MASK,
                                           EnoUART_TX_SDA_MISO_HSIOM_POS, EnoUART_TX_SDA_MISO_HSIOM_SEL_EnoUART);
        }
    #endif /* (EnoUART_TX_SDA_MISO_PIN_PIN) */

    #if !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1)
        #if (EnoUART_SS0_PIN)
            if (EnoUART_CHECK_SS0_PIN_USED)
            {
                /* Set SCB EnoUART to drive the output pin */
                EnoUART_SET_HSIOM_SEL(EnoUART_SS0_HSIOM_REG, EnoUART_SS0_HSIOM_MASK,
                                               EnoUART_SS0_HSIOM_POS, EnoUART_SS0_HSIOM_SEL_EnoUART);
            }
        #endif /* (EnoUART_SS0_PIN) */
    #endif /* !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1) */

#else
    #if (EnoUART_EnoUART_TX_PIN)
         /* Set SCB EnoUART to drive the output pin */
        EnoUART_SET_HSIOM_SEL(EnoUART_TX_HSIOM_REG, EnoUART_TX_HSIOM_MASK,
                                       EnoUART_TX_HSIOM_POS, EnoUART_TX_HSIOM_SEL_EnoUART);
    #endif /* (EnoUART_EnoUART_TX_PIN) */

    #if (EnoUART_EnoUART_RTS_PIN)
        /* Set SCB EnoUART to drive the output pin */
        EnoUART_SET_HSIOM_SEL(EnoUART_RTS_HSIOM_REG, EnoUART_RTS_HSIOM_MASK,
                                       EnoUART_RTS_HSIOM_POS, EnoUART_RTS_HSIOM_SEL_EnoUART);
    #endif /* (EnoUART_EnoUART_RTS_PIN) */
#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */

    /* Restore TX interrupt sources. */
    EnoUART_SetTxInterruptMode(EnoUART_IntrTxMask);
}


/*******************************************************************************
* Function Name: EnoUART_EnoUARTStop
****************************************************************************//**
*
*  Changes the HSIOM settings for the EnoUART output pins (TX and/or RTS) to keep
*  them inactive after the block is disabled. The output pins are controlled by
*  the GPIO data register. Also, the function disables the skip start feature
*  to not cause it to trigger after the component is enabled.
*
*******************************************************************************/
void EnoUART_EnoUARTStop(void)
{
#if(EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    #if (EnoUART_TX_SDA_MISO_PIN)
        if (EnoUART_CHECK_TX_SDA_MISO_PIN_USED)
        {
            /* Set GPIO to drive output pin */
            EnoUART_SET_HSIOM_SEL(EnoUART_TX_SDA_MISO_HSIOM_REG, EnoUART_TX_SDA_MISO_HSIOM_MASK,
                                           EnoUART_TX_SDA_MISO_HSIOM_POS, EnoUART_TX_SDA_MISO_HSIOM_SEL_GPIO);
        }
    #endif /* (EnoUART_TX_SDA_MISO_PIN_PIN) */

    #if !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1)
        #if (EnoUART_SS0_PIN)
            if (EnoUART_CHECK_SS0_PIN_USED)
            {
                /* Set output pin state after block is disabled */
                EnoUART_spi_ss0_Write(EnoUART_GET_EnoUART_RTS_INACTIVE);

                /* Set GPIO to drive output pin */
                EnoUART_SET_HSIOM_SEL(EnoUART_SS0_HSIOM_REG, EnoUART_SS0_HSIOM_MASK,
                                               EnoUART_SS0_HSIOM_POS, EnoUART_SS0_HSIOM_SEL_GPIO);
            }
        #endif /* (EnoUART_SS0_PIN) */
    #endif /* !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1) */

#else
    #if (EnoUART_EnoUART_TX_PIN)
        /* Set GPIO to drive output pin */
        EnoUART_SET_HSIOM_SEL(EnoUART_TX_HSIOM_REG, EnoUART_TX_HSIOM_MASK,
                                       EnoUART_TX_HSIOM_POS, EnoUART_TX_HSIOM_SEL_GPIO);
    #endif /* (EnoUART_EnoUART_TX_PIN) */

    #if (EnoUART_EnoUART_RTS_PIN)
        /* Set output pin state after block is disabled */
        EnoUART_rts_Write(EnoUART_GET_EnoUART_RTS_INACTIVE);

        /* Set GPIO to drive output pin */
        EnoUART_SET_HSIOM_SEL(EnoUART_RTS_HSIOM_REG, EnoUART_RTS_HSIOM_MASK,
                                       EnoUART_RTS_HSIOM_POS, EnoUART_RTS_HSIOM_SEL_GPIO);
    #endif /* (EnoUART_EnoUART_RTS_PIN) */

#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */

#if (EnoUART_EnoUART_WAKE_ENABLE_CONST)
    /* Disable skip start feature used for wakeup */
    EnoUART_EnoUART_RX_CTRL_REG &= (uint32) ~EnoUART_EnoUART_RX_CTRL_SKIP_START;
#endif /* (EnoUART_EnoUART_WAKE_ENABLE_CONST) */

    /* Store TX interrupt sources (exclude level triggered). */
    EnoUART_IntrTxMask = LO16(EnoUART_GetTxInterruptMode() & EnoUART_INTR_EnoUART_TX_RESTORE);
}


/*******************************************************************************
* Function Name: EnoUART_EnoUARTSetRxAddress
****************************************************************************//**
*
*  Sets the hardware detectable receiver address for the EnoUART in the
*  Multiprocessor mode.
*
*  \param address: Address for hardware address detection.
*
*******************************************************************************/
void EnoUART_EnoUARTSetRxAddress(uint32 address)
{
     uint32 matchReg;

    matchReg = EnoUART_RX_MATCH_REG;

    matchReg &= ((uint32) ~EnoUART_RX_MATCH_ADDR_MASK); /* Clear address bits */
    matchReg |= ((uint32)  (address & EnoUART_RX_MATCH_ADDR_MASK)); /* Set address  */

    EnoUART_RX_MATCH_REG = matchReg;
}


/*******************************************************************************
* Function Name: EnoUART_EnoUARTSetRxAddressMask
****************************************************************************//**
*
*  Sets the hardware address mask for the EnoUART in the Multiprocessor mode.
*
*  \param addressMask: Address mask.
*   - Bit value 0 – excludes bit from address comparison.
*   - Bit value 1 – the bit needs to match with the corresponding bit
*     of the address.
*
*******************************************************************************/
void EnoUART_EnoUARTSetRxAddressMask(uint32 addressMask)
{
    uint32 matchReg;

    matchReg = EnoUART_RX_MATCH_REG;

    matchReg &= ((uint32) ~EnoUART_RX_MATCH_MASK_MASK); /* Clear address mask bits */
    matchReg |= ((uint32) (addressMask << EnoUART_RX_MATCH_MASK_POS));

    EnoUART_RX_MATCH_REG = matchReg;
}


#if(EnoUART_EnoUART_RX_DIRECTION)
    /*******************************************************************************
    * Function Name: EnoUART_EnoUARTGetChar
    ****************************************************************************//**
    *
    *  Retrieves next data element from receive buffer.
    *  This function is designed for ASCII characters and returns a char where
    *  1 to 255 are valid characters and 0 indicates an error occurred or no data
    *  is present.
    *  - RX software buffer is disabled: Returns data element retrieved from RX
    *    FIFO.
    *  - RX software buffer is enabled: Returns data element from the software
    *    receive buffer.
    *
    *  \return
    *   Next data element from the receive buffer. ASCII character values from
    *   1 to 255 are valid. A returned zero signifies an error condition or no
    *   data available.
    *
    *  \sideeffect
    *   The errors bits may not correspond with reading characters due to
    *   RX FIFO and software buffer usage.
    *   RX software buffer is enabled: The internal software buffer overflow
    *   is not treated as an error condition.
    *   Check EnoUART_rxBufferOverflow to capture that error condition.
    *
    *******************************************************************************/
    uint32 EnoUART_EnoUARTGetChar(void)
    {
        uint32 rxData = 0u;

        /* Reads data only if there is data to read */
        if (0u != EnoUART_SpiEnoUARTGetRxBufferSize())
        {
            rxData = EnoUART_SpiEnoUARTReadRxData();
        }

        if (EnoUART_CHECK_INTR_RX(EnoUART_INTR_RX_ERR))
        {
            rxData = 0u; /* Error occurred: returns zero */
            EnoUART_ClearRxInterruptSource(EnoUART_INTR_RX_ERR);
        }

        return (rxData);
    }


    /*******************************************************************************
    * Function Name: EnoUART_EnoUARTGetByte
    ****************************************************************************//**
    *
    *  Retrieves the next data element from the receive buffer, returns the
    *  received byte and error condition.
    *   - The RX software buffer is disabled: returns the data element retrieved
    *     from the RX FIFO. Undefined data will be returned if the RX FIFO is
    *     empty.
    *   - The RX software buffer is enabled: returns data element from the
    *     software receive buffer.
    *
    *  \return
    *   Bits 7-0 contain the next data element from the receive buffer and
    *   other bits contain the error condition.
    *   - EnoUART_EnoUART_RX_OVERFLOW - Attempt to write to a full
    *     receiver FIFO.
    *   - EnoUART_EnoUART_RX_UNDERFLOW	Attempt to read from an empty
    *     receiver FIFO.
    *   - EnoUART_EnoUART_RX_FRAME_ERROR - EnoUART framing error detected.
    *   - EnoUART_EnoUART_RX_PARITY_ERROR - EnoUART parity error detected.
    *
    *  \sideeffect
    *   The errors bits may not correspond with reading characters due to
    *   RX FIFO and software buffer usage.
    *   RX software buffer is enabled: The internal software buffer overflow
    *   is not treated as an error condition.
    *   Check EnoUART_rxBufferOverflow to capture that error condition.
    *
    *******************************************************************************/
    uint32 EnoUART_EnoUARTGetByte(void)
    {
        uint32 rxData;
        uint32 tmpStatus;

        #if (EnoUART_CHECK_RX_SW_BUFFER)
        {
            EnoUART_DisableInt();
        }
        #endif

        if (0u != EnoUART_SpiEnoUARTGetRxBufferSize())
        {
            /* Enables interrupt to receive more bytes: at least one byte is in
            * buffer.
            */
            #if (EnoUART_CHECK_RX_SW_BUFFER)
            {
                EnoUART_EnableInt();
            }
            #endif

            /* Get received byte */
            rxData = EnoUART_SpiEnoUARTReadRxData();
        }
        else
        {
            /* Reads a byte directly from RX FIFO: underflow is raised in the
            * case of empty. Otherwise the first received byte will be read.
            */
            rxData = EnoUART_RX_FIFO_RD_REG;


            /* Enables interrupt to receive more bytes. */
            #if (EnoUART_CHECK_RX_SW_BUFFER)
            {

                /* The byte has been read from RX FIFO. Clear RX interrupt to
                * not involve interrupt handler when RX FIFO is empty.
                */
                EnoUART_ClearRxInterruptSource(EnoUART_INTR_RX_NOT_EMPTY);

                EnoUART_EnableInt();
            }
            #endif
        }

        /* Get and clear RX error mask */
        tmpStatus = (EnoUART_GetRxInterruptSource() & EnoUART_INTR_RX_ERR);
        EnoUART_ClearRxInterruptSource(EnoUART_INTR_RX_ERR);

        /* Puts together data and error status:
        * MP mode and accept address: 9th bit is set to notify mark.
        */
        rxData |= ((uint32) (tmpStatus << 8u));

        return (rxData);
    }


    #if !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1)
        /*******************************************************************************
        * Function Name: EnoUART_EnoUARTSetRtsPolarity
        ****************************************************************************//**
        *
        *  Sets active polarity of RTS output signal.
        *  Only available for PSoC 4100 BLE / PSoC 4200 BLE / PSoC 4100M / PSoC 4200M /
        *  PSoC 4200L / PSoC 4000S / PSoC 4100S / PSoC Analog Coprocessor devices.
        *
        *  \param polarity: Active polarity of RTS output signal.
        *   - EnoUART_EnoUART_RTS_ACTIVE_LOW  - RTS signal is active low.
        *   - EnoUART_EnoUART_RTS_ACTIVE_HIGH - RTS signal is active high.
        *
        *******************************************************************************/
        void EnoUART_EnoUARTSetRtsPolarity(uint32 polarity)
        {
            if(0u != polarity)
            {
                EnoUART_EnoUART_FLOW_CTRL_REG |= (uint32)  EnoUART_EnoUART_FLOW_CTRL_RTS_POLARITY;
            }
            else
            {
                EnoUART_EnoUART_FLOW_CTRL_REG &= (uint32) ~EnoUART_EnoUART_FLOW_CTRL_RTS_POLARITY;
            }
        }


        /*******************************************************************************
        * Function Name: EnoUART_EnoUARTSetRtsFifoLevel
        ****************************************************************************//**
        *
        *  Sets level in the RX FIFO for RTS signal activation.
        *  While the RX FIFO has fewer entries than the RX FIFO level the RTS signal
        *  remains active, otherwise the RTS signal becomes inactive.
        *  Only available for PSoC 4100 BLE / PSoC 4200 BLE / PSoC 4100M / PSoC 4200M /
        *  PSoC 4200L / PSoC 4000S / PSoC 4100S / PSoC Analog Coprocessor devices.
        *
        *  \param level: Level in the RX FIFO for RTS signal activation.
        *   The range of valid level values is between 0 and RX FIFO depth - 1.
        *   Setting level value to 0 disables RTS signal activation.
        *
        *******************************************************************************/
        void EnoUART_EnoUARTSetRtsFifoLevel(uint32 level)
        {
            uint32 EnoUARTFlowCtrl;

            EnoUARTFlowCtrl = EnoUART_EnoUART_FLOW_CTRL_REG;

            EnoUARTFlowCtrl &= ((uint32) ~EnoUART_EnoUART_FLOW_CTRL_TRIGGER_LEVEL_MASK); /* Clear level mask bits */
            EnoUARTFlowCtrl |= ((uint32) (EnoUART_EnoUART_FLOW_CTRL_TRIGGER_LEVEL_MASK & level));

            EnoUART_EnoUART_FLOW_CTRL_REG = EnoUARTFlowCtrl;
        }
    #endif /* !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1) */

#endif /* (EnoUART_EnoUART_RX_DIRECTION) */


#if(EnoUART_EnoUART_TX_DIRECTION)
    /*******************************************************************************
    * Function Name: EnoUART_EnoUARTPutString
    ****************************************************************************//**
    *
    *  Places a NULL terminated string in the transmit buffer to be sent at the
    *  next available bus time.
    *  This function is blocking and waits until there is a space available to put
    *  requested data in transmit buffer.
    *
    *  \param string: pointer to the null terminated string array to be placed in the
    *   transmit buffer.
    *
    *******************************************************************************/
    void EnoUART_EnoUARTPutString(const char8 string[])
    {
        uint32 bufIndex;

        bufIndex = 0u;

        /* Blocks the control flow until all data has been sent */
        while(string[bufIndex] != ((char8) 0))
        {
            EnoUART_EnoUARTPutChar((uint32) string[bufIndex]);
            bufIndex++;
        }
    }


    /*******************************************************************************
    * Function Name: EnoUART_EnoUARTPutCRLF
    ****************************************************************************//**
    *
    *  Places byte of data followed by a carriage return (0x0D) and line feed
    *  (0x0A) in the transmit buffer.
    *  This function is blocking and waits until there is a space available to put
    *  all requested data in transmit buffer.
    *
    *  \param txDataByte: the data to be transmitted.
    *
    *******************************************************************************/
    void EnoUART_EnoUARTPutCRLF(uint32 txDataByte)
    {
        EnoUART_EnoUARTPutChar(txDataByte);  /* Blocks control flow until all data has been sent */
        EnoUART_EnoUARTPutChar(0x0Du);       /* Blocks control flow until all data has been sent */
        EnoUART_EnoUARTPutChar(0x0Au);       /* Blocks control flow until all data has been sent */
    }


    #if !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1)
        /*******************************************************************************
        * Function Name: EnoUARTSCB_EnoUARTEnableCts
        ****************************************************************************//**
        *
        *  Enables usage of CTS input signal by the EnoUART transmitter.
        *  Only available for PSoC 4100 BLE / PSoC 4200 BLE / PSoC 4100M / PSoC 4200M /
        *  PSoC 4200L / PSoC 4000S / PSoC 4100S / PSoC Analog Coprocessor devices.
        *
        *******************************************************************************/
        void EnoUART_EnoUARTEnableCts(void)
        {
            EnoUART_EnoUART_FLOW_CTRL_REG |= (uint32)  EnoUART_EnoUART_FLOW_CTRL_CTS_ENABLE;
        }


        /*******************************************************************************
        * Function Name: EnoUART_EnoUARTDisableCts
        ****************************************************************************//**
        *
        *  Disables usage of CTS input signal by the EnoUART transmitter.
        *  Only available for PSoC 4100 BLE / PSoC 4200 BLE / PSoC 4100M / PSoC 4200M /
        *  PSoC 4200L / PSoC 4000S / PSoC 4100S / PSoC Analog Coprocessor devices.
        *
        *******************************************************************************/
        void EnoUART_EnoUARTDisableCts(void)
        {
            EnoUART_EnoUART_FLOW_CTRL_REG &= (uint32) ~EnoUART_EnoUART_FLOW_CTRL_CTS_ENABLE;
        }


        /*******************************************************************************
        * Function Name: EnoUART_EnoUARTSetCtsPolarity
        ****************************************************************************//**
        *
        *  Sets active polarity of CTS input signal.
        *  Only available for PSoC 4100 BLE / PSoC 4200 BLE / PSoC 4100M / PSoC 4200M /
        *  PSoC 4200L / PSoC 4000S / PSoC 4100S / PSoC Analog Coprocessor devices.
        *
        *  \param polarity: Active polarity of CTS output signal.
        *   - EnoUART_EnoUART_CTS_ACTIVE_LOW  - CTS signal is active low.
        *   - EnoUART_EnoUART_CTS_ACTIVE_HIGH - CTS signal is active high.
        *
        *******************************************************************************/
        void EnoUART_EnoUARTSetCtsPolarity(uint32 polarity)
        {
            if (0u != polarity)
            {
                EnoUART_EnoUART_FLOW_CTRL_REG |= (uint32)  EnoUART_EnoUART_FLOW_CTRL_CTS_POLARITY;
            }
            else
            {
                EnoUART_EnoUART_FLOW_CTRL_REG &= (uint32) ~EnoUART_EnoUART_FLOW_CTRL_CTS_POLARITY;
            }
        }
    #endif /* !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1) */

#endif /* (EnoUART_EnoUART_TX_DIRECTION) */


#if (EnoUART_EnoUART_WAKE_ENABLE_CONST)
    /*******************************************************************************
    * Function Name: EnoUART_EnoUARTSaveConfig
    ****************************************************************************//**
    *
    *  Clears and enables an interrupt on a falling edge of the Rx input. The GPIO
    *  interrupt does not track in the active mode, therefore requires to be 
    *  cleared by this API.
    *
    *******************************************************************************/
    void EnoUART_EnoUARTSaveConfig(void)
    {
    #if (EnoUART_EnoUART_RX_WAKEUP_IRQ)
        /* Set SKIP_START if requested (set by default). */
        if (0u != EnoUART_skipStart)
        {
            EnoUART_EnoUART_RX_CTRL_REG |= (uint32)  EnoUART_EnoUART_RX_CTRL_SKIP_START;
        }
        else
        {
            EnoUART_EnoUART_RX_CTRL_REG &= (uint32) ~EnoUART_EnoUART_RX_CTRL_SKIP_START;
        }
        
        /* Clear RX GPIO interrupt status and pending interrupt in NVIC because
        * falling edge on RX line occurs while EnoUART communication in active mode.
        * Enable interrupt: next interrupt trigger should wakeup device.
        */
        EnoUART_CLEAR_EnoUART_RX_WAKE_INTR;
        EnoUART_RxWakeClearPendingInt();
        EnoUART_RxWakeEnableInt();
    #endif /* (EnoUART_EnoUART_RX_WAKEUP_IRQ) */
    }


    /*******************************************************************************
    * Function Name: EnoUART_EnoUARTRestoreConfig
    ****************************************************************************//**
    *
    *  Disables the RX GPIO interrupt. Until this function is called the interrupt
    *  remains active and triggers on every falling edge of the EnoUART RX line.
    *
    *******************************************************************************/
    void EnoUART_EnoUARTRestoreConfig(void)
    {
    #if (EnoUART_EnoUART_RX_WAKEUP_IRQ)
        /* Disable interrupt: no more triggers in active mode */
        EnoUART_RxWakeDisableInt();
    #endif /* (EnoUART_EnoUART_RX_WAKEUP_IRQ) */
    }


    #if (EnoUART_EnoUART_RX_WAKEUP_IRQ)
        /*******************************************************************************
        * Function Name: EnoUART_EnoUART_WAKEUP_ISR
        ****************************************************************************//**
        *
        *  Handles the Interrupt Service Routine for the SCB EnoUART mode GPIO wakeup
        *  event. This event is configured to trigger on a falling edge of the RX line.
        *
        *******************************************************************************/
        CY_ISR(EnoUART_EnoUART_WAKEUP_ISR)
        {
        #ifdef EnoUART_EnoUART_WAKEUP_ISR_ENTRY_CALLBACK
            EnoUART_EnoUART_WAKEUP_ISR_EntryCallback();
        #endif /* EnoUART_EnoUART_WAKEUP_ISR_ENTRY_CALLBACK */

            EnoUART_CLEAR_EnoUART_RX_WAKE_INTR;

        #ifdef EnoUART_EnoUART_WAKEUP_ISR_EXIT_CALLBACK
            EnoUART_EnoUART_WAKEUP_ISR_ExitCallback();
        #endif /* EnoUART_EnoUART_WAKEUP_ISR_EXIT_CALLBACK */
        }
    #endif /* (EnoUART_EnoUART_RX_WAKEUP_IRQ) */
#endif /* (EnoUART_EnoUART_RX_WAKEUP_IRQ) */


/* [] END OF FILE */
