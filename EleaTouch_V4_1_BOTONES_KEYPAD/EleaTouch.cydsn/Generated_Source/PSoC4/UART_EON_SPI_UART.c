/*******************************************************************************
* File Name: EnoUART_EON_SPI_EnoUART.c
* Version 3.0
*
* Description:
*  This file provides the source code to the API for the SCB Component in
*  SPI and EnoUART modes.
*
* Note:
*
*******************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "EnoUART_EON_PVT.h"
#include "EnoUART_EON_SPI_EnoUART_PVT.h"

/***************************************
*        SPI/EnoUART Private Vars
***************************************/

#if(EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST)
    volatile uint32 EnoUART_EON_rxBufferHead;
    volatile uint32 EnoUART_EON_rxBufferTail;
    volatile uint8  EnoUART_EON_rxBufferOverflow;
#endif /* (EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST) */

#if(EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST)
    volatile uint32 EnoUART_EON_txBufferHead;
    volatile uint32 EnoUART_EON_txBufferTail;
#endif /* (EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST) */

#if(EnoUART_EON_INTERNAL_RX_SW_BUFFER)
    /* Add one element to the buffer to receive full packet. One byte in receive buffer is always empty */
    volatile uint8 EnoUART_EON_rxBufferInternal[EnoUART_EON_INTERNAL_RX_BUFFER_SIZE];
#endif /* (EnoUART_EON_INTERNAL_RX_SW_BUFFER) */

#if(EnoUART_EON_INTERNAL_TX_SW_BUFFER)
    volatile uint8 EnoUART_EON_txBufferInternal[EnoUART_EON_TX_BUFFER_SIZE];
#endif /* (EnoUART_EON_INTERNAL_TX_SW_BUFFER) */


#if(EnoUART_EON_RX_DIRECTION)
    /*******************************************************************************
    * Function Name: EnoUART_EON_SpiEnoUARTReadRxData
    ********************************************************************************
    *
    * Summary:
    *  Retrieves the next data element from the receive buffer.
    *   - RX software buffer is disabled: Returns data element retrieved from
    *     RX FIFO. Undefined data will be returned if the RX FIFO is empty.
    *   - RX software buffer is enabled: Returns data element from the software
    *     receive buffer. Zero value is returned if the software receive buffer
    *     is empty.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  Next data element from the receive buffer.
    *
    * Global Variables:
    *  Look into EnoUART_EON_SpiInit for description.
    *
    *******************************************************************************/
    uint32 EnoUART_EON_SpiEnoUARTReadRxData(void)
    {
        uint32 rxData = 0u;

    #if (EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST)
        uint32 locTail;
    #endif /* (EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST) */

        #if (EnoUART_EON_CHECK_RX_SW_BUFFER)
        {
            if (EnoUART_EON_rxBufferHead != EnoUART_EON_rxBufferTail)
            {
                /* There is data in RX software buffer */

                /* Calculate index to read from */
                locTail = (EnoUART_EON_rxBufferTail + 1u);

                if (EnoUART_EON_INTERNAL_RX_BUFFER_SIZE == locTail)
                {
                    locTail = 0u;
                }

                /* Get data from RX software buffer */
                rxData = EnoUART_EON_GetWordFromRxBuffer(locTail);

                /* Change index in the buffer */
                EnoUART_EON_rxBufferTail = locTail;

                #if (EnoUART_EON_CHECK_EnoUART_RTS_CONTROL_FLOW)
                {
                    /* Check if RX Not Empty is disabled in the interrupt */
                    if (0u == (EnoUART_EON_INTR_RX_MASK_REG & EnoUART_EON_INTR_RX_NOT_EMPTY))
                    {
                        /* Enable RX Not Empty interrupt source to continue
                        * receiving data into software buffer.
                        */
                        EnoUART_EON_INTR_RX_MASK_REG |= EnoUART_EON_INTR_RX_NOT_EMPTY;
                    }
                }
                #endif

            }
        }
        #else
        {
            /* Read data from RX FIFO */
            rxData = EnoUART_EON_RX_FIFO_RD_REG;
        }
        #endif

        return (rxData);
    }


    /*******************************************************************************
    * Function Name: EnoUART_EON_SpiEnoUARTGetRxBufferSize
    ********************************************************************************
    *
    * Summary:
    *  Returns the number of received data elements in the receive buffer.
    *   - RX software buffer disabled: returns the number of used entries in
    *     RX FIFO.
    *   - RX software buffer enabled: returns the number of elements which were
    *     placed in the receive buffer. This does not include the hardware RX FIFO.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  Number of received data elements
    *
    *******************************************************************************/
    uint32 EnoUART_EON_SpiEnoUARTGetRxBufferSize(void)
    {
        uint32 size;
    #if (EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST)
        uint32 locHead;
    #endif /* (EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST) */

        #if (EnoUART_EON_CHECK_RX_SW_BUFFER)
        {
            locHead = EnoUART_EON_rxBufferHead;

            if(locHead >= EnoUART_EON_rxBufferTail)
            {
                size = (locHead - EnoUART_EON_rxBufferTail);
            }
            else
            {
                size = (locHead + (EnoUART_EON_INTERNAL_RX_BUFFER_SIZE - EnoUART_EON_rxBufferTail));
            }
        }
        #else
        {
            size = EnoUART_EON_GET_RX_FIFO_ENTRIES;
        }
        #endif

        return (size);
    }


    /*******************************************************************************
    * Function Name: EnoUART_EON_SpiEnoUARTClearRxBuffer
    ********************************************************************************
    *
    * Summary:
    *  Clears the receive buffer and RX FIFO.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_SpiEnoUARTClearRxBuffer(void)
    {
        #if (EnoUART_EON_CHECK_RX_SW_BUFFER)
        {
            /* Lock from component interruption */
            EnoUART_EON_DisableInt();

            /* Flush RX software buffer */
            EnoUART_EON_rxBufferHead = EnoUART_EON_rxBufferTail;
            EnoUART_EON_rxBufferOverflow = 0u;

            EnoUART_EON_CLEAR_RX_FIFO;
            EnoUART_EON_ClearRxInterruptSource(EnoUART_EON_INTR_RX_ALL);

            #if (EnoUART_EON_CHECK_EnoUART_RTS_CONTROL_FLOW)
            {
                /* Enable RX Not Empty interrupt source to continue receiving
                * data into software buffer.
                */
                EnoUART_EON_INTR_RX_MASK_REG |= EnoUART_EON_INTR_RX_NOT_EMPTY;
            }
            #endif

            /* Release lock */
            EnoUART_EON_EnableInt();
        }
        #else
        {
            EnoUART_EON_CLEAR_RX_FIFO;
        }
        #endif
    }

#endif /* (EnoUART_EON_RX_DIRECTION) */


#if(EnoUART_EON_TX_DIRECTION)
    /*******************************************************************************
    * Function Name: EnoUART_EON_SpiEnoUARTWriteTxData
    ********************************************************************************
    *
    * Summary:
    *  Places a data entry into the transmit buffer to be sent at the next available
    *  bus time.
    *  This function is blocking and waits until there is space available to put the
    *  requested data in the transmit buffer.
    *
    * Parameters:
    *  txDataByte: the data to be transmitted.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_SpiEnoUARTWriteTxData(uint32 txData)
    {
    #if (EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST)
        uint32 locHead;
    #endif /* (EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST) */

        #if (EnoUART_EON_CHECK_TX_SW_BUFFER)
        {
            /* Put data directly into the TX FIFO */
            if ((EnoUART_EON_txBufferHead == EnoUART_EON_txBufferTail) &&
                (EnoUART_EON_SPI_EnoUART_FIFO_SIZE != EnoUART_EON_GET_TX_FIFO_ENTRIES))
            {
                /* TX software buffer is empty: put data directly in TX FIFO */
                EnoUART_EON_TX_FIFO_WR_REG = txData;
            }
            /* Put data into TX software buffer */
            else
            {
                /* Head index to put data */
                locHead = (EnoUART_EON_txBufferHead + 1u);

                /* Adjust TX software buffer index */
                if (EnoUART_EON_TX_BUFFER_SIZE == locHead)
                {
                    locHead = 0u;
                }

                /* Wait for space in TX software buffer */
                while (locHead == EnoUART_EON_txBufferTail)
                {
                }

                /* TX software buffer has at least one room */

                /* Clear old status of INTR_TX_NOT_FULL. It sets at the end of transfer when TX FIFO is empty. */
                EnoUART_EON_ClearTxInterruptSource(EnoUART_EON_INTR_TX_NOT_FULL);

                EnoUART_EON_PutWordInTxBuffer(locHead, txData);

                EnoUART_EON_txBufferHead = locHead;

                /* Check if TX Not Full is disabled in interrupt */
                if (0u == (EnoUART_EON_INTR_TX_MASK_REG & EnoUART_EON_INTR_TX_NOT_FULL))
                {
                    /* Enable TX Not Full interrupt source to transmit from software buffer */
                    EnoUART_EON_INTR_TX_MASK_REG |= (uint32) EnoUART_EON_INTR_TX_NOT_FULL;
                }
            }
        }
        #else
        {
            /* Wait until TX FIFO has space to put data element */
            while (EnoUART_EON_SPI_EnoUART_FIFO_SIZE == EnoUART_EON_GET_TX_FIFO_ENTRIES)
            {
            }

            EnoUART_EON_TX_FIFO_WR_REG = txData;
        }
        #endif
    }


    /*******************************************************************************
    * Function Name: EnoUART_EON_SpiEnoUARTPutArray
    ********************************************************************************
    *
    * Summary:
    *  Places an array of data into the transmit buffer to be sent.
    *  This function is blocking and waits until there is a space available to put
    *  all the requested data in the transmit buffer. The array size can be greater
    *  than transmit buffer size.
    *
    * Parameters:
    *  wrBuf:  pointer to an array with data to be placed in transmit buffer.
    *  count:  number of data elements to be placed in the transmit buffer.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_SpiEnoUARTPutArray(const uint8 wrBuf[], uint32 count)
    {
        uint32 i;

        for (i=0u; i < count; i++)
        {
            EnoUART_EON_SpiEnoUARTWriteTxData((uint32) wrBuf[i]);
        }
    }


    /*******************************************************************************
    * Function Name: EnoUART_EON_SpiEnoUARTGetTxBufferSize
    ********************************************************************************
    *
    * Summary:
    * Returns the number of elements currently in the transmit buffer.
    *  - TX software buffer is disabled: returns the number of used entries in
    *    TX FIFO.
    *  - TX software buffer is enabled: returns the number of elements currently
    *    used in the transmit buffer. This number does not include used entries in
    *    the TX FIFO. The transmit buffer size is zero until the TX FIFO is
    *    not full.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  Number of data elements ready to transmit.
    *
    *******************************************************************************/
    uint32 EnoUART_EON_SpiEnoUARTGetTxBufferSize(void)
    {
        uint32 size;
    #if (EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST)
        uint32 locTail;
    #endif /* (EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST) */

        #if (EnoUART_EON_CHECK_TX_SW_BUFFER)
        {
            /* Get current Tail index */
            locTail = EnoUART_EON_txBufferTail;

            if (EnoUART_EON_txBufferHead >= locTail)
            {
                size = (EnoUART_EON_txBufferHead - locTail);
            }
            else
            {
                size = (EnoUART_EON_txBufferHead + (EnoUART_EON_TX_BUFFER_SIZE - locTail));
            }
        }
        #else
        {
            size = EnoUART_EON_GET_TX_FIFO_ENTRIES;
        }
        #endif

        return (size);
    }


    /*******************************************************************************
    * Function Name: EnoUART_EON_SpiEnoUARTClearTxBuffer
    ********************************************************************************
    *
    * Summary:
    *  Clears the transmit buffer and TX FIFO.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_SpiEnoUARTClearTxBuffer(void)
    {
        #if (EnoUART_EON_CHECK_TX_SW_BUFFER)
        {
            /* Lock from component interruption */
            EnoUART_EON_DisableInt();

            /* Flush TX software buffer */
            EnoUART_EON_txBufferHead = EnoUART_EON_txBufferTail;

            EnoUART_EON_INTR_TX_MASK_REG &= (uint32) ~EnoUART_EON_INTR_TX_NOT_FULL;
            EnoUART_EON_CLEAR_TX_FIFO;
            EnoUART_EON_ClearTxInterruptSource(EnoUART_EON_INTR_TX_ALL);

            /* Release lock */
            EnoUART_EON_EnableInt();
        }
        #else
        {
            EnoUART_EON_CLEAR_TX_FIFO;
        }
        #endif
    }

#endif /* (EnoUART_EON_TX_DIRECTION) */


/*******************************************************************************
* Function Name: EnoUART_EON_SpiEnoUARTDisableIntRx
********************************************************************************
*
* Summary:
*  Disables the RX interrupt sources.
*
* Parameters:
*  None
*
* Return:
*  Returns the RX interrupt sources enabled before the function call.
*
*******************************************************************************/
uint32 EnoUART_EON_SpiEnoUARTDisableIntRx(void)
{
    uint32 intSource;

    intSource = EnoUART_EON_GetRxInterruptMode();

    EnoUART_EON_SetRxInterruptMode(EnoUART_EON_NO_INTR_SOURCES);

    return (intSource);
}


/*******************************************************************************
* Function Name: EnoUART_EON_SpiEnoUARTDisableIntTx
********************************************************************************
*
* Summary:
*  Disables TX interrupt sources.
*
* Parameters:
*  None
*
* Return:
*  Returns TX interrupt sources enabled before function call.
*
*******************************************************************************/
uint32 EnoUART_EON_SpiEnoUARTDisableIntTx(void)
{
    uint32 intSourceMask;

    intSourceMask = EnoUART_EON_GetTxInterruptMode();

    EnoUART_EON_SetTxInterruptMode(EnoUART_EON_NO_INTR_SOURCES);

    return (intSourceMask);
}


#if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
    /*******************************************************************************
    * Function Name: EnoUART_EON_PutWordInRxBuffer
    ********************************************************************************
    *
    * Summary:
    *  Stores a byte/word into the RX buffer.
    *  Only available in the Unconfigured operation mode.
    *
    * Parameters:
    *  index:      index to store data byte/word in the RX buffer.
    *  rxDataByte: byte/word to store.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_PutWordInRxBuffer(uint32 idx, uint32 rxDataByte)
    {
        /* Put data in buffer */
        if (EnoUART_EON_ONE_BYTE_WIDTH == EnoUART_EON_rxDataBits)
        {
            EnoUART_EON_rxBuffer[idx] = ((uint8) rxDataByte);
        }
        else
        {
            EnoUART_EON_rxBuffer[(uint32)(idx << 1u)]      = LO8(LO16(rxDataByte));
            EnoUART_EON_rxBuffer[(uint32)(idx << 1u) + 1u] = HI8(LO16(rxDataByte));
        }
    }


    /*******************************************************************************
    * Function Name: EnoUART_EON_GetWordFromRxBuffer
    ********************************************************************************
    *
    * Summary:
    *  Reads byte/word from RX buffer.
    *  Only available in the Unconfigured operation mode.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  Returns byte/word read from RX buffer.
    *
    *******************************************************************************/
    uint32 EnoUART_EON_GetWordFromRxBuffer(uint32 idx)
    {
        uint32 value;

        if (EnoUART_EON_ONE_BYTE_WIDTH == EnoUART_EON_rxDataBits)
        {
            value = EnoUART_EON_rxBuffer[idx];
        }
        else
        {
            value  = (uint32) EnoUART_EON_rxBuffer[(uint32)(idx << 1u)];
            value |= (uint32) ((uint32)EnoUART_EON_rxBuffer[(uint32)(idx << 1u) + 1u] << 8u);
        }

        return (value);
    }


    /*******************************************************************************
    * Function Name: EnoUART_EON_PutWordInTxBuffer
    ********************************************************************************
    *
    * Summary:
    *  Stores byte/word into the TX buffer.
    *  Only available in the Unconfigured operation mode.
    *
    * Parameters:
    *  idx:        index to store data byte/word in the TX buffer.
    *  txDataByte: byte/word to store.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_PutWordInTxBuffer(uint32 idx, uint32 txDataByte)
    {
        /* Put data in buffer */
        if (EnoUART_EON_ONE_BYTE_WIDTH == EnoUART_EON_txDataBits)
        {
            EnoUART_EON_txBuffer[idx] = ((uint8) txDataByte);
        }
        else
        {
            EnoUART_EON_txBuffer[(uint32)(idx << 1u)]      = LO8(LO16(txDataByte));
            EnoUART_EON_txBuffer[(uint32)(idx << 1u) + 1u] = HI8(LO16(txDataByte));
        }
    }


    /*******************************************************************************
    * Function Name: EnoUART_EON_GetWordFromTxBuffer
    ********************************************************************************
    *
    * Summary:
    *  Reads byte/word from the TX buffer.
    *  Only available in the Unconfigured operation mode.
    *
    * Parameters:
    *  idx: index to get data byte/word from the TX buffer.
    *
    * Return:
    *  Returns byte/word read from the TX buffer.
    *
    *******************************************************************************/
    uint32 EnoUART_EON_GetWordFromTxBuffer(uint32 idx)
    {
        uint32 value;

        if (EnoUART_EON_ONE_BYTE_WIDTH == EnoUART_EON_txDataBits)
        {
            value = (uint32) EnoUART_EON_txBuffer[idx];
        }
        else
        {
            value  = (uint32) EnoUART_EON_txBuffer[(uint32)(idx << 1u)];
            value |= (uint32) ((uint32) EnoUART_EON_txBuffer[(uint32)(idx << 1u) + 1u] << 8u);
        }

        return (value);
    }

#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */


/* [] END OF FILE */
