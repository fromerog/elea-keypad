/*******************************************************************************
* File Name: .h
* Version 3.0
*
* Description:
*  This private file provides constants and parameter values for the
*  SCB Component.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PVT_EnoUART_EON_H)
#define CY_SCB_PVT_EnoUART_EON_H

#include "EnoUART_EON.h"


/***************************************
*     Private Function Prototypes
***************************************/

/* APIs to service INTR_I2C_EC register */
#define EnoUART_EON_SetI2CExtClkInterruptMode(interruptMask) EnoUART_EON_WRITE_INTR_I2C_EC_MASK(interruptMask)
#define EnoUART_EON_ClearI2CExtClkInterruptSource(interruptMask) EnoUART_EON_CLEAR_INTR_I2C_EC(interruptMask)
#define EnoUART_EON_GetI2CExtClkInterruptSource()                (EnoUART_EON_INTR_I2C_EC_REG)
#define EnoUART_EON_GetI2CExtClkInterruptMode()                  (EnoUART_EON_INTR_I2C_EC_MASK_REG)
#define EnoUART_EON_GetI2CExtClkInterruptSourceMasked()          (EnoUART_EON_INTR_I2C_EC_MASKED_REG)

#if (!EnoUART_EON_CY_SCBIP_V1)
    /* APIs to service INTR_SPI_EC register */
    #define EnoUART_EON_SetSpiExtClkInterruptMode(interruptMask) \
                                                                EnoUART_EON_WRITE_INTR_SPI_EC_MASK(interruptMask)
    #define EnoUART_EON_ClearSpiExtClkInterruptSource(interruptMask) \
                                                                EnoUART_EON_CLEAR_INTR_SPI_EC(interruptMask)
    #define EnoUART_EON_GetExtSpiClkInterruptSource()                 (EnoUART_EON_INTR_SPI_EC_REG)
    #define EnoUART_EON_GetExtSpiClkInterruptMode()                   (EnoUART_EON_INTR_SPI_EC_MASK_REG)
    #define EnoUART_EON_GetExtSpiClkInterruptSourceMasked()           (EnoUART_EON_INTR_SPI_EC_MASKED_REG)
#endif /* (!EnoUART_EON_CY_SCBIP_V1) */

#if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
    extern void EnoUART_EON_SetPins(uint32 mode, uint32 subMode, uint32 EnoUARTEnableMask);
#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*     Vars with External Linkage
***************************************/

#if (EnoUART_EON_SCB_IRQ_INTERNAL)
#if !defined (CY_REMOVE_EnoUART_EON_CUSTOM_INTR_HANDLER)
    extern cyisraddress EnoUART_EON_customIntrHandler;
#endif /* !defined (CY_REMOVE_EnoUART_EON_CUSTOM_INTR_HANDLER) */
#endif /* (EnoUART_EON_SCB_IRQ_INTERNAL) */

extern EnoUART_EON_BACKUP_STRUCT EnoUART_EON_backup;

#if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Common configuration variables */
    extern uint8 EnoUART_EON_scbMode;
    extern uint8 EnoUART_EON_scbEnableWake;
    extern uint8 EnoUART_EON_scbEnableIntr;

    /* I2C configuration variables */
    extern uint8 EnoUART_EON_mode;
    extern uint8 EnoUART_EON_acceptAddr;

    /* SPI/EnoUART configuration variables */
    extern volatile uint8 * EnoUART_EON_rxBuffer;
    extern uint8   EnoUART_EON_rxDataBits;
    extern uint32  EnoUART_EON_rxBufferSize;

    extern volatile uint8 * EnoUART_EON_txBuffer;
    extern uint8   EnoUART_EON_txDataBits;
    extern uint32  EnoUART_EON_txBufferSize;

    /* EZI2C configuration variables */
    extern uint8 EnoUART_EON_numberOfAddr;
    extern uint8 EnoUART_EON_subAddrSize;
#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*        Conditional Macro
****************************************/

#if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Defines run time operation mode */
    #define EnoUART_EON_SCB_MODE_I2C_RUNTM_CFG     (EnoUART_EON_SCB_MODE_I2C      == EnoUART_EON_scbMode)
    #define EnoUART_EON_SCB_MODE_SPI_RUNTM_CFG     (EnoUART_EON_SCB_MODE_SPI      == EnoUART_EON_scbMode)
    #define EnoUART_EON_SCB_MODE_EnoUART_RUNTM_CFG    (EnoUART_EON_SCB_MODE_EnoUART     == EnoUART_EON_scbMode)
    #define EnoUART_EON_SCB_MODE_EZI2C_RUNTM_CFG   (EnoUART_EON_SCB_MODE_EZI2C    == EnoUART_EON_scbMode)
    #define EnoUART_EON_SCB_MODE_UNCONFIG_RUNTM_CFG \
                                                        (EnoUART_EON_SCB_MODE_UNCONFIG == EnoUART_EON_scbMode)

    /* Defines wakeup enable */
    #define EnoUART_EON_SCB_WAKE_ENABLE_CHECK       (0u != EnoUART_EON_scbEnableWake)
#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */

/* Defines maximum number of SCB pins */
#if (!EnoUART_EON_CY_SCBIP_V1)
    #define EnoUART_EON_SCB_PINS_NUMBER    (7u)
#else
    #define EnoUART_EON_SCB_PINS_NUMBER    (2u)
#endif /* (!EnoUART_EON_CY_SCBIP_V1) */

#endif /* (CY_SCB_PVT_EnoUART_EON_H) */


/* [] END OF FILE */
