/***************************************************************************//**
* \file ENO_EnoUART_SPI_EnoUART_INT.c
* \version 3.20
*
* \brief
*  This file provides the source code to the Interrupt Service Routine for
*  the SCB Component in SPI and EnoUART modes.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ENO_EnoUART_PVT.h"
#include "ENO_EnoUART_SPI_EnoUART_PVT.h"
#include "cyapicallbacks.h"

#if (ENO_EnoUART_SCB_IRQ_INTERNAL)
/*******************************************************************************
* Function Name: ENO_EnoUART_SPI_EnoUART_ISR
****************************************************************************//**
*
*  Handles the Interrupt Service Routine for the SCB SPI or EnoUART modes.
*
*******************************************************************************/
CY_ISR(ENO_EnoUART_SPI_EnoUART_ISR)
{
#if (ENO_EnoUART_INTERNAL_RX_SW_BUFFER_CONST)
    uint32 locHead;
#endif /* (ENO_EnoUART_INTERNAL_RX_SW_BUFFER_CONST) */

#if (ENO_EnoUART_INTERNAL_TX_SW_BUFFER_CONST)
    uint32 locTail;
#endif /* (ENO_EnoUART_INTERNAL_TX_SW_BUFFER_CONST) */

#ifdef ENO_EnoUART_SPI_EnoUART_ISR_ENTRY_CALLBACK
    ENO_EnoUART_SPI_EnoUART_ISR_EntryCallback();
#endif /* ENO_EnoUART_SPI_EnoUART_ISR_ENTRY_CALLBACK */

    if (NULL != ENO_EnoUART_customIntrHandler)
    {
        ENO_EnoUART_customIntrHandler();
    }

    #if(ENO_EnoUART_CHECK_SPI_WAKE_ENABLE)
    {
        /* Clear SPI wakeup source */
        ENO_EnoUART_ClearSpiExtClkInterruptSource(ENO_EnoUART_INTR_SPI_EC_WAKE_UP);
    }
    #endif

    #if (ENO_EnoUART_CHECK_RX_SW_BUFFER)
    {
        if (ENO_EnoUART_CHECK_INTR_RX_MASKED(ENO_EnoUART_INTR_RX_NOT_EMPTY))
        {
            do
            {
                /* Move local head index */
                locHead = (ENO_EnoUART_rxBufferHead + 1u);

                /* Adjust local head index */
                if (ENO_EnoUART_INTERNAL_RX_BUFFER_SIZE == locHead)
                {
                    locHead = 0u;
                }

                if (locHead == ENO_EnoUART_rxBufferTail)
                {
                    #if (ENO_EnoUART_CHECK_EnoUART_RTS_CONTROL_FLOW)
                    {
                        /* There is no space in the software buffer - disable the
                        * RX Not Empty interrupt source. The data elements are
                        * still being received into the RX FIFO until the RTS signal
                        * stops the transmitter. After the data element is read from the
                        * buffer, the RX Not Empty interrupt source is enabled to
                        * move the next data element in the software buffer.
                        */
                        ENO_EnoUART_INTR_RX_MASK_REG &= ~ENO_EnoUART_INTR_RX_NOT_EMPTY;
                        break;
                    }
                    #else
                    {
                        /* Overflow: through away received data element */
                        (void) ENO_EnoUART_RX_FIFO_RD_REG;
                        ENO_EnoUART_rxBufferOverflow = (uint8) ENO_EnoUART_INTR_RX_OVERFLOW;
                    }
                    #endif
                }
                else
                {
                    /* Store received data */
                    ENO_EnoUART_PutWordInRxBuffer(locHead, ENO_EnoUART_RX_FIFO_RD_REG);

                    /* Move head index */
                    ENO_EnoUART_rxBufferHead = locHead;
                }
            }
            while(0u != ENO_EnoUART_GET_RX_FIFO_ENTRIES);

            ENO_EnoUART_ClearRxInterruptSource(ENO_EnoUART_INTR_RX_NOT_EMPTY);
        }
    }
    #endif


    #if (ENO_EnoUART_CHECK_TX_SW_BUFFER)
    {
        if (ENO_EnoUART_CHECK_INTR_TX_MASKED(ENO_EnoUART_INTR_TX_NOT_FULL))
        {
            do
            {
                /* Check for room in TX software buffer */
                if (ENO_EnoUART_txBufferHead != ENO_EnoUART_txBufferTail)
                {
                    /* Move local tail index */
                    locTail = (ENO_EnoUART_txBufferTail + 1u);

                    /* Adjust local tail index */
                    if (ENO_EnoUART_TX_BUFFER_SIZE == locTail)
                    {
                        locTail = 0u;
                    }

                    /* Put data into TX FIFO */
                    ENO_EnoUART_TX_FIFO_WR_REG = ENO_EnoUART_GetWordFromTxBuffer(locTail);

                    /* Move tail index */
                    ENO_EnoUART_txBufferTail = locTail;
                }
                else
                {
                    /* TX software buffer is empty: complete transfer */
                    ENO_EnoUART_DISABLE_INTR_TX(ENO_EnoUART_INTR_TX_NOT_FULL);
                    break;
                }
            }
            while (ENO_EnoUART_SPI_EnoUART_FIFO_SIZE != ENO_EnoUART_GET_TX_FIFO_ENTRIES);

            ENO_EnoUART_ClearTxInterruptSource(ENO_EnoUART_INTR_TX_NOT_FULL);
        }
    }
    #endif

#ifdef ENO_EnoUART_SPI_EnoUART_ISR_EXIT_CALLBACK
    ENO_EnoUART_SPI_EnoUART_ISR_ExitCallback();
#endif /* ENO_EnoUART_SPI_EnoUART_ISR_EXIT_CALLBACK */

}

#endif /* (ENO_EnoUART_SCB_IRQ_INTERNAL) */


/* [] END OF FILE */
