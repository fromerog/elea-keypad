/***************************************************************************//**
* \file EnoUART_PM.c
* \version 3.20
*
* \brief
*  This file provides the source code to the Power Management support for
*  the SCB Component.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "EnoUART.h"
#include "EnoUART_PVT.h"

#if(EnoUART_SCB_MODE_I2C_INC)
    #include "EnoUART_I2C_PVT.h"
#endif /* (EnoUART_SCB_MODE_I2C_INC) */

#if(EnoUART_SCB_MODE_EZI2C_INC)
    #include "EnoUART_EZI2C_PVT.h"
#endif /* (EnoUART_SCB_MODE_EZI2C_INC) */

#if(EnoUART_SCB_MODE_SPI_INC || EnoUART_SCB_MODE_EnoUART_INC)
    #include "EnoUART_SPI_EnoUART_PVT.h"
#endif /* (EnoUART_SCB_MODE_SPI_INC || EnoUART_SCB_MODE_EnoUART_INC) */


/***************************************
*   Backup Structure declaration
***************************************/

#if(EnoUART_SCB_MODE_UNCONFIG_CONST_CFG || \
   (EnoUART_SCB_MODE_I2C_CONST_CFG   && (!EnoUART_I2C_WAKE_ENABLE_CONST))   || \
   (EnoUART_SCB_MODE_EZI2C_CONST_CFG && (!EnoUART_EZI2C_WAKE_ENABLE_CONST)) || \
   (EnoUART_SCB_MODE_SPI_CONST_CFG   && (!EnoUART_SPI_WAKE_ENABLE_CONST))   || \
   (EnoUART_SCB_MODE_EnoUART_CONST_CFG  && (!EnoUART_EnoUART_WAKE_ENABLE_CONST)))

    EnoUART_BACKUP_STRUCT EnoUART_backup =
    {
        0u, /* enableState */
    };
#endif


/*******************************************************************************
* Function Name: EnoUART_Sleep
****************************************************************************//**
*
*  Prepares the EnoUART component to enter Deep Sleep.
*  The “Enable wakeup from Deep Sleep Mode” selection has an influence on this 
*  function implementation:
*  - Checked: configures the component to be wakeup source from Deep Sleep.
*  - Unchecked: stores the current component state (enabled or disabled) and 
*    disables the component. See SCB_Stop() function for details about component 
*    disabling.
*
*  Call the EnoUART_Sleep() function before calling the 
*  CyPmSysDeepSleep() function. 
*  Refer to the PSoC Creator System Reference Guide for more information about 
*  power management functions and Low power section of this document for the 
*  selected mode.
*
*  This function should not be called before entering Sleep.
*
*******************************************************************************/
void EnoUART_Sleep(void)
{
#if(EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)

    if(EnoUART_SCB_WAKE_ENABLE_CHECK)
    {
        if(EnoUART_SCB_MODE_I2C_RUNTM_CFG)
        {
            EnoUART_I2CSaveConfig();
        }
        else if(EnoUART_SCB_MODE_EZI2C_RUNTM_CFG)
        {
            EnoUART_EzI2CSaveConfig();
        }
    #if(!EnoUART_CY_SCBIP_V1)
        else if(EnoUART_SCB_MODE_SPI_RUNTM_CFG)
        {
            EnoUART_SpiSaveConfig();
        }
        else if(EnoUART_SCB_MODE_EnoUART_RUNTM_CFG)
        {
            EnoUART_EnoUARTSaveConfig();
        }
    #endif /* (!EnoUART_CY_SCBIP_V1) */
        else
        {
            /* Unknown mode */
        }
    }
    else
    {
        EnoUART_backup.enableState = (uint8) EnoUART_GET_CTRL_ENABLED;

        if(0u != EnoUART_backup.enableState)
        {
            EnoUART_Stop();
        }
    }

#else

    #if (EnoUART_SCB_MODE_I2C_CONST_CFG && EnoUART_I2C_WAKE_ENABLE_CONST)
        EnoUART_I2CSaveConfig();

    #elif (EnoUART_SCB_MODE_EZI2C_CONST_CFG && EnoUART_EZI2C_WAKE_ENABLE_CONST)
        EnoUART_EzI2CSaveConfig();

    #elif (EnoUART_SCB_MODE_SPI_CONST_CFG && EnoUART_SPI_WAKE_ENABLE_CONST)
        EnoUART_SpiSaveConfig();

    #elif (EnoUART_SCB_MODE_EnoUART_CONST_CFG && EnoUART_EnoUART_WAKE_ENABLE_CONST)
        EnoUART_EnoUARTSaveConfig();

    #else

        EnoUART_backup.enableState = (uint8) EnoUART_GET_CTRL_ENABLED;

        if(0u != EnoUART_backup.enableState)
        {
            EnoUART_Stop();
        }

    #endif /* defined (EnoUART_SCB_MODE_I2C_CONST_CFG) && (EnoUART_I2C_WAKE_ENABLE_CONST) */

#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: EnoUART_Wakeup
****************************************************************************//**
*
*  Prepares the EnoUART component for Active mode operation after 
*  Deep Sleep.
*  The “Enable wakeup from Deep Sleep Mode” selection has influence on this 
*  function implementation:
*  - Checked: restores the component Active mode configuration.
*  - Unchecked: enables the component if it was enabled before enter Deep Sleep.
*
*  This function should not be called after exiting Sleep.
*
*  \sideeffect
*   Calling the EnoUART_Wakeup() function without first calling the 
*   EnoUART_Sleep() function may produce unexpected behavior.
*
*******************************************************************************/
void EnoUART_Wakeup(void)
{
#if(EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)

    if(EnoUART_SCB_WAKE_ENABLE_CHECK)
    {
        if(EnoUART_SCB_MODE_I2C_RUNTM_CFG)
        {
            EnoUART_I2CRestoreConfig();
        }
        else if(EnoUART_SCB_MODE_EZI2C_RUNTM_CFG)
        {
            EnoUART_EzI2CRestoreConfig();
        }
    #if(!EnoUART_CY_SCBIP_V1)
        else if(EnoUART_SCB_MODE_SPI_RUNTM_CFG)
        {
            EnoUART_SpiRestoreConfig();
        }
        else if(EnoUART_SCB_MODE_EnoUART_RUNTM_CFG)
        {
            EnoUART_EnoUARTRestoreConfig();
        }
    #endif /* (!EnoUART_CY_SCBIP_V1) */
        else
        {
            /* Unknown mode */
        }
    }
    else
    {
        if(0u != EnoUART_backup.enableState)
        {
            EnoUART_Enable();
        }
    }

#else

    #if (EnoUART_SCB_MODE_I2C_CONST_CFG  && EnoUART_I2C_WAKE_ENABLE_CONST)
        EnoUART_I2CRestoreConfig();

    #elif (EnoUART_SCB_MODE_EZI2C_CONST_CFG && EnoUART_EZI2C_WAKE_ENABLE_CONST)
        EnoUART_EzI2CRestoreConfig();

    #elif (EnoUART_SCB_MODE_SPI_CONST_CFG && EnoUART_SPI_WAKE_ENABLE_CONST)
        EnoUART_SpiRestoreConfig();

    #elif (EnoUART_SCB_MODE_EnoUART_CONST_CFG && EnoUART_EnoUART_WAKE_ENABLE_CONST)
        EnoUART_EnoUARTRestoreConfig();

    #else

        if(0u != EnoUART_backup.enableState)
        {
            EnoUART_Enable();
        }

    #endif /* (EnoUART_I2C_WAKE_ENABLE_CONST) */

#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/* [] END OF FILE */
