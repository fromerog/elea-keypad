/*******************************************************************************
* File Name: EnoUART_EON_BOOT.c
* Version 3.0
*
* Description:
*  This file provides the source code of the bootloader communication APIs
*  for the SCB Component Unconfigured mode.
*
* Note:
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "EnoUART_EON_BOOT.h"

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_EON_BTLDR_COMM_ENABLED) && \
                                (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)

/*******************************************************************************
* Function Name: EnoUART_EON_CyBtldrCommStart
********************************************************************************
*
* Summary:
*  Calls the CyBtldrCommStart function of the bootloader communication
*  component for the selected mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void EnoUART_EON_CyBtldrCommStart(void)
{
    if (EnoUART_EON_SCB_MODE_I2C_RUNTM_CFG)
    {
        EnoUART_EON_I2CCyBtldrCommStart();
    }
    else if (EnoUART_EON_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        EnoUART_EON_EzI2CCyBtldrCommStart();
    }
#if (!EnoUART_EON_CY_SCBIP_V1)
    else if (EnoUART_EON_SCB_MODE_SPI_RUNTM_CFG)
    {
        EnoUART_EON_SpiCyBtldrCommStart();
    }
    else if (EnoUART_EON_SCB_MODE_EnoUART_RUNTM_CFG)
    {
        EnoUART_EON_EnoUARTCyBtldrCommStart();
    }
#endif /* (!EnoUART_EON_CY_SCBIP_V1) */
    else
    {
        /* Unknown mode: do nothing */
    }
}


/*******************************************************************************
* Function Name: EnoUART_EON_CyBtldrCommStop
********************************************************************************
*
* Summary:
*  Calls the CyBtldrCommStop function of the bootloader communication
*  component for the selected mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void EnoUART_EON_CyBtldrCommStop(void)
{
    if (EnoUART_EON_SCB_MODE_I2C_RUNTM_CFG)
    {
        EnoUART_EON_I2CCyBtldrCommStop();
    }
    else if (EnoUART_EON_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        EnoUART_EON_EzI2CCyBtldrCommStop();
    }
#if (!EnoUART_EON_CY_SCBIP_V1)
    else if (EnoUART_EON_SCB_MODE_SPI_RUNTM_CFG)
    {
        EnoUART_EON_SpiCyBtldrCommStop();
    }
    else if (EnoUART_EON_SCB_MODE_EnoUART_RUNTM_CFG)
    {
        EnoUART_EON_EnoUARTCyBtldrCommStop();
    }
#endif /* (!EnoUART_EON_CY_SCBIP_V1) */
    else
    {
        /* Unknown mode: do nothing */
    }
}


/*******************************************************************************
* Function Name: EnoUART_EON_CyBtldrCommReset
********************************************************************************
*
* Summary:
*  Calls the CyBtldrCommReset function of the bootloader communication
*  component for the selected mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void EnoUART_EON_CyBtldrCommReset(void)
{
    if(EnoUART_EON_SCB_MODE_I2C_RUNTM_CFG)
    {
        EnoUART_EON_I2CCyBtldrCommReset();
    }
    else if(EnoUART_EON_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        EnoUART_EON_EzI2CCyBtldrCommReset();
    }
#if (!EnoUART_EON_CY_SCBIP_V1)
    else if(EnoUART_EON_SCB_MODE_SPI_RUNTM_CFG)
    {
        EnoUART_EON_SpiCyBtldrCommReset();
    }
    else if(EnoUART_EON_SCB_MODE_EnoUART_RUNTM_CFG)
    {
        EnoUART_EON_EnoUARTCyBtldrCommReset();
    }
#endif /* (!EnoUART_EON_CY_SCBIP_V1) */
    else
    {
        /* Unknown mode: do nothing */
    }
}


/*******************************************************************************
* Function Name: EnoUART_EON_CyBtldrCommRead
********************************************************************************
*
* Summary:
*  Calls the CyBtldrCommRead function of the bootloader communication
*  component for the selected mode.
*
* Parameters:
*  pData:    Pointer to storage for the block of data to be read from the
*            bootloader host
*  size:     Number of bytes to be read.
*  count:    Pointer to the variable to write the number of bytes actually
*            read.
*  timeOut:  Number of units in 10 ms to wait before returning because of a
*            timeout.
*
* Return:
*  Returns CYRET_SUCCESS if no problem was encountered or returns the value
*  that best describes the problem.
*
*******************************************************************************/
cystatus EnoUART_EON_CyBtldrCommRead(uint8 pData[], uint16 size, uint16 * count, uint8 timeOut)
{
    cystatus status;

    if(EnoUART_EON_SCB_MODE_I2C_RUNTM_CFG)
    {
        status = EnoUART_EON_I2CCyBtldrCommRead(pData, size, count, timeOut);
    }
    else if(EnoUART_EON_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        status = EnoUART_EON_EzI2CCyBtldrCommRead(pData, size, count, timeOut);
    }
#if (!EnoUART_EON_CY_SCBIP_V1)
    else if(EnoUART_EON_SCB_MODE_SPI_RUNTM_CFG)
    {
        status = EnoUART_EON_SpiCyBtldrCommRead(pData, size, count, timeOut);
    }
    else if(EnoUART_EON_SCB_MODE_EnoUART_RUNTM_CFG)
    {
        status = EnoUART_EON_EnoUARTCyBtldrCommRead(pData, size, count, timeOut);
    }
#endif /* (!EnoUART_EON_CY_SCBIP_V1) */
    else
    {
        status = CYRET_INVALID_STATE; /* Unknown mode: return invalid status */
    }

    return(status);
}


/*******************************************************************************
* Function Name: EnoUART_EON_CyBtldrCommWrite
********************************************************************************
*
* Summary:
*  Calls the CyBtldrCommWrite  function of the bootloader communication
*  component for the selected mode.
*
* Parameters:
*  pData:    Pointer to the block of data to be written to the bootloader host.
*  size:     Number of bytes to be written.
*  count:    Pointer to the variable to write the number of bytes actually
*            written.
*  timeOut:  Number of units in 10 ms to wait before returning because of a
*            timeout.
*
* Return:
*  Returns CYRET_SUCCESS if no problem was encountered or returns the value
*  that best describes the problem.
*
*******************************************************************************/
cystatus EnoUART_EON_CyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut)
{
    cystatus status;

    if(EnoUART_EON_SCB_MODE_I2C_RUNTM_CFG)
    {
        status = EnoUART_EON_I2CCyBtldrCommWrite(pData, size, count, timeOut);
    }
    else if(EnoUART_EON_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        status = EnoUART_EON_EzI2CCyBtldrCommWrite(pData, size, count, timeOut);
    }
#if (!EnoUART_EON_CY_SCBIP_V1)
    else if(EnoUART_EON_SCB_MODE_SPI_RUNTM_CFG)
    {
        status = EnoUART_EON_SpiCyBtldrCommWrite(pData, size, count, timeOut);
    }
    else if(EnoUART_EON_SCB_MODE_EnoUART_RUNTM_CFG)
    {
        status = EnoUART_EON_EnoUARTCyBtldrCommWrite(pData, size, count, timeOut);
    }
#endif /* (!EnoUART_EON_CY_SCBIP_V1) */
    else
    {
        status = CYRET_INVALID_STATE; /* Unknown mode: return invalid status */
    }

    return(status);
}

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_EON_BTLDR_COMM_MODE_ENABLED) */


/* [] END OF FILE */
