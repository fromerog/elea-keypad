/*******************************************************************************
* File Name: EnoCLK.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_EnoCLK_H)
#define CY_CLOCK_EnoCLK_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void EnoCLK_StartEx(uint32 alignClkDiv);
#define EnoCLK_Start() \
    EnoCLK_StartEx(EnoCLK__PA_DIV_ID)

#else

void EnoCLK_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void EnoCLK_Stop(void);

void EnoCLK_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 EnoCLK_GetDividerRegister(void);
uint8  EnoCLK_GetFractionalDividerRegister(void);

#define EnoCLK_Enable()                         EnoCLK_Start()
#define EnoCLK_Disable()                        EnoCLK_Stop()
#define EnoCLK_SetDividerRegister(clkDivider, reset)  \
    EnoCLK_SetFractionalDividerRegister((clkDivider), 0u)
#define EnoCLK_SetDivider(clkDivider)           EnoCLK_SetDividerRegister((clkDivider), 1u)
#define EnoCLK_SetDividerValue(clkDivider)      EnoCLK_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define EnoCLK_DIV_ID     EnoCLK__DIV_ID

#define EnoCLK_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define EnoCLK_CTRL_REG   (*(reg32 *)EnoCLK__CTRL_REGISTER)
#define EnoCLK_DIV_REG    (*(reg32 *)EnoCLK__DIV_REGISTER)

#define EnoCLK_CMD_DIV_SHIFT          (0u)
#define EnoCLK_CMD_PA_DIV_SHIFT       (8u)
#define EnoCLK_CMD_DISABLE_SHIFT      (30u)
#define EnoCLK_CMD_ENABLE_SHIFT       (31u)

#define EnoCLK_CMD_DISABLE_MASK       ((uint32)((uint32)1u << EnoCLK_CMD_DISABLE_SHIFT))
#define EnoCLK_CMD_ENABLE_MASK        ((uint32)((uint32)1u << EnoCLK_CMD_ENABLE_SHIFT))

#define EnoCLK_DIV_FRAC_MASK  (0x000000F8u)
#define EnoCLK_DIV_FRAC_SHIFT (3u)
#define EnoCLK_DIV_INT_MASK   (0xFFFFFF00u)
#define EnoCLK_DIV_INT_SHIFT  (8u)

#else 

#define EnoCLK_DIV_REG        (*(reg32 *)EnoCLK__REGISTER)
#define EnoCLK_ENABLE_REG     EnoCLK_DIV_REG
#define EnoCLK_DIV_FRAC_MASK  EnoCLK__FRAC_MASK
#define EnoCLK_DIV_FRAC_SHIFT (16u)
#define EnoCLK_DIV_INT_MASK   EnoCLK__DIVIDER_MASK
#define EnoCLK_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_EnoCLK_H) */

/* [] END OF FILE */
