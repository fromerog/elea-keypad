/***************************************************************************//**
* \file EnoUART_SPI_EnoUART.c
* \version 3.20
*
* \brief
*  This file provides the source code to the API for the SCB Component in
*  SPI and EnoUART modes.
*
* Note:
*
*******************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "EnoUART_PVT.h"
#include "EnoUART_SPI_EnoUART_PVT.h"

/***************************************
*        SPI/EnoUART Private Vars
***************************************/

#if(EnoUART_INTERNAL_RX_SW_BUFFER_CONST)
    /* Start index to put data into the software receive buffer.*/
    volatile uint32 EnoUART_rxBufferHead;
    /* Start index to get data from the software receive buffer.*/
    volatile uint32 EnoUART_rxBufferTail;
    /**
    * \addtogroup group_globals
    * \{
    */
    /** Sets when internal software receive buffer overflow
    *  was occurred.
    */
    volatile uint8  EnoUART_rxBufferOverflow;
    /** \} globals */
#endif /* (EnoUART_INTERNAL_RX_SW_BUFFER_CONST) */

#if(EnoUART_INTERNAL_TX_SW_BUFFER_CONST)
    /* Start index to put data into the software transmit buffer.*/
    volatile uint32 EnoUART_txBufferHead;
    /* Start index to get data from the software transmit buffer.*/
    volatile uint32 EnoUART_txBufferTail;
#endif /* (EnoUART_INTERNAL_TX_SW_BUFFER_CONST) */

#if(EnoUART_INTERNAL_RX_SW_BUFFER)
    /* Add one element to the buffer to receive full packet. One byte in receive buffer is always empty */
    volatile uint8 EnoUART_rxBufferInternal[EnoUART_INTERNAL_RX_BUFFER_SIZE];
#endif /* (EnoUART_INTERNAL_RX_SW_BUFFER) */

#if(EnoUART_INTERNAL_TX_SW_BUFFER)
    volatile uint8 EnoUART_txBufferInternal[EnoUART_TX_BUFFER_SIZE];
#endif /* (EnoUART_INTERNAL_TX_SW_BUFFER) */


#if(EnoUART_RX_DIRECTION)
    /*******************************************************************************
    * Function Name: EnoUART_SpiEnoUARTReadRxData
    ****************************************************************************//**
    *
    *  Retrieves the next data element from the receive buffer.
    *   - RX software buffer is disabled: Returns data element retrieved from
    *     RX FIFO. Undefined data will be returned if the RX FIFO is empty.
    *   - RX software buffer is enabled: Returns data element from the software
    *     receive buffer. Zero value is returned if the software receive buffer
    *     is empty.
    *
    * \return
    *  Next data element from the receive buffer. 
    *  The amount of data bits to be received depends on RX data bits selection 
    *  (the data bit counting starts from LSB of return value).
    *
    * \globalvars
    *  EnoUART_rxBufferHead - the start index to put data into the 
    *  software receive buffer.
    *  EnoUART_rxBufferTail - the start index to get data from the 
    *  software receive buffer.
    *
    *******************************************************************************/
    uint32 EnoUART_SpiEnoUARTReadRxData(void)
    {
        uint32 rxData = 0u;

    #if (EnoUART_INTERNAL_RX_SW_BUFFER_CONST)
        uint32 locTail;
    #endif /* (EnoUART_INTERNAL_RX_SW_BUFFER_CONST) */

        #if (EnoUART_CHECK_RX_SW_BUFFER)
        {
            if (EnoUART_rxBufferHead != EnoUART_rxBufferTail)
            {
                /* There is data in RX software buffer */

                /* Calculate index to read from */
                locTail = (EnoUART_rxBufferTail + 1u);

                if (EnoUART_INTERNAL_RX_BUFFER_SIZE == locTail)
                {
                    locTail = 0u;
                }

                /* Get data from RX software buffer */
                rxData = EnoUART_GetWordFromRxBuffer(locTail);

                /* Change index in the buffer */
                EnoUART_rxBufferTail = locTail;

                #if (EnoUART_CHECK_EnoUART_RTS_CONTROL_FLOW)
                {
                    /* Check if RX Not Empty is disabled in the interrupt */
                    if (0u == (EnoUART_INTR_RX_MASK_REG & EnoUART_INTR_RX_NOT_EMPTY))
                    {
                        /* Enable RX Not Empty interrupt source to continue
                        * receiving data into software buffer.
                        */
                        EnoUART_INTR_RX_MASK_REG |= EnoUART_INTR_RX_NOT_EMPTY;
                    }
                }
                #endif

            }
        }
        #else
        {
            /* Read data from RX FIFO */
            rxData = EnoUART_RX_FIFO_RD_REG;
        }
        #endif

        return (rxData);
    }


    /*******************************************************************************
    * Function Name: EnoUART_SpiEnoUARTGetRxBufferSize
    ****************************************************************************//**
    *
    *  Returns the number of received data elements in the receive buffer.
    *   - RX software buffer disabled: returns the number of used entries in
    *     RX FIFO.
    *   - RX software buffer enabled: returns the number of elements which were
    *     placed in the receive buffer. This does not include the hardware RX FIFO.
    *
    * \return
    *  Number of received data elements.
    *
    * \globalvars
    *  EnoUART_rxBufferHead - the start index to put data into the 
    *  software receive buffer.
    *  EnoUART_rxBufferTail - the start index to get data from the 
    *  software receive buffer.
    *
    *******************************************************************************/
    uint32 EnoUART_SpiEnoUARTGetRxBufferSize(void)
    {
        uint32 size;
    #if (EnoUART_INTERNAL_RX_SW_BUFFER_CONST)
        uint32 locHead;
    #endif /* (EnoUART_INTERNAL_RX_SW_BUFFER_CONST) */

        #if (EnoUART_CHECK_RX_SW_BUFFER)
        {
            locHead = EnoUART_rxBufferHead;

            if(locHead >= EnoUART_rxBufferTail)
            {
                size = (locHead - EnoUART_rxBufferTail);
            }
            else
            {
                size = (locHead + (EnoUART_INTERNAL_RX_BUFFER_SIZE - EnoUART_rxBufferTail));
            }
        }
        #else
        {
            size = EnoUART_GET_RX_FIFO_ENTRIES;
        }
        #endif

        return (size);
    }


    /*******************************************************************************
    * Function Name: EnoUART_SpiEnoUARTClearRxBuffer
    ****************************************************************************//**
    *
    *  Clears the receive buffer and RX FIFO.
    *
    * \globalvars
    *  EnoUART_rxBufferHead - the start index to put data into the 
    *  software receive buffer.
    *  EnoUART_rxBufferTail - the start index to get data from the 
    *  software receive buffer.
    *
    *******************************************************************************/
    void EnoUART_SpiEnoUARTClearRxBuffer(void)
    {
        #if (EnoUART_CHECK_RX_SW_BUFFER)
        {
            /* Lock from component interruption */
            EnoUART_DisableInt();

            /* Flush RX software buffer */
            EnoUART_rxBufferHead = EnoUART_rxBufferTail;
            EnoUART_rxBufferOverflow = 0u;

            EnoUART_CLEAR_RX_FIFO;
            EnoUART_ClearRxInterruptSource(EnoUART_INTR_RX_ALL);

            #if (EnoUART_CHECK_EnoUART_RTS_CONTROL_FLOW)
            {
                /* Enable RX Not Empty interrupt source to continue receiving
                * data into software buffer.
                */
                EnoUART_INTR_RX_MASK_REG |= EnoUART_INTR_RX_NOT_EMPTY;
            }
            #endif
            
            /* Release lock */
            EnoUART_EnableInt();
        }
        #else
        {
            EnoUART_CLEAR_RX_FIFO;
        }
        #endif
    }

#endif /* (EnoUART_RX_DIRECTION) */


#if(EnoUART_TX_DIRECTION)
    /*******************************************************************************
    * Function Name: EnoUART_SpiEnoUARTWriteTxData
    ****************************************************************************//**
    *
    *  Places a data entry into the transmit buffer to be sent at the next available
    *  bus time.
    *  This function is blocking and waits until there is space available to put the
    *  requested data in the transmit buffer.
    *
    *  \param txDataByte: the data to be transmitted.
    *   The amount of data bits to be transmitted depends on TX data bits selection 
    *   (the data bit counting starts from LSB of txDataByte).
    *
    * \globalvars
    *  EnoUART_txBufferHead - the start index to put data into the 
    *  software transmit buffer.
    *  EnoUART_txBufferTail - start index to get data from the software
    *  transmit buffer.
    *
    *******************************************************************************/
    void EnoUART_SpiEnoUARTWriteTxData(uint32 txData)
    {
    #if (EnoUART_INTERNAL_TX_SW_BUFFER_CONST)
        uint32 locHead;
    #endif /* (EnoUART_INTERNAL_TX_SW_BUFFER_CONST) */

        #if (EnoUART_CHECK_TX_SW_BUFFER)
        {
            /* Put data directly into the TX FIFO */
            if ((EnoUART_txBufferHead == EnoUART_txBufferTail) &&
                (EnoUART_SPI_EnoUART_FIFO_SIZE != EnoUART_GET_TX_FIFO_ENTRIES))
            {
                /* TX software buffer is empty: put data directly in TX FIFO */
                EnoUART_TX_FIFO_WR_REG = txData;
            }
            /* Put data into TX software buffer */
            else
            {
                /* Head index to put data */
                locHead = (EnoUART_txBufferHead + 1u);

                /* Adjust TX software buffer index */
                if (EnoUART_TX_BUFFER_SIZE == locHead)
                {
                    locHead = 0u;
                }

                /* Wait for space in TX software buffer */
                while (locHead == EnoUART_txBufferTail)
                {
                }

                /* TX software buffer has at least one room */

                /* Clear old status of INTR_TX_NOT_FULL. It sets at the end of transfer when TX FIFO is empty. */
                EnoUART_ClearTxInterruptSource(EnoUART_INTR_TX_NOT_FULL);

                EnoUART_PutWordInTxBuffer(locHead, txData);

                EnoUART_txBufferHead = locHead;

                /* Check if TX Not Full is disabled in interrupt */
                if (0u == (EnoUART_INTR_TX_MASK_REG & EnoUART_INTR_TX_NOT_FULL))
                {
                    /* Enable TX Not Full interrupt source to transmit from software buffer */
                    EnoUART_INTR_TX_MASK_REG |= (uint32) EnoUART_INTR_TX_NOT_FULL;
                }
            }
        }
        #else
        {
            /* Wait until TX FIFO has space to put data element */
            while (EnoUART_SPI_EnoUART_FIFO_SIZE == EnoUART_GET_TX_FIFO_ENTRIES)
            {
            }

            EnoUART_TX_FIFO_WR_REG = txData;
        }
        #endif
    }


    /*******************************************************************************
    * Function Name: EnoUART_SpiEnoUARTPutArray
    ****************************************************************************//**
    *
    *  Places an array of data into the transmit buffer to be sent.
    *  This function is blocking and waits until there is a space available to put
    *  all the requested data in the transmit buffer. The array size can be greater
    *  than transmit buffer size.
    *
    * \param wrBuf: pointer to an array of data to be placed in transmit buffer. 
    *  The width of the data to be transmitted depends on TX data width selection 
    *  (the data bit counting starts from LSB for each array element).
    * \param count: number of data elements to be placed in the transmit buffer.
    *
    * \globalvars
    *  EnoUART_txBufferHead - the start index to put data into the 
    *  software transmit buffer.
    *  EnoUART_txBufferTail - start index to get data from the software
    *  transmit buffer.
    *
    *******************************************************************************/
    void EnoUART_SpiEnoUARTPutArray(const uint8 wrBuf[], uint32 count)
    {
        uint32 i;

        for (i=0u; i < count; i++)
        {
            EnoUART_SpiEnoUARTWriteTxData((uint32) wrBuf[i]);
        }
    }


    /*******************************************************************************
    * Function Name: EnoUART_SpiEnoUARTGetTxBufferSize
    ****************************************************************************//**
    *
    *  Returns the number of elements currently in the transmit buffer.
    *   - TX software buffer is disabled: returns the number of used entries in
    *     TX FIFO.
    *   - TX software buffer is enabled: returns the number of elements currently
    *     used in the transmit buffer. This number does not include used entries in
    *     the TX FIFO. The transmit buffer size is zero until the TX FIFO is
    *     not full.
    *
    * \return
    *  Number of data elements ready to transmit.
    *
    * \globalvars
    *  EnoUART_txBufferHead - the start index to put data into the 
    *  software transmit buffer.
    *  EnoUART_txBufferTail - start index to get data from the software
    *  transmit buffer.
    *
    *******************************************************************************/
    uint32 EnoUART_SpiEnoUARTGetTxBufferSize(void)
    {
        uint32 size;
    #if (EnoUART_INTERNAL_TX_SW_BUFFER_CONST)
        uint32 locTail;
    #endif /* (EnoUART_INTERNAL_TX_SW_BUFFER_CONST) */

        #if (EnoUART_CHECK_TX_SW_BUFFER)
        {
            /* Get current Tail index */
            locTail = EnoUART_txBufferTail;

            if (EnoUART_txBufferHead >= locTail)
            {
                size = (EnoUART_txBufferHead - locTail);
            }
            else
            {
                size = (EnoUART_txBufferHead + (EnoUART_TX_BUFFER_SIZE - locTail));
            }
        }
        #else
        {
            size = EnoUART_GET_TX_FIFO_ENTRIES;
        }
        #endif

        return (size);
    }


    /*******************************************************************************
    * Function Name: EnoUART_SpiEnoUARTClearTxBuffer
    ****************************************************************************//**
    *
    *  Clears the transmit buffer and TX FIFO.
    *
    * \globalvars
    *  EnoUART_txBufferHead - the start index to put data into the 
    *  software transmit buffer.
    *  EnoUART_txBufferTail - start index to get data from the software
    *  transmit buffer.
    *
    *******************************************************************************/
    void EnoUART_SpiEnoUARTClearTxBuffer(void)
    {
        #if (EnoUART_CHECK_TX_SW_BUFFER)
        {
            /* Lock from component interruption */
            EnoUART_DisableInt();

            /* Flush TX software buffer */
            EnoUART_txBufferHead = EnoUART_txBufferTail;

            EnoUART_INTR_TX_MASK_REG &= (uint32) ~EnoUART_INTR_TX_NOT_FULL;
            EnoUART_CLEAR_TX_FIFO;
            EnoUART_ClearTxInterruptSource(EnoUART_INTR_TX_ALL);

            /* Release lock */
            EnoUART_EnableInt();
        }
        #else
        {
            EnoUART_CLEAR_TX_FIFO;
        }
        #endif
    }

#endif /* (EnoUART_TX_DIRECTION) */


/*******************************************************************************
* Function Name: EnoUART_SpiEnoUARTDisableIntRx
****************************************************************************//**
*
*  Disables the RX interrupt sources.
*
*  \return
*   Returns the RX interrupt sources enabled before the function call.
*
*******************************************************************************/
uint32 EnoUART_SpiEnoUARTDisableIntRx(void)
{
    uint32 intSource;

    intSource = EnoUART_GetRxInterruptMode();

    EnoUART_SetRxInterruptMode(EnoUART_NO_INTR_SOURCES);

    return (intSource);
}


/*******************************************************************************
* Function Name: EnoUART_SpiEnoUARTDisableIntTx
****************************************************************************//**
*
*  Disables TX interrupt sources.
*
*  \return
*   Returns TX interrupt sources enabled before function call.
*
*******************************************************************************/
uint32 EnoUART_SpiEnoUARTDisableIntTx(void)
{
    uint32 intSourceMask;

    intSourceMask = EnoUART_GetTxInterruptMode();

    EnoUART_SetTxInterruptMode(EnoUART_NO_INTR_SOURCES);

    return (intSourceMask);
}


#if(EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    /*******************************************************************************
    * Function Name: EnoUART_PutWordInRxBuffer
    ****************************************************************************//**
    *
    *  Stores a byte/word into the RX buffer.
    *  Only available in the Unconfigured operation mode.
    *
    *  \param index:      index to store data byte/word in the RX buffer.
    *  \param rxDataByte: byte/word to store.
    *
    *******************************************************************************/
    void EnoUART_PutWordInRxBuffer(uint32 idx, uint32 rxDataByte)
    {
        /* Put data in buffer */
        if (EnoUART_ONE_BYTE_WIDTH == EnoUART_rxDataBits)
        {
            EnoUART_rxBuffer[idx] = ((uint8) rxDataByte);
        }
        else
        {
            EnoUART_rxBuffer[(uint32)(idx << 1u)]      = LO8(LO16(rxDataByte));
            EnoUART_rxBuffer[(uint32)(idx << 1u) + 1u] = HI8(LO16(rxDataByte));
        }
    }


    /*******************************************************************************
    * Function Name: EnoUART_GetWordFromRxBuffer
    ****************************************************************************//**
    *
    *  Reads byte/word from RX buffer.
    *  Only available in the Unconfigured operation mode.
    *
    *  \return
    *   Returns byte/word read from RX buffer.
    *
    *******************************************************************************/
    uint32 EnoUART_GetWordFromRxBuffer(uint32 idx)
    {
        uint32 value;

        if (EnoUART_ONE_BYTE_WIDTH == EnoUART_rxDataBits)
        {
            value = EnoUART_rxBuffer[idx];
        }
        else
        {
            value  = (uint32) EnoUART_rxBuffer[(uint32)(idx << 1u)];
            value |= (uint32) ((uint32)EnoUART_rxBuffer[(uint32)(idx << 1u) + 1u] << 8u);
        }

        return (value);
    }


    /*******************************************************************************
    * Function Name: EnoUART_PutWordInTxBuffer
    ****************************************************************************//**
    *
    *  Stores byte/word into the TX buffer.
    *  Only available in the Unconfigured operation mode.
    *
    *  \param idx:        index to store data byte/word in the TX buffer.
    *  \param txDataByte: byte/word to store.
    *
    *******************************************************************************/
    void EnoUART_PutWordInTxBuffer(uint32 idx, uint32 txDataByte)
    {
        /* Put data in buffer */
        if (EnoUART_ONE_BYTE_WIDTH == EnoUART_txDataBits)
        {
            EnoUART_txBuffer[idx] = ((uint8) txDataByte);
        }
        else
        {
            EnoUART_txBuffer[(uint32)(idx << 1u)]      = LO8(LO16(txDataByte));
            EnoUART_txBuffer[(uint32)(idx << 1u) + 1u] = HI8(LO16(txDataByte));
        }
    }


    /*******************************************************************************
    * Function Name: EnoUART_GetWordFromTxBuffer
    ****************************************************************************//**
    *
    *  Reads byte/word from the TX buffer.
    *  Only available in the Unconfigured operation mode.
    *
    *  \param idx: index to get data byte/word from the TX buffer.
    *
    *  \return
    *   Returns byte/word read from the TX buffer.
    *
    *******************************************************************************/
    uint32 EnoUART_GetWordFromTxBuffer(uint32 idx)
    {
        uint32 value;

        if (EnoUART_ONE_BYTE_WIDTH == EnoUART_txDataBits)
        {
            value = (uint32) EnoUART_txBuffer[idx];
        }
        else
        {
            value  = (uint32) EnoUART_txBuffer[(uint32)(idx << 1u)];
            value |= (uint32) ((uint32) EnoUART_txBuffer[(uint32)(idx << 1u) + 1u] << 8u);
        }

        return (value);
    }

#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */


/* [] END OF FILE */
