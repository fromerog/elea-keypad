/*******************************************************************************
* File Name: EnoUART_EON_BOOT.h
* Version 3.0
*
* Description:
*  This file provides constants and parameter values of the bootloader
*  communication APIs for the SCB Component.
*
* Note:
*
********************************************************************************
* Copyright 2014-2015, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_BOOT_EnoUART_EON_H)
#define CY_SCB_BOOT_EnoUART_EON_H

#include "EnoUART_EON_PVT.h"

#if (EnoUART_EON_SCB_MODE_I2C_INC)
    #include "EnoUART_EON_I2C.h"
#endif /* (EnoUART_EON_SCB_MODE_I2C_INC) */

#if (EnoUART_EON_SCB_MODE_EZI2C_INC)
    #include "EnoUART_EON_EZI2C.h"
#endif /* (EnoUART_EON_SCB_MODE_EZI2C_INC) */

#if (EnoUART_EON_SCB_MODE_SPI_INC || EnoUART_EON_SCB_MODE_EnoUART_INC)
    #include "EnoUART_EON_SPI_EnoUART.h"
#endif /* (EnoUART_EON_SCB_MODE_SPI_INC || EnoUART_EON_SCB_MODE_EnoUART_INC) */


/***************************************
*  Conditional Compilation Parameters
****************************************/

/* Bootloader communication interface enable */
#define EnoUART_EON_BTLDR_COMM_ENABLED ((CYDEV_BOOTLOADER_IO_COMP == CyBtldr_EnoUART_EON) || \
                                             (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_Custom_Interface))

/* Enable I2C bootloader communication */
#if (EnoUART_EON_SCB_MODE_I2C_INC)
    #define EnoUART_EON_I2C_BTLDR_COMM_ENABLED     (EnoUART_EON_BTLDR_COMM_ENABLED && \
                                                            (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             EnoUART_EON_I2C_SLAVE_CONST))
#else
     #define EnoUART_EON_I2C_BTLDR_COMM_ENABLED    (0u)
#endif /* (EnoUART_EON_SCB_MODE_I2C_INC) */

/* EZI2C does not support bootloader communication. Provide empty APIs */
#if (EnoUART_EON_SCB_MODE_EZI2C_INC)
    #define EnoUART_EON_EZI2C_BTLDR_COMM_ENABLED   (EnoUART_EON_BTLDR_COMM_ENABLED && \
                                                         EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
#else
    #define EnoUART_EON_EZI2C_BTLDR_COMM_ENABLED   (0u)
#endif /* (EnoUART_EON_EZI2C_BTLDR_COMM_ENABLED) */

/* Enable SPI bootloader communication */
#if (EnoUART_EON_SCB_MODE_SPI_INC)
    #define EnoUART_EON_SPI_BTLDR_COMM_ENABLED     (EnoUART_EON_BTLDR_COMM_ENABLED && \
                                                            (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             EnoUART_EON_SPI_SLAVE_CONST))
#else
        #define EnoUART_EON_SPI_BTLDR_COMM_ENABLED (0u)
#endif /* (EnoUART_EON_SPI_BTLDR_COMM_ENABLED) */

/* Enable EnoUART bootloader communication */
#if (EnoUART_EON_SCB_MODE_EnoUART_INC)
       #define EnoUART_EON_EnoUART_BTLDR_COMM_ENABLED    (EnoUART_EON_BTLDR_COMM_ENABLED && \
                                                            (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             (EnoUART_EON_EnoUART_RX_DIRECTION && \
                                                              EnoUART_EON_EnoUART_TX_DIRECTION)))
#else
     #define EnoUART_EON_EnoUART_BTLDR_COMM_ENABLED   (0u)
#endif /* (EnoUART_EON_EnoUART_BTLDR_COMM_ENABLED) */

/* Enable bootloader communication */
#define EnoUART_EON_BTLDR_COMM_MODE_ENABLED    (EnoUART_EON_I2C_BTLDR_COMM_ENABLED   || \
                                                     EnoUART_EON_SPI_BTLDR_COMM_ENABLED   || \
                                                     EnoUART_EON_EZI2C_BTLDR_COMM_ENABLED || \
                                                     EnoUART_EON_EnoUART_BTLDR_COMM_ENABLED)


/***************************************
*        Function Prototypes
***************************************/

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_EON_I2C_BTLDR_COMM_ENABLED)
    /* I2C Bootloader physical layer functions */
    void EnoUART_EON_I2CCyBtldrCommStart(void);
    void EnoUART_EON_I2CCyBtldrCommStop (void);
    void EnoUART_EON_I2CCyBtldrCommReset(void);
    cystatus EnoUART_EON_I2CCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus EnoUART_EON_I2CCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map I2C specific bootloader communication APIs to SCB specific APIs */
    #if (EnoUART_EON_SCB_MODE_I2C_CONST_CFG)
        #define EnoUART_EON_CyBtldrCommStart   EnoUART_EON_I2CCyBtldrCommStart
        #define EnoUART_EON_CyBtldrCommStop    EnoUART_EON_I2CCyBtldrCommStop
        #define EnoUART_EON_CyBtldrCommReset   EnoUART_EON_I2CCyBtldrCommReset
        #define EnoUART_EON_CyBtldrCommRead    EnoUART_EON_I2CCyBtldrCommRead
        #define EnoUART_EON_CyBtldrCommWrite   EnoUART_EON_I2CCyBtldrCommWrite
    #endif /* (EnoUART_EON_SCB_MODE_I2C_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_EON_I2C_BTLDR_COMM_ENABLED) */


#if defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_EON_EZI2C_BTLDR_COMM_ENABLED)
    /* Bootloader physical layer functions */
    void EnoUART_EON_EzI2CCyBtldrCommStart(void);
    void EnoUART_EON_EzI2CCyBtldrCommStop (void);
    void EnoUART_EON_EzI2CCyBtldrCommReset(void);
    cystatus EnoUART_EON_EzI2CCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus EnoUART_EON_EzI2CCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map EZI2C specific bootloader communication APIs to SCB specific APIs */
    #if (EnoUART_EON_SCB_MODE_EZI2C_CONST_CFG)
        #define EnoUART_EON_CyBtldrCommStart   EnoUART_EON_EzI2CCyBtldrCommStart
        #define EnoUART_EON_CyBtldrCommStop    EnoUART_EON_EzI2CCyBtldrCommStop
        #define EnoUART_EON_CyBtldrCommReset   EnoUART_EON_EzI2CCyBtldrCommReset
        #define EnoUART_EON_CyBtldrCommRead    EnoUART_EON_EzI2CCyBtldrCommRead
        #define EnoUART_EON_CyBtldrCommWrite   EnoUART_EON_EzI2CCyBtldrCommWrite
    #endif /* (EnoUART_EON_SCB_MODE_EZI2C_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_EON_EZI2C_BTLDR_COMM_ENABLED) */

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_EON_SPI_BTLDR_COMM_ENABLED)
    /* SPI Bootloader physical layer functions */
    void EnoUART_EON_SpiCyBtldrCommStart(void);
    void EnoUART_EON_SpiCyBtldrCommStop (void);
    void EnoUART_EON_SpiCyBtldrCommReset(void);
    cystatus EnoUART_EON_SpiCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus EnoUART_EON_SpiCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map SPI specific bootloader communication APIs to SCB specific APIs */
    #if (EnoUART_EON_SCB_MODE_SPI_CONST_CFG)
        #define EnoUART_EON_CyBtldrCommStart   EnoUART_EON_SpiCyBtldrCommStart
        #define EnoUART_EON_CyBtldrCommStop    EnoUART_EON_SpiCyBtldrCommStop
        #define EnoUART_EON_CyBtldrCommReset   EnoUART_EON_SpiCyBtldrCommReset
        #define EnoUART_EON_CyBtldrCommRead    EnoUART_EON_SpiCyBtldrCommRead
        #define EnoUART_EON_CyBtldrCommWrite   EnoUART_EON_SpiCyBtldrCommWrite
    #endif /* (EnoUART_EON_SCB_MODE_SPI_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_EON_SPI_BTLDR_COMM_ENABLED) */

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_EON_EnoUART_BTLDR_COMM_ENABLED)
    /* EnoUART Bootloader physical layer functions */
    void EnoUART_EON_EnoUARTCyBtldrCommStart(void);
    void EnoUART_EON_EnoUARTCyBtldrCommStop (void);
    void EnoUART_EON_EnoUARTCyBtldrCommReset(void);
    cystatus EnoUART_EON_EnoUARTCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus EnoUART_EON_EnoUARTCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map EnoUART specific bootloader communication APIs to SCB specific APIs */
    #if (EnoUART_EON_SCB_MODE_EnoUART_CONST_CFG)
        #define EnoUART_EON_CyBtldrCommStart   EnoUART_EON_EnoUARTCyBtldrCommStart
        #define EnoUART_EON_CyBtldrCommStop    EnoUART_EON_EnoUARTCyBtldrCommStop
        #define EnoUART_EON_CyBtldrCommReset   EnoUART_EON_EnoUARTCyBtldrCommReset
        #define EnoUART_EON_CyBtldrCommRead    EnoUART_EON_EnoUARTCyBtldrCommRead
        #define EnoUART_EON_CyBtldrCommWrite   EnoUART_EON_EnoUARTCyBtldrCommWrite
    #endif /* (EnoUART_EON_SCB_MODE_EnoUART_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_EON_EnoUART_BTLDR_COMM_ENABLED) */

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_EON_BTLDR_COMM_ENABLED)
    #if (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
        /* Bootloader physical layer functions */
        void EnoUART_EON_CyBtldrCommStart(void);
        void EnoUART_EON_CyBtldrCommStop (void);
        void EnoUART_EON_CyBtldrCommReset(void);
        cystatus EnoUART_EON_CyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
        cystatus EnoUART_EON_CyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    #endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */

    /* Map SCB specific bootloader communication APIs to common APIs */
    #if (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_EnoUART_EON)
        #define CyBtldrCommStart    EnoUART_EON_CyBtldrCommStart
        #define CyBtldrCommStop     EnoUART_EON_CyBtldrCommStop
        #define CyBtldrCommReset    EnoUART_EON_CyBtldrCommReset
        #define CyBtldrCommWrite    EnoUART_EON_CyBtldrCommWrite
        #define CyBtldrCommRead     EnoUART_EON_CyBtldrCommRead
    #endif /* (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_EnoUART_EON) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_EON_BTLDR_COMM_ENABLED) */


/***************************************
*           API Constants
***************************************/

/* Timeout unit in milliseconds */
#define EnoUART_EON_WAIT_1_MS  (1u)

/* Return number of bytes to copy into bootloader buffer */
#define EnoUART_EON_BYTES_TO_COPY(actBufSize, bufSize) \
                            ( ((uint32)(actBufSize) < (uint32)(bufSize)) ? \
                                ((uint32) (actBufSize)) : ((uint32) (bufSize)) )

/* Size of Read/Write buffers for I2C bootloader  */
#define EnoUART_EON_I2C_BTLDR_SIZEOF_READ_BUFFER   (64u)
#define EnoUART_EON_I2C_BTLDR_SIZEOF_WRITE_BUFFER  (64u)

/* Byte to byte time interval: calculated basing on current component
* data rate configuration, can be defined in project if required.
*/
#ifndef EnoUART_EON_SPI_BYTE_TO_BYTE
    #define EnoUART_EON_SPI_BYTE_TO_BYTE   (160u)
#endif

/* Byte to byte time interval: calculated basing on current component
* baud rate configuration, can be defined in the project if required.
*/
#ifndef EnoUART_EON_EnoUART_BYTE_TO_BYTE
    #define EnoUART_EON_EnoUART_BYTE_TO_BYTE  (350u)
#endif /* EnoUART_EON_EnoUART_BYTE_TO_BYTE */

#endif /* (CY_SCB_BOOT_EnoUART_EON_H) */


/* [] END OF FILE */
