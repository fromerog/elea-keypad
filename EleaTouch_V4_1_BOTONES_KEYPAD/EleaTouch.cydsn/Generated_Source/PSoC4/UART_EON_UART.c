/*******************************************************************************
* File Name: EnoUART_EON_EnoUART.c
* Version 3.0
*
* Description:
*  This file provides the source code to the API for the SCB Component in
*  EnoUART mode.
*
* Note:
*
*******************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "EnoUART_EON_PVT.h"
#include "EnoUART_EON_SPI_EnoUART_PVT.h"
#include "cyapicallbacks.h"

#if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)

    /***************************************
    *  Configuration Structure Initialization
    ***************************************/

    const EnoUART_EON_EnoUART_INIT_STRUCT EnoUART_EON_configEnoUART =
    {
        EnoUART_EON_EnoUART_SUB_MODE,
        EnoUART_EON_EnoUART_DIRECTION,
        EnoUART_EON_EnoUART_DATA_BITS_NUM,
        EnoUART_EON_EnoUART_PARITY_TYPE,
        EnoUART_EON_EnoUART_STOP_BITS_NUM,
        EnoUART_EON_EnoUART_OVS_FACTOR,
        EnoUART_EON_EnoUART_IRDA_LOW_POWER,
        EnoUART_EON_EnoUART_MEDIAN_FILTER_ENABLE,
        EnoUART_EON_EnoUART_RETRY_ON_NACK,
        EnoUART_EON_EnoUART_IRDA_POLARITY,
        EnoUART_EON_EnoUART_DROP_ON_PARITY_ERR,
        EnoUART_EON_EnoUART_DROP_ON_FRAME_ERR,
        EnoUART_EON_EnoUART_WAKE_ENABLE,
        0u,
        NULL,
        0u,
        NULL,
        EnoUART_EON_EnoUART_MP_MODE_ENABLE,
        EnoUART_EON_EnoUART_MP_ACCEPT_ADDRESS,
        EnoUART_EON_EnoUART_MP_RX_ADDRESS,
        EnoUART_EON_EnoUART_MP_RX_ADDRESS_MASK,
        (uint32) EnoUART_EON_SCB_IRQ_INTERNAL,
        EnoUART_EON_EnoUART_INTR_RX_MASK,
        EnoUART_EON_EnoUART_RX_TRIGGER_LEVEL,
        EnoUART_EON_EnoUART_INTR_TX_MASK,
        EnoUART_EON_EnoUART_TX_TRIGGER_LEVEL,
        (uint8) EnoUART_EON_EnoUART_BYTE_MODE_ENABLE,
        (uint8) EnoUART_EON_EnoUART_CTS_ENABLE,
        (uint8) EnoUART_EON_EnoUART_CTS_POLARITY,
        (uint8) EnoUART_EON_EnoUART_RTS_POLARITY,
        (uint8) EnoUART_EON_EnoUART_RTS_FIFO_LEVEL
    };


    /*******************************************************************************
    * Function Name: EnoUART_EON_EnoUARTInit
    ********************************************************************************
    *
    * Summary:
    *  Configures the SCB for the EnoUART operation.
    *
    * Parameters:
    *  config:  Pointer to a structure that contains the following ordered list of
    *           fields. These fields match the selections available in the
    *           customizer.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_EnoUARTInit(const EnoUART_EON_EnoUART_INIT_STRUCT *config)
    {
        uint32 pinsConfig;

        if (NULL == config)
        {
            CYASSERT(0u != 0u); /* Halt execution due to bad function parameter */
        }
        else
        {
            /* Get direction to configure EnoUART pins: TX, RX or TX+RX */
            pinsConfig  = config->direction;

        #if !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1)
            /* Add RTS and CTS pins to configure */
            pinsConfig |= (0u != config->rtsRxFifoLevel) ? (EnoUART_EON_EnoUART_RTS_PIN_ENABLE) : (0u);
            pinsConfig |= (0u != config->enableCts)      ? (EnoUART_EON_EnoUART_CTS_PIN_ENABLE) : (0u);
        #endif /* !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1) */

            /* Configure pins */
            EnoUART_EON_SetPins(EnoUART_EON_SCB_MODE_EnoUART, config->mode, pinsConfig);

            /* Store internal configuration */
            EnoUART_EON_scbMode       = (uint8) EnoUART_EON_SCB_MODE_EnoUART;
            EnoUART_EON_scbEnableWake = (uint8) config->enableWake;
            EnoUART_EON_scbEnableIntr = (uint8) config->enableInterrupt;

            /* Set RX direction internal variables */
            EnoUART_EON_rxBuffer      =         config->rxBuffer;
            EnoUART_EON_rxDataBits    = (uint8) config->dataBits;
            EnoUART_EON_rxBufferSize  = (uint8) config->rxBufferSize;

            /* Set TX direction internal variables */
            EnoUART_EON_txBuffer      =         config->txBuffer;
            EnoUART_EON_txDataBits    = (uint8) config->dataBits;
            EnoUART_EON_txBufferSize  = (uint8) config->txBufferSize;

            /* Configure EnoUART interface */
            if(EnoUART_EON_EnoUART_MODE_IRDA == config->mode)
            {
                /* OVS settings: IrDA */
                EnoUART_EON_CTRL_REG  = ((0u != config->enableIrdaLowPower) ?
                                                (EnoUART_EON_EnoUART_GET_CTRL_OVS_IRDA_LP(config->oversample)) :
                                                (EnoUART_EON_CTRL_OVS_IRDA_OVS16));
            }
            else
            {
                /* OVS settings: EnoUART and SmartCard */
                EnoUART_EON_CTRL_REG  = EnoUART_EON_GET_CTRL_OVS(config->oversample);
            }

            EnoUART_EON_CTRL_REG     |= EnoUART_EON_GET_CTRL_BYTE_MODE  (config->enableByteMode)      |
                                             EnoUART_EON_GET_CTRL_ADDR_ACCEPT(config->multiprocAcceptAddr) |
                                             EnoUART_EON_CTRL_EnoUART;

            /* Configure sub-mode: EnoUART, SmartCard or IrDA */
            EnoUART_EON_EnoUART_CTRL_REG = EnoUART_EON_GET_EnoUART_CTRL_MODE(config->mode);

            /* Configure RX direction */
            EnoUART_EON_EnoUART_RX_CTRL_REG = EnoUART_EON_GET_EnoUART_RX_CTRL_MODE(config->stopBits)              |
                                        EnoUART_EON_GET_EnoUART_RX_CTRL_POLARITY(config->enableInvertedRx)          |
                                        EnoUART_EON_GET_EnoUART_RX_CTRL_MP_MODE(config->enableMultiproc)            |
                                        EnoUART_EON_GET_EnoUART_RX_CTRL_DROP_ON_PARITY_ERR(config->dropOnParityErr) |
                                        EnoUART_EON_GET_EnoUART_RX_CTRL_DROP_ON_FRAME_ERR(config->dropOnFrameErr);

            if(EnoUART_EON_EnoUART_PARITY_NONE != config->parity)
            {
               EnoUART_EON_EnoUART_RX_CTRL_REG |= EnoUART_EON_GET_EnoUART_RX_CTRL_PARITY(config->parity) |
                                                    EnoUART_EON_EnoUART_RX_CTRL_PARITY_ENABLED;
            }

            EnoUART_EON_RX_CTRL_REG      = EnoUART_EON_GET_RX_CTRL_DATA_WIDTH(config->dataBits)       |
                                                EnoUART_EON_GET_RX_CTRL_MEDIAN(config->enableMedianFilter) |
                                                EnoUART_EON_GET_EnoUART_RX_CTRL_ENABLED(config->direction);

            EnoUART_EON_RX_FIFO_CTRL_REG = EnoUART_EON_GET_RX_FIFO_CTRL_TRIGGER_LEVEL(config->rxTriggerLevel);

            /* Configure MP address */
            EnoUART_EON_RX_MATCH_REG     = EnoUART_EON_GET_RX_MATCH_ADDR(config->multiprocAddr) |
                                                EnoUART_EON_GET_RX_MATCH_MASK(config->multiprocAddrMask);

            /* Configure RX direction */
            EnoUART_EON_EnoUART_TX_CTRL_REG = EnoUART_EON_GET_EnoUART_TX_CTRL_MODE(config->stopBits) |
                                                EnoUART_EON_GET_EnoUART_TX_CTRL_RETRY_NACK(config->enableRetryNack);

            if(EnoUART_EON_EnoUART_PARITY_NONE != config->parity)
            {
               EnoUART_EON_EnoUART_TX_CTRL_REG |= EnoUART_EON_GET_EnoUART_TX_CTRL_PARITY(config->parity) |
                                                    EnoUART_EON_EnoUART_TX_CTRL_PARITY_ENABLED;
            }

            EnoUART_EON_TX_CTRL_REG      = EnoUART_EON_GET_TX_CTRL_DATA_WIDTH(config->dataBits)    |
                                                EnoUART_EON_GET_EnoUART_TX_CTRL_ENABLED(config->direction);

            EnoUART_EON_TX_FIFO_CTRL_REG = EnoUART_EON_GET_TX_FIFO_CTRL_TRIGGER_LEVEL(config->txTriggerLevel);

        #if !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1)
            EnoUART_EON_EnoUART_FLOW_CTRL_REG = EnoUART_EON_GET_EnoUART_FLOW_CTRL_CTS_ENABLE(config->enableCts) | \
                                            EnoUART_EON_GET_EnoUART_FLOW_CTRL_CTS_POLARITY (config->ctsPolarity)  | \
                                            EnoUART_EON_GET_EnoUART_FLOW_CTRL_RTS_POLARITY (config->rtsPolarity)  | \
                                            EnoUART_EON_GET_EnoUART_FLOW_CTRL_TRIGGER_LEVEL(config->rtsRxFifoLevel);
        #endif /* !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1) */

            /* Configure interrupt with EnoUART handler but do not enable it */
            CyIntDisable    (EnoUART_EON_ISR_NUMBER);
            CyIntSetPriority(EnoUART_EON_ISR_NUMBER, EnoUART_EON_ISR_PRIORITY);
            (void) CyIntSetVector(EnoUART_EON_ISR_NUMBER, &EnoUART_EON_SPI_EnoUART_ISR);

            /* Configure WAKE interrupt */
        #if(EnoUART_EON_EnoUART_RX_WAKEUP_IRQ)
            CyIntDisable    (EnoUART_EON_RX_WAKE_ISR_NUMBER);
            CyIntSetPriority(EnoUART_EON_RX_WAKE_ISR_NUMBER, EnoUART_EON_RX_WAKE_ISR_PRIORITY);
            (void) CyIntSetVector(EnoUART_EON_RX_WAKE_ISR_NUMBER, &EnoUART_EON_EnoUART_WAKEUP_ISR);
        #endif /* (EnoUART_EON_EnoUART_RX_WAKEUP_IRQ) */

            /* Configure interrupt sources */
            EnoUART_EON_INTR_I2C_EC_MASK_REG = EnoUART_EON_NO_INTR_SOURCES;
            EnoUART_EON_INTR_SPI_EC_MASK_REG = EnoUART_EON_NO_INTR_SOURCES;
            EnoUART_EON_INTR_SLAVE_MASK_REG  = EnoUART_EON_NO_INTR_SOURCES;
            EnoUART_EON_INTR_MASTER_MASK_REG = EnoUART_EON_NO_INTR_SOURCES;
            EnoUART_EON_INTR_RX_MASK_REG     = config->rxInterruptMask;
            EnoUART_EON_INTR_TX_MASK_REG     = config->txInterruptMask;

            /* Clear RX buffer indexes */
            EnoUART_EON_rxBufferHead     = 0u;
            EnoUART_EON_rxBufferTail     = 0u;
            EnoUART_EON_rxBufferOverflow = 0u;

            /* Clear TX buffer indexes */
            EnoUART_EON_txBufferHead = 0u;
            EnoUART_EON_txBufferTail = 0u;
        }
    }

#else

    /*******************************************************************************
    * Function Name: EnoUART_EON_EnoUARTInit
    ********************************************************************************
    *
    * Summary:
    *  Configures the SCB for the EnoUART operation.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_EnoUARTInit(void)
    {
        /* Configure EnoUART interface */
        EnoUART_EON_CTRL_REG = EnoUART_EON_EnoUART_DEFAULT_CTRL;

        /* Configure sub-mode: EnoUART, SmartCard or IrDA */
        EnoUART_EON_EnoUART_CTRL_REG = EnoUART_EON_EnoUART_DEFAULT_EnoUART_CTRL;

        /* Configure RX direction */
        EnoUART_EON_EnoUART_RX_CTRL_REG = EnoUART_EON_EnoUART_DEFAULT_EnoUART_RX_CTRL;
        EnoUART_EON_RX_CTRL_REG      = EnoUART_EON_EnoUART_DEFAULT_RX_CTRL;
        EnoUART_EON_RX_FIFO_CTRL_REG = EnoUART_EON_EnoUART_DEFAULT_RX_FIFO_CTRL;
        EnoUART_EON_RX_MATCH_REG     = EnoUART_EON_EnoUART_DEFAULT_RX_MATCH_REG;

        /* Configure TX direction */
        EnoUART_EON_EnoUART_TX_CTRL_REG = EnoUART_EON_EnoUART_DEFAULT_EnoUART_TX_CTRL;
        EnoUART_EON_TX_CTRL_REG      = EnoUART_EON_EnoUART_DEFAULT_TX_CTRL;
        EnoUART_EON_TX_FIFO_CTRL_REG = EnoUART_EON_EnoUART_DEFAULT_TX_FIFO_CTRL;

    #if !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1)
        EnoUART_EON_EnoUART_FLOW_CTRL_REG = EnoUART_EON_EnoUART_DEFAULT_FLOW_CTRL;
    #endif /* !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1) */

        /* Configure interrupt with EnoUART handler but do not enable it */
    #if(EnoUART_EON_SCB_IRQ_INTERNAL)
        CyIntDisable    (EnoUART_EON_ISR_NUMBER);
        CyIntSetPriority(EnoUART_EON_ISR_NUMBER, EnoUART_EON_ISR_PRIORITY);
        (void) CyIntSetVector(EnoUART_EON_ISR_NUMBER, &EnoUART_EON_SPI_EnoUART_ISR);
    #endif /* (EnoUART_EON_SCB_IRQ_INTERNAL) */

        /* Configure WAKE interrupt */
    #if(EnoUART_EON_EnoUART_RX_WAKEUP_IRQ)
        CyIntDisable    (EnoUART_EON_RX_WAKE_ISR_NUMBER);
        CyIntSetPriority(EnoUART_EON_RX_WAKE_ISR_NUMBER, EnoUART_EON_RX_WAKE_ISR_PRIORITY);
        (void) CyIntSetVector(EnoUART_EON_RX_WAKE_ISR_NUMBER, &EnoUART_EON_EnoUART_WAKEUP_ISR);
    #endif /* (EnoUART_EON_EnoUART_RX_WAKEUP_IRQ) */

        /* Configure interrupt sources */
        EnoUART_EON_INTR_I2C_EC_MASK_REG = EnoUART_EON_EnoUART_DEFAULT_INTR_I2C_EC_MASK;
        EnoUART_EON_INTR_SPI_EC_MASK_REG = EnoUART_EON_EnoUART_DEFAULT_INTR_SPI_EC_MASK;
        EnoUART_EON_INTR_SLAVE_MASK_REG  = EnoUART_EON_EnoUART_DEFAULT_INTR_SLAVE_MASK;
        EnoUART_EON_INTR_MASTER_MASK_REG = EnoUART_EON_EnoUART_DEFAULT_INTR_MASTER_MASK;
        EnoUART_EON_INTR_RX_MASK_REG     = EnoUART_EON_EnoUART_DEFAULT_INTR_RX_MASK;
        EnoUART_EON_INTR_TX_MASK_REG     = EnoUART_EON_EnoUART_DEFAULT_INTR_TX_MASK;

    #if(EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST)
        EnoUART_EON_rxBufferHead     = 0u;
        EnoUART_EON_rxBufferTail     = 0u;
        EnoUART_EON_rxBufferOverflow = 0u;
    #endif /* (EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST) */

    #if(EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST)
        EnoUART_EON_txBufferHead = 0u;
        EnoUART_EON_txBufferTail = 0u;
    #endif /* (EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST) */
    }
#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */


/*******************************************************************************
* Function Name: EnoUART_EON_EnoUARTPostEnable
********************************************************************************
*
* Summary:
*  Restores HSIOM settings for the EnoUART output pins (TX and/or RTS) to be 
*  controlled by the SCB EnoUART.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void EnoUART_EON_EnoUARTPostEnable(void)
{
#if (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)

#if (EnoUART_EON_TX_SDA_MISO_PIN)
    if (EnoUART_EON_CHECK_TX_SDA_MISO_PIN_USED)
    {
        /* Set SCB EnoUART to drive the output pin */
        EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_TX_SDA_MISO_HSIOM_REG, EnoUART_EON_TX_SDA_MISO_HSIOM_MASK,
                                       EnoUART_EON_TX_SDA_MISO_HSIOM_POS, EnoUART_EON_HSIOM_EnoUART_SEL);
    }
#endif /* (EnoUART_EON_TX_SDA_MISO_PIN_PIN) */

#if (EnoUART_EON_SS0_PIN)
    if (EnoUART_EON_CHECK_SS0_PIN_USED)
    {
        /* Set SCB EnoUART to drive the output pin */
        EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_SS0_HSIOM_REG, EnoUART_EON_SS0_HSIOM_MASK,
                                       EnoUART_EON_SS0_HSIOM_POS, EnoUART_EON_HSIOM_EnoUART_SEL);
    }
#endif /* (EnoUART_EON_SS0_PIN) */

#else
#if (EnoUART_EON_EnoUART_TX_PIN)
     /* Set SCB EnoUART to drive the output pin */
    EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_TX_HSIOM_REG, EnoUART_EON_TX_HSIOM_MASK,
                                   EnoUART_EON_TX_HSIOM_POS, EnoUART_EON_HSIOM_EnoUART_SEL);
#endif /* (EnoUART_EON_EnoUART_TX_PIN) */

#if (EnoUART_EON_EnoUART_RTS_PIN)
    /* Set SCB EnoUART to drive the output pin */
    EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_RTS_HSIOM_REG, EnoUART_EON_RTS_HSIOM_MASK,
                                   EnoUART_EON_RTS_HSIOM_POS, EnoUART_EON_HSIOM_EnoUART_SEL);
#endif /* (EnoUART_EON_EnoUART_RTS_PIN) */

#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: EnoUART_EON_EnoUARTStop
********************************************************************************
*
* Summary:
*  Changes the HSIOM settings for the EnoUART output pins (TX and/or RTS) to keep
*  them inactive after the block is disabled. The output pins are controlled by
*  the GPIO data register. Also, the function disables the skip start feature to
*  not cause it to trigger after the component is enabled.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void EnoUART_EON_EnoUARTStop(void)
{
#if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
    #if (EnoUART_EON_TX_SDA_MISO_PIN)
        if (EnoUART_EON_CHECK_TX_SDA_MISO_PIN_USED)
        {
            /* Set GPIO to drive output pin */
            EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_TX_SDA_MISO_HSIOM_REG, EnoUART_EON_TX_SDA_MISO_HSIOM_MASK,
                                           EnoUART_EON_TX_SDA_MISO_HSIOM_POS, EnoUART_EON_HSIOM_GPIO_SEL);
        }
    #endif /* (EnoUART_EON_TX_SDA_MISO_PIN_PIN) */

    #if (EnoUART_EON_SS0_PIN)
        if (EnoUART_EON_CHECK_SS0_PIN_USED)
        {
            /* Set output pin state after block is disabled */
            EnoUART_EON_spi_ss0_Write(EnoUART_EON_GET_EnoUART_RTS_INACTIVE);

            /* Set GPIO to drive output pin */
            EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_SS0_HSIOM_REG, EnoUART_EON_SS0_HSIOM_MASK,
                                           EnoUART_EON_SS0_HSIOM_POS, EnoUART_EON_HSIOM_GPIO_SEL);
        }
    #endif /* (EnoUART_EON_SS0_PIN) */

#else
    #if (EnoUART_EON_EnoUART_TX_PIN)
        /* Set GPIO to drive output pin */
        EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_TX_HSIOM_REG, EnoUART_EON_TX_HSIOM_MASK,
                                       EnoUART_EON_TX_HSIOM_POS, EnoUART_EON_HSIOM_GPIO_SEL);
    #endif /* (EnoUART_EON_EnoUART_TX_PIN) */

    #if (EnoUART_EON_EnoUART_RTS_PIN)
        /* Set output pin state after block is disabled */
        EnoUART_EON_rts_Write(EnoUART_EON_GET_EnoUART_RTS_INACTIVE);

        /* Set GPIO to drive output pin */
        EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_RTS_HSIOM_REG, EnoUART_EON_RTS_HSIOM_MASK,
                                       EnoUART_EON_RTS_HSIOM_POS, EnoUART_EON_HSIOM_GPIO_SEL);
    #endif /* (EnoUART_EON_EnoUART_RTS_PIN) */

#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */

#if (EnoUART_EON_EnoUART_WAKE_ENABLE_CONST)
    /* Disable skip start feature used for wakeup */
    EnoUART_EON_EnoUART_RX_CTRL_REG &= (uint32) ~EnoUART_EON_EnoUART_RX_CTRL_SKIP_START;
#endif /* (EnoUART_EON_EnoUART_WAKE_ENABLE_CONST) */
}


/*******************************************************************************
* Function Name: EnoUART_EON_EnoUARTSetRxAddress
********************************************************************************
*
* Summary:
*  Sets the hardware detectable receiver address for the EnoUART in the
*  Multiprocessor mode.
*
* Parameters:
*  address: Address for hardware address detection.
*
* Return:
*  None
*
*******************************************************************************/
void EnoUART_EON_EnoUARTSetRxAddress(uint32 address)
{
     uint32 matchReg;

    matchReg = EnoUART_EON_RX_MATCH_REG;

    matchReg &= ((uint32) ~EnoUART_EON_RX_MATCH_ADDR_MASK); /* Clear address bits */
    matchReg |= ((uint32)  (address & EnoUART_EON_RX_MATCH_ADDR_MASK)); /* Set address  */

    EnoUART_EON_RX_MATCH_REG = matchReg;
}


/*******************************************************************************
* Function Name: EnoUART_EON_EnoUARTSetRxAddressMask
********************************************************************************
*
* Summary:
*  Sets the hardware address mask for the EnoUART in the Multiprocessor mode.
*
* Parameters:
*  addressMask: Address mask.
*   0 - address bit does not care while comparison.
*   1 - address bit is significant while comparison.
*
* Return:
*  None
*
*******************************************************************************/
void EnoUART_EON_EnoUARTSetRxAddressMask(uint32 addressMask)
{
    uint32 matchReg;

    matchReg = EnoUART_EON_RX_MATCH_REG;

    matchReg &= ((uint32) ~EnoUART_EON_RX_MATCH_MASK_MASK); /* Clear address mask bits */
    matchReg |= ((uint32) (addressMask << EnoUART_EON_RX_MATCH_MASK_POS));

    EnoUART_EON_RX_MATCH_REG = matchReg;
}


#if(EnoUART_EON_EnoUART_RX_DIRECTION)
    /*******************************************************************************
    * Function Name: EnoUART_EON_EnoUARTGetChar
    ********************************************************************************
    *
    * Summary:
    *  Retrieves the next data element from the receive buffer.
    *  This function is designed for ASCII characters and returns a char
    *  where 1 to 255 are valid characters and 0 indicates an error occurred or
    *  no data present.
    *  - The RX software buffer is disabled: returns the data element
    *    retrieved from the RX FIFO.
    *    Undefined data will be returned if the RX FIFO is empty.
    *  - The RX software buffer is enabled: returns the data element from
    *    the software receive buffer.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  The next data element from the receive buffer.
    *  ASCII character values from 1 to 255 are valid.
    *  A returned zero signifies an error condition or no data available.
    *
    * Side Effects:
    *  The errors bits may not correspond with reading characters due to RX FIFO
    *  and software buffer usage.
    *  RX software buffer is enabled: The internal software buffer overflow
    *  does not treat as an error condition.
    *  Check SCB_rxBufferOverflow to capture that error condition.
    *
    *******************************************************************************/
    uint32 EnoUART_EON_EnoUARTGetChar(void)
    {
        uint32 rxData = 0u;

        /* Reads data only if there is data to read */
        if (0u != EnoUART_EON_SpiEnoUARTGetRxBufferSize())
        {
            rxData = EnoUART_EON_SpiEnoUARTReadRxData();
        }

        if (EnoUART_EON_CHECK_INTR_RX(EnoUART_EON_INTR_RX_ERR))
        {
            rxData = 0u; /* Error occurred: returns zero */
            EnoUART_EON_ClearRxInterruptSource(EnoUART_EON_INTR_RX_ERR);
        }

        return (rxData);
    }


    /*******************************************************************************
    * Function Name: EnoUART_EON_EnoUARTGetByte
    ********************************************************************************
    *
    * Summary:
    *  Retrieves the next data element from the receive buffer, returns the
    *  received byte and error condition.
    *   - The RX software buffer is disabled: returns the data element retrieved
    *     from the RX FIFO. Undefined data will be returned if the RX FIFO is
    *     empty.
    *   - The RX software buffer is enabled: returns data element from the
    *     software receive buffer.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  Bits 7-0 contain the next data element from the receive buffer and
    *  other bits contain the error condition.
    *
    * Side Effects:
    *  The errors bits may not correspond with reading characters due to RX FIFO
    *  and software buffer usage.
    *  RX software buffer is disabled: The internal software buffer overflow
    *  is not returned as status by this function.
    *  Check SCB_rxBufferOverflow to capture that error condition.
    *
    *******************************************************************************/
    uint32 EnoUART_EON_EnoUARTGetByte(void)
    {
        uint32 rxData;
        uint32 tmpStatus;

        #if (EnoUART_EON_CHECK_RX_SW_BUFFER)
        {
            EnoUART_EON_DisableInt();
        }
        #endif

        if (0u != EnoUART_EON_SpiEnoUARTGetRxBufferSize())
        {
            /* Enables interrupt to receive more bytes: at least one byte is in
            * buffer.
            */
            #if (EnoUART_EON_CHECK_RX_SW_BUFFER)
            {            
                EnoUART_EON_EnableInt();
            }
            #endif

            /* Get received byte */
            rxData = EnoUART_EON_SpiEnoUARTReadRxData();
        }
        else
        {
            /* Reads a byte directly from RX FIFO: underflow is raised in the case
            * of empty. Otherwise the first received byte will be read.
            */
            rxData = EnoUART_EON_RX_FIFO_RD_REG;

            /* Enables interrupt to receive more bytes.
            * The RX_NOT_EMPTY interrupt is cleared by the interrupt routine
            * in case the byte was received and read by code above.
            */
            #if (EnoUART_EON_CHECK_RX_SW_BUFFER)
            {
                EnoUART_EON_EnableInt();
            }
            #endif
        }

        /* Get and clear RX error mask */
        tmpStatus = (EnoUART_EON_GetRxInterruptSource() & EnoUART_EON_INTR_RX_ERR);
        EnoUART_EON_ClearRxInterruptSource(EnoUART_EON_INTR_RX_ERR);

        /* Puts together data and error status:
        * MP mode and accept address: 9th bit is set to notify mark.
        */
        rxData |= ((uint32) (tmpStatus << 8u));

        return (rxData);
    }


    #if !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1)
        /*******************************************************************************
        * Function Name: EnoUART_EON_EnoUARTSetRtsPolarity
        ********************************************************************************
        *
        * Summary:
        *  Sets active polarity of RTS output signal.
        *
        * Parameters:
        *  polarity: Active polarity of RTS output signal.
        *   EnoUART_EON_EnoUART_RTS_ACTIVE_LOW  - RTS signal is active low.
        *   EnoUART_EON_EnoUART_RTS_ACTIVE_HIGH - RTS signal is active high.
        *
        * Return:
        *  None
        *
        *******************************************************************************/
        void EnoUART_EON_EnoUARTSetRtsPolarity(uint32 polarity)
        {
            if(0u != polarity)
            {
                EnoUART_EON_EnoUART_FLOW_CTRL_REG |= (uint32)  EnoUART_EON_EnoUART_FLOW_CTRL_RTS_POLARITY;
            }
            else
            {
                EnoUART_EON_EnoUART_FLOW_CTRL_REG &= (uint32) ~EnoUART_EON_EnoUART_FLOW_CTRL_RTS_POLARITY;
            }
        }


        /*******************************************************************************
        * Function Name: EnoUART_EON_EnoUARTSetRtsFifoLevel
        ********************************************************************************
        *
        * Summary:
        *  Sets level in the RX FIFO for RTS signal activation.
        *  While the RX FIFO has fewer entries than the RX FIFO level the RTS signal
        *  remains active, otherwise the RTS signal becomes inactive.
        *
        * Parameters:
        *  level: Level in the RX FIFO for RTS signal activation.
        *         The range of valid level values is between 0 and RX FIFO depth - 1.
        *         Setting level value to 0 disables RTS signal activation.
        *
        * Return:
        *  None
        *
        *******************************************************************************/
        void EnoUART_EON_EnoUARTSetRtsFifoLevel(uint32 level)
        {
            uint32 EnoUARTFlowCtrl;

            EnoUARTFlowCtrl = EnoUART_EON_EnoUART_FLOW_CTRL_REG;

            EnoUARTFlowCtrl &= ((uint32) ~EnoUART_EON_EnoUART_FLOW_CTRL_TRIGGER_LEVEL_MASK); /* Clear level mask bits */
            EnoUARTFlowCtrl |= ((uint32) (EnoUART_EON_EnoUART_FLOW_CTRL_TRIGGER_LEVEL_MASK & level));

            EnoUART_EON_EnoUART_FLOW_CTRL_REG = EnoUARTFlowCtrl;
        }
    #endif /* !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1) */

#endif /* (EnoUART_EON_EnoUART_RX_DIRECTION) */


#if(EnoUART_EON_EnoUART_TX_DIRECTION)
    /*******************************************************************************
    * Function Name: EnoUART_EON_EnoUARTPutString
    ********************************************************************************
    *
    * Summary:
    *  Places a NULL terminated string in the transmit buffer to be sent at the
    *  next available bus time.
    *  This function is blocking and waits until there is space available to put
    *  all the requested data into the  transmit buffer.
    *
    * Parameters:
    *  string: pointer to the null terminated string array to be placed in the
    *          transmit buffer.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_EnoUARTPutString(const char8 string[])
    {
        uint32 bufIndex;

        bufIndex = 0u;

        /* Blocks the control flow until all data has been sent */
        while(string[bufIndex] != ((char8) 0))
        {
            EnoUART_EON_EnoUARTPutChar((uint32) string[bufIndex]);
            bufIndex++;
        }
    }


    /*******************************************************************************
    * Function Name: EnoUART_EON_EnoUARTPutCRLF
    ********************************************************************************
    *
    * Summary:
    *  Places a byte of data followed by a carriage return (0x0D) and
    *  line feed (0x0A) into the transmit buffer.
    *  This function is blocking and waits until there is space available to put
    *  all the requested data into the  transmit buffer.
    *
    * Parameters:
    *  txDataByte : the data to be transmitted.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_EnoUARTPutCRLF(uint32 txDataByte)
    {
        EnoUART_EON_EnoUARTPutChar(txDataByte);  /* Blocks control flow until all data has been sent */
        EnoUART_EON_EnoUARTPutChar(0x0Du);       /* Blocks control flow until all data has been sent */
        EnoUART_EON_EnoUARTPutChar(0x0Au);       /* Blocks control flow until all data has been sent */
    }


    #if !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1)
        /*******************************************************************************
        * Function Name: EnoUART_EONSCB_EnoUARTEnableCts
        ********************************************************************************
        *
        * Summary:
        *  Enables usage of CTS input signal by the EnoUART transmitter.
        *
        * Parameters:
        *  None
        *
        * Return:
        *  None
        *
        *******************************************************************************/
        void EnoUART_EON_EnoUARTEnableCts(void)
        {
            EnoUART_EON_EnoUART_FLOW_CTRL_REG |= (uint32)  EnoUART_EON_EnoUART_FLOW_CTRL_CTS_ENABLE;
        }


        /*******************************************************************************
        * Function Name: EnoUART_EON_EnoUARTDisableCts
        ********************************************************************************
        *
        * Summary:
        *  Disables usage of CTS input signal by the EnoUART transmitter.
        *
        * Parameters:
        *  None
        *
        * Return:
        *  None
        *
        *******************************************************************************/
        void EnoUART_EON_EnoUARTDisableCts(void)
        {
            EnoUART_EON_EnoUART_FLOW_CTRL_REG &= (uint32) ~EnoUART_EON_EnoUART_FLOW_CTRL_CTS_ENABLE;
        }


        /*******************************************************************************
        * Function Name: EnoUART_EON_EnoUARTSetCtsPolarity
        ********************************************************************************
        *
        * Summary:
        *  Sets active polarity of CTS input signal.
        *
        * Parameters:
        *  polarity: Active polarity of CTS output signal.
        *   EnoUART_EON_EnoUART_CTS_ACTIVE_LOW  - CTS signal is active low.
        *   EnoUART_EON_EnoUART_CTS_ACTIVE_HIGH - CTS signal is active high.
        *
        * Return:
        *  None
        *
        *******************************************************************************/
        void EnoUART_EON_EnoUARTSetCtsPolarity(uint32 polarity)
        {
            if (0u != polarity)
            {
                EnoUART_EON_EnoUART_FLOW_CTRL_REG |= (uint32)  EnoUART_EON_EnoUART_FLOW_CTRL_CTS_POLARITY;
            }
            else
            {
                EnoUART_EON_EnoUART_FLOW_CTRL_REG &= (uint32) ~EnoUART_EON_EnoUART_FLOW_CTRL_CTS_POLARITY;
            }
        }
    #endif /* !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1) */

#endif /* (EnoUART_EON_EnoUART_TX_DIRECTION) */


#if(EnoUART_EON_EnoUART_WAKE_ENABLE_CONST)
    /*******************************************************************************
    * Function Name: EnoUART_EON_EnoUARTSaveConfig
    ********************************************************************************
    *
    * Summary:
    *  Clears and enables interrupt on a falling edge of the Rx input. The GPIO
    *  event wakes up the device and SKIP_START feature allows the EnoUART continue
    *  receiving data bytes properly. The GPIO interrupt does not track in the
    *  active mode therefore requires to be cleared by this API.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_EnoUARTSaveConfig(void)
    {
        /* Clear interrupt activity:
        *  - set skip start and disable RX. At GPIO wakeup RX will be enabled.
        *  - clear rx_wake interrupt source as it triggers during normal operation.
        *  - clear wake interrupt pending state as it becomes pending in active mode.
        */

        EnoUART_EON_EnoUART_RX_CTRL_REG |= EnoUART_EON_EnoUART_RX_CTRL_SKIP_START;

    #if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
        #if(EnoUART_EON_MOSI_SCL_RX_WAKE_PIN)
            (void) EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi_ClearInterrupt();
        #endif /* (EnoUART_EON_MOSI_SCL_RX_WAKE_PIN) */
    #else
        #if(EnoUART_EON_EnoUART_RX_WAKE_PIN)
            (void) EnoUART_EON_rx_wake_ClearInterrupt();
        #endif /* (EnoUART_EON_EnoUART_RX_WAKE_PIN) */
    #endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */

    #if(EnoUART_EON_EnoUART_RX_WAKEUP_IRQ)
        EnoUART_EON_RxWakeClearPendingInt();
        EnoUART_EON_RxWakeEnableInt();
    #endif /* (EnoUART_EON_EnoUART_RX_WAKEUP_IRQ) */
    }


    /*******************************************************************************
    * Function Name: EnoUART_EON_EnoUARTRestoreConfig
    ********************************************************************************
    *
    * Summary:
    *  Disables the RX GPIO interrupt. Until this function is called the interrupt
    *  remains active and triggers on every falling edge of the EnoUART RX line.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_EnoUARTRestoreConfig(void)
    {
    /* Disable RX GPIO interrupt: no more triggers in active mode */
    #if(EnoUART_EON_EnoUART_RX_WAKEUP_IRQ)
        EnoUART_EON_RxWakeDisableInt();
    #endif /* (EnoUART_EON_EnoUART_RX_WAKEUP_IRQ) */
    }
#endif /* (EnoUART_EON_EnoUART_WAKE_ENABLE_CONST) */


#if(EnoUART_EON_EnoUART_RX_WAKEUP_IRQ)
    /*******************************************************************************
    * Function Name: EnoUART_EON_EnoUART_WAKEUP_ISR
    ********************************************************************************
    *
    * Summary:
    *  Handles the Interrupt Service Routine for the SCB EnoUART mode GPIO wakeup
    *  event. This event is configured to trigger on a falling edge of the RX line.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    CY_ISR(EnoUART_EON_EnoUART_WAKEUP_ISR)
    {
    #ifdef EnoUART_EON_EnoUART_WAKEUP_ISR_ENTRY_CALLBACK
        EnoUART_EON_EnoUART_WAKEUP_ISR_EntryCallback();
    #endif /* EnoUART_EON_EnoUART_WAKEUP_ISR_ENTRY_CALLBACK */

        /* Clear interrupt source: the event becomes multi triggered and is
        * only disabled by EnoUART_EON_EnoUARTRestoreConfig() call.
        */
    #if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
        #if(EnoUART_EON_MOSI_SCL_RX_WAKE_PIN)
            (void) EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi_ClearInterrupt();
        #endif /* (EnoUART_EON_MOSI_SCL_RX_WAKE_PIN) */
    #else
        #if(EnoUART_EON_EnoUART_RX_WAKE_PIN)
            (void) EnoUART_EON_rx_wake_ClearInterrupt();
        #endif /* (EnoUART_EON_EnoUART_RX_WAKE_PIN) */
    #endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */

    #ifdef EnoUART_EON_EnoUART_WAKEUP_ISR_EXIT_CALLBACK
        EnoUART_EON_EnoUART_WAKEUP_ISR_ExitCallback();
    #endif /* EnoUART_EON_EnoUART_WAKEUP_ISR_EXIT_CALLBACK */
    }
#endif /* (EnoUART_EON_EnoUART_RX_WAKEUP_IRQ) */


/* [] END OF FILE */
