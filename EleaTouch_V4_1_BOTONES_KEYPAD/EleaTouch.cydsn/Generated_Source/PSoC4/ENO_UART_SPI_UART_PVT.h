/***************************************************************************//**
* \file ENO_EnoUART_SPI_EnoUART_PVT.h
* \version 3.20
*
* \brief
*  This private file provides constants and parameter values for the
*  SCB Component in SPI and EnoUART modes.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_SPI_EnoUART_PVT_ENO_EnoUART_H)
#define CY_SCB_SPI_EnoUART_PVT_ENO_EnoUART_H

#include "ENO_EnoUART_SPI_EnoUART.h"


/***************************************
*     Internal Global Vars
***************************************/

#if (ENO_EnoUART_INTERNAL_RX_SW_BUFFER_CONST)
    extern volatile uint32  ENO_EnoUART_rxBufferHead;
    extern volatile uint32  ENO_EnoUART_rxBufferTail;
    
    /**
    * \addtogroup group_globals
    * @{
    */
    
    /** Sets when internal software receive buffer overflow
     *  was occurred.
    */  
    extern volatile uint8   ENO_EnoUART_rxBufferOverflow;
    /** @} globals */
#endif /* (ENO_EnoUART_INTERNAL_RX_SW_BUFFER_CONST) */

#if (ENO_EnoUART_INTERNAL_TX_SW_BUFFER_CONST)
    extern volatile uint32  ENO_EnoUART_txBufferHead;
    extern volatile uint32  ENO_EnoUART_txBufferTail;
#endif /* (ENO_EnoUART_INTERNAL_TX_SW_BUFFER_CONST) */

#if (ENO_EnoUART_INTERNAL_RX_SW_BUFFER)
    extern volatile uint8 ENO_EnoUART_rxBufferInternal[ENO_EnoUART_INTERNAL_RX_BUFFER_SIZE];
#endif /* (ENO_EnoUART_INTERNAL_RX_SW_BUFFER) */

#if (ENO_EnoUART_INTERNAL_TX_SW_BUFFER)
    extern volatile uint8 ENO_EnoUART_txBufferInternal[ENO_EnoUART_TX_BUFFER_SIZE];
#endif /* (ENO_EnoUART_INTERNAL_TX_SW_BUFFER) */


/***************************************
*     Private Function Prototypes
***************************************/

void ENO_EnoUART_SpiPostEnable(void);
void ENO_EnoUART_SpiStop(void);

#if (ENO_EnoUART_SCB_MODE_SPI_CONST_CFG)
    void ENO_EnoUART_SpiInit(void);
#endif /* (ENO_EnoUART_SCB_MODE_SPI_CONST_CFG) */

#if (ENO_EnoUART_SPI_WAKE_ENABLE_CONST)
    void ENO_EnoUART_SpiSaveConfig(void);
    void ENO_EnoUART_SpiRestoreConfig(void);
#endif /* (ENO_EnoUART_SPI_WAKE_ENABLE_CONST) */

void ENO_EnoUART_EnoUARTPostEnable(void);
void ENO_EnoUART_EnoUARTStop(void);

#if (ENO_EnoUART_SCB_MODE_EnoUART_CONST_CFG)
    void ENO_EnoUART_EnoUARTInit(void);
#endif /* (ENO_EnoUART_SCB_MODE_EnoUART_CONST_CFG) */

#if (ENO_EnoUART_EnoUART_WAKE_ENABLE_CONST)
    void ENO_EnoUART_EnoUARTSaveConfig(void);
    void ENO_EnoUART_EnoUARTRestoreConfig(void);
#endif /* (ENO_EnoUART_EnoUART_WAKE_ENABLE_CONST) */


/***************************************
*         EnoUART API Constants
***************************************/

/* EnoUART RX and TX position to be used in ENO_EnoUART_SetPins() */
#define ENO_EnoUART_EnoUART_RX_PIN_ENABLE    (ENO_EnoUART_EnoUART_RX)
#define ENO_EnoUART_EnoUART_TX_PIN_ENABLE    (ENO_EnoUART_EnoUART_TX)

/* EnoUART RTS and CTS position to be used in  ENO_EnoUART_SetPins() */
#define ENO_EnoUART_EnoUART_RTS_PIN_ENABLE    (0x10u)
#define ENO_EnoUART_EnoUART_CTS_PIN_ENABLE    (0x20u)


/***************************************
* The following code is DEPRECATED and
* must not be used.
***************************************/

/* Interrupt processing */
#define ENO_EnoUART_SpiEnoUARTEnableIntRx(intSourceMask)  ENO_EnoUART_SetRxInterruptMode(intSourceMask)
#define ENO_EnoUART_SpiEnoUARTEnableIntTx(intSourceMask)  ENO_EnoUART_SetTxInterruptMode(intSourceMask)
uint32  ENO_EnoUART_SpiEnoUARTDisableIntRx(void);
uint32  ENO_EnoUART_SpiEnoUARTDisableIntTx(void);


#endif /* (CY_SCB_SPI_EnoUART_PVT_ENO_EnoUART_H) */


/* [] END OF FILE */
