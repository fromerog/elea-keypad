/***************************************************************************//**
* \file ENO_EnoUART_BOOT.c
* \version 3.20
*
* \brief
*  This file provides the source code of the bootloader communication APIs
*  for the SCB Component Unconfigured mode.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ENO_EnoUART_BOOT.h"

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (ENO_EnoUART_BTLDR_COMM_ENABLED) && \
                                (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)

/*******************************************************************************
* Function Name: ENO_EnoUART_CyBtldrCommStart
****************************************************************************//**
*
*  Starts ENO_EnoUART component. After this function call the component is 
*  ready for communication.
*
*******************************************************************************/
void ENO_EnoUART_CyBtldrCommStart(void)
{
    if (ENO_EnoUART_SCB_MODE_I2C_RUNTM_CFG)
    {
        ENO_EnoUART_I2CCyBtldrCommStart();
    }
    else if (ENO_EnoUART_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        ENO_EnoUART_EzI2CCyBtldrCommStart();
    }
#if (!ENO_EnoUART_CY_SCBIP_V1)
    else if (ENO_EnoUART_SCB_MODE_SPI_RUNTM_CFG)
    {
        ENO_EnoUART_SpiCyBtldrCommStart();
    }
    else if (ENO_EnoUART_SCB_MODE_EnoUART_RUNTM_CFG)
    {
        ENO_EnoUART_EnoUARTCyBtldrCommStart();
    }
#endif /* (!ENO_EnoUART_CY_SCBIP_V1) */
    else
    {
        /* Unknown mode: do nothing */
    }
}


/*******************************************************************************
* Function Name: ENO_EnoUART_CyBtldrCommStop
****************************************************************************//**
*
*  Stops ENO_EnoUART component.
*
*******************************************************************************/
void ENO_EnoUART_CyBtldrCommStop(void)
{
    if (ENO_EnoUART_SCB_MODE_I2C_RUNTM_CFG)
    {
        ENO_EnoUART_I2CCyBtldrCommStop();
    }
    else if (ENO_EnoUART_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        ENO_EnoUART_EzI2CCyBtldrCommStop();
    }
#if (!ENO_EnoUART_CY_SCBIP_V1)
    else if (ENO_EnoUART_SCB_MODE_SPI_RUNTM_CFG)
    {
        ENO_EnoUART_SpiCyBtldrCommStop();
    }
    else if (ENO_EnoUART_SCB_MODE_EnoUART_RUNTM_CFG)
    {
        ENO_EnoUART_EnoUARTCyBtldrCommStop();
    }
#endif /* (!ENO_EnoUART_CY_SCBIP_V1) */
    else
    {
        /* Unknown mode: do nothing */
    }
}


/*******************************************************************************
* Function Name: ENO_EnoUART_CyBtldrCommReset
****************************************************************************//**
*
*  Clears ENO_EnoUART component buffers.
*
*******************************************************************************/
void ENO_EnoUART_CyBtldrCommReset(void)
{
    if(ENO_EnoUART_SCB_MODE_I2C_RUNTM_CFG)
    {
        ENO_EnoUART_I2CCyBtldrCommReset();
    }
    else if(ENO_EnoUART_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        ENO_EnoUART_EzI2CCyBtldrCommReset();
    }
#if (!ENO_EnoUART_CY_SCBIP_V1)
    else if(ENO_EnoUART_SCB_MODE_SPI_RUNTM_CFG)
    {
        ENO_EnoUART_SpiCyBtldrCommReset();
    }
    else if(ENO_EnoUART_SCB_MODE_EnoUART_RUNTM_CFG)
    {
        ENO_EnoUART_EnoUARTCyBtldrCommReset();
    }
#endif /* (!ENO_EnoUART_CY_SCBIP_V1) */
    else
    {
        /* Unknown mode: do nothing */
    }
}


/*******************************************************************************
* Function Name: ENO_EnoUART_CyBtldrCommRead
****************************************************************************//**
*
*  Allows the caller to read data from the bootloader host (the host writes the 
*  data). The function handles polling to allow a block of data to be completely
*  received from the host device.
*
*  \param pData: Pointer to storage for the block of data to be read from the
*   bootloader host.
*  \param size: Number of bytes to be read.
*  \param count: Pointer to the variable to write the number of bytes actually
*   read.
*  \param timeOut: Number of units in 10 ms to wait before returning because of a
*   timeout.
*
* \return
*  \return
*  cystatus: Returns CYRET_SUCCESS if no problem was encountered or returns the
*  value that best describes the problem. For more information refer to 
*  the �Return Codes� section of the System Reference Guide.
*
*******************************************************************************/
cystatus ENO_EnoUART_CyBtldrCommRead(uint8 pData[], uint16 size, uint16 * count, uint8 timeOut)
{
    cystatus status;

    if(ENO_EnoUART_SCB_MODE_I2C_RUNTM_CFG)
    {
        status = ENO_EnoUART_I2CCyBtldrCommRead(pData, size, count, timeOut);
    }
    else if(ENO_EnoUART_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        status = ENO_EnoUART_EzI2CCyBtldrCommRead(pData, size, count, timeOut);
    }
#if (!ENO_EnoUART_CY_SCBIP_V1)
    else if(ENO_EnoUART_SCB_MODE_SPI_RUNTM_CFG)
    {
        status = ENO_EnoUART_SpiCyBtldrCommRead(pData, size, count, timeOut);
    }
    else if(ENO_EnoUART_SCB_MODE_EnoUART_RUNTM_CFG)
    {
        status = ENO_EnoUART_EnoUARTCyBtldrCommRead(pData, size, count, timeOut);
    }
#endif /* (!ENO_EnoUART_CY_SCBIP_V1) */
    else
    {
        status = CYRET_INVALID_STATE; /* Unknown mode: return invalid status */
    }

    return(status);
}


/*******************************************************************************
* Function Name: ENO_EnoUART_CyBtldrCommWrite
****************************************************************************//**
*
*  Allows the caller to write data to the bootloader host (the host reads the 
*  data). The function does not use timeout and returns after data has been copied
*  into the slave read buffer. This data available to be read by the bootloader
*  host until following host data write.
*
*  \param pData: Pointer to the block of data to be written to the bootloader host.
*  \param size: Number of bytes to be written.
*  \param count: Pointer to the variable to write the number of bytes actually
*   written.
*  \param timeOut: Number of units in 10 ms to wait before returning because of a
*   timeout.
*
*  \return
*  cystatus: Returns CYRET_SUCCESS if no problem was encountered or returns the
*  value that best describes the problem. For more information refer to 
*  the �Return Codes� section of the System Reference Guide.
*
*******************************************************************************/
cystatus ENO_EnoUART_CyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut)
{
    cystatus status;

    if(ENO_EnoUART_SCB_MODE_I2C_RUNTM_CFG)
    {
        status = ENO_EnoUART_I2CCyBtldrCommWrite(pData, size, count, timeOut);
    }
    else if(ENO_EnoUART_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        status = ENO_EnoUART_EzI2CCyBtldrCommWrite(pData, size, count, timeOut);
    }
#if (!ENO_EnoUART_CY_SCBIP_V1)
    else if(ENO_EnoUART_SCB_MODE_SPI_RUNTM_CFG)
    {
        status = ENO_EnoUART_SpiCyBtldrCommWrite(pData, size, count, timeOut);
    }
    else if(ENO_EnoUART_SCB_MODE_EnoUART_RUNTM_CFG)
    {
        status = ENO_EnoUART_EnoUARTCyBtldrCommWrite(pData, size, count, timeOut);
    }
#endif /* (!ENO_EnoUART_CY_SCBIP_V1) */
    else
    {
        status = CYRET_INVALID_STATE; /* Unknown mode: return invalid status */
    }

    return(status);
}

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (ENO_EnoUART_BTLDR_COMM_MODE_ENABLED) */


/* [] END OF FILE */
