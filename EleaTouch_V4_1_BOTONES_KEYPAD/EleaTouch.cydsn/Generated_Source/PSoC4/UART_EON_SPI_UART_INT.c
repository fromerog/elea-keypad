/*******************************************************************************
* File Name: EnoUART_EON_SPI_EnoUART_INT.c
* Version 3.0
*
* Description:
*  This file provides the source code to the Interrupt Service Routine for
*  the SCB Component in SPI and EnoUART modes.
*
* Note:
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "EnoUART_EON_PVT.h"
#include "EnoUART_EON_SPI_EnoUART_PVT.h"
#include "cyapicallbacks.h"

#if (EnoUART_EON_SCB_IRQ_INTERNAL)
/*******************************************************************************
* Function Name: EnoUART_EON_SPI_EnoUART_ISR
********************************************************************************
*
* Summary:
*  Handles the Interrupt Service Routine for the SCB SPI or EnoUART modes.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
CY_ISR(EnoUART_EON_SPI_EnoUART_ISR)
{
#if (EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST)
    uint32 locHead;
#endif /* (EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST) */

#if (EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST)
    uint32 locTail;
#endif /* (EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST) */

#ifdef EnoUART_EON_SPI_EnoUART_ISR_ENTRY_CALLBACK
    EnoUART_EON_SPI_EnoUART_ISR_EntryCallback();
#endif /* EnoUART_EON_SPI_EnoUART_ISR_ENTRY_CALLBACK */

    if (NULL != EnoUART_EON_customIntrHandler)
    {
        EnoUART_EON_customIntrHandler();
    }

    #if (EnoUART_EON_CHECK_SPI_WAKE_ENABLE)
    {
        /* Clear SPI wakeup source */
        EnoUART_EON_ClearSpiExtClkInterruptSource(EnoUART_EON_INTR_SPI_EC_WAKE_UP);
    }
    #endif

    #if (EnoUART_EON_CHECK_RX_SW_BUFFER)
    {
        if (EnoUART_EON_CHECK_INTR_RX_MASKED(EnoUART_EON_INTR_RX_NOT_EMPTY))
        {
            do
            {
                /* Move local head index */
                locHead = (EnoUART_EON_rxBufferHead + 1u);

                /* Adjust local head index */
                if (EnoUART_EON_INTERNAL_RX_BUFFER_SIZE == locHead)
                {
                    locHead = 0u;
                }

                if (locHead == EnoUART_EON_rxBufferTail)
                {
                    #if (EnoUART_EON_CHECK_EnoUART_RTS_CONTROL_FLOW)
                    {
                        /* There is no space in the software buffer - disable the
                        * RX Not Empty interrupt source. The data elements are
                        * still being received into the RX FIFO until the RTS signal
                        * stops the transmitter. After the data element is read from the
                        * buffer, the RX Not Empty interrupt source is enabled to
                        * move the next data element in the software buffer.
                        */
                        EnoUART_EON_INTR_RX_MASK_REG &= ~EnoUART_EON_INTR_RX_NOT_EMPTY;
                        break;
                    }
                    #else
                    {
                        /* Overflow: through away received data element */
                        (void) EnoUART_EON_RX_FIFO_RD_REG;
                        EnoUART_EON_rxBufferOverflow = (uint8) EnoUART_EON_INTR_RX_OVERFLOW;
                    }
                    #endif
                }
                else
                {
                    /* Store received data */
                    EnoUART_EON_PutWordInRxBuffer(locHead, EnoUART_EON_RX_FIFO_RD_REG);

                    /* Move head index */
                    EnoUART_EON_rxBufferHead = locHead;
                }
            }
            while(0u != EnoUART_EON_GET_RX_FIFO_ENTRIES);

            EnoUART_EON_ClearRxInterruptSource(EnoUART_EON_INTR_RX_NOT_EMPTY);
        }
    }
    #endif


    #if (EnoUART_EON_CHECK_TX_SW_BUFFER)
    {
        if (EnoUART_EON_CHECK_INTR_TX_MASKED(EnoUART_EON_INTR_TX_NOT_FULL))
        {
            do
            {
                /* Check for room in TX software buffer */
                if (EnoUART_EON_txBufferHead != EnoUART_EON_txBufferTail)
                {
                    /* Move local tail index */
                    locTail = (EnoUART_EON_txBufferTail + 1u);

                    /* Adjust local tail index */
                    if (EnoUART_EON_TX_BUFFER_SIZE == locTail)
                    {
                        locTail = 0u;
                    }

                    /* Put data into TX FIFO */
                    EnoUART_EON_TX_FIFO_WR_REG = EnoUART_EON_GetWordFromTxBuffer(locTail);

                    /* Move tail index */
                    EnoUART_EON_txBufferTail = locTail;
                }
                else
                {
                    /* TX software buffer is empty: complete transfer */
                    EnoUART_EON_DISABLE_INTR_TX(EnoUART_EON_INTR_TX_NOT_FULL);
                    break;
                }
            }
            while (EnoUART_EON_SPI_EnoUART_FIFO_SIZE != EnoUART_EON_GET_TX_FIFO_ENTRIES);

            EnoUART_EON_ClearTxInterruptSource(EnoUART_EON_INTR_TX_NOT_FULL);
        }
    }
    #endif
    
#ifdef EnoUART_EON_SPI_EnoUART_ISR_EXIT_CALLBACK
    EnoUART_EON_SPI_EnoUART_ISR_ExitCallback();
#endif /* EnoUART_EON_SPI_EnoUART_ISR_EXIT_CALLBACK */
    
}

#endif /* (EnoUART_EON_SCB_IRQ_INTERNAL) */


/* [] END OF FILE */
