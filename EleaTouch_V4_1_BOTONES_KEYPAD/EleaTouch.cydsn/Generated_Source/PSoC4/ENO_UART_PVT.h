/***************************************************************************//**
* \file .h
* \version 3.20
*
* \brief
*  This private file provides constants and parameter values for the
*  SCB Component.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PVT_ENO_EnoUART_H)
#define CY_SCB_PVT_ENO_EnoUART_H

#include "ENO_EnoUART.h"


/***************************************
*     Private Function Prototypes
***************************************/

/* APIs to service INTR_I2C_EC register */
#define ENO_EnoUART_SetI2CExtClkInterruptMode(interruptMask) ENO_EnoUART_WRITE_INTR_I2C_EC_MASK(interruptMask)
#define ENO_EnoUART_ClearI2CExtClkInterruptSource(interruptMask) ENO_EnoUART_CLEAR_INTR_I2C_EC(interruptMask)
#define ENO_EnoUART_GetI2CExtClkInterruptSource()                (ENO_EnoUART_INTR_I2C_EC_REG)
#define ENO_EnoUART_GetI2CExtClkInterruptMode()                  (ENO_EnoUART_INTR_I2C_EC_MASK_REG)
#define ENO_EnoUART_GetI2CExtClkInterruptSourceMasked()          (ENO_EnoUART_INTR_I2C_EC_MASKED_REG)

#if (!ENO_EnoUART_CY_SCBIP_V1)
    /* APIs to service INTR_SPI_EC register */
    #define ENO_EnoUART_SetSpiExtClkInterruptMode(interruptMask) \
                                                                ENO_EnoUART_WRITE_INTR_SPI_EC_MASK(interruptMask)
    #define ENO_EnoUART_ClearSpiExtClkInterruptSource(interruptMask) \
                                                                ENO_EnoUART_CLEAR_INTR_SPI_EC(interruptMask)
    #define ENO_EnoUART_GetExtSpiClkInterruptSource()                 (ENO_EnoUART_INTR_SPI_EC_REG)
    #define ENO_EnoUART_GetExtSpiClkInterruptMode()                   (ENO_EnoUART_INTR_SPI_EC_MASK_REG)
    #define ENO_EnoUART_GetExtSpiClkInterruptSourceMasked()           (ENO_EnoUART_INTR_SPI_EC_MASKED_REG)
#endif /* (!ENO_EnoUART_CY_SCBIP_V1) */

#if(ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    extern void ENO_EnoUART_SetPins(uint32 mode, uint32 subMode, uint32 EnoUARTEnableMask);
#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*     Vars with External Linkage
***************************************/

#if (ENO_EnoUART_SCB_IRQ_INTERNAL)
#if !defined (CY_REMOVE_ENO_EnoUART_CUSTOM_INTR_HANDLER)
    extern cyisraddress ENO_EnoUART_customIntrHandler;
#endif /* !defined (CY_REMOVE_ENO_EnoUART_CUSTOM_INTR_HANDLER) */
#endif /* (ENO_EnoUART_SCB_IRQ_INTERNAL) */

extern ENO_EnoUART_BACKUP_STRUCT ENO_EnoUART_backup;

#if(ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Common configuration variables */
    extern uint8 ENO_EnoUART_scbMode;
    extern uint8 ENO_EnoUART_scbEnableWake;
    extern uint8 ENO_EnoUART_scbEnableIntr;

    /* I2C configuration variables */
    extern uint8 ENO_EnoUART_mode;
    extern uint8 ENO_EnoUART_acceptAddr;

    /* SPI/EnoUART configuration variables */
    extern volatile uint8 * ENO_EnoUART_rxBuffer;
    extern uint8   ENO_EnoUART_rxDataBits;
    extern uint32  ENO_EnoUART_rxBufferSize;

    extern volatile uint8 * ENO_EnoUART_txBuffer;
    extern uint8   ENO_EnoUART_txDataBits;
    extern uint32  ENO_EnoUART_txBufferSize;

    /* EZI2C configuration variables */
    extern uint8 ENO_EnoUART_numberOfAddr;
    extern uint8 ENO_EnoUART_subAddrSize;
#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */

#if (! (ENO_EnoUART_SCB_MODE_I2C_CONST_CFG || \
        ENO_EnoUART_SCB_MODE_EZI2C_CONST_CFG))
    extern uint16 ENO_EnoUART_IntrTxMask;
#endif /* (! (ENO_EnoUART_SCB_MODE_I2C_CONST_CFG || \
              ENO_EnoUART_SCB_MODE_EZI2C_CONST_CFG)) */


/***************************************
*        Conditional Macro
****************************************/

#if(ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Defines run time operation mode */
    #define ENO_EnoUART_SCB_MODE_I2C_RUNTM_CFG     (ENO_EnoUART_SCB_MODE_I2C      == ENO_EnoUART_scbMode)
    #define ENO_EnoUART_SCB_MODE_SPI_RUNTM_CFG     (ENO_EnoUART_SCB_MODE_SPI      == ENO_EnoUART_scbMode)
    #define ENO_EnoUART_SCB_MODE_EnoUART_RUNTM_CFG    (ENO_EnoUART_SCB_MODE_EnoUART     == ENO_EnoUART_scbMode)
    #define ENO_EnoUART_SCB_MODE_EZI2C_RUNTM_CFG   (ENO_EnoUART_SCB_MODE_EZI2C    == ENO_EnoUART_scbMode)
    #define ENO_EnoUART_SCB_MODE_UNCONFIG_RUNTM_CFG \
                                                        (ENO_EnoUART_SCB_MODE_UNCONFIG == ENO_EnoUART_scbMode)

    /* Defines wakeup enable */
    #define ENO_EnoUART_SCB_WAKE_ENABLE_CHECK       (0u != ENO_EnoUART_scbEnableWake)
#endif /* (ENO_EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */

/* Defines maximum number of SCB pins */
#if (!ENO_EnoUART_CY_SCBIP_V1)
    #define ENO_EnoUART_SCB_PINS_NUMBER    (7u)
#else
    #define ENO_EnoUART_SCB_PINS_NUMBER    (2u)
#endif /* (!ENO_EnoUART_CY_SCBIP_V1) */

#endif /* (CY_SCB_PVT_ENO_EnoUART_H) */


/* [] END OF FILE */
