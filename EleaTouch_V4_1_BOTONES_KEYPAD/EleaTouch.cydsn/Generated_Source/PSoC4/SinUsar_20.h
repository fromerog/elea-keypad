/*******************************************************************************
* File Name: SinUsar_20.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_SinUsar_20_H) /* Pins SinUsar_20_H */
#define CY_PINS_SinUsar_20_H

#include "cytypes.h"
#include "cyfitter.h"
#include "SinUsar_20_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} SinUsar_20_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   SinUsar_20_Read(void);
void    SinUsar_20_Write(uint8 value);
uint8   SinUsar_20_ReadDataReg(void);
#if defined(SinUsar_20__PC) || (CY_PSOC4_4200L) 
    void    SinUsar_20_SetDriveMode(uint8 mode);
#endif
void    SinUsar_20_SetInterruptMode(uint16 position, uint16 mode);
uint8   SinUsar_20_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void SinUsar_20_Sleep(void); 
void SinUsar_20_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(SinUsar_20__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define SinUsar_20_DRIVE_MODE_BITS        (3)
    #define SinUsar_20_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - SinUsar_20_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the SinUsar_20_SetDriveMode() function.
         *  @{
         */
        #define SinUsar_20_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define SinUsar_20_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define SinUsar_20_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define SinUsar_20_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define SinUsar_20_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define SinUsar_20_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define SinUsar_20_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define SinUsar_20_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define SinUsar_20_MASK               SinUsar_20__MASK
#define SinUsar_20_SHIFT              SinUsar_20__SHIFT
#define SinUsar_20_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in SinUsar_20_SetInterruptMode() function.
     *  @{
     */
        #define SinUsar_20_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define SinUsar_20_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define SinUsar_20_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define SinUsar_20_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(SinUsar_20__SIO)
    #define SinUsar_20_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(SinUsar_20__PC) && (CY_PSOC4_4200L)
    #define SinUsar_20_USBIO_ENABLE               ((uint32)0x80000000u)
    #define SinUsar_20_USBIO_DISABLE              ((uint32)(~SinUsar_20_USBIO_ENABLE))
    #define SinUsar_20_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define SinUsar_20_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define SinUsar_20_USBIO_ENTER_SLEEP          ((uint32)((1u << SinUsar_20_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << SinUsar_20_USBIO_SUSPEND_DEL_SHIFT)))
    #define SinUsar_20_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << SinUsar_20_USBIO_SUSPEND_SHIFT)))
    #define SinUsar_20_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << SinUsar_20_USBIO_SUSPEND_DEL_SHIFT)))
    #define SinUsar_20_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(SinUsar_20__PC)
    /* Port Configuration */
    #define SinUsar_20_PC                 (* (reg32 *) SinUsar_20__PC)
#endif
/* Pin State */
#define SinUsar_20_PS                     (* (reg32 *) SinUsar_20__PS)
/* Data Register */
#define SinUsar_20_DR                     (* (reg32 *) SinUsar_20__DR)
/* Input Buffer Disable Override */
#define SinUsar_20_INP_DIS                (* (reg32 *) SinUsar_20__PC2)

/* Interrupt configuration Registers */
#define SinUsar_20_INTCFG                 (* (reg32 *) SinUsar_20__INTCFG)
#define SinUsar_20_INTSTAT                (* (reg32 *) SinUsar_20__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define SinUsar_20_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(SinUsar_20__SIO)
    #define SinUsar_20_SIO_REG            (* (reg32 *) SinUsar_20__SIO)
#endif /* (SinUsar_20__SIO_CFG) */

/* USBIO registers */
#if !defined(SinUsar_20__PC) && (CY_PSOC4_4200L)
    #define SinUsar_20_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define SinUsar_20_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define SinUsar_20_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define SinUsar_20_DRIVE_MODE_SHIFT       (0x00u)
#define SinUsar_20_DRIVE_MODE_MASK        (0x07u << SinUsar_20_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins SinUsar_20_H */


/* [] END OF FILE */
