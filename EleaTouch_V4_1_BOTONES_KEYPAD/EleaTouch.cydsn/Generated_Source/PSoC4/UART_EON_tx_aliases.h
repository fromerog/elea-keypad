/*******************************************************************************
* File Name: EnoUART_EON_tx.h  
* Version 2.10
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_EnoUART_EON_tx_ALIASES_H) /* Pins EnoUART_EON_tx_ALIASES_H */
#define CY_PINS_EnoUART_EON_tx_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define EnoUART_EON_tx_0		(EnoUART_EON_tx__0__PC)
#define EnoUART_EON_tx_0_PS		(EnoUART_EON_tx__0__PS)
#define EnoUART_EON_tx_0_PC		(EnoUART_EON_tx__0__PC)
#define EnoUART_EON_tx_0_DR		(EnoUART_EON_tx__0__DR)
#define EnoUART_EON_tx_0_SHIFT	(EnoUART_EON_tx__0__SHIFT)


#endif /* End Pins EnoUART_EON_tx_ALIASES_H */


/* [] END OF FILE */
