/*******************************************************************************
* File Name: EnoUART_rx_wake.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_EnoUART_rx_wake_H) /* Pins EnoUART_rx_wake_H */
#define CY_PINS_EnoUART_rx_wake_H

#include "cytypes.h"
#include "cyfitter.h"
#include "EnoUART_rx_wake_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} EnoUART_rx_wake_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   EnoUART_rx_wake_Read(void);
void    EnoUART_rx_wake_Write(uint8 value);
uint8   EnoUART_rx_wake_ReadDataReg(void);
#if defined(EnoUART_rx_wake__PC) || (CY_PSOC4_4200L) 
    void    EnoUART_rx_wake_SetDriveMode(uint8 mode);
#endif
void    EnoUART_rx_wake_SetInterruptMode(uint16 position, uint16 mode);
uint8   EnoUART_rx_wake_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void EnoUART_rx_wake_Sleep(void); 
void EnoUART_rx_wake_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(EnoUART_rx_wake__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define EnoUART_rx_wake_DRIVE_MODE_BITS        (3)
    #define EnoUART_rx_wake_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - EnoUART_rx_wake_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the EnoUART_rx_wake_SetDriveMode() function.
         *  @{
         */
        #define EnoUART_rx_wake_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define EnoUART_rx_wake_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define EnoUART_rx_wake_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define EnoUART_rx_wake_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define EnoUART_rx_wake_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define EnoUART_rx_wake_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define EnoUART_rx_wake_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define EnoUART_rx_wake_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define EnoUART_rx_wake_MASK               EnoUART_rx_wake__MASK
#define EnoUART_rx_wake_SHIFT              EnoUART_rx_wake__SHIFT
#define EnoUART_rx_wake_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in EnoUART_rx_wake_SetInterruptMode() function.
     *  @{
     */
        #define EnoUART_rx_wake_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define EnoUART_rx_wake_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define EnoUART_rx_wake_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define EnoUART_rx_wake_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(EnoUART_rx_wake__SIO)
    #define EnoUART_rx_wake_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(EnoUART_rx_wake__PC) && (CY_PSOC4_4200L)
    #define EnoUART_rx_wake_USBIO_ENABLE               ((uint32)0x80000000u)
    #define EnoUART_rx_wake_USBIO_DISABLE              ((uint32)(~EnoUART_rx_wake_USBIO_ENABLE))
    #define EnoUART_rx_wake_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define EnoUART_rx_wake_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define EnoUART_rx_wake_USBIO_ENTER_SLEEP          ((uint32)((1u << EnoUART_rx_wake_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << EnoUART_rx_wake_USBIO_SUSPEND_DEL_SHIFT)))
    #define EnoUART_rx_wake_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << EnoUART_rx_wake_USBIO_SUSPEND_SHIFT)))
    #define EnoUART_rx_wake_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << EnoUART_rx_wake_USBIO_SUSPEND_DEL_SHIFT)))
    #define EnoUART_rx_wake_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(EnoUART_rx_wake__PC)
    /* Port Configuration */
    #define EnoUART_rx_wake_PC                 (* (reg32 *) EnoUART_rx_wake__PC)
#endif
/* Pin State */
#define EnoUART_rx_wake_PS                     (* (reg32 *) EnoUART_rx_wake__PS)
/* Data Register */
#define EnoUART_rx_wake_DR                     (* (reg32 *) EnoUART_rx_wake__DR)
/* Input Buffer Disable Override */
#define EnoUART_rx_wake_INP_DIS                (* (reg32 *) EnoUART_rx_wake__PC2)

/* Interrupt configuration Registers */
#define EnoUART_rx_wake_INTCFG                 (* (reg32 *) EnoUART_rx_wake__INTCFG)
#define EnoUART_rx_wake_INTSTAT                (* (reg32 *) EnoUART_rx_wake__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define EnoUART_rx_wake_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(EnoUART_rx_wake__SIO)
    #define EnoUART_rx_wake_SIO_REG            (* (reg32 *) EnoUART_rx_wake__SIO)
#endif /* (EnoUART_rx_wake__SIO_CFG) */

/* USBIO registers */
#if !defined(EnoUART_rx_wake__PC) && (CY_PSOC4_4200L)
    #define EnoUART_rx_wake_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define EnoUART_rx_wake_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define EnoUART_rx_wake_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define EnoUART_rx_wake_DRIVE_MODE_SHIFT       (0x00u)
#define EnoUART_rx_wake_DRIVE_MODE_MASK        (0x07u << EnoUART_rx_wake_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins EnoUART_rx_wake_H */


/* [] END OF FILE */
