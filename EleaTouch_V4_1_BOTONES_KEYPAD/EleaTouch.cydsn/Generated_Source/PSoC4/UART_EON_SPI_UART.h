/*******************************************************************************
* File Name: EnoUART_EON_SPI_EnoUART.h
* Version 3.0
*
* Description:
*  This file provides constants and parameter values for the SCB Component in
*  SPI and EnoUART modes.
*
* Note:
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_SPI_EnoUART_EnoUART_EON_H)
#define CY_SCB_SPI_EnoUART_EnoUART_EON_H

#include "EnoUART_EON.h"


/***************************************
*   SPI Initial Parameter Constants
****************************************/

#define EnoUART_EON_SPI_MODE                   (0u)
#define EnoUART_EON_SPI_SUB_MODE               (0u)
#define EnoUART_EON_SPI_CLOCK_MODE             (0u)
#define EnoUART_EON_SPI_OVS_FACTOR             (16u)
#define EnoUART_EON_SPI_MEDIAN_FILTER_ENABLE   (0u)
#define EnoUART_EON_SPI_LATE_MISO_SAMPLE_ENABLE (0u)
#define EnoUART_EON_SPI_RX_DATA_BITS_NUM       (8u)
#define EnoUART_EON_SPI_TX_DATA_BITS_NUM       (8u)
#define EnoUART_EON_SPI_WAKE_ENABLE            (0u)
#define EnoUART_EON_SPI_BITS_ORDER             (1u)
#define EnoUART_EON_SPI_TRANSFER_SEPARATION    (1u)
#define EnoUART_EON_SPI_NUMBER_OF_SS_LINES     (1u)
#define EnoUART_EON_SPI_RX_BUFFER_SIZE         (8u)
#define EnoUART_EON_SPI_TX_BUFFER_SIZE         (8u)

#define EnoUART_EON_SPI_INTERRUPT_MODE         (0u)

#define EnoUART_EON_SPI_INTR_RX_MASK           (0u)
#define EnoUART_EON_SPI_INTR_TX_MASK           (0u)

#define EnoUART_EON_SPI_RX_TRIGGER_LEVEL       (7u)
#define EnoUART_EON_SPI_TX_TRIGGER_LEVEL       (0u)

#define EnoUART_EON_SPI_BYTE_MODE_ENABLE       (0u)
#define EnoUART_EON_SPI_FREE_RUN_SCLK_ENABLE   (0u)
#define EnoUART_EON_SPI_SS0_POLARITY           (0u)
#define EnoUART_EON_SPI_SS1_POLARITY           (0u)
#define EnoUART_EON_SPI_SS2_POLARITY           (0u)
#define EnoUART_EON_SPI_SS3_POLARITY           (0u)


/***************************************
*   EnoUART Initial Parameter Constants
****************************************/

#define EnoUART_EON_EnoUART_SUB_MODE              (0u)
#define EnoUART_EON_EnoUART_DIRECTION             (3u)
#define EnoUART_EON_EnoUART_DATA_BITS_NUM         (8u)
#define EnoUART_EON_EnoUART_PARITY_TYPE           (2u)
#define EnoUART_EON_EnoUART_STOP_BITS_NUM         (2u)
#define EnoUART_EON_EnoUART_OVS_FACTOR            (12u)
#define EnoUART_EON_EnoUART_IRDA_LOW_POWER        (0u)
#define EnoUART_EON_EnoUART_MEDIAN_FILTER_ENABLE  (0u)
#define EnoUART_EON_EnoUART_RETRY_ON_NACK         (0u)
#define EnoUART_EON_EnoUART_IRDA_POLARITY         (0u)
#define EnoUART_EON_EnoUART_DROP_ON_FRAME_ERR     (0u)
#define EnoUART_EON_EnoUART_DROP_ON_PARITY_ERR    (0u)
#define EnoUART_EON_EnoUART_WAKE_ENABLE           (0u)
#define EnoUART_EON_EnoUART_RX_BUFFER_SIZE        (8u)
#define EnoUART_EON_EnoUART_TX_BUFFER_SIZE        (8u)
#define EnoUART_EON_EnoUART_MP_MODE_ENABLE        (0u)
#define EnoUART_EON_EnoUART_MP_ACCEPT_ADDRESS     (0u)
#define EnoUART_EON_EnoUART_MP_RX_ADDRESS         (2u)
#define EnoUART_EON_EnoUART_MP_RX_ADDRESS_MASK    (255u)

#define EnoUART_EON_EnoUART_INTERRUPT_MODE        (0u)

#define EnoUART_EON_EnoUART_INTR_RX_MASK          (0u)
#define EnoUART_EON_EnoUART_INTR_TX_MASK          (0u)

#define EnoUART_EON_EnoUART_RX_TRIGGER_LEVEL      (7u)
#define EnoUART_EON_EnoUART_TX_TRIGGER_LEVEL      (0u)

#define EnoUART_EON_EnoUART_BYTE_MODE_ENABLE      (0u)
#define EnoUART_EON_EnoUART_CTS_ENABLE            (0u)
#define EnoUART_EON_EnoUART_CTS_POLARITY          (0u)
#define EnoUART_EON_EnoUART_RTS_ENABLE            (0u)
#define EnoUART_EON_EnoUART_RTS_POLARITY          (0u)
#define EnoUART_EON_EnoUART_RTS_FIFO_LEVEL        (4u)

/* SPI mode enum */
#define EnoUART_EON_SPI_SLAVE  (0u)
#define EnoUART_EON_SPI_MASTER (1u)

/* EnoUART direction enum */
#define EnoUART_EON_EnoUART_RX    (1u)
#define EnoUART_EON_EnoUART_TX    (2u)
#define EnoUART_EON_EnoUART_TX_RX (3u)


/***************************************
*   Conditional Compilation Parameters
****************************************/

#if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)

    /* Mode */
    #define EnoUART_EON_SPI_SLAVE_CONST        (1u)
    #define EnoUART_EON_SPI_MASTER_CONST       (1u)

    /* Direction */
    #define EnoUART_EON_RX_DIRECTION           (1u)
    #define EnoUART_EON_TX_DIRECTION           (1u)
    #define EnoUART_EON_EnoUART_RX_DIRECTION      (1u)
    #define EnoUART_EON_EnoUART_TX_DIRECTION      (1u)

    /* Only external RX and TX buffer for Uncofigured mode */
    #define EnoUART_EON_INTERNAL_RX_SW_BUFFER   (0u)
    #define EnoUART_EON_INTERNAL_TX_SW_BUFFER   (0u)

    /* Get RX and TX buffer size */
    #define EnoUART_EON_INTERNAL_RX_BUFFER_SIZE    (EnoUART_EON_rxBufferSize + 1u)
    #define EnoUART_EON_RX_BUFFER_SIZE             (EnoUART_EON_rxBufferSize)
    #define EnoUART_EON_TX_BUFFER_SIZE             (EnoUART_EON_txBufferSize)

    /* Return true if buffer is provided */
    #define EnoUART_EON_CHECK_RX_SW_BUFFER (NULL != EnoUART_EON_rxBuffer)
    #define EnoUART_EON_CHECK_TX_SW_BUFFER (NULL != EnoUART_EON_txBuffer)

    /* Always provide global variables to support RX and TX buffers */
    #define EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST    (1u)
    #define EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST    (1u)

    /* Get wakeup enable option */
    #define EnoUART_EON_SPI_WAKE_ENABLE_CONST  (1u)
    #define EnoUART_EON_CHECK_SPI_WAKE_ENABLE  (0u != EnoUART_EON_scbEnableWake)
    #define EnoUART_EON_EnoUART_WAKE_ENABLE_CONST (1u)

    /* SPI/EnoUART: TX or RX FIFO size */
    #if (EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1)
        #define EnoUART_EON_SPI_EnoUART_FIFO_SIZE             (EnoUART_EON_FIFO_SIZE)
        #define EnoUART_EON_CHECK_EnoUART_RTS_CONTROL_FLOW    (0u)
    #else
        #define EnoUART_EON_SPI_EnoUART_FIFO_SIZE (EnoUART_EON_GET_FIFO_SIZE(EnoUART_EON_CTRL_REG & \
                                                                                    EnoUART_EON_CTRL_BYTE_MODE))

        #define EnoUART_EON_CHECK_EnoUART_RTS_CONTROL_FLOW \
                    ((EnoUART_EON_SCB_MODE_EnoUART_RUNTM_CFG) && \
                     (0u != EnoUART_EON_GET_EnoUART_FLOW_CTRL_TRIGGER_LEVEL(EnoUART_EON_EnoUART_FLOW_CTRL_REG)))
    #endif /* (EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1) */

#else

    /* Internal RX and TX buffer: for SPI or EnoUART */
    #if (EnoUART_EON_SCB_MODE_SPI_CONST_CFG)

        /* SPI Direction */
        #define EnoUART_EON_SPI_RX_DIRECTION (1u)
        #define EnoUART_EON_SPI_TX_DIRECTION (1u)

        /* Get FIFO size */
        #if (EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1)
            #define EnoUART_EON_SPI_EnoUART_FIFO_SIZE    (EnoUART_EON_FIFO_SIZE)
        #else
            #define EnoUART_EON_SPI_EnoUART_FIFO_SIZE \
                                           EnoUART_EON_GET_FIFO_SIZE(EnoUART_EON_SPI_BYTE_MODE_ENABLE)

        #endif /* (EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1) */

        /* SPI internal RX and TX buffers */
        #define EnoUART_EON_INTERNAL_SPI_RX_SW_BUFFER  (EnoUART_EON_SPI_RX_BUFFER_SIZE > \
                                                                EnoUART_EON_SPI_EnoUART_FIFO_SIZE)
        #define EnoUART_EON_INTERNAL_SPI_TX_SW_BUFFER  (EnoUART_EON_SPI_TX_BUFFER_SIZE > \
                                                                EnoUART_EON_SPI_EnoUART_FIFO_SIZE)

        /* Internal SPI RX and TX buffer */
        #define EnoUART_EON_INTERNAL_RX_SW_BUFFER  (EnoUART_EON_INTERNAL_SPI_RX_SW_BUFFER)
        #define EnoUART_EON_INTERNAL_TX_SW_BUFFER  (EnoUART_EON_INTERNAL_SPI_TX_SW_BUFFER)

        /* Internal SPI RX and TX buffer size */
        #define EnoUART_EON_INTERNAL_RX_BUFFER_SIZE    (EnoUART_EON_SPI_RX_BUFFER_SIZE + 1u)
        #define EnoUART_EON_RX_BUFFER_SIZE             (EnoUART_EON_SPI_RX_BUFFER_SIZE)
        #define EnoUART_EON_TX_BUFFER_SIZE             (EnoUART_EON_SPI_TX_BUFFER_SIZE)

        /* Get wakeup enable option */
        #define EnoUART_EON_SPI_WAKE_ENABLE_CONST  (0u != EnoUART_EON_SPI_WAKE_ENABLE)
        #define EnoUART_EON_EnoUART_WAKE_ENABLE_CONST (0u)

    #else

        /* EnoUART Direction */
        #define EnoUART_EON_EnoUART_RX_DIRECTION (0u != (EnoUART_EON_EnoUART_DIRECTION & EnoUART_EON_EnoUART_RX))
        #define EnoUART_EON_EnoUART_TX_DIRECTION (0u != (EnoUART_EON_EnoUART_DIRECTION & EnoUART_EON_EnoUART_TX))

        /* Get FIFO size */
        #if (EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1)
            #define EnoUART_EON_SPI_EnoUART_FIFO_SIZE    (EnoUART_EON_FIFO_SIZE)
        #else
            #define EnoUART_EON_SPI_EnoUART_FIFO_SIZE \
                                           EnoUART_EON_GET_FIFO_SIZE(EnoUART_EON_EnoUART_BYTE_MODE_ENABLE)
        #endif /* (EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1) */

        /* EnoUART internal RX and TX buffers */
        #define EnoUART_EON_INTERNAL_EnoUART_RX_SW_BUFFER  (EnoUART_EON_EnoUART_RX_BUFFER_SIZE > \
                                                                EnoUART_EON_SPI_EnoUART_FIFO_SIZE)
        #define EnoUART_EON_INTERNAL_EnoUART_TX_SW_BUFFER  (EnoUART_EON_EnoUART_TX_BUFFER_SIZE > \
                                                                    EnoUART_EON_SPI_EnoUART_FIFO_SIZE)

        /* Internal EnoUART RX and TX buffer */
        #define EnoUART_EON_INTERNAL_RX_SW_BUFFER  (EnoUART_EON_INTERNAL_EnoUART_RX_SW_BUFFER)
        #define EnoUART_EON_INTERNAL_TX_SW_BUFFER  (EnoUART_EON_INTERNAL_EnoUART_TX_SW_BUFFER)

        /* Internal EnoUART RX and TX buffer size */
        #define EnoUART_EON_INTERNAL_RX_BUFFER_SIZE    (EnoUART_EON_EnoUART_RX_BUFFER_SIZE + 1u)
        #define EnoUART_EON_RX_BUFFER_SIZE             (EnoUART_EON_EnoUART_RX_BUFFER_SIZE)
        #define EnoUART_EON_TX_BUFFER_SIZE             (EnoUART_EON_EnoUART_TX_BUFFER_SIZE)

        /* Get wakeup enable option */
        #define EnoUART_EON_SPI_WAKE_ENABLE_CONST  (0u)
        #define EnoUART_EON_EnoUART_WAKE_ENABLE_CONST (0u != EnoUART_EON_EnoUART_WAKE_ENABLE)

    #endif /* (EnoUART_EON_SCB_MODE_SPI_CONST_CFG) */

    /* Mode */
    #define EnoUART_EON_SPI_SLAVE_CONST    (EnoUART_EON_SPI_MODE == EnoUART_EON_SPI_SLAVE)
    #define EnoUART_EON_SPI_MASTER_CONST   (EnoUART_EON_SPI_MODE == EnoUART_EON_SPI_MASTER)

    /* Direction */
    #define EnoUART_EON_RX_DIRECTION ((EnoUART_EON_SCB_MODE_SPI_CONST_CFG) ? \
                                            (EnoUART_EON_SPI_RX_DIRECTION) : (EnoUART_EON_EnoUART_RX_DIRECTION))

    #define EnoUART_EON_TX_DIRECTION ((EnoUART_EON_SCB_MODE_SPI_CONST_CFG) ? \
                                            (EnoUART_EON_SPI_TX_DIRECTION) : (EnoUART_EON_EnoUART_TX_DIRECTION))

    /* Internal RX and TX buffer: for SPI or EnoUART. Used in conditional compilation check */
    #define EnoUART_EON_CHECK_RX_SW_BUFFER (EnoUART_EON_INTERNAL_RX_SW_BUFFER)
    #define EnoUART_EON_CHECK_TX_SW_BUFFER (EnoUART_EON_INTERNAL_TX_SW_BUFFER)

    /* Provide global variables to support RX and TX buffers */
    #define EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST    (EnoUART_EON_INTERNAL_RX_SW_BUFFER)
    #define EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST    (EnoUART_EON_INTERNAL_TX_SW_BUFFER)

    /* SPI wakeup */
    #define EnoUART_EON_CHECK_SPI_WAKE_ENABLE  (EnoUART_EON_SPI_WAKE_ENABLE_CONST)

    /* EnoUART flow control: not applicable for CY_SCBIP_V0 || CY_SCBIP_V1 */
    #define EnoUART_EON_CHECK_EnoUART_RTS_CONTROL_FLOW    (EnoUART_EON_SCB_MODE_EnoUART_CONST_CFG && \
                                                             EnoUART_EON_EnoUART_RTS_ENABLE)

#endif /* End (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*       Type Definitions
***************************************/

/* EnoUART_EON_SPI_INIT_STRUCT */
typedef struct
{
    uint32 mode;
    uint32 submode;
    uint32 sclkMode;
    uint32 oversample;
    uint32 enableMedianFilter;
    uint32 enableLateSampling;
    uint32 enableWake;
    uint32 rxDataBits;
    uint32 txDataBits;
    uint32 bitOrder;
    uint32 transferSeperation;
    uint32 rxBufferSize;
    uint8* rxBuffer;
    uint32 txBufferSize;
    uint8* txBuffer;
    uint32 enableInterrupt;
    uint32 rxInterruptMask;
    uint32 rxTriggerLevel;
    uint32 txInterruptMask;
    uint32 txTriggerLevel;
    uint8 enableByteMode;
    uint8 enableFreeRunSclk;
    uint8 polaritySs;
} EnoUART_EON_SPI_INIT_STRUCT;

/* EnoUART_EON_EnoUART_INIT_STRUCT */
typedef struct
{
    uint32 mode;
    uint32 direction;
    uint32 dataBits;
    uint32 parity;
    uint32 stopBits;
    uint32 oversample;
    uint32 enableIrdaLowPower;
    uint32 enableMedianFilter;
    uint32 enableRetryNack;
    uint32 enableInvertedRx;
    uint32 dropOnParityErr;
    uint32 dropOnFrameErr;
    uint32 enableWake;
    uint32 rxBufferSize;
    uint8* rxBuffer;
    uint32 txBufferSize;
    uint8* txBuffer;
    uint32 enableMultiproc;
    uint32 multiprocAcceptAddr;
    uint32 multiprocAddr;
    uint32 multiprocAddrMask;
    uint32 enableInterrupt;
    uint32 rxInterruptMask;
    uint32 rxTriggerLevel;
    uint32 txInterruptMask;
    uint32 txTriggerLevel;
    uint8 enableByteMode;
    uint8 enableCts;
    uint8 ctsPolarity;
    uint8 rtsRxFifoLevel;
    uint8 rtsPolarity;
} EnoUART_EON_EnoUART_INIT_STRUCT;


/***************************************
*        Function Prototypes
***************************************/

/* SPI specific functions */
#if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
    void EnoUART_EON_SpiInit(const EnoUART_EON_SPI_INIT_STRUCT *config);
#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */

#if(EnoUART_EON_SCB_MODE_SPI_INC)
    #define EnoUART_EON_SpiIsBusBusy() ((uint32) (0u != (EnoUART_EON_SPI_STATUS_REG & \
                                                              EnoUART_EON_SPI_STATUS_BUS_BUSY)))

    #if (EnoUART_EON_SPI_MASTER_CONST)
        void EnoUART_EON_SpiSetActiveSlaveSelect(uint32 slaveSelect);
    #endif /*(EnoUART_EON_SPI_MASTER_CONST) */

    #if !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1)
        void EnoUART_EON_SpiSetSlaveSelectPolarity(uint32 slaveSelect, uint32 polarity);
    #endif /* !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1) */
#endif /* (EnoUART_EON_SCB_MODE_SPI_INC) */

/* EnoUART specific functions */
#if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
    void EnoUART_EON_EnoUARTInit(const EnoUART_EON_EnoUART_INIT_STRUCT *config);
#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */

#if(EnoUART_EON_SCB_MODE_EnoUART_INC)
    void EnoUART_EON_EnoUARTSetRxAddress(uint32 address);
    void EnoUART_EON_EnoUARTSetRxAddressMask(uint32 addressMask);

    /* EnoUART RX direction APIs */
    #if(EnoUART_EON_EnoUART_RX_DIRECTION)
        uint32 EnoUART_EON_EnoUARTGetChar(void);
        uint32 EnoUART_EON_EnoUARTGetByte(void);

        #if !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1)
            /* EnoUART APIs for Flow Control */
            void EnoUART_EON_EnoUARTSetRtsPolarity(uint32 polarity);
            void EnoUART_EON_EnoUARTSetRtsFifoLevel(uint32 level);
        #endif /* !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1) */
    #endif /* (EnoUART_EON_EnoUART_RX_DIRECTION) */

    /* EnoUART TX direction APIs */
    #if(EnoUART_EON_EnoUART_TX_DIRECTION)
        #define EnoUART_EON_EnoUARTPutChar(ch)    EnoUART_EON_SpiEnoUARTWriteTxData((uint32)(ch))
        void EnoUART_EON_EnoUARTPutString(const char8 string[]);
        void EnoUART_EON_EnoUARTPutCRLF(uint32 txDataByte);

        #if !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1)
            /* EnoUART APIs for Flow Control */
            void EnoUART_EON_EnoUARTEnableCts(void);
            void EnoUART_EON_EnoUARTDisableCts(void);
            void EnoUART_EON_EnoUARTSetCtsPolarity(uint32 polarity);
        #endif /* !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1) */
    #endif /* (EnoUART_EON_EnoUART_TX_DIRECTION) */
#endif /* (EnoUART_EON_SCB_MODE_EnoUART_INC) */

/* Common APIs RX direction */
#if(EnoUART_EON_RX_DIRECTION)
    uint32 EnoUART_EON_SpiEnoUARTReadRxData(void);
    uint32 EnoUART_EON_SpiEnoUARTGetRxBufferSize(void);
    void   EnoUART_EON_SpiEnoUARTClearRxBuffer(void);
#endif /* (EnoUART_EON_RX_DIRECTION) */

/* Common APIs TX direction */
#if(EnoUART_EON_TX_DIRECTION)
    void   EnoUART_EON_SpiEnoUARTWriteTxData(uint32 txData);
    void   EnoUART_EON_SpiEnoUARTPutArray(const uint8 wrBuf[], uint32 count);
    uint32 EnoUART_EON_SpiEnoUARTGetTxBufferSize(void);
    void   EnoUART_EON_SpiEnoUARTClearTxBuffer(void);
#endif /* (EnoUART_EON_TX_DIRECTION) */

CY_ISR_PROTO(EnoUART_EON_SPI_EnoUART_ISR);

#if(EnoUART_EON_EnoUART_RX_WAKEUP_IRQ)
    CY_ISR_PROTO(EnoUART_EON_EnoUART_WAKEUP_ISR);
#endif /* (EnoUART_EON_EnoUART_RX_WAKEUP_IRQ) */


/***************************************
*     Buffer Access Macro Definitions
***************************************/

#if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
    /* RX direction */
    void   EnoUART_EON_PutWordInRxBuffer  (uint32 idx, uint32 rxDataByte);
    uint32 EnoUART_EON_GetWordFromRxBuffer(uint32 idx);

    /* TX direction */
    void   EnoUART_EON_PutWordInTxBuffer  (uint32 idx, uint32 txDataByte);
    uint32 EnoUART_EON_GetWordFromTxBuffer(uint32 idx);

#else
    /* RX direction */
    #if(EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST)
        #define EnoUART_EON_PutWordInRxBuffer(idx, rxDataByte) \
                do{                                                 \
                    EnoUART_EON_rxBufferInternal[(idx)] = ((uint8) (rxDataByte)); \
                }while(0)

        #define EnoUART_EON_GetWordFromRxBuffer(idx) EnoUART_EON_rxBufferInternal[(idx)]

    #endif /* (EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST) */

    /* TX direction */
    #if(EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST)
        #define EnoUART_EON_PutWordInTxBuffer(idx, txDataByte) \
                    do{                                             \
                        EnoUART_EON_txBufferInternal[(idx)] = ((uint8) (txDataByte)); \
                    }while(0)

        #define EnoUART_EON_GetWordFromTxBuffer(idx) EnoUART_EON_txBufferInternal[(idx)]

    #endif /* (EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST) */

#endif /* (EnoUART_EON_TX_SW_BUFFER_ENABLE) */


/***************************************
*         SPI API Constants
***************************************/

/* SPI sub mode enum */
#define EnoUART_EON_SPI_MODE_MOTOROLA      (0x00u)
#define EnoUART_EON_SPI_MODE_TI_COINCIDES  (0x01u)
#define EnoUART_EON_SPI_MODE_TI_PRECEDES   (0x11u)
#define EnoUART_EON_SPI_MODE_NATIONAL      (0x02u)
#define EnoUART_EON_SPI_MODE_MASK          (0x03u)
#define EnoUART_EON_SPI_MODE_TI_PRECEDES_MASK  (0x10u)
#define EnoUART_EON_SPI_MODE_NS_MICROWIRE  (EnoUART_EON_SPI_MODE_NATIONAL)

/* SPI phase and polarity mode enum */
#define EnoUART_EON_SPI_SCLK_CPHA0_CPOL0   (0x00u)
#define EnoUART_EON_SPI_SCLK_CPHA0_CPOL1   (0x02u)
#define EnoUART_EON_SPI_SCLK_CPHA1_CPOL0   (0x01u)
#define EnoUART_EON_SPI_SCLK_CPHA1_CPOL1   (0x03u)

/* SPI bits order enum */
#define EnoUART_EON_BITS_ORDER_LSB_FIRST   (0u)
#define EnoUART_EON_BITS_ORDER_MSB_FIRST   (1u)

/* SPI transfer separation enum */
#define EnoUART_EON_SPI_TRANSFER_SEPARATED     (0u)
#define EnoUART_EON_SPI_TRANSFER_CONTINUOUS    (1u)

/* SPI slave select constants */
#define EnoUART_EON_SPI_SLAVE_SELECT0    (EnoUART_EON_SCB__SS0_POSISTION)
#define EnoUART_EON_SPI_SLAVE_SELECT1    (EnoUART_EON_SCB__SS1_POSISTION)
#define EnoUART_EON_SPI_SLAVE_SELECT2    (EnoUART_EON_SCB__SS2_POSISTION)
#define EnoUART_EON_SPI_SLAVE_SELECT3    (EnoUART_EON_SCB__SS3_POSISTION)

/* SPI slave select polarity settings */
#define EnoUART_EON_SPI_SS_ACTIVE_LOW  (0u)
#define EnoUART_EON_SPI_SS_ACTIVE_HIGH (1u)


/***************************************
*         EnoUART API Constants
***************************************/

/* EnoUART sub-modes enum */
#define EnoUART_EON_EnoUART_MODE_STD          (0u)
#define EnoUART_EON_EnoUART_MODE_SMARTCARD    (1u)
#define EnoUART_EON_EnoUART_MODE_IRDA         (2u)

/* EnoUART direction enum */
#define EnoUART_EON_EnoUART_RX    (1u)
#define EnoUART_EON_EnoUART_TX    (2u)
#define EnoUART_EON_EnoUART_TX_RX (3u)

/* EnoUART parity enum */
#define EnoUART_EON_EnoUART_PARITY_EVEN   (0u)
#define EnoUART_EON_EnoUART_PARITY_ODD    (1u)
#define EnoUART_EON_EnoUART_PARITY_NONE   (2u)

/* EnoUART stop bits enum */
#define EnoUART_EON_EnoUART_STOP_BITS_1   (2u)
#define EnoUART_EON_EnoUART_STOP_BITS_1_5 (3u)
#define EnoUART_EON_EnoUART_STOP_BITS_2   (4u)

/* EnoUART IrDA low power OVS enum */
#define EnoUART_EON_EnoUART_IRDA_LP_OVS16     (16u)
#define EnoUART_EON_EnoUART_IRDA_LP_OVS32     (32u)
#define EnoUART_EON_EnoUART_IRDA_LP_OVS48     (48u)
#define EnoUART_EON_EnoUART_IRDA_LP_OVS96     (96u)
#define EnoUART_EON_EnoUART_IRDA_LP_OVS192    (192u)
#define EnoUART_EON_EnoUART_IRDA_LP_OVS768    (768u)
#define EnoUART_EON_EnoUART_IRDA_LP_OVS1536   (1536u)

/* EnoUART MP: mark (address) and space (data) bit definitions */
#define EnoUART_EON_EnoUART_MP_MARK       (0x100u)
#define EnoUART_EON_EnoUART_MP_SPACE      (0x000u)

/* EnoUART CTS/RTS polarity settings */
#define EnoUART_EON_EnoUART_CTS_ACTIVE_LOW    (0u)
#define EnoUART_EON_EnoUART_CTS_ACTIVE_HIGH   (1u)
#define EnoUART_EON_EnoUART_RTS_ACTIVE_LOW    (0u)
#define EnoUART_EON_EnoUART_RTS_ACTIVE_HIGH   (1u)

/* Sources of RX errors */
#define EnoUART_EON_INTR_RX_ERR        (EnoUART_EON_INTR_RX_OVERFLOW    | \
                                             EnoUART_EON_INTR_RX_UNDERFLOW   | \
                                             EnoUART_EON_INTR_RX_FRAME_ERROR | \
                                             EnoUART_EON_INTR_RX_PARITY_ERROR)

/* Shifted INTR_RX_ERR defines ONLY for EnoUART_EON_EnoUARTGetByte() */
#define EnoUART_EON_EnoUART_RX_OVERFLOW       (EnoUART_EON_INTR_RX_OVERFLOW << 8u)
#define EnoUART_EON_EnoUART_RX_UNDERFLOW      (EnoUART_EON_INTR_RX_UNDERFLOW << 8u)
#define EnoUART_EON_EnoUART_RX_FRAME_ERROR    (EnoUART_EON_INTR_RX_FRAME_ERROR << 8u)
#define EnoUART_EON_EnoUART_RX_PARITY_ERROR   (EnoUART_EON_INTR_RX_PARITY_ERROR << 8u)
#define EnoUART_EON_EnoUART_RX_ERROR_MASK     (EnoUART_EON_EnoUART_RX_OVERFLOW    | \
                                                 EnoUART_EON_EnoUART_RX_UNDERFLOW   | \
                                                 EnoUART_EON_EnoUART_RX_FRAME_ERROR | \
                                                 EnoUART_EON_EnoUART_RX_PARITY_ERROR)


/***************************************
*     Vars with External Linkage
***************************************/

#if(EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
    extern const EnoUART_EON_SPI_INIT_STRUCT  EnoUART_EON_configSpi;
    extern const EnoUART_EON_EnoUART_INIT_STRUCT EnoUART_EON_configEnoUART;
#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*    Specific SPI Macro Definitions
***************************************/

#define EnoUART_EON_GET_SPI_INTR_SLAVE_MASK(sourceMask)  ((sourceMask) & EnoUART_EON_INTR_SLAVE_SPI_BUS_ERROR)
#define EnoUART_EON_GET_SPI_INTR_MASTER_MASK(sourceMask) ((sourceMask) & EnoUART_EON_INTR_MASTER_SPI_DONE)
#define EnoUART_EON_GET_SPI_INTR_RX_MASK(sourceMask) \
                                             ((sourceMask) & (uint32) ~EnoUART_EON_INTR_SLAVE_SPI_BUS_ERROR)

#define EnoUART_EON_GET_SPI_INTR_TX_MASK(sourceMask) \
                                             ((sourceMask) & (uint32) ~EnoUART_EON_INTR_MASTER_SPI_DONE)


/***************************************
*    Specific EnoUART Macro Definitions
***************************************/

#define EnoUART_EON_EnoUART_GET_CTRL_OVS_IRDA_LP(oversample) \
        ((EnoUART_EON_EnoUART_IRDA_LP_OVS16   == (oversample)) ? EnoUART_EON_CTRL_OVS_IRDA_LP_OVS16 : \
         ((EnoUART_EON_EnoUART_IRDA_LP_OVS32   == (oversample)) ? EnoUART_EON_CTRL_OVS_IRDA_LP_OVS32 : \
          ((EnoUART_EON_EnoUART_IRDA_LP_OVS48   == (oversample)) ? EnoUART_EON_CTRL_OVS_IRDA_LP_OVS48 : \
           ((EnoUART_EON_EnoUART_IRDA_LP_OVS96   == (oversample)) ? EnoUART_EON_CTRL_OVS_IRDA_LP_OVS96 : \
            ((EnoUART_EON_EnoUART_IRDA_LP_OVS192  == (oversample)) ? EnoUART_EON_CTRL_OVS_IRDA_LP_OVS192 : \
             ((EnoUART_EON_EnoUART_IRDA_LP_OVS768  == (oversample)) ? EnoUART_EON_CTRL_OVS_IRDA_LP_OVS768 : \
              ((EnoUART_EON_EnoUART_IRDA_LP_OVS1536 == (oversample)) ? EnoUART_EON_CTRL_OVS_IRDA_LP_OVS1536 : \
                                                                          EnoUART_EON_CTRL_OVS_IRDA_LP_OVS16)))))))

#define EnoUART_EON_GET_EnoUART_RX_CTRL_ENABLED(direction) ((0u != (EnoUART_EON_EnoUART_RX & (direction))) ? \
                                                                     (EnoUART_EON_RX_CTRL_ENABLED) : (0u))

#define EnoUART_EON_GET_EnoUART_TX_CTRL_ENABLED(direction) ((0u != (EnoUART_EON_EnoUART_TX & (direction))) ? \
                                                                     (EnoUART_EON_TX_CTRL_ENABLED) : (0u))


/***************************************
*        SPI Register Settings
***************************************/

#define EnoUART_EON_CTRL_SPI      (EnoUART_EON_CTRL_MODE_SPI)
#define EnoUART_EON_SPI_RX_CTRL   (EnoUART_EON_RX_CTRL_ENABLED)
#define EnoUART_EON_SPI_TX_CTRL   (EnoUART_EON_TX_CTRL_ENABLED)


/***************************************
*       SPI Init Register Settings
***************************************/

#define EnoUART_EON_SPI_SS_POLARITY \
             (((uint32) EnoUART_EON_SPI_SS0_POLARITY << EnoUART_EON_SPI_SLAVE_SELECT0) | \
              ((uint32) EnoUART_EON_SPI_SS1_POLARITY << EnoUART_EON_SPI_SLAVE_SELECT1) | \
              ((uint32) EnoUART_EON_SPI_SS2_POLARITY << EnoUART_EON_SPI_SLAVE_SELECT2) | \
              ((uint32) EnoUART_EON_SPI_SS3_POLARITY << EnoUART_EON_SPI_SLAVE_SELECT3))

#if(EnoUART_EON_SCB_MODE_SPI_CONST_CFG)

    /* SPI Configuration */
    #define EnoUART_EON_SPI_DEFAULT_CTRL \
                    (EnoUART_EON_GET_CTRL_OVS(EnoUART_EON_SPI_OVS_FACTOR) | \
                     EnoUART_EON_GET_CTRL_BYTE_MODE (EnoUART_EON_SPI_BYTE_MODE_ENABLE) | \
                     EnoUART_EON_GET_CTRL_EC_AM_MODE(EnoUART_EON_SPI_WAKE_ENABLE)      | \
                     EnoUART_EON_CTRL_SPI)

    #define EnoUART_EON_SPI_DEFAULT_SPI_CTRL \
                    (EnoUART_EON_GET_SPI_CTRL_CONTINUOUS    (EnoUART_EON_SPI_TRANSFER_SEPARATION)       | \
                     EnoUART_EON_GET_SPI_CTRL_SELECT_PRECEDE(EnoUART_EON_SPI_SUB_MODE &                   \
                                                                  EnoUART_EON_SPI_MODE_TI_PRECEDES_MASK)     | \
                     EnoUART_EON_GET_SPI_CTRL_SCLK_MODE     (EnoUART_EON_SPI_CLOCK_MODE)                | \
                     EnoUART_EON_GET_SPI_CTRL_LATE_MISO_SAMPLE(EnoUART_EON_SPI_LATE_MISO_SAMPLE_ENABLE) | \
                     EnoUART_EON_GET_SPI_CTRL_SCLK_CONTINUOUS(EnoUART_EON_SPI_FREE_RUN_SCLK_ENABLE)     | \
                     EnoUART_EON_GET_SPI_CTRL_SSEL_POLARITY (EnoUART_EON_SPI_SS_POLARITY)               | \
                     EnoUART_EON_GET_SPI_CTRL_SUB_MODE      (EnoUART_EON_SPI_SUB_MODE)                  | \
                     EnoUART_EON_GET_SPI_CTRL_MASTER_MODE   (EnoUART_EON_SPI_MODE))

    /* RX direction */
    #define EnoUART_EON_SPI_DEFAULT_RX_CTRL \
                    (EnoUART_EON_GET_RX_CTRL_DATA_WIDTH(EnoUART_EON_SPI_RX_DATA_BITS_NUM)     | \
                     EnoUART_EON_GET_RX_CTRL_BIT_ORDER (EnoUART_EON_SPI_BITS_ORDER)           | \
                     EnoUART_EON_GET_RX_CTRL_MEDIAN    (EnoUART_EON_SPI_MEDIAN_FILTER_ENABLE) | \
                     EnoUART_EON_SPI_RX_CTRL)

    #define EnoUART_EON_SPI_DEFAULT_RX_FIFO_CTRL \
                    EnoUART_EON_GET_RX_FIFO_CTRL_TRIGGER_LEVEL(EnoUART_EON_SPI_RX_TRIGGER_LEVEL)

    /* TX direction */
    #define EnoUART_EON_SPI_DEFAULT_TX_CTRL \
                    (EnoUART_EON_GET_TX_CTRL_DATA_WIDTH(EnoUART_EON_SPI_TX_DATA_BITS_NUM) | \
                     EnoUART_EON_GET_TX_CTRL_BIT_ORDER (EnoUART_EON_SPI_BITS_ORDER)       | \
                     EnoUART_EON_SPI_TX_CTRL)

    #define EnoUART_EON_SPI_DEFAULT_TX_FIFO_CTRL \
                    EnoUART_EON_GET_TX_FIFO_CTRL_TRIGGER_LEVEL(EnoUART_EON_SPI_TX_TRIGGER_LEVEL)

    /* Interrupt sources */
    #define EnoUART_EON_SPI_DEFAULT_INTR_SPI_EC_MASK   (EnoUART_EON_NO_INTR_SOURCES)

    #define EnoUART_EON_SPI_DEFAULT_INTR_I2C_EC_MASK   (EnoUART_EON_NO_INTR_SOURCES)
    #define EnoUART_EON_SPI_DEFAULT_INTR_SLAVE_MASK \
                    (EnoUART_EON_SPI_INTR_RX_MASK & EnoUART_EON_INTR_SLAVE_SPI_BUS_ERROR)

    #define EnoUART_EON_SPI_DEFAULT_INTR_MASTER_MASK \
                    (EnoUART_EON_SPI_INTR_TX_MASK & EnoUART_EON_INTR_MASTER_SPI_DONE)

    #define EnoUART_EON_SPI_DEFAULT_INTR_RX_MASK \
                    (EnoUART_EON_SPI_INTR_RX_MASK & (uint32) ~EnoUART_EON_INTR_SLAVE_SPI_BUS_ERROR)

    #define EnoUART_EON_SPI_DEFAULT_INTR_TX_MASK \
                    (EnoUART_EON_SPI_INTR_TX_MASK & (uint32) ~EnoUART_EON_INTR_MASTER_SPI_DONE)

#endif /* (EnoUART_EON_SCB_MODE_SPI_CONST_CFG) */


/***************************************
*        EnoUART Register Settings
***************************************/

#define EnoUART_EON_CTRL_EnoUART      (EnoUART_EON_CTRL_MODE_EnoUART)
#define EnoUART_EON_EnoUART_RX_CTRL   (EnoUART_EON_RX_CTRL_LSB_FIRST) /* LSB for EnoUART goes first */
#define EnoUART_EON_EnoUART_TX_CTRL   (EnoUART_EON_TX_CTRL_LSB_FIRST) /* LSB for EnoUART goes first */


/***************************************
*      EnoUART Init Register Settings
***************************************/

#if(EnoUART_EON_SCB_MODE_EnoUART_CONST_CFG)

    /* EnoUART configuration */
    #if(EnoUART_EON_EnoUART_MODE_IRDA == EnoUART_EON_EnoUART_SUB_MODE)

        #define EnoUART_EON_DEFAULT_CTRL_OVS   ((0u != EnoUART_EON_EnoUART_IRDA_LOW_POWER) ?              \
                                (EnoUART_EON_EnoUART_GET_CTRL_OVS_IRDA_LP(EnoUART_EON_EnoUART_OVS_FACTOR)) : \
                                (EnoUART_EON_CTRL_OVS_IRDA_OVS16))

    #else

        #define EnoUART_EON_DEFAULT_CTRL_OVS   EnoUART_EON_GET_CTRL_OVS(EnoUART_EON_EnoUART_OVS_FACTOR)

    #endif /* (EnoUART_EON_EnoUART_MODE_IRDA == EnoUART_EON_EnoUART_SUB_MODE) */

    #define EnoUART_EON_EnoUART_DEFAULT_CTRL \
                                (EnoUART_EON_GET_CTRL_BYTE_MODE  (EnoUART_EON_EnoUART_BYTE_MODE_ENABLE)  | \
                                 EnoUART_EON_GET_CTRL_ADDR_ACCEPT(EnoUART_EON_EnoUART_MP_ACCEPT_ADDRESS) | \
                                 EnoUART_EON_DEFAULT_CTRL_OVS                                              | \
                                 EnoUART_EON_CTRL_EnoUART)

    #define EnoUART_EON_EnoUART_DEFAULT_EnoUART_CTRL \
                                    (EnoUART_EON_GET_EnoUART_CTRL_MODE(EnoUART_EON_EnoUART_SUB_MODE))

    /* RX direction */
    #define EnoUART_EON_EnoUART_DEFAULT_RX_CTRL_PARITY \
                                ((EnoUART_EON_EnoUART_PARITY_NONE != EnoUART_EON_EnoUART_PARITY_TYPE) ?      \
                                  (EnoUART_EON_GET_EnoUART_RX_CTRL_PARITY(EnoUART_EON_EnoUART_PARITY_TYPE) | \
                                   EnoUART_EON_EnoUART_RX_CTRL_PARITY_ENABLED) : (0u))

    #define EnoUART_EON_EnoUART_DEFAULT_EnoUART_RX_CTRL \
                    (EnoUART_EON_GET_EnoUART_RX_CTRL_MODE(EnoUART_EON_EnoUART_STOP_BITS_NUM)                    | \
                     EnoUART_EON_GET_EnoUART_RX_CTRL_POLARITY(EnoUART_EON_EnoUART_IRDA_POLARITY)                | \
                     EnoUART_EON_GET_EnoUART_RX_CTRL_MP_MODE(EnoUART_EON_EnoUART_MP_MODE_ENABLE)                | \
                     EnoUART_EON_GET_EnoUART_RX_CTRL_DROP_ON_PARITY_ERR(EnoUART_EON_EnoUART_DROP_ON_PARITY_ERR) | \
                     EnoUART_EON_GET_EnoUART_RX_CTRL_DROP_ON_FRAME_ERR(EnoUART_EON_EnoUART_DROP_ON_FRAME_ERR)   | \
                     EnoUART_EON_EnoUART_DEFAULT_RX_CTRL_PARITY)

    #define EnoUART_EON_EnoUART_DEFAULT_RX_CTRL \
                                (EnoUART_EON_GET_RX_CTRL_DATA_WIDTH(EnoUART_EON_EnoUART_DATA_BITS_NUM)        | \
                                 EnoUART_EON_GET_RX_CTRL_MEDIAN    (EnoUART_EON_EnoUART_MEDIAN_FILTER_ENABLE) | \
                                 EnoUART_EON_GET_EnoUART_RX_CTRL_ENABLED(EnoUART_EON_EnoUART_DIRECTION))

    #define EnoUART_EON_EnoUART_DEFAULT_RX_FIFO_CTRL \
                                EnoUART_EON_GET_RX_FIFO_CTRL_TRIGGER_LEVEL(EnoUART_EON_EnoUART_RX_TRIGGER_LEVEL)

    #define EnoUART_EON_EnoUART_DEFAULT_RX_MATCH_REG  ((0u != EnoUART_EON_EnoUART_MP_MODE_ENABLE) ?          \
                                (EnoUART_EON_GET_RX_MATCH_ADDR(EnoUART_EON_EnoUART_MP_RX_ADDRESS) | \
                                 EnoUART_EON_GET_RX_MATCH_MASK(EnoUART_EON_EnoUART_MP_RX_ADDRESS_MASK)) : (0u))

    /* TX direction */
    #define EnoUART_EON_EnoUART_DEFAULT_TX_CTRL_PARITY (EnoUART_EON_EnoUART_DEFAULT_RX_CTRL_PARITY)

    #define EnoUART_EON_EnoUART_DEFAULT_EnoUART_TX_CTRL \
                                (EnoUART_EON_GET_EnoUART_TX_CTRL_MODE(EnoUART_EON_EnoUART_STOP_BITS_NUM)       | \
                                 EnoUART_EON_GET_EnoUART_TX_CTRL_RETRY_NACK(EnoUART_EON_EnoUART_RETRY_ON_NACK) | \
                                 EnoUART_EON_EnoUART_DEFAULT_TX_CTRL_PARITY)

    #define EnoUART_EON_EnoUART_DEFAULT_TX_CTRL \
                                (EnoUART_EON_GET_TX_CTRL_DATA_WIDTH(EnoUART_EON_EnoUART_DATA_BITS_NUM) | \
                                 EnoUART_EON_GET_EnoUART_TX_CTRL_ENABLED(EnoUART_EON_EnoUART_DIRECTION))

    #define EnoUART_EON_EnoUART_DEFAULT_TX_FIFO_CTRL \
                                EnoUART_EON_GET_TX_FIFO_CTRL_TRIGGER_LEVEL(EnoUART_EON_EnoUART_TX_TRIGGER_LEVEL)

    #define EnoUART_EON_EnoUART_DEFAULT_FLOW_CTRL \
                        (EnoUART_EON_GET_EnoUART_FLOW_CTRL_TRIGGER_LEVEL(EnoUART_EON_EnoUART_RTS_FIFO_LEVEL) | \
                         EnoUART_EON_GET_EnoUART_FLOW_CTRL_RTS_POLARITY (EnoUART_EON_EnoUART_RTS_POLARITY)   | \
                         EnoUART_EON_GET_EnoUART_FLOW_CTRL_CTS_POLARITY (EnoUART_EON_EnoUART_CTS_POLARITY)   | \
                         EnoUART_EON_GET_EnoUART_FLOW_CTRL_CTS_ENABLE   (EnoUART_EON_EnoUART_CTS_ENABLE))

    /* Interrupt sources */
    #define EnoUART_EON_EnoUART_DEFAULT_INTR_I2C_EC_MASK  (EnoUART_EON_NO_INTR_SOURCES)
    #define EnoUART_EON_EnoUART_DEFAULT_INTR_SPI_EC_MASK  (EnoUART_EON_NO_INTR_SOURCES)
    #define EnoUART_EON_EnoUART_DEFAULT_INTR_SLAVE_MASK   (EnoUART_EON_NO_INTR_SOURCES)
    #define EnoUART_EON_EnoUART_DEFAULT_INTR_MASTER_MASK  (EnoUART_EON_NO_INTR_SOURCES)
    #define EnoUART_EON_EnoUART_DEFAULT_INTR_RX_MASK      (EnoUART_EON_EnoUART_INTR_RX_MASK)
    #define EnoUART_EON_EnoUART_DEFAULT_INTR_TX_MASK      (EnoUART_EON_EnoUART_INTR_TX_MASK)

#endif /* (EnoUART_EON_SCB_MODE_EnoUART_CONST_CFG) */


/***************************************
* The following code is DEPRECATED and
* must not be used.
***************************************/

#define EnoUART_EON_SPIM_ACTIVE_SS0    (EnoUART_EON_SPI_SLAVE_SELECT0)
#define EnoUART_EON_SPIM_ACTIVE_SS1    (EnoUART_EON_SPI_SLAVE_SELECT1)
#define EnoUART_EON_SPIM_ACTIVE_SS2    (EnoUART_EON_SPI_SLAVE_SELECT2)
#define EnoUART_EON_SPIM_ACTIVE_SS3    (EnoUART_EON_SPI_SLAVE_SELECT3)

#endif /* CY_SCB_SPI_EnoUART_EnoUART_EON_H */


/* [] END OF FILE */
