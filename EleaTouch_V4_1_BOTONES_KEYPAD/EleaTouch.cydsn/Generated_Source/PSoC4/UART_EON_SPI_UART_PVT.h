/*******************************************************************************
* File Name: EnoUART_EON_SPI_EnoUART_PVT.h
* Version 3.0
*
* Description:
*  This private file provides constants and parameter values for the
*  SCB Component in SPI and EnoUART modes.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_SPI_EnoUART_PVT_EnoUART_EON_H)
#define CY_SCB_SPI_EnoUART_PVT_EnoUART_EON_H

#include "EnoUART_EON_SPI_EnoUART.h"


/***************************************
*     Internal Global Vars
***************************************/

#if (EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST)
    extern volatile uint32  EnoUART_EON_rxBufferHead;
    extern volatile uint32  EnoUART_EON_rxBufferTail;
    extern volatile uint8   EnoUART_EON_rxBufferOverflow;
#endif /* (EnoUART_EON_INTERNAL_RX_SW_BUFFER_CONST) */

#if (EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST)
    extern volatile uint32  EnoUART_EON_txBufferHead;
    extern volatile uint32  EnoUART_EON_txBufferTail;
#endif /* (EnoUART_EON_INTERNAL_TX_SW_BUFFER_CONST) */

#if (EnoUART_EON_INTERNAL_RX_SW_BUFFER)
    extern volatile uint8 EnoUART_EON_rxBufferInternal[EnoUART_EON_INTERNAL_RX_BUFFER_SIZE];
#endif /* (EnoUART_EON_INTERNAL_RX_SW_BUFFER) */

#if (EnoUART_EON_INTERNAL_TX_SW_BUFFER)
    extern volatile uint8 EnoUART_EON_txBufferInternal[EnoUART_EON_TX_BUFFER_SIZE];
#endif /* (EnoUART_EON_INTERNAL_TX_SW_BUFFER) */


/***************************************
*     Private Function Prototypes
***************************************/

void EnoUART_EON_SpiPostEnable(void);
void EnoUART_EON_SpiStop(void);

#if (EnoUART_EON_SCB_MODE_SPI_CONST_CFG)
    void EnoUART_EON_SpiInit(void);
#endif /* (EnoUART_EON_SCB_MODE_SPI_CONST_CFG) */

#if (EnoUART_EON_SPI_WAKE_ENABLE_CONST)
    void EnoUART_EON_SpiSaveConfig(void);
    void EnoUART_EON_SpiRestoreConfig(void);
#endif /* (EnoUART_EON_SPI_WAKE_ENABLE_CONST) */

void EnoUART_EON_EnoUARTPostEnable(void);
void EnoUART_EON_EnoUARTStop(void);

#if (EnoUART_EON_SCB_MODE_EnoUART_CONST_CFG)
    void EnoUART_EON_EnoUARTInit(void);
#endif /* (EnoUART_EON_SCB_MODE_EnoUART_CONST_CFG) */

#if (EnoUART_EON_EnoUART_WAKE_ENABLE_CONST)
    void EnoUART_EON_EnoUARTSaveConfig(void);
    void EnoUART_EON_EnoUARTRestoreConfig(void);
#endif /* (EnoUART_EON_EnoUART_WAKE_ENABLE_CONST) */


/***************************************
*         EnoUART API Constants
***************************************/

/* EnoUART RX and TX position to be used in EnoUART_EON_SetPins() */
#define EnoUART_EON_EnoUART_RX_PIN_ENABLE    (EnoUART_EON_EnoUART_RX)
#define EnoUART_EON_EnoUART_TX_PIN_ENABLE    (EnoUART_EON_EnoUART_TX)

/* EnoUART RTS and CTS position to be used in  EnoUART_EON_SetPins() */
#define EnoUART_EON_EnoUART_RTS_PIN_ENABLE    (0x10u)
#define EnoUART_EON_EnoUART_CTS_PIN_ENABLE    (0x20u)


/***************************************
* The following code is DEPRECATED and
* must not be used.
***************************************/

/* Interrupt processing */
#define EnoUART_EON_SpiEnoUARTEnableIntRx(intSourceMask)  EnoUART_EON_SetRxInterruptMode(intSourceMask)
#define EnoUART_EON_SpiEnoUARTEnableIntTx(intSourceMask)  EnoUART_EON_SetTxInterruptMode(intSourceMask)
uint32  EnoUART_EON_SpiEnoUARTDisableIntRx(void);
uint32  EnoUART_EON_SpiEnoUARTDisableIntTx(void);


#endif /* (CY_SCB_SPI_EnoUART_PVT_EnoUART_EON_H) */


/* [] END OF FILE */
