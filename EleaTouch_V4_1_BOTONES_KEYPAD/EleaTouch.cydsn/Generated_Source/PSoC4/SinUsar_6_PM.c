/*******************************************************************************
* File Name: SinUsar_6.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "SinUsar_6.h"

static SinUsar_6_BACKUP_STRUCT  SinUsar_6_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: SinUsar_6_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function applies only to SIO and USBIO pins.
*  It should not be called for GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet SinUsar_6_SUT.c usage_SinUsar_6_Sleep_Wakeup
*******************************************************************************/
void SinUsar_6_Sleep(void)
{
    #if defined(SinUsar_6__PC)
        SinUsar_6_backup.pcState = SinUsar_6_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            SinUsar_6_backup.usbState = SinUsar_6_CR1_REG;
            SinUsar_6_USB_POWER_REG |= SinUsar_6_USBIO_ENTER_SLEEP;
            SinUsar_6_CR1_REG &= SinUsar_6_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(SinUsar_6__SIO)
        SinUsar_6_backup.sioState = SinUsar_6_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        SinUsar_6_SIO_REG &= (uint32)(~SinUsar_6_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: SinUsar_6_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep(). This 
* function applies only to SIO and USBIO pins. It should not be called for
* GPIO or GPIO_OVT pins.
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to SinUsar_6_Sleep() for an example usage.
*******************************************************************************/
void SinUsar_6_Wakeup(void)
{
    #if defined(SinUsar_6__PC)
        SinUsar_6_PC = SinUsar_6_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            SinUsar_6_USB_POWER_REG &= SinUsar_6_USBIO_EXIT_SLEEP_PH1;
            SinUsar_6_CR1_REG = SinUsar_6_backup.usbState;
            SinUsar_6_USB_POWER_REG &= SinUsar_6_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(SinUsar_6__SIO)
        SinUsar_6_SIO_REG = SinUsar_6_backup.sioState;
    #endif
}


/* [] END OF FILE */
