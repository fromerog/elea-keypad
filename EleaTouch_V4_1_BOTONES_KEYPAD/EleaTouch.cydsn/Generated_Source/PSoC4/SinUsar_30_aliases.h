/*******************************************************************************
* File Name: SinUsar_30.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_SinUsar_30_ALIASES_H) /* Pins SinUsar_30_ALIASES_H */
#define CY_PINS_SinUsar_30_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define SinUsar_30_0			(SinUsar_30__0__PC)
#define SinUsar_30_0_PS		(SinUsar_30__0__PS)
#define SinUsar_30_0_PC		(SinUsar_30__0__PC)
#define SinUsar_30_0_DR		(SinUsar_30__0__DR)
#define SinUsar_30_0_SHIFT	(SinUsar_30__0__SHIFT)
#define SinUsar_30_0_INTR	((uint16)((uint16)0x0003u << (SinUsar_30__0__SHIFT*2u)))

#define SinUsar_30_INTR_ALL	 ((uint16)(SinUsar_30_0_INTR))


#endif /* End Pins SinUsar_30_ALIASES_H */


/* [] END OF FILE */
