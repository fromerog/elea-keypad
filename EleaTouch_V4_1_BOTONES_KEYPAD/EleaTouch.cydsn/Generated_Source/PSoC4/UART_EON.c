/*******************************************************************************
* File Name: EnoUART_EON.c
* Version 3.0
*
* Description:
*  This file provides the source code to the API for the SCB Component.
*
* Note:
*
*******************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "EnoUART_EON_PVT.h"

#if (EnoUART_EON_SCB_MODE_I2C_INC)
    #include "EnoUART_EON_I2C_PVT.h"
#endif /* (EnoUART_EON_SCB_MODE_I2C_INC) */

#if (EnoUART_EON_SCB_MODE_EZI2C_INC)
    #include "EnoUART_EON_EZI2C_PVT.h"
#endif /* (EnoUART_EON_SCB_MODE_EZI2C_INC) */

#if (EnoUART_EON_SCB_MODE_SPI_INC || EnoUART_EON_SCB_MODE_EnoUART_INC)
    #include "EnoUART_EON_SPI_EnoUART_PVT.h"
#endif /* (EnoUART_EON_SCB_MODE_SPI_INC || EnoUART_EON_SCB_MODE_EnoUART_INC) */


/***************************************
*    Run Time Configuration Vars
***************************************/

/* Stores internal component configuration for Unconfigured mode */
#if (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Common configuration variables */
    uint8 EnoUART_EON_scbMode = EnoUART_EON_SCB_MODE_UNCONFIG;
    uint8 EnoUART_EON_scbEnableWake;
    uint8 EnoUART_EON_scbEnableIntr;

    /* I2C configuration variables */
    uint8 EnoUART_EON_mode;
    uint8 EnoUART_EON_acceptAddr;

    /* SPI/EnoUART configuration variables */
    volatile uint8 * EnoUART_EON_rxBuffer;
    uint8  EnoUART_EON_rxDataBits;
    uint32 EnoUART_EON_rxBufferSize;

    volatile uint8 * EnoUART_EON_txBuffer;
    uint8  EnoUART_EON_txDataBits;
    uint32 EnoUART_EON_txBufferSize;

    /* EZI2C configuration variables */
    uint8 EnoUART_EON_numberOfAddr;
    uint8 EnoUART_EON_subAddrSize;
#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*     Common SCB Vars
***************************************/

uint8 EnoUART_EON_initVar = 0u;

#if (EnoUART_EON_SCB_IRQ_INTERNAL)
#if !defined (CY_REMOVE_EnoUART_EON_CUSTOM_INTR_HANDLER)
    void (*EnoUART_EON_customIntrHandler)(void) = NULL;
#endif /* !defined (CY_REMOVE_EnoUART_EON_CUSTOM_INTR_HANDLER) */
#endif /* (EnoUART_EON_SCB_IRQ_INTERNAL) */


/***************************************
*    Private Function Prototypes
***************************************/

static void EnoUART_EON_ScbEnableIntr(void);
static void EnoUART_EON_ScbModeStop(void);
static void EnoUART_EON_ScbModePostEnable(void);


/*******************************************************************************
* Function Name: EnoUART_EON_Init
********************************************************************************
*
* Summary:
*  Initializes the SCB component to operate in one of the selected
*  configurations: I2C, SPI, EnoUART or EZI2C.
*  When the configuration is set to "Unconfigured SCB", this function does
*  not do any initialization. Use mode-specific initialization APIs instead:
*  SCB_I2CInit, SCB_SpiInit, SCB_EnoUARTInit or SCB_EzI2CInit.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void EnoUART_EON_Init(void)
{
#if (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
    if (EnoUART_EON_SCB_MODE_UNCONFIG_RUNTM_CFG)
    {
        EnoUART_EON_initVar = 0u;
    }
    else
    {
        /* Initialization was done before this function call */
    }

#elif (EnoUART_EON_SCB_MODE_I2C_CONST_CFG)
    EnoUART_EON_I2CInit();

#elif (EnoUART_EON_SCB_MODE_SPI_CONST_CFG)
    EnoUART_EON_SpiInit();

#elif (EnoUART_EON_SCB_MODE_EnoUART_CONST_CFG)
    EnoUART_EON_EnoUARTInit();

#elif (EnoUART_EON_SCB_MODE_EZI2C_CONST_CFG)
    EnoUART_EON_EzI2CInit();

#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: EnoUART_EON_Enable
********************************************************************************
*
* Summary:
*  Enables the SCB component operation.
*  The SCB configuration should be not changed when the component is enabled.
*  Any configuration changes should be made after disabling the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void EnoUART_EON_Enable(void)
{
#if (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Enable SCB block, only if it is already configured */
    if (!EnoUART_EON_SCB_MODE_UNCONFIG_RUNTM_CFG)
    {
        EnoUART_EON_CTRL_REG |= EnoUART_EON_CTRL_ENABLED;

        EnoUART_EON_ScbEnableIntr();

        /* Call PostEnable function specific to current operation mode */
        EnoUART_EON_ScbModePostEnable();
    }
#else
    EnoUART_EON_CTRL_REG |= EnoUART_EON_CTRL_ENABLED;

    EnoUART_EON_ScbEnableIntr();

    /* Call PostEnable function specific to current operation mode */
    EnoUART_EON_ScbModePostEnable();
#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: EnoUART_EON_Start
********************************************************************************
*
* Summary:
*  Invokes SCB_Init() and SCB_Enable().
*  After this function call, the component is enabled and ready for operation.
*  When configuration is set to "Unconfigured SCB", the component must first be
*  initialized to operate in one of the following configurations: I2C, SPI, EnoUART
*  or EZ I2C. Otherwise this function does not enable the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  EnoUART_EON_initVar - used to check initial configuration, modified
*  on first function call.
*
*******************************************************************************/
void EnoUART_EON_Start(void)
{
    if (0u == EnoUART_EON_initVar)
    {
        EnoUART_EON_Init();
        EnoUART_EON_initVar = 1u; /* Component was initialized */
    }

    EnoUART_EON_Enable();
}


/*******************************************************************************
* Function Name: EnoUART_EON_Stop
********************************************************************************
*
* Summary:
*  Disables the SCB component and its interrupt.
*  It also disables all TX interrupt sources so as not to cause an unexpected
*  interrupt trigger because after the component is enabled, the TX FIFO 
*  is empty.
*
* Parameters:
*  None
*
* Return:
*  None
* 
*******************************************************************************/
void EnoUART_EON_Stop(void)
{
#if (EnoUART_EON_SCB_IRQ_INTERNAL)
    EnoUART_EON_DisableInt();
#endif /* (EnoUART_EON_SCB_IRQ_INTERNAL) */

    /* Call Stop function specific to current operation mode */
    EnoUART_EON_ScbModeStop();

    /* Disable SCB IP */
    EnoUART_EON_CTRL_REG &= (uint32) ~EnoUART_EON_CTRL_ENABLED;

    /* Disable all TX interrupt sources so as not to cause an unexpected
    * interrupt trigger because after the component is enabled, the TX FIFO
    * is empty.
    * For SCB IP v0, it is critical as it does not mask-out interrupt
    * sources when they are disabled. This can cause a code lock-up in the
    * interrupt handler because TX FIFO cannot be loaded after the block
    * is disabled.
    */
    EnoUART_EON_SetTxInterruptMode(EnoUART_EON_NO_INTR_SOURCES);

#if (EnoUART_EON_SCB_IRQ_INTERNAL)
    EnoUART_EON_ClearPendingInt();
#endif /* (EnoUART_EON_SCB_IRQ_INTERNAL) */
}


/*******************************************************************************
* Function Name: EnoUART_EON_SetRxFifoLevel
********************************************************************************
*
* Summary:
*  Sets level in the RX FIFO to generate a RX level interrupt.
*  When the RX FIFO has more entries than the RX FIFO level an RX level
*  interrupt request is generated.
*
* Parameters:
*  level: Level in the RX FIFO to generate RX level interrupt.
*         The range of valid level values is between 0 and RX FIFO depth - 1.
*
* Return:
*  None
*
*******************************************************************************/
void EnoUART_EON_SetRxFifoLevel(uint32 level)
{
    uint32 rxFifoCtrl;

    rxFifoCtrl = EnoUART_EON_RX_FIFO_CTRL_REG;

    rxFifoCtrl &= ((uint32) ~EnoUART_EON_RX_FIFO_CTRL_TRIGGER_LEVEL_MASK); /* Clear level mask bits */
    rxFifoCtrl |= ((uint32) (EnoUART_EON_RX_FIFO_CTRL_TRIGGER_LEVEL_MASK & level));

    EnoUART_EON_RX_FIFO_CTRL_REG = rxFifoCtrl;
}


/*******************************************************************************
* Function Name: EnoUART_EON_SetTxFifoLevel
********************************************************************************
*
* Summary:
*  Sets level in the TX FIFO to generate a TX level interrupt.
*  When the TX FIFO has more entries than the TX FIFO level an TX level
*  interrupt request is generated.
*
* Parameters:
*  level: Level in the TX FIFO to generate TX level interrupt.
*         The range of valid level values is between 0 and TX FIFO depth - 1.
*
* Return:
*  None
*
*******************************************************************************/
void EnoUART_EON_SetTxFifoLevel(uint32 level)
{
    uint32 txFifoCtrl;

    txFifoCtrl = EnoUART_EON_TX_FIFO_CTRL_REG;

    txFifoCtrl &= ((uint32) ~EnoUART_EON_TX_FIFO_CTRL_TRIGGER_LEVEL_MASK); /* Clear level mask bits */
    txFifoCtrl |= ((uint32) (EnoUART_EON_TX_FIFO_CTRL_TRIGGER_LEVEL_MASK & level));

    EnoUART_EON_TX_FIFO_CTRL_REG = txFifoCtrl;
}


#if (EnoUART_EON_SCB_IRQ_INTERNAL)
    /*******************************************************************************
    * Function Name: EnoUART_EON_SetCustomInterruptHandler
    ********************************************************************************
    *
    * Summary:
    *  Registers a function to be called by the internal interrupt handler.
    *  First the function that is registered is called, then the internal interrupt
    *  handler performs any operation such as software buffer management functions
    *  before the interrupt returns.  It is the user's responsibility not to break
    *  the software buffer operations. Only one custom handler is supported, which
    *  is the function provided by the most recent call.
    *  At the initialization time no custom handler is registered.
    *
    * Parameters:
    *  func: Pointer to the function to register.
    *        The value NULL indicates to remove the current custom interrupt
    *        handler.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_SetCustomInterruptHandler(void (*func)(void))
    {
    #if !defined (CY_REMOVE_EnoUART_EON_CUSTOM_INTR_HANDLER)
        EnoUART_EON_customIntrHandler = func; /* Register interrupt handler */
    #else
        if (NULL != func)
        {
            /* Suppress compiler warning */
        }
    #endif /* !defined (CY_REMOVE_EnoUART_EON_CUSTOM_INTR_HANDLER) */
    }
#endif /* (EnoUART_EON_SCB_IRQ_INTERNAL) */


/*******************************************************************************
* Function Name: EnoUART_EON_ScbModeEnableIntr
********************************************************************************
*
* Summary:
*  Enables an interrupt for a specific mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
static void EnoUART_EON_ScbEnableIntr(void)
{
#if (EnoUART_EON_SCB_IRQ_INTERNAL)
    /* Enable interrupt in NVIC */
    #if (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
        if (0u != EnoUART_EON_scbEnableIntr)
        {
            EnoUART_EON_EnableInt();
        }

    #else
        EnoUART_EON_EnableInt();

    #endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */
#endif /* (EnoUART_EON_SCB_IRQ_INTERNAL) */
}


/*******************************************************************************
* Function Name: EnoUART_EON_ScbModePostEnable
********************************************************************************
*
* Summary:
*  Calls the PostEnable function for a specific operation mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
static void EnoUART_EON_ScbModePostEnable(void)
{
#if (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
#if (!EnoUART_EON_CY_SCBIP_V1)
    if (EnoUART_EON_SCB_MODE_SPI_RUNTM_CFG)
    {
        EnoUART_EON_SpiPostEnable();
    }
    else if (EnoUART_EON_SCB_MODE_EnoUART_RUNTM_CFG)
    {
        EnoUART_EON_EnoUARTPostEnable();
    }
    else
    {
        /* Unknown mode: do nothing */
    }
#endif /* (!EnoUART_EON_CY_SCBIP_V1) */

#elif (EnoUART_EON_SCB_MODE_SPI_CONST_CFG)
    EnoUART_EON_SpiPostEnable();

#elif (EnoUART_EON_SCB_MODE_EnoUART_CONST_CFG)
    EnoUART_EON_EnoUARTPostEnable();

#else
    /* Unknown mode: do nothing */
#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: EnoUART_EON_ScbModeStop
********************************************************************************
*
* Summary:
*  Calls the Stop function for a specific operation mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
static void EnoUART_EON_ScbModeStop(void)
{
#if (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
    if (EnoUART_EON_SCB_MODE_I2C_RUNTM_CFG)
    {
        EnoUART_EON_I2CStop();
    }
    else if (EnoUART_EON_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        EnoUART_EON_EzI2CStop();
    }
#if (!EnoUART_EON_CY_SCBIP_V1)
    else if (EnoUART_EON_SCB_MODE_SPI_RUNTM_CFG)
    {
        EnoUART_EON_SpiStop();
    }
    else if (EnoUART_EON_SCB_MODE_EnoUART_RUNTM_CFG)
    {
        EnoUART_EON_EnoUARTStop();
    }
#endif /* (!EnoUART_EON_CY_SCBIP_V1) */
    else
    {
        /* Unknown mode: do nothing */
    }
#elif (EnoUART_EON_SCB_MODE_I2C_CONST_CFG)
    EnoUART_EON_I2CStop();

#elif (EnoUART_EON_SCB_MODE_EZI2C_CONST_CFG)
    EnoUART_EON_EzI2CStop();

#elif (EnoUART_EON_SCB_MODE_SPI_CONST_CFG)
    EnoUART_EON_SpiStop();

#elif (EnoUART_EON_SCB_MODE_EnoUART_CONST_CFG)
    EnoUART_EON_EnoUARTStop();

#else
    /* Unknown mode: do nothing */
#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */
}


#if (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG)
    /*******************************************************************************
    * Function Name: EnoUART_EON_SetPins
    ********************************************************************************
    *
    * Summary:
    *  Sets the pins settings accordingly to the selected operation mode.
    *  Only available in the Unconfigured operation mode. The mode specific
    *  initialization function calls it.
    *  Pins configuration is set by PSoC Creator when a specific mode of operation
    *  is selected in design time.
    *
    * Parameters:
    *  mode:      Mode of SCB operation.
    *  subMode:   Sub-mode of SCB operation. It is only required for SPI and EnoUART
    *             modes.
    *  EnoUARTEnableMask: enables TX or RX direction and RTS and CTS signals.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_SetPins(uint32 mode, uint32 subMode, uint32 EnoUARTEnableMask)
    {
        uint32 hsiomSel [EnoUART_EON_SCB_PINS_NUMBER];
        uint32 pinsDm   [EnoUART_EON_SCB_PINS_NUMBER];

    #if (!EnoUART_EON_CY_SCBIP_V1)
        uint32 pinsInBuf = 0u;
    #endif /* (!EnoUART_EON_CY_SCBIP_V1) */

        uint32 i;

        /* Set default HSIOM to GPIO and Drive Mode to Analog Hi-Z */
        for (i = 0u; i < EnoUART_EON_SCB_PINS_NUMBER; i++)
        {
            hsiomSel[i]  = EnoUART_EON_HSIOM_DEF_SEL;
            pinsDm[i]    = EnoUART_EON_PIN_DM_ALG_HIZ;
        }

        if ((EnoUART_EON_SCB_MODE_I2C   == mode) ||
           (EnoUART_EON_SCB_MODE_EZI2C == mode))
        {
            hsiomSel[EnoUART_EON_RX_SCL_MOSI_PIN_INDEX] = EnoUART_EON_HSIOM_I2C_SEL;
            hsiomSel[EnoUART_EON_TX_SDA_MISO_PIN_INDEX] = EnoUART_EON_HSIOM_I2C_SEL;

            pinsDm[EnoUART_EON_RX_SCL_MOSI_PIN_INDEX] = EnoUART_EON_PIN_DM_OD_LO;
            pinsDm[EnoUART_EON_TX_SDA_MISO_PIN_INDEX] = EnoUART_EON_PIN_DM_OD_LO;
        }
    #if (!EnoUART_EON_CY_SCBIP_V1)
        else if (EnoUART_EON_SCB_MODE_SPI == mode)
        {
            hsiomSel[EnoUART_EON_RX_SCL_MOSI_PIN_INDEX] = EnoUART_EON_HSIOM_SPI_SEL;
            hsiomSel[EnoUART_EON_TX_SDA_MISO_PIN_INDEX] = EnoUART_EON_HSIOM_SPI_SEL;
            hsiomSel[EnoUART_EON_SCLK_PIN_INDEX] = EnoUART_EON_HSIOM_SPI_SEL;

            if (EnoUART_EON_SPI_SLAVE == subMode)
            {
                /* Slave */
                pinsDm[EnoUART_EON_RX_SCL_MOSI_PIN_INDEX] = EnoUART_EON_PIN_DM_DIG_HIZ;
                pinsDm[EnoUART_EON_TX_SDA_MISO_PIN_INDEX] = EnoUART_EON_PIN_DM_STRONG;
                pinsDm[EnoUART_EON_SCLK_PIN_INDEX] = EnoUART_EON_PIN_DM_DIG_HIZ;

            #if (EnoUART_EON_SS0_PIN)
                /* Only SS0 is valid choice for Slave */
                hsiomSel[EnoUART_EON_SS0_PIN_INDEX] = EnoUART_EON_HSIOM_SPI_SEL;
                pinsDm  [EnoUART_EON_SS0_PIN_INDEX] = EnoUART_EON_PIN_DM_DIG_HIZ;
            #endif /* (EnoUART_EON_SS0_PIN) */

            #if (EnoUART_EON_TX_SDA_MISO_PIN)
                /* Disable input buffer */
                 pinsInBuf |= EnoUART_EON_TX_SDA_MISO_PIN_MASK;
            #endif /* (EnoUART_EON_TX_SDA_MISO_PIN) */
            }
            else /* (Master) */
            {
                pinsDm[EnoUART_EON_RX_SCL_MOSI_PIN_INDEX] = EnoUART_EON_PIN_DM_STRONG;
                pinsDm[EnoUART_EON_TX_SDA_MISO_PIN_INDEX] = EnoUART_EON_PIN_DM_DIG_HIZ;
                pinsDm[EnoUART_EON_SCLK_PIN_INDEX] = EnoUART_EON_PIN_DM_STRONG;

            #if (EnoUART_EON_SS0_PIN)
                hsiomSel [EnoUART_EON_SS0_PIN_INDEX] = EnoUART_EON_HSIOM_SPI_SEL;
                pinsDm   [EnoUART_EON_SS0_PIN_INDEX] = EnoUART_EON_PIN_DM_STRONG;
                pinsInBuf |= EnoUART_EON_SS0_PIN_MASK;
            #endif /* (EnoUART_EON_SS0_PIN) */

            #if (EnoUART_EON_SS1_PIN)
                hsiomSel [EnoUART_EON_SS1_PIN_INDEX] = EnoUART_EON_HSIOM_SPI_SEL;
                pinsDm   [EnoUART_EON_SS1_PIN_INDEX] = EnoUART_EON_PIN_DM_STRONG;
                pinsInBuf |= EnoUART_EON_SS1_PIN_MASK;
            #endif /* (EnoUART_EON_SS1_PIN) */

            #if (EnoUART_EON_SS2_PIN)
                hsiomSel [EnoUART_EON_SS2_PIN_INDEX] = EnoUART_EON_HSIOM_SPI_SEL;
                pinsDm   [EnoUART_EON_SS2_PIN_INDEX] = EnoUART_EON_PIN_DM_STRONG;
                pinsInBuf |= EnoUART_EON_SS2_PIN_MASK;
            #endif /* (EnoUART_EON_SS2_PIN) */

            #if (EnoUART_EON_SS3_PIN)
                hsiomSel [EnoUART_EON_SS3_PIN_INDEX] = EnoUART_EON_HSIOM_SPI_SEL;
                pinsDm   [EnoUART_EON_SS3_PIN_INDEX] = EnoUART_EON_PIN_DM_STRONG;
                pinsInBuf |= EnoUART_EON_SS3_PIN_MASK;
            #endif /* (EnoUART_EON_SS3_PIN) */

                /* Disable input buffers */
            #if (EnoUART_EON_RX_SCL_MOSI_PIN)
                pinsInBuf |= EnoUART_EON_RX_SCL_MOSI_PIN_MASK;
            #endif /* (EnoUART_EON_RX_SCL_MOSI_PIN) */

             #if (EnoUART_EON_RX_WAKE_SCL_MOSI_PIN)
                pinsInBuf |= EnoUART_EON_RX_WAKE_SCL_MOSI_PIN_MASK;
            #endif /* (EnoUART_EON_RX_WAKE_SCL_MOSI_PIN) */

            #if (EnoUART_EON_SCLK_PIN)
                pinsInBuf |= EnoUART_EON_SCLK_PIN_MASK;
            #endif /* (EnoUART_EON_SCLK_PIN) */
            }
        }
        else /* EnoUART */
        {
            if (EnoUART_EON_EnoUART_MODE_SMARTCARD == subMode)
            {
                /* SmartCard */
                hsiomSel[EnoUART_EON_TX_SDA_MISO_PIN_INDEX] = EnoUART_EON_HSIOM_EnoUART_SEL;
                pinsDm  [EnoUART_EON_TX_SDA_MISO_PIN_INDEX] = EnoUART_EON_PIN_DM_OD_LO;
            }
            else /* Standard or IrDA */
            {
                if (0u != (EnoUART_EON_EnoUART_RX_PIN_ENABLE & EnoUARTEnableMask))
                {
                    hsiomSel[EnoUART_EON_RX_SCL_MOSI_PIN_INDEX] = EnoUART_EON_HSIOM_EnoUART_SEL;
                    pinsDm  [EnoUART_EON_RX_SCL_MOSI_PIN_INDEX] = EnoUART_EON_PIN_DM_DIG_HIZ;
                }

                if (0u != (EnoUART_EON_EnoUART_TX_PIN_ENABLE & EnoUARTEnableMask))
                {
                    hsiomSel[EnoUART_EON_TX_SDA_MISO_PIN_INDEX] = EnoUART_EON_HSIOM_EnoUART_SEL;
                    pinsDm  [EnoUART_EON_TX_SDA_MISO_PIN_INDEX] = EnoUART_EON_PIN_DM_STRONG;

                #if (EnoUART_EON_TX_SDA_MISO_PIN)
                     pinsInBuf |= EnoUART_EON_TX_SDA_MISO_PIN_MASK;
                #endif /* (EnoUART_EON_TX_SDA_MISO_PIN) */
                }

            #if !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1)
                if (EnoUART_EON_EnoUART_MODE_STD == subMode)
                {
                    if (0u != (EnoUART_EON_EnoUART_CTS_PIN_ENABLE & EnoUARTEnableMask))
                    {
                        /* CTS input is multiplexed with SCLK */
                        hsiomSel[EnoUART_EON_SCLK_PIN_INDEX] = EnoUART_EON_HSIOM_EnoUART_SEL;
                        pinsDm  [EnoUART_EON_SCLK_PIN_INDEX] = EnoUART_EON_PIN_DM_DIG_HIZ;
                    }

                    if (0u != (EnoUART_EON_EnoUART_RTS_PIN_ENABLE & EnoUARTEnableMask))
                    {
                        /* RTS output is multiplexed with SS0 */
                        hsiomSel[EnoUART_EON_SS0_PIN_INDEX] = EnoUART_EON_HSIOM_EnoUART_SEL;
                        pinsDm  [EnoUART_EON_SS0_PIN_INDEX] = EnoUART_EON_PIN_DM_STRONG;

                    #if (EnoUART_EON_SS0_PIN)
                        /* Disable input buffer */
                        pinsInBuf |= EnoUART_EON_SS0_PIN_MASK;
                    #endif /* (EnoUART_EON_SS0_PIN) */
                    }
                }
            #endif /* !(EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1) */
            }
        }
    #endif /* (!EnoUART_EON_CY_SCBIP_V1) */

    /* Configure pins: set HSIOM, DM and InputBufEnable */
    /* Note: the DR register settings do not effect the pin output if HSIOM is other than GPIO */

    #if (EnoUART_EON_RX_WAKE_SCL_MOSI_PIN)
        EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_REG,
                                       EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_MASK,
                                       EnoUART_EON_RX_WAKE_SCL_MOSI_HSIOM_POS,
                                       hsiomSel[EnoUART_EON_RX_WAKE_SCL_MOSI_PIN_INDEX]);

        EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi_SetDriveMode((uint8)
                                                               pinsDm[EnoUART_EON_RX_WAKE_SCL_MOSI_PIN_INDEX]);

        EnoUART_EON_SET_INP_DIS(EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi_INP_DIS,
                                     EnoUART_EON_EnoUART_rx_wake_i2c_scl_spi_mosi_MASK,
                                     (0u != (pinsInBuf & EnoUART_EON_RX_WAKE_SCL_MOSI_PIN_MASK)));

         /* Set interrupt on falling edge */
        EnoUART_EON_SET_INCFG_TYPE(EnoUART_EON_RX_WAKE_SCL_MOSI_INTCFG_REG,
                                        EnoUART_EON_RX_WAKE_SCL_MOSI_INTCFG_TYPE_MASK,
                                        EnoUART_EON_RX_WAKE_SCL_MOSI_INTCFG_TYPE_POS,
                                        EnoUART_EON_INTCFG_TYPE_FALLING_EDGE);
    #endif /* (EnoUART_EON_RX_WAKE_SCL_MOSI_PIN) */

    #if (EnoUART_EON_RX_SCL_MOSI_PIN)
        EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_RX_SCL_MOSI_HSIOM_REG,
                                       EnoUART_EON_RX_SCL_MOSI_HSIOM_MASK,
                                       EnoUART_EON_RX_SCL_MOSI_HSIOM_POS,
                                        hsiomSel[EnoUART_EON_RX_SCL_MOSI_PIN_INDEX]);

        EnoUART_EON_EnoUART_rx_i2c_scl_spi_mosi_SetDriveMode((uint8) pinsDm[EnoUART_EON_RX_SCL_MOSI_PIN_INDEX]);

    #if (!EnoUART_EON_CY_SCBIP_V1)
        EnoUART_EON_SET_INP_DIS(EnoUART_EON_EnoUART_rx_i2c_scl_spi_mosi_INP_DIS,
                                     EnoUART_EON_EnoUART_rx_i2c_scl_spi_mosi_MASK,
                                     (0u != (pinsInBuf & EnoUART_EON_RX_SCL_MOSI_PIN_MASK)));
    #endif /* (!EnoUART_EON_CY_SCBIP_V1) */
    #endif /* (EnoUART_EON_RX_SCL_MOSI_PIN) */

    #if (EnoUART_EON_TX_SDA_MISO_PIN)
        EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_TX_SDA_MISO_HSIOM_REG,
                                       EnoUART_EON_TX_SDA_MISO_HSIOM_MASK,
                                       EnoUART_EON_TX_SDA_MISO_HSIOM_POS,
                                        hsiomSel[EnoUART_EON_TX_SDA_MISO_PIN_INDEX]);

        EnoUART_EON_EnoUART_tx_i2c_sda_spi_miso_SetDriveMode((uint8) pinsDm[EnoUART_EON_TX_SDA_MISO_PIN_INDEX]);

    #if (!EnoUART_EON_CY_SCBIP_V1)
        EnoUART_EON_SET_INP_DIS(EnoUART_EON_EnoUART_tx_i2c_sda_spi_miso_INP_DIS,
                                     EnoUART_EON_EnoUART_tx_i2c_sda_spi_miso_MASK,
                                    (0u != (pinsInBuf & EnoUART_EON_TX_SDA_MISO_PIN_MASK)));
    #endif /* (!EnoUART_EON_CY_SCBIP_V1) */
    #endif /* (EnoUART_EON_RX_SCL_MOSI_PIN) */

    #if (EnoUART_EON_SCLK_PIN)
        EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_SCLK_HSIOM_REG,
                                       EnoUART_EON_SCLK_HSIOM_MASK,
                                       EnoUART_EON_SCLK_HSIOM_POS,
                                       hsiomSel[EnoUART_EON_SCLK_PIN_INDEX]);

        EnoUART_EON_spi_sclk_SetDriveMode((uint8) pinsDm[EnoUART_EON_SCLK_PIN_INDEX]);

        EnoUART_EON_SET_INP_DIS(EnoUART_EON_spi_sclk_INP_DIS,
                                     EnoUART_EON_spi_sclk_MASK,
                                     (0u != (pinsInBuf & EnoUART_EON_SCLK_PIN_MASK)));
    #endif /* (EnoUART_EON_SCLK_PIN) */

    #if (EnoUART_EON_SS0_PIN)
        EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_SS0_HSIOM_REG,
                                       EnoUART_EON_SS0_HSIOM_MASK,
                                       EnoUART_EON_SS0_HSIOM_POS,
                                       hsiomSel[EnoUART_EON_SS0_PIN_INDEX]);

        EnoUART_EON_spi_ss0_SetDriveMode((uint8) pinsDm[EnoUART_EON_SS0_PIN_INDEX]);

        EnoUART_EON_SET_INP_DIS(EnoUART_EON_spi_ss0_INP_DIS,
                                     EnoUART_EON_spi_ss0_MASK,
                                     (0u != (pinsInBuf & EnoUART_EON_SS0_PIN_MASK)));
    #endif /* (EnoUART_EON_SS0_PIN) */

    #if (EnoUART_EON_SS1_PIN)
        EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_SS1_HSIOM_REG,
                                       EnoUART_EON_SS1_HSIOM_MASK,
                                       EnoUART_EON_SS1_HSIOM_POS,
                                       hsiomSel[EnoUART_EON_SS1_PIN_INDEX]);

        EnoUART_EON_spi_ss1_SetDriveMode((uint8) pinsDm[EnoUART_EON_SS1_PIN_INDEX]);

        EnoUART_EON_SET_INP_DIS(EnoUART_EON_spi_ss1_INP_DIS,
                                     EnoUART_EON_spi_ss1_MASK,
                                     (0u != (pinsInBuf & EnoUART_EON_SS1_PIN_MASK)));
    #endif /* (EnoUART_EON_SS1_PIN) */

    #if (EnoUART_EON_SS2_PIN)
        EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_SS2_HSIOM_REG,
                                       EnoUART_EON_SS2_HSIOM_MASK,
                                       EnoUART_EON_SS2_HSIOM_POS,
                                       hsiomSel[EnoUART_EON_SS2_PIN_INDEX]);

        EnoUART_EON_spi_ss2_SetDriveMode((uint8) pinsDm[EnoUART_EON_SS2_PIN_INDEX]);

        EnoUART_EON_SET_INP_DIS(EnoUART_EON_spi_ss2_INP_DIS,
                                     EnoUART_EON_spi_ss2_MASK,
                                     (0u != (pinsInBuf & EnoUART_EON_SS2_PIN_MASK)));
    #endif /* (EnoUART_EON_SS2_PIN) */

    #if (EnoUART_EON_SS3_PIN)
        EnoUART_EON_SET_HSIOM_SEL(EnoUART_EON_SS3_HSIOM_REG,
                                       EnoUART_EON_SS3_HSIOM_MASK,
                                       EnoUART_EON_SS3_HSIOM_POS,
                                       hsiomSel[EnoUART_EON_SS3_PIN_INDEX]);

        EnoUART_EON_spi_ss3_SetDriveMode((uint8) pinsDm[EnoUART_EON_SS3_PIN_INDEX]);

        EnoUART_EON_SET_INP_DIS(EnoUART_EON_spi_ss3_INP_DIS,
                                     EnoUART_EON_spi_ss3_MASK,
                                     (0u != (pinsInBuf & EnoUART_EON_SS3_PIN_MASK)));
    #endif /* (EnoUART_EON_SS3_PIN) */
    }

#endif /* (EnoUART_EON_SCB_MODE_UNCONFIG_CONST_CFG) */


#if (EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1)
    /*******************************************************************************
    * Function Name: EnoUART_EON_I2CSlaveNackGeneration
    ********************************************************************************
    *
    * Summary:
    *  Sets command to generate NACK to the address or data.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void EnoUART_EON_I2CSlaveNackGeneration(void)
    {
        /* Check for EC_AM toggle condition: EC_AM and clock stretching for address are enabled */
        if ((0u != (EnoUART_EON_CTRL_REG & EnoUART_EON_CTRL_EC_AM_MODE)) &&
            (0u == (EnoUART_EON_I2C_CTRL_REG & EnoUART_EON_I2C_CTRL_S_NOT_READY_ADDR_NACK)))
        {
            /* Toggle EC_AM before NACK generation */
            EnoUART_EON_CTRL_REG &= ~EnoUART_EON_CTRL_EC_AM_MODE;
            EnoUART_EON_CTRL_REG |=  EnoUART_EON_CTRL_EC_AM_MODE;
        }

        EnoUART_EON_I2C_SLAVE_CMD_REG = EnoUART_EON_I2C_SLAVE_CMD_S_NACK;
    }
#endif /* (EnoUART_EON_CY_SCBIP_V0 || EnoUART_EON_CY_SCBIP_V1) */


/* [] END OF FILE */
