/*******************************************************************************
* File Name: ENO_RESET.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ENO_RESET_ALIASES_H) /* Pins ENO_RESET_ALIASES_H */
#define CY_PINS_ENO_RESET_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define ENO_RESET_0			(ENO_RESET__0__PC)
#define ENO_RESET_0_PS		(ENO_RESET__0__PS)
#define ENO_RESET_0_PC		(ENO_RESET__0__PC)
#define ENO_RESET_0_DR		(ENO_RESET__0__DR)
#define ENO_RESET_0_SHIFT	(ENO_RESET__0__SHIFT)
#define ENO_RESET_0_INTR	((uint16)((uint16)0x0003u << (ENO_RESET__0__SHIFT*2u)))

#define ENO_RESET_INTR_ALL	 ((uint16)(ENO_RESET_0_INTR))


#endif /* End Pins ENO_RESET_ALIASES_H */


/* [] END OF FILE */
