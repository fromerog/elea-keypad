/*******************************************************************************
* File Name: Power_EON.c  
* Version 2.10
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "Power_EON.h"

#define SetP4PinDriveMode(shift, mode)  \
    do { \
        Power_EON_PC =   (Power_EON_PC & \
                                (uint32)(~(uint32)(Power_EON_DRIVE_MODE_IND_MASK << (Power_EON_DRIVE_MODE_BITS * (shift))))) | \
                                (uint32)((uint32)(mode) << (Power_EON_DRIVE_MODE_BITS * (shift))); \
    } while (0)


/*******************************************************************************
* Function Name: Power_EON_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None 
*  
*******************************************************************************/
void Power_EON_Write(uint8 value) 
{
    uint8 drVal = (uint8)(Power_EON_DR & (uint8)(~Power_EON_MASK));
    drVal = (drVal | ((uint8)(value << Power_EON_SHIFT) & Power_EON_MASK));
    Power_EON_DR = (uint32)drVal;
}


/*******************************************************************************
* Function Name: Power_EON_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to one of the following drive modes.
*
*  Power_EON_DM_STRONG     Strong Drive 
*  Power_EON_DM_OD_HI      Open Drain, Drives High 
*  Power_EON_DM_OD_LO      Open Drain, Drives Low 
*  Power_EON_DM_RES_UP     Resistive Pull Up 
*  Power_EON_DM_RES_DWN    Resistive Pull Down 
*  Power_EON_DM_RES_UPDWN  Resistive Pull Up/Down 
*  Power_EON_DM_DIG_HIZ    High Impedance Digital 
*  Power_EON_DM_ALG_HIZ    High Impedance Analog 
*
* Return: 
*  None
*
*******************************************************************************/
void Power_EON_SetDriveMode(uint8 mode) 
{
	SetP4PinDriveMode(Power_EON__0__SHIFT, mode);
}


/*******************************************************************************
* Function Name: Power_EON_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro Power_EON_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 Power_EON_Read(void) 
{
    return (uint8)((Power_EON_PS & Power_EON_MASK) >> Power_EON_SHIFT);
}


/*******************************************************************************
* Function Name: Power_EON_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 Power_EON_ReadDataReg(void) 
{
    return (uint8)((Power_EON_DR & Power_EON_MASK) >> Power_EON_SHIFT);
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(Power_EON_INTSTAT) 

    /*******************************************************************************
    * Function Name: Power_EON_ClearInterrupt
    ********************************************************************************
    *
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 Power_EON_ClearInterrupt(void) 
    {
		uint8 maskedStatus = (uint8)(Power_EON_INTSTAT & Power_EON_MASK);
		Power_EON_INTSTAT = maskedStatus;
        return maskedStatus >> Power_EON_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 


/* [] END OF FILE */
