/*******************************************************************************
* File Name: Reset_EON.h  
* Version 2.10
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Reset_EON_H) /* Pins Reset_EON_H */
#define CY_PINS_Reset_EON_H

#include "cytypes.h"
#include "cyfitter.h"
#include "Reset_EON_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    Reset_EON_Write(uint8 value) ;
void    Reset_EON_SetDriveMode(uint8 mode) ;
uint8   Reset_EON_ReadDataReg(void) ;
uint8   Reset_EON_Read(void) ;
uint8   Reset_EON_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define Reset_EON_DRIVE_MODE_BITS        (3)
#define Reset_EON_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - Reset_EON_DRIVE_MODE_BITS))

#define Reset_EON_DM_ALG_HIZ         (0x00u)
#define Reset_EON_DM_DIG_HIZ         (0x01u)
#define Reset_EON_DM_RES_UP          (0x02u)
#define Reset_EON_DM_RES_DWN         (0x03u)
#define Reset_EON_DM_OD_LO           (0x04u)
#define Reset_EON_DM_OD_HI           (0x05u)
#define Reset_EON_DM_STRONG          (0x06u)
#define Reset_EON_DM_RES_UPDWN       (0x07u)

/* Digital Port Constants */
#define Reset_EON_MASK               Reset_EON__MASK
#define Reset_EON_SHIFT              Reset_EON__SHIFT
#define Reset_EON_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Reset_EON_PS                     (* (reg32 *) Reset_EON__PS)
/* Port Configuration */
#define Reset_EON_PC                     (* (reg32 *) Reset_EON__PC)
/* Data Register */
#define Reset_EON_DR                     (* (reg32 *) Reset_EON__DR)
/* Input Buffer Disable Override */
#define Reset_EON_INP_DIS                (* (reg32 *) Reset_EON__PC2)


#if defined(Reset_EON__INTSTAT)  /* Interrupt Registers */

    #define Reset_EON_INTSTAT                (* (reg32 *) Reset_EON__INTSTAT)

#endif /* Interrupt Registers */


/***************************************
* The following code is DEPRECATED and 
* must not be used.
***************************************/

#define Reset_EON_DRIVE_MODE_SHIFT       (0x00u)
#define Reset_EON_DRIVE_MODE_MASK        (0x07u << Reset_EON_DRIVE_MODE_SHIFT)


#endif /* End Pins Reset_EON_H */


/* [] END OF FILE */
