/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "funciones.h"
#include <project.h>

//------------------------------------------------------------------------------------
/* 
Rele_TESA(int accion)
    Esta funcion activa un rele para accionar un mando pulsador de TESA.
*/
//------------------------------------------------------------------------------------
void Rele_TESA(int accion)
{
    if (accion==ON)
    {
        Relay_SetDriveMode(Relay_DM_STRONG);
        Relay_Write(0u);
        
    }
    else
    {
        
        Relay_Write(1u);
        Relay_SetDriveMode(Relay_DM_RES_UP);       
    }
}

//------------------------------------------------------------------------------------
/* 
LED_Alarma(int accion)
    Esta funcion recibe un ON o un OFF y en funcion de ello enciende o apaga el 
    LED de aviso de emergencias.
*/
//------------------------------------------------------------------------------------
void LED_Alarma(int accion)
{
    if (accion==ON)
    {
        LED_A_SetDriveMode(LED_A_DM_STRONG);
        LED_A_Write(0u);
        
    }
    else
    {
        
        LED_A_Write(1u);
        LED_A_SetDriveMode(LED_A_DM_RES_UP);       
    }
}
//------------------------------------------------------------------------------------
/* 
LED_Bateria(int accion)
    Esta funcion recibe un ON o un OFF y en funcion de ello enciende o apaga el 
    LED de aviso de bateria baja.
*/
//------------------------------------------------------------------------------------
void LED_Bateria(int accion)
{
    if (accion==ON)
    {
        LED_B_SetDriveMode(LED_B_DM_STRONG);
        LED_B_Write(0u);
    }
    else
    {
        LED_B_Write(1u);
        LED_B_SetDriveMode(LED_B_DM_RES_UP);       
    }
}
//------------------------------------------------------------------------------------
/* 
Zumbador(int accion)
    Esta funcion recibe un ON o un OFF y en funcion de ello enciende o apaga el 
    zumbador.
*/
//------------------------------------------------------------------------------------
void Zumbador(int accion)
{    
    if (accion==ON)
    {
        
#ifdef BUZZER_ACT_LOW
        BUZZER_SetDriveMode(BUZZER_DM_STRONG);
        BUZZER_Write(0u);
#else
        BUZZER_SetDriveMode(BUZZER_DM_RES_DWN);
        BUZZER_Write(1u);
#endif
        
   
    }
    else
    {
#ifdef BUZZER_ACT_LOW
        BUZZER_Write(1u);
        BUZZER_SetDriveMode(BUZZER_DM_RES_UP);
#else
        BUZZER_Write(0u);
        BUZZER_SetDriveMode(BUZZER_DM_RES_DWN);
#endif
        
    }
}

/* [] END OF FILE */
